/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.ems.entity;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author nutthawoot.noo
 */
public class EMSCUA {

    private String EMCACUSID;
    private String EMCAADD1;
    private String EMCAADD2;
    private String EMCAADD3;
    private String EMCAADD4;
    private String EMCAADD5;
    private String EMCAUSER;

    public EMSCUA() {

    }

    public String getEMCACUSID() {
        return EMCACUSID;
    }

    public void setEMCACUSID(String EMCACUSID) {
        this.EMCACUSID = EMCACUSID;
    }

    public String getEMCAADD1() {
        return EMCAADD1;
    }

    public void setEMCAADD1(String EMCAADD1) {
        this.EMCAADD1 = EMCAADD1;
    }

    public String getEMCAADD2() {
        return EMCAADD2;
    }

    public void setEMCAADD2(String EMCAADD2) {
        this.EMCAADD2 = EMCAADD2;
    }

    public String getEMCAADD3() {
        return EMCAADD3;
    }

    public void setEMCAADD3(String EMCAADD3) {
        this.EMCAADD3 = EMCAADD3;
    }

    public String getEMCAADD4() {
        return EMCAADD4;
    }

    public void setEMCAADD4(String EMCAADD4) {
        this.EMCAADD4 = EMCAADD4;
    }

    public String getEMCAADD5() {
        return EMCAADD5;
    }

    public void setEMCAADD5(String EMCAADD5) {
        this.EMCAADD5 = EMCAADD5;
    }

    public String getEMCAUSER() {
        return EMCAUSER;
    }

    public void setEMCAUSER(String EMCAUSER) {
        this.EMCAUSER = EMCAUSER;
    }

}
