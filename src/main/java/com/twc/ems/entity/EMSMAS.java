/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.ems.entity;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author nutthawoot.noo
 */
public class EMSMAS {

    private String EMMZONE;
    private String EMMSTYLE;
    private String EMMCUP;
    private String EMMSIZE;
    private String EMMTYPE;
    private String EMMCOMPO1;
    private String EMMCOMPO2;
    private String EMMCOMPO3;
    private String EMMWEIGHT;
    private String EMMPRICE;
    private String EMMUNIT;
    private String EMMHSCODE;
    private String EMMRANGE;
    private String EMMBOXS6E;
    private String EMMBOXS6D;
    private String EMMBOXS4E;
    private String EMMBOXS4D;
    private String EMMBOXS2E;
    private String EMMPPACK;
    private String EMMUSER;
    private String EMMWG6E;
    private String EMMWG6D;
    private String EMMWG4E;
    private String EMMWG4D;
    private String EMMWG2E;

    public EMSMAS() {

    }

    @Override
    public String toString() {
        return "EMSMAS{" + "EMMZONE=" + EMMZONE + ", EMMSTYLE=" + EMMSTYLE + ", EMMCUP=" + EMMCUP + ", EMMSIZE=" + EMMSIZE + '}';
    }

    public String getEMMZONE() {
        return EMMZONE;
    }

    public void setEMMZONE(String EMMZONE) {
        this.EMMZONE = EMMZONE;
    }

    public String getEMMSTYLE() {
        return EMMSTYLE;
    }

    public void setEMMSTYLE(String EMMSTYLE) {
        this.EMMSTYLE = EMMSTYLE;
    }

    public String getEMMCUP() {
        return EMMCUP;
    }

    public void setEMMCUP(String EMMCUP) {
        this.EMMCUP = EMMCUP;
    }

    public String getEMMSIZE() {
        return EMMSIZE;
    }

    public void setEMMSIZE(String EMMSIZE) {
        this.EMMSIZE = EMMSIZE;
    }

    public String getEMMTYPE() {
        return EMMTYPE;
    }

    public void setEMMTYPE(String EMMTYPE) {
        this.EMMTYPE = EMMTYPE;
    }

    public String getEMMCOMPO1() {
        return EMMCOMPO1;
    }

    public void setEMMCOMPO1(String EMMCOMPO1) {
        this.EMMCOMPO1 = EMMCOMPO1;
    }

    public String getEMMCOMPO2() {
        return EMMCOMPO2;
    }

    public void setEMMCOMPO2(String EMMCOMPO2) {
        this.EMMCOMPO2 = EMMCOMPO2;
    }

    public String getEMMCOMPO3() {
        return EMMCOMPO3;
    }

    public void setEMMCOMPO3(String EMMCOMPO3) {
        this.EMMCOMPO3 = EMMCOMPO3;
    }

    public String getEMMWEIGHT() {
        return EMMWEIGHT;
    }

    public void setEMMWEIGHT(String EMMWEIGHT) {
        this.EMMWEIGHT = EMMWEIGHT;
    }

    public String getEMMPRICE() {
        return EMMPRICE;
    }

    public void setEMMPRICE(String EMMPRICE) {
        this.EMMPRICE = EMMPRICE;
    }

    public String getEMMUNIT() {
        return EMMUNIT;
    }

    public void setEMMUNIT(String EMMUNIT) {
        this.EMMUNIT = EMMUNIT;
    }

    public String getEMMHSCODE() {
        return EMMHSCODE;
    }

    public void setEMMHSCODE(String EMMHSCODE) {
        this.EMMHSCODE = EMMHSCODE;
    }

    public String getEMMRANGE() {
        return EMMRANGE;
    }

    public void setEMMRANGE(String EMMRANGE) {
        this.EMMRANGE = EMMRANGE;
    }

    public String getEMMBOXS6E() {
        return EMMBOXS6E;
    }

    public void setEMMBOXS6E(String EMMBOXS6E) {
        this.EMMBOXS6E = EMMBOXS6E;
    }

    public String getEMMBOXS6D() {
        return EMMBOXS6D;
    }

    public void setEMMBOXS6D(String EMMBOXS6D) {
        this.EMMBOXS6D = EMMBOXS6D;
    }

    public String getEMMBOXS4E() {
        return EMMBOXS4E;
    }

    public void setEMMBOXS4E(String EMMBOXS4E) {
        this.EMMBOXS4E = EMMBOXS4E;
    }

    public String getEMMBOXS4D() {
        return EMMBOXS4D;
    }

    public void setEMMBOXS4D(String EMMBOXS4D) {
        this.EMMBOXS4D = EMMBOXS4D;
    }

    public String getEMMBOXS2E() {
        return EMMBOXS2E;
    }

    public void setEMMBOXS2E(String EMMBOXS2E) {
        this.EMMBOXS2E = EMMBOXS2E;
    }

    public String getEMMPPACK() {
        return EMMPPACK;
    }

    public void setEMMPPACK(String EMMPPACK) {
        this.EMMPPACK = EMMPPACK;
    }

    public String getEMMUSER() {
        return EMMUSER;
    }

    public void setEMMUSER(String EMMUSER) {
        this.EMMUSER = EMMUSER;
    }

    public String getEMMWG6E() {
        return EMMWG6E;
    }

    public void setEMMWG6E(String EMMWG6E) {
        this.EMMWG6E = EMMWG6E;
    }

    public String getEMMWG6D() {
        return EMMWG6D;
    }

    public void setEMMWG6D(String EMMWG6D) {
        this.EMMWG6D = EMMWG6D;
    }

    public String getEMMWG4E() {
        return EMMWG4E;
    }

    public void setEMMWG4E(String EMMWG4E) {
        this.EMMWG4E = EMMWG4E;
    }

    public String getEMMWG4D() {
        return EMMWG4D;
    }

    public void setEMMWG4D(String EMMWG4D) {
        this.EMMWG4D = EMMWG4D;
    }

    public String getEMMWG2E() {
        return EMMWG2E;
    }

    public void setEMMWG2E(String EMMWG2E) {
        this.EMMWG2E = EMMWG2E;
    }

}
