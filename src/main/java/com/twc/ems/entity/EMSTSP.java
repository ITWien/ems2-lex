/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.ems.entity;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author nutthawoot.noo
 */
public class EMSTSP {

    private String EMTPID;
    private String EMTPDESC;
    private String EMTPUSER;

    public EMSTSP() {

    }

    public String getEMTPID() {
        return EMTPID;
    }

    public void setEMTPID(String EMTPID) {
        this.EMTPID = EMTPID;
    }

    public String getEMTPDESC() {
        return EMTPDESC;
    }

    public void setEMTPDESC(String EMTPDESC) {
        this.EMTPDESC = EMTPDESC;
    }

    public String getEMTPUSER() {
        return EMTPUSER;
    }

    public void setEMTPUSER(String EMTPUSER) {
        this.EMTPUSER = EMTPUSER;
    }
}
