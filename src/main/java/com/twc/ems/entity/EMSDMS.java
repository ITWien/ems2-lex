/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.ems.entity;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author nutthawoot.noo
 */
public class EMSDMS {

    private String EMDSZONE;
    private String EMDSBOX;
    private String EMDSWEIGHT;
    private String EMDSDEEP;
    private String EMDSHEIGHT;
    private String EMDSBOXW;
    private String EMDSUSER;

    public EMSDMS() {

    }

    public String getEMDSZONE() {
        return EMDSZONE;
    }

    public void setEMDSZONE(String EMDSZONE) {
        this.EMDSZONE = EMDSZONE;
    }

    public String getEMDSBOX() {
        return EMDSBOX;
    }

    public void setEMDSBOX(String EMDSBOX) {
        this.EMDSBOX = EMDSBOX;
    }

    public String getEMDSWEIGHT() {
        return EMDSWEIGHT;
    }

    public void setEMDSWEIGHT(String EMDSWEIGHT) {
        this.EMDSWEIGHT = EMDSWEIGHT;
    }

    public String getEMDSDEEP() {
        return EMDSDEEP;
    }

    public void setEMDSDEEP(String EMDSDEEP) {
        this.EMDSDEEP = EMDSDEEP;
    }

    public String getEMDSHEIGHT() {
        return EMDSHEIGHT;
    }

    public void setEMDSHEIGHT(String EMDSHEIGHT) {
        this.EMDSHEIGHT = EMDSHEIGHT;
    }

    public String getEMDSBOXW() {
        return EMDSBOXW;
    }

    public void setEMDSBOXW(String EMDSBOXW) {
        this.EMDSBOXW = EMDSBOXW;
    }

    public String getEMDSUSER() {
        return EMDSUSER;
    }

    public void setEMDSUSER(String EMDSUSER) {
        this.EMDSUSER = EMDSUSER;
    }

}
