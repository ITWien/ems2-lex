/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.ems.entity;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author nutthawoot.noo
 */
public class EMSINVH {

    private String EMHINVID;
    private String EMHCUSID;
    private String EMHCUSSHIPID;
    private String EMHSHIPID;
    private String EMHTRANSPID;
    private String EMHCURRENCY;
    private String EMHUNIT;
    private String EMHSHIPDATE;
    private String EMHAMTCTN;
    private String EMHUSER;
    private String EMHNONTWC;
    private String EMHNONTOT;
    private String EMHNONNET;
    private String EMHNONGROSS;
    private String EMHNONM;
    private String EMHNONDI1;
    private String EMHNONDI2;
    private String EMHNONDI3;
    private String EMHZONE;
    private String NONTWC;
    private String EMHCDT;
    private String EMHDELNO;

    public EMSINVH() {

    }

    public String getEMHDELNO() {
        return EMHDELNO;
    }

    public void setEMHDELNO(String EMHDELNO) {
        this.EMHDELNO = EMHDELNO;
    }

    public String getEMHCDT() {
        return EMHCDT;
    }

    public void setEMHCDT(String EMHCDT) {
        this.EMHCDT = EMHCDT;
    }

    @Override
    public String toString() {
        return "EMSINVH{" + "EMHINVID=" + EMHINVID + ", EMHCUSID=" + EMHCUSID + ", EMHCUSSHIPID=" + EMHCUSSHIPID + ", EMHSHIPID=" + EMHSHIPID + ", EMHTRANSPID=" + EMHTRANSPID + ", EMHCURRENCY=" + EMHCURRENCY + ", EMHUNIT=" + EMHUNIT + ", EMHSHIPDATE=" + EMHSHIPDATE + ", EMHNONTWC=" + EMHNONTWC + ", EMHZONE=" + EMHZONE + '}';
    }

    public String getNONTWC() {
        return NONTWC;
    }

    public void setNONTWC(String NONTWC) {
        this.NONTWC = NONTWC;
    }

    public String getEMHZONE() {
        return EMHZONE;
    }

    public void setEMHZONE(String EMHZONE) {
        this.EMHZONE = EMHZONE;
    }

    public String getEMHNONTOT() {
        return EMHNONTOT;
    }

    public void setEMHNONTOT(String EMHNONTOT) {
        this.EMHNONTOT = EMHNONTOT;
    }

    public String getEMHNONNET() {
        return EMHNONNET;
    }

    public void setEMHNONNET(String EMHNONNET) {
        this.EMHNONNET = EMHNONNET;
    }

    public String getEMHNONGROSS() {
        return EMHNONGROSS;
    }

    public void setEMHNONGROSS(String EMHNONGROSS) {
        this.EMHNONGROSS = EMHNONGROSS;
    }

    public String getEMHNONM() {
        return EMHNONM;
    }

    public void setEMHNONM(String EMHNONM) {
        this.EMHNONM = EMHNONM;
    }

    public String getEMHNONDI1() {
        return EMHNONDI1;
    }

    public void setEMHNONDI1(String EMHNONDI1) {
        this.EMHNONDI1 = EMHNONDI1;
    }

    public String getEMHNONDI2() {
        return EMHNONDI2;
    }

    public void setEMHNONDI2(String EMHNONDI2) {
        this.EMHNONDI2 = EMHNONDI2;
    }

    public String getEMHNONDI3() {
        return EMHNONDI3;
    }

    public void setEMHNONDI3(String EMHNONDI3) {
        this.EMHNONDI3 = EMHNONDI3;
    }

    public String getEMHNONTWC() {
        return EMHNONTWC;
    }

    public void setEMHNONTWC(String EMHNONTWC) {
        this.EMHNONTWC = EMHNONTWC;
    }

    public String getEMHINVID() {
        return EMHINVID;
    }

    public void setEMHINVID(String EMHINVID) {
        this.EMHINVID = EMHINVID;
    }

    public String getEMHCUSID() {
        return EMHCUSID;
    }

    public void setEMHCUSID(String EMHCUSID) {
        this.EMHCUSID = EMHCUSID;
    }

    public String getEMHCUSSHIPID() {
        return EMHCUSSHIPID;
    }

    public void setEMHCUSSHIPID(String EMHCUSSHIPID) {
        this.EMHCUSSHIPID = EMHCUSSHIPID;
    }

    public String getEMHSHIPID() {
        return EMHSHIPID;
    }

    public void setEMHSHIPID(String EMHSHIPID) {
        this.EMHSHIPID = EMHSHIPID;
    }

    public String getEMHTRANSPID() {
        return EMHTRANSPID;
    }

    public void setEMHTRANSPID(String EMHTRANSPID) {
        this.EMHTRANSPID = EMHTRANSPID;
    }

    public String getEMHCURRENCY() {
        return EMHCURRENCY;
    }

    public void setEMHCURRENCY(String EMHCURRENCY) {
        this.EMHCURRENCY = EMHCURRENCY;
    }

    public String getEMHUNIT() {
        return EMHUNIT;
    }

    public void setEMHUNIT(String EMHUNIT) {
        this.EMHUNIT = EMHUNIT;
    }

    public String getEMHSHIPDATE() {
        return EMHSHIPDATE;
    }

    public void setEMHSHIPDATE(String EMHSHIPDATE) {
        this.EMHSHIPDATE = EMHSHIPDATE;
    }

    public String getEMHAMTCTN() {
        return EMHAMTCTN;
    }

    public void setEMHAMTCTN(String EMHAMTCTN) {
        this.EMHAMTCTN = EMHAMTCTN;
    }

    public String getEMHUSER() {
        return EMHUSER;
    }

    public void setEMHUSER(String EMHUSER) {
        this.EMHUSER = EMHUSER;
    }

}
