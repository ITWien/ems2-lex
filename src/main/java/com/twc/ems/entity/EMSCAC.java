/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.ems.entity;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author nutthawoot.noo
 */
public class EMSCAC {

    private String EMCCOM;
    private String EMCINVID;
    private String EMCCTNO;
    private String EMCORDID;
    private String EMCPONO;
    private String EMCFGNO;
    private String EMCSTYLE;
    private String EMCCOLOR;
    private String EMCCUP;
    private String EMCSIZES;
    private String EMCPCS;
    private String EMCNET;
    private String EMCGROSS;
    private String EMCBOXTYPE;
    private String EMCEDT;
    private String EMCCDT;
    private String EMCUSER;
    private String EMCAUTH;
    private String tbody;
    private String BOXPACK;
    private String weight;
    private String boxweight;
    private String EMCSIZELIST;
    private String EMCCARTONNO;
    private String EMCFLAG;
    private String tEditSize;
    private String EMCCUPLIST;
    private String EMCCOLORLIST;
    private String EMCCUPOLIST;
    private String EMCCUPLIST2;
    private String EMCZONE;
    private String EMCCARTONNOdata;

    public EMSCAC() {

    }

    public String getEMCCARTONNOdata() {
        return EMCCARTONNOdata;
    }

    public void setEMCCARTONNOdata(String EMCCARTONNOdata) {
        this.EMCCARTONNOdata = EMCCARTONNOdata;
    }

    public String getEMCFGNO() {
        return EMCFGNO;
    }

    public void setEMCFGNO(String EMCFGNO) {
        this.EMCFGNO = EMCFGNO;
    }

    public String getEMCCOLORLIST() {
        return EMCCOLORLIST;
    }

    public void setEMCCOLORLIST(String EMCCOLORLIST) {
        this.EMCCOLORLIST = EMCCOLORLIST;
    }

    public String getEMCZONE() {
        return EMCZONE;
    }

    public void setEMCZONE(String EMCZONE) {
        this.EMCZONE = EMCZONE;
    }

    public String getEMCCUPLIST2() {
        return EMCCUPLIST2;
    }

    public void setEMCCUPLIST2(String EMCCUPLIST2) {
        this.EMCCUPLIST2 = EMCCUPLIST2;
    }

    public String getEMCCUPOLIST() {
        return EMCCUPOLIST;
    }

    public void setEMCCUPOLIST(String EMCCUPOLIST) {
        this.EMCCUPOLIST = EMCCUPOLIST;
    }

    public String getEMCCUPLIST() {
        return EMCCUPLIST;
    }

    public void setEMCCUPLIST(String EMCCUPLIST) {
        this.EMCCUPLIST = EMCCUPLIST;
    }

    public String gettEditSize() {
        return tEditSize;
    }

    public void settEditSize(String tEditSize) {
        this.tEditSize = tEditSize;
    }

    public String getEMCFLAG() {
        return EMCFLAG;
    }

    public void setEMCFLAG(String EMCFLAG) {
        this.EMCFLAG = EMCFLAG;
    }

    public String getEMCCARTONNO() {
        return EMCCARTONNO;
    }

    public void setEMCCARTONNO(String EMCCARTONNO) {
        this.EMCCARTONNO = EMCCARTONNO;
    }

    public String getEMCSIZELIST() {
        return EMCSIZELIST;
    }

    public void setEMCSIZELIST(String EMCSIZELIST) {
        this.EMCSIZELIST = EMCSIZELIST;
    }

    public String getBoxweight() {
        return boxweight;
    }

    public void setBoxweight(String boxweight) {
        this.boxweight = boxweight;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getBOXPACK() {
        return BOXPACK;
    }

    public void setBOXPACK(String BOXPACK) {
        this.BOXPACK = BOXPACK;
    }

    public String getTbody() {
        return tbody;
    }

    public void setTbody(String tbody) {
        this.tbody = tbody;
    }

    public String getEMCAUTH() {
        return EMCAUTH;
    }

    public void setEMCAUTH(String EMCAUTH) {
        this.EMCAUTH = EMCAUTH;
    }

    public String getEMCCOM() {
        return EMCCOM;
    }

    public void setEMCCOM(String EMCCOM) {
        this.EMCCOM = EMCCOM;
    }

    public String getEMCINVID() {
        return EMCINVID;
    }

    public void setEMCINVID(String EMCINVID) {
        this.EMCINVID = EMCINVID;
    }

    public String getEMCCTNO() {
        return EMCCTNO;
    }

    public void setEMCCTNO(String EMCCTNO) {
        this.EMCCTNO = EMCCTNO;
    }

    public String getEMCORDID() {
        return EMCORDID;
    }

    public void setEMCORDID(String EMCORDID) {
        this.EMCORDID = EMCORDID;
    }

    public String getEMCPONO() {
        return EMCPONO;
    }

    public void setEMCPONO(String EMCPONO) {
        this.EMCPONO = EMCPONO;
    }

    public String getEMCSTYLE() {
        return EMCSTYLE;
    }

    public void setEMCSTYLE(String EMCSTYLE) {
        this.EMCSTYLE = EMCSTYLE;
    }

    public String getEMCCOLOR() {
        return EMCCOLOR;
    }

    public void setEMCCOLOR(String EMCCOLOR) {
        this.EMCCOLOR = EMCCOLOR;
    }

    public String getEMCCUP() {
        return EMCCUP;
    }

    public void setEMCCUP(String EMCCUP) {
        this.EMCCUP = EMCCUP;
    }

    public String getEMCSIZES() {
        return EMCSIZES;
    }

    public void setEMCSIZES(String EMCSIZES) {
        this.EMCSIZES = EMCSIZES;
    }

    public String getEMCPCS() {
        return EMCPCS;
    }

    public void setEMCPCS(String EMCPCS) {
        this.EMCPCS = EMCPCS;
    }

    public String getEMCNET() {
        return EMCNET;
    }

    public void setEMCNET(String EMCNET) {
        this.EMCNET = EMCNET;
    }

    public String getEMCGROSS() {
        return EMCGROSS;
    }

    public void setEMCGROSS(String EMCGROSS) {
        this.EMCGROSS = EMCGROSS;
    }

    public String getEMCBOXTYPE() {
        return EMCBOXTYPE;
    }

    public void setEMCBOXTYPE(String EMCBOXTYPE) {
        this.EMCBOXTYPE = EMCBOXTYPE;
    }

    public String getEMCEDT() {
        return EMCEDT;
    }

    public void setEMCEDT(String EMCEDT) {
        this.EMCEDT = EMCEDT;
    }

    public String getEMCCDT() {
        return EMCCDT;
    }

    public void setEMCCDT(String EMCCDT) {
        this.EMCCDT = EMCCDT;
    }

    public String getEMCUSER() {
        return EMCUSER;
    }

    public void setEMCUSER(String EMCUSER) {
        this.EMCUSER = EMCUSER;
    }

}
