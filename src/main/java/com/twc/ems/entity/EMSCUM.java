/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.ems.entity;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author nutthawoot.noo
 */
public class EMSCUM {

    private String EMCMSHIPID;
    private String EMCMDESC1;
    private String EMCMDESC2;
    private String EMCMDESC3;
    private String EMCMDESC4;
    private String EMCMDESC5;
    private String EMCMUSER;

    public EMSCUM() {

    }

    public String getEMCMSHIPID() {
        return EMCMSHIPID;
    }

    public void setEMCMSHIPID(String EMCMSHIPID) {
        this.EMCMSHIPID = EMCMSHIPID;
    }

    public String getEMCMDESC1() {
        return EMCMDESC1;
    }

    public void setEMCMDESC1(String EMCMDESC1) {
        this.EMCMDESC1 = EMCMDESC1;
    }

    public String getEMCMDESC2() {
        return EMCMDESC2;
    }

    public void setEMCMDESC2(String EMCMDESC2) {
        this.EMCMDESC2 = EMCMDESC2;
    }

    public String getEMCMDESC3() {
        return EMCMDESC3;
    }

    public void setEMCMDESC3(String EMCMDESC3) {
        this.EMCMDESC3 = EMCMDESC3;
    }

    public String getEMCMDESC4() {
        return EMCMDESC4;
    }

    public void setEMCMDESC4(String EMCMDESC4) {
        this.EMCMDESC4 = EMCMDESC4;
    }

    public String getEMCMDESC5() {
        return EMCMDESC5;
    }

    public void setEMCMDESC5(String EMCMDESC5) {
        this.EMCMDESC5 = EMCMDESC5;
    }

    public String getEMCMUSER() {
        return EMCMUSER;
    }

    public void setEMCMUSER(String EMCMUSER) {
        this.EMCMUSER = EMCMUSER;
    }

}
