/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.ems.entity;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author nutthawoot.noo
 */
public class EMSINVD {

    private String EMDINVID;
    private String EMDORDID;
    private String EMDORDIDdata;
    private String EMDLOT;
    private String EMDPONO;
    private String EMDPONOdata;
    private String EMDSTYLE;
    private String EMDSTYLEdata;
    private String EMDCOLOR;
    private String EMDCOLORdata;
    private String EMDCUP;
    private String EMDCUPdata;
    private String EMDSIZES;
    private String EMDA1;
    private String EMDSTYONHD;
    private String EMDMT;
    private String EMDPRIME;
    private String EMDSEC;
    private String EMDAUTH;
    private String EMDAUTHdata;
    private String EMDBRAND;
    private String EMDBRANDdata;
    private String EMDTYPE;
    private String EMDTYPEdata;
    private String EMDAMOUNT;
    private String EMDAMOUNTPACK;
    private String EMDOTSTYLE;
    private String EMDCOMPS;
    private String EMDDISCOUNT;
    private String EMDCOST;
    private String EMDPOSHIP;
    private String EMDCODE;
    private String EMDCODEdata;
    private String EMDCODEdata2;
    private String BOXTYPE;
    private String BOXPACK;
    private String PCS;
    private String NET;
    private String GROSS;
    private String tbody;
    private String flag;
    private String PLANT;
    private String realFlag;

    public EMSINVD() {

    }

    @Override
    public String toString() {
        return "EMSINVD{" + "EMDINVID=" + EMDINVID + ", EMDORDID=" + EMDORDID + ", EMDPONO=" + EMDPONO + ", EMDSTYLE=" + EMDSTYLE + ", EMDCOLOR=" + EMDCOLOR + ", EMDCUP=" + EMDCUP + ", EMDSIZES=" + EMDSIZES + ", EMDSTYONHD=" + EMDSTYONHD + ", EMDAUTH=" + EMDAUTH + ", EMDBRAND=" + EMDBRAND + ", EMDTYPE=" + EMDTYPE + ", EMDAMOUNT=" + EMDAMOUNT + ", EMDDISCOUNT=" + EMDDISCOUNT + ", EMDCOST=" + EMDCOST + ", EMDPOSHIP=" + EMDPOSHIP + ", PLANT=" + PLANT + '}';
    }

    public String getRealFlag() {
        return realFlag;
    }

    public void setRealFlag(String realFlag) {
        this.realFlag = realFlag;
    }

    public String getPLANT() {
        return PLANT;
    }

    public void setPLANT(String PLANT) {
        this.PLANT = PLANT;
    }

    public String getEMDORDIDdata() {
        return EMDORDIDdata;
    }

    public void setEMDORDIDdata(String EMDORDIDdata) {
        this.EMDORDIDdata = EMDORDIDdata;
    }

    public String getEMDSTYLEdata() {
        return EMDSTYLEdata;
    }

    public void setEMDSTYLEdata(String EMDSTYLEdata) {
        this.EMDSTYLEdata = EMDSTYLEdata;
    }

    public String getEMDCOLORdata() {
        return EMDCOLORdata;
    }

    public void setEMDCOLORdata(String EMDCOLORdata) {
        this.EMDCOLORdata = EMDCOLORdata;
    }

    public String getEMDCUPdata() {
        return EMDCUPdata;
    }

    public void setEMDCUPdata(String EMDCUPdata) {
        this.EMDCUPdata = EMDCUPdata;
    }

    public String getEMDAUTHdata() {
        return EMDAUTHdata;
    }

    public void setEMDAUTHdata(String EMDAUTHdata) {
        this.EMDAUTHdata = EMDAUTHdata;
    }

    public String getEMDBRANDdata() {
        return EMDBRANDdata;
    }

    public void setEMDBRANDdata(String EMDBRANDdata) {
        this.EMDBRANDdata = EMDBRANDdata;
    }

    public String getEMDTYPEdata() {
        return EMDTYPEdata;
    }

    public void setEMDTYPEdata(String EMDTYPEdata) {
        this.EMDTYPEdata = EMDTYPEdata;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getTbody() {
        return tbody;
    }

    public void setTbody(String tbody) {
        this.tbody = tbody;
    }

    public String getNET() {
        return NET;
    }

    public void setNET(String NET) {
        this.NET = NET;
    }

    public String getGROSS() {
        return GROSS;
    }

    public void setGROSS(String GROSS) {
        this.GROSS = GROSS;
    }

    public String getPCS() {
        return PCS;
    }

    public void setPCS(String PCS) {
        this.PCS = PCS;
    }

    public String getBOXTYPE() {
        return BOXTYPE;
    }

    public void setBOXTYPE(String BOXTYPE) {
        this.BOXTYPE = BOXTYPE;
    }

    public String getBOXPACK() {
        return BOXPACK;
    }

    public void setBOXPACK(String BOXPACK) {
        this.BOXPACK = BOXPACK;
    }

    public String getEMDCODEdata2() {
        return EMDCODEdata2;
    }

    public void setEMDCODEdata2(String EMDCODEdata2) {
        this.EMDCODEdata2 = EMDCODEdata2;
    }

    public String getEMDCODE() {
        return EMDCODE;
    }

    public void setEMDCODE(String EMDCODE) {
        this.EMDCODE = EMDCODE;
    }

    public String getEMDCODEdata() {
        return EMDCODEdata;
    }

    public void setEMDCODEdata(String EMDCODEdata) {
        this.EMDCODEdata = EMDCODEdata;
    }

    public String getEMDPONOdata() {
        return EMDPONOdata;
    }

    public void setEMDPONOdata(String EMDPONOdata) {
        this.EMDPONOdata = EMDPONOdata;
    }

    public String getEMDINVID() {
        return EMDINVID;
    }

    public void setEMDINVID(String EMDINVID) {
        this.EMDINVID = EMDINVID;
    }

    public String getEMDORDID() {
        return EMDORDID;
    }

    public void setEMDORDID(String EMDORDID) {
        this.EMDORDID = EMDORDID;
    }

    public String getEMDLOT() {
        return EMDLOT;
    }

    public void setEMDLOT(String EMDLOT) {
        this.EMDLOT = EMDLOT;
    }

    public String getEMDPONO() {
        return EMDPONO;
    }

    public void setEMDPONO(String EMDPONO) {
        this.EMDPONO = EMDPONO;
    }

    public String getEMDSTYLE() {
        return EMDSTYLE;
    }

    public void setEMDSTYLE(String EMDSTYLE) {
        this.EMDSTYLE = EMDSTYLE;
    }

    public String getEMDCOLOR() {
        return EMDCOLOR;
    }

    public void setEMDCOLOR(String EMDCOLOR) {
        this.EMDCOLOR = EMDCOLOR;
    }

    public String getEMDCUP() {
        return EMDCUP;
    }

    public void setEMDCUP(String EMDCUP) {
        this.EMDCUP = EMDCUP;
    }

    public String getEMDSIZES() {
        return EMDSIZES;
    }

    public void setEMDSIZES(String EMDSIZES) {
        this.EMDSIZES = EMDSIZES;
    }

    public String getEMDA1() {
        return EMDA1;
    }

    public void setEMDA1(String EMDA1) {
        this.EMDA1 = EMDA1;
    }

    public String getEMDSTYONHD() {
        return EMDSTYONHD;
    }

    public void setEMDSTYONHD(String EMDSTYONHD) {
        this.EMDSTYONHD = EMDSTYONHD;
    }

    public String getEMDMT() {
        return EMDMT;
    }

    public void setEMDMT(String EMDMT) {
        this.EMDMT = EMDMT;
    }

    public String getEMDPRIME() {
        return EMDPRIME;
    }

    public void setEMDPRIME(String EMDPRIME) {
        this.EMDPRIME = EMDPRIME;
    }

    public String getEMDSEC() {
        return EMDSEC;
    }

    public void setEMDSEC(String EMDSEC) {
        this.EMDSEC = EMDSEC;
    }

    public String getEMDAUTH() {
        return EMDAUTH;
    }

    public void setEMDAUTH(String EMDAUTH) {
        this.EMDAUTH = EMDAUTH;
    }

    public String getEMDBRAND() {
        return EMDBRAND;
    }

    public void setEMDBRAND(String EMDBRAND) {
        this.EMDBRAND = EMDBRAND;
    }

    public String getEMDTYPE() {
        return EMDTYPE;
    }

    public void setEMDTYPE(String EMDTYPE) {
        this.EMDTYPE = EMDTYPE;
    }

    public String getEMDAMOUNT() {
        return EMDAMOUNT;
    }

    public void setEMDAMOUNT(String EMDAMOUNT) {
        this.EMDAMOUNT = EMDAMOUNT;
    }

    public String getEMDAMOUNTPACK() {
        return EMDAMOUNTPACK;
    }

    public void setEMDAMOUNTPACK(String EMDAMOUNTPACK) {
        this.EMDAMOUNTPACK = EMDAMOUNTPACK;
    }

    public String getEMDOTSTYLE() {
        return EMDOTSTYLE;
    }

    public void setEMDOTSTYLE(String EMDOTSTYLE) {
        this.EMDOTSTYLE = EMDOTSTYLE;
    }

    public String getEMDCOMPS() {
        return EMDCOMPS;
    }

    public void setEMDCOMPS(String EMDCOMPS) {
        this.EMDCOMPS = EMDCOMPS;
    }

    public String getEMDDISCOUNT() {
        return EMDDISCOUNT;
    }

    public void setEMDDISCOUNT(String EMDDISCOUNT) {
        this.EMDDISCOUNT = EMDDISCOUNT;
    }

    public String getEMDCOST() {
        return EMDCOST;
    }

    public void setEMDCOST(String EMDCOST) {
        this.EMDCOST = EMDCOST;
    }

    public String getEMDPOSHIP() {
        return EMDPOSHIP;
    }

    public void setEMDPOSHIP(String EMDPOSHIP) {
        this.EMDPOSHIP = EMDPOSHIP;
    }

}
