/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.ems.entity;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author nutthawoot.noo
 */
public class EMSUNIT {

    private String EMUMID;
    private String EMUMDESC;
    private String EMUMUSER;

    public EMSUNIT() {

    }

    public String getEMUMID() {
        return EMUMID;
    }

    public void setEMUMID(String EMUMID) {
        this.EMUMID = EMUMID;
    }

    public String getEMUMDESC() {
        return EMUMDESC;
    }

    public void setEMUMDESC(String EMUMDESC) {
        this.EMUMDESC = EMUMDESC;
    }

    public String getEMUMUSER() {
        return EMUMUSER;
    }

    public void setEMUMUSER(String EMUMUSER) {
        this.EMUMUSER = EMUMUSER;
    }

}
