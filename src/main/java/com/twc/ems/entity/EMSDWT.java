/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.ems.entity;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author nutthawoot.noo
 */
public class EMSDWT {

    private String EMSDWQNO;
    private String EMSDWDELNO;
    private String EMSDWTRDT;
    private String EMSDWSALEORG;
    private String EMSDWSHIPMARK;
    private String EMSDWTRANSPORT;
    private String EMSDWDISTCH;
    private String EMSDWEXP;
    private String EMSDWNONTWC;
    private String EMSDWUSER;
    private String EMSDWZONE;
    private String EMSDWINVNO;

    public EMSDWT() {

    }

    public String getEMSDWINVNO() {
        return EMSDWINVNO;
    }

    public void setEMSDWINVNO(String EMSDWINVNO) {
        this.EMSDWINVNO = EMSDWINVNO;
    }

    public String getEMSDWQNO() {
        return EMSDWQNO;
    }

    public void setEMSDWQNO(String EMSDWQNO) {
        this.EMSDWQNO = EMSDWQNO;
    }

    public String getEMSDWZONE() {
        return EMSDWZONE;
    }

    public void setEMSDWZONE(String EMSDWZONE) {
        this.EMSDWZONE = EMSDWZONE;
    }

    public String getEMSDWNONTWC() {
        return EMSDWNONTWC;
    }

    public void setEMSDWNONTWC(String EMSDWNONTWC) {
        this.EMSDWNONTWC = EMSDWNONTWC;
    }

    public String getEMSDWDELNO() {
        return EMSDWDELNO;
    }

    public void setEMSDWDELNO(String EMSDWDELNO) {
        this.EMSDWDELNO = EMSDWDELNO;
    }

    public String getEMSDWTRDT() {
        return EMSDWTRDT;
    }

    public void setEMSDWTRDT(String EMSDWTRDT) {
        this.EMSDWTRDT = EMSDWTRDT;
    }

    public String getEMSDWSALEORG() {
        return EMSDWSALEORG;
    }

    public void setEMSDWSALEORG(String EMSDWSALEORG) {
        this.EMSDWSALEORG = EMSDWSALEORG;
    }

    public String getEMSDWSHIPMARK() {
        return EMSDWSHIPMARK;
    }

    public void setEMSDWSHIPMARK(String EMSDWSHIPMARK) {
        this.EMSDWSHIPMARK = EMSDWSHIPMARK;
    }

    public String getEMSDWTRANSPORT() {
        return EMSDWTRANSPORT;
    }

    public void setEMSDWTRANSPORT(String EMSDWTRANSPORT) {
        this.EMSDWTRANSPORT = EMSDWTRANSPORT;
    }

    public String getEMSDWDISTCH() {
        return EMSDWDISTCH;
    }

    public void setEMSDWDISTCH(String EMSDWDISTCH) {
        this.EMSDWDISTCH = EMSDWDISTCH;
    }

    public String getEMSDWEXP() {
        return EMSDWEXP;
    }

    public void setEMSDWEXP(String EMSDWEXP) {
        this.EMSDWEXP = EMSDWEXP;
    }

    public String getEMSDWUSER() {
        return EMSDWUSER;
    }

    public void setEMSDWUSER(String EMSDWUSER) {
        this.EMSDWUSER = EMSDWUSER;
    }

}
