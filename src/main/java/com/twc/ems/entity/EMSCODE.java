/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.ems.entity;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author nutthawoot.noo
 */
public class EMSCODE {

    private String EMCDZONE;
    private String EMCDCODE;
    private String EMCDDESC;
    private String EMCDUSER;

    public EMSCODE() {

    }

    @Override
    public String toString() {
        return "EMSCODE{" + "EMCDZONE=" + EMCDZONE + ", EMCDCODE=" + EMCDCODE + '}';
    }

    public String getEMCDZONE() {
        return EMCDZONE;
    }

    public void setEMCDZONE(String EMCDZONE) {
        this.EMCDZONE = EMCDZONE;
    }

    public String getEMCDCODE() {
        return EMCDCODE;
    }

    public void setEMCDCODE(String EMCDCODE) {
        this.EMCDCODE = EMCDCODE;
    }

    public String getEMCDDESC() {
        return EMCDDESC;
    }

    public void setEMCDDESC(String EMCDDESC) {
        this.EMCDDESC = EMCDDESC;
    }

    public String getEMCDUSER() {
        return EMCDUSER;
    }

    public void setEMCDUSER(String EMCDUSER) {
        this.EMCDUSER = EMCDUSER;
    }

}
