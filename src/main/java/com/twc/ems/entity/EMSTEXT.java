/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.ems.entity;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author nutthawoot.noo
 */
public class EMSTEXT {

    private String EMTXINVNO;
    private String EMTXDATE;
    private String EMTXORDOF;
    private String EMTXPMT;
    private String EMTXTERM;
    private String EMTXSUNIT;
    private String EMTXSHFROM;
    private String EMTXSHTO;
    private String EMTXDELADD1;
    private String EMTXDELADD2;
    private String EMTXDELADD3;
    private String EMTXDELADD4;
    private String EMTXUSER;
    private String EMTXMIDCODE;
    private String EMTXMANUFAC1;
    private String EMTXMANUFAC2;
    private String EMTXMANUFAC3;

    public EMSTEXT() {

    }

    public String getEMTXINVNO() {
        return EMTXINVNO;
    }

    public void setEMTXINVNO(String EMTXINVNO) {
        this.EMTXINVNO = EMTXINVNO;
    }

    public String getEMTXDATE() {
        return EMTXDATE;
    }

    public void setEMTXDATE(String EMTXDATE) {
        this.EMTXDATE = EMTXDATE;
    }

    public String getEMTXORDOF() {
        return EMTXORDOF;
    }

    public void setEMTXORDOF(String EMTXORDOF) {
        this.EMTXORDOF = EMTXORDOF;
    }

    public String getEMTXPMT() {
        return EMTXPMT;
    }

    public void setEMTXPMT(String EMTXPMT) {
        this.EMTXPMT = EMTXPMT;
    }

    public String getEMTXTERM() {
        return EMTXTERM;
    }

    public void setEMTXTERM(String EMTXTERM) {
        this.EMTXTERM = EMTXTERM;
    }

    public String getEMTXSUNIT() {
        return EMTXSUNIT;
    }

    public void setEMTXSUNIT(String EMTXSUNIT) {
        this.EMTXSUNIT = EMTXSUNIT;
    }

    public String getEMTXSHFROM() {
        return EMTXSHFROM;
    }

    public void setEMTXSHFROM(String EMTXSHFROM) {
        this.EMTXSHFROM = EMTXSHFROM;
    }

    public String getEMTXSHTO() {
        return EMTXSHTO;
    }

    public void setEMTXSHTO(String EMTXSHTO) {
        this.EMTXSHTO = EMTXSHTO;
    }

    public String getEMTXDELADD1() {
        return EMTXDELADD1;
    }

    public void setEMTXDELADD1(String EMTXDELADD1) {
        this.EMTXDELADD1 = EMTXDELADD1;
    }

    public String getEMTXDELADD2() {
        return EMTXDELADD2;
    }

    public void setEMTXDELADD2(String EMTXDELADD2) {
        this.EMTXDELADD2 = EMTXDELADD2;
    }

    public String getEMTXDELADD3() {
        return EMTXDELADD3;
    }

    public void setEMTXDELADD3(String EMTXDELADD3) {
        this.EMTXDELADD3 = EMTXDELADD3;
    }

    public String getEMTXDELADD4() {
        return EMTXDELADD4;
    }

    public void setEMTXDELADD4(String EMTXDELADD4) {
        this.EMTXDELADD4 = EMTXDELADD4;
    }

    public String getEMTXUSER() {
        return EMTXUSER;
    }

    public void setEMTXUSER(String EMTXUSER) {
        this.EMTXUSER = EMTXUSER;
    }

    public String getEMTXMIDCODE() {
        return EMTXMIDCODE;
    }

    public void setEMTXMIDCODE(String EMTXMIDCODE) {
        this.EMTXMIDCODE = EMTXMIDCODE;
    }

    public String getEMTXMANUFAC1() {
        return EMTXMANUFAC1;
    }

    public void setEMTXMANUFAC1(String EMTXMANUFAC1) {
        this.EMTXMANUFAC1 = EMTXMANUFAC1;
    }

    public String getEMTXMANUFAC2() {
        return EMTXMANUFAC2;
    }

    public void setEMTXMANUFAC2(String EMTXMANUFAC2) {
        this.EMTXMANUFAC2 = EMTXMANUFAC2;
    }

    public String getEMTXMANUFAC3() {
        return EMTXMANUFAC3;
    }

    public void setEMTXMANUFAC3(String EMTXMANUFAC3) {
        this.EMTXMANUFAC3 = EMTXMANUFAC3;
    }

}
