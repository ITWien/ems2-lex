/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.ems.controller;

import com.twc.ems.dao.EMSCACDao;
import com.twc.ems.dao.EMSINVDDao;
import com.twc.ems.dao.EMSINVHDao;
import com.twc.ems.dao.EMSMASDao;
import com.twc.ems.entity.EMSINVD;
import com.twc.ems.entity.EMSINVH;
import java.io.IOException;
import java.util.Calendar;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author wien
 */
public class DropBoxControllerEMS100 extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;

    public DropBoxControllerEMS100() {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String dist = request.getParameter("dist");
        String inv = request.getParameter("inv");
        String userid = request.getParameter("userid");

        request.setAttribute("inv", inv);

        EMSINVH p = new EMSINVHDao().findByCode(inv, "0");

        request.setAttribute("EMHINVID", p.getEMHINVID());
        request.setAttribute("EMHCUSID", p.getEMHCUSID());
        request.setAttribute("EMHCUSSHIPID", p.getEMHCUSSHIPID());
        request.setAttribute("EMHSHIPID", p.getEMHSHIPID());
        request.setAttribute("EMHTRANSPID", p.getEMHTRANSPID());
        request.setAttribute("EMHCURRENCY", p.getEMHCURRENCY());
        request.setAttribute("EMHUNIT", p.getEMHUNIT());
        request.setAttribute("EMHSHIPDATE", p.getEMHSHIPDATE());
        request.setAttribute("EMHAMTCTN", p.getEMHAMTCTN());

        System.out.println("---------------------");

        Calendar c = Calendar.getInstance();
        System.out.print("start : ");
        System.out.println(c.get(Calendar.HOUR) + ":" + c.get(Calendar.MINUTE) + ":" + c.get(Calendar.SECOND));

        if (dist.equals("europe")) {//****************EUROPE
            List<EMSINVD> aheadList = new EMSINVDDao().findByInvDrop(inv, dist.toUpperCase());

            new EMSCACDao().delete(inv, dist.toUpperCase());
            new EMSCACDao().addByObj(aheadList, inv, userid, dist.toUpperCase());
//
//            for (int i = 0; i < aheadList.size(); i++) {
//
//                new EMSCACDao().add(inv, Integer.toString(i + 1), aheadList.get(i).getEMDORDID(),
//                        aheadList.get(i).getEMDPONO(), aheadList.get(i).getEMDSTYLE(),
//                        aheadList.get(i).getEMDCOLOR(), aheadList.get(i).getEMDCUP(),
//                        aheadList.get(i).getEMDSIZES(), aheadList.get(i).getPCS(),
//                        aheadList.get(i).getNET(), aheadList.get(i).getGROSS(),
//                        aheadList.get(i).getBOXTYPE(), userid, aheadList.get(i).getEMDAUTH(), aheadList.get(i).getFlag(), dist.toUpperCase(), aheadList.get(i).getRealFlag());
//
//            }

        } else if (dist.equals("america") || dist.equals("america2")) {//****************AMERICA
            List<EMSINVD> aheadList = new EMSINVDDao().findByInvDrop(inv, dist.toUpperCase());

            new EMSCACDao().delete(inv, dist.toUpperCase());
            new EMSCACDao().addByObj(aheadList, inv, userid, dist.toUpperCase());
//
//            for (int i = 0; i < aheadList.size(); i++) {
//
//                new EMSCACDao().add(inv, Integer.toString(i + 1), aheadList.get(i).getEMDORDID(),
//                        aheadList.get(i).getEMDPONO(), aheadList.get(i).getEMDSTYLE(),
//                        aheadList.get(i).getEMDCOLOR(), aheadList.get(i).getEMDCUP(),
//                        aheadList.get(i).getEMDSIZES(), aheadList.get(i).getPCS(),
//                        aheadList.get(i).getNET(), aheadList.get(i).getGROSS(),
//                        aheadList.get(i).getBOXTYPE(), userid, aheadList.get(i).getEMDAUTH(), aheadList.get(i).getFlag(), dist.toUpperCase(), aheadList.get(i).getRealFlag());
//
//            }
//
        } else if (dist.equals("brazil")) {//****************BRAZIL
            List<EMSINVD> aheadList = new EMSINVDDao().findByInvDrop(inv, dist.toUpperCase());

            new EMSCACDao().delete(inv, dist.toUpperCase());
            new EMSCACDao().addByObj(aheadList, inv, userid, dist.toUpperCase());
//
//            for (int i = 0; i < aheadList.size(); i++) {
//
//                new EMSCACDao().add(inv, Integer.toString(i + 1), aheadList.get(i).getEMDORDID(),
//                        aheadList.get(i).getEMDPONO(), aheadList.get(i).getEMDSTYLE(),
//                        aheadList.get(i).getEMDCOLOR(), aheadList.get(i).getEMDCUP(),
//                        aheadList.get(i).getEMDSIZES(), aheadList.get(i).getPCS(),
//                        aheadList.get(i).getNET(), aheadList.get(i).getGROSS(),
//                        aheadList.get(i).getBOXTYPE(), userid, aheadList.get(i).getEMDAUTH(), aheadList.get(i).getFlag(), dist.toUpperCase(), aheadList.get(i).getRealFlag());
//
//            }

        } else if (dist.equals("usa-cwx")) {//****************USA-CWX
            List<EMSINVD> aheadList = new EMSINVDDao().findByInvDrop(inv, dist.toUpperCase());

            new EMSCACDao().delete(inv, dist.toUpperCase());
            new EMSCACDao().addByObj(aheadList, inv, userid, dist.toUpperCase());
//
//            for (int i = 0; i < aheadList.size(); i++) {
//
//                new EMSCACDao().add(inv, Integer.toString(i + 1), aheadList.get(i).getEMDORDID(),
//                        aheadList.get(i).getEMDPONO(), aheadList.get(i).getEMDSTYLE(),
//                        aheadList.get(i).getEMDCOLOR(), aheadList.get(i).getEMDCUP(),
//                        aheadList.get(i).getEMDSIZES(), aheadList.get(i).getPCS(),
//                        aheadList.get(i).getNET(), aheadList.get(i).getGROSS(),
//                        aheadList.get(i).getBOXTYPE(), userid, aheadList.get(i).getEMDAUTH(), aheadList.get(i).getFlag(), dist.toUpperCase(), aheadList.get(i).getRealFlag());
//
//            }

        } else if (dist.equals("asean")) {//****************ASEAN
            List<EMSINVD> aheadList = new EMSINVDDao().findByInvDrop(inv, dist.toUpperCase());

            new EMSCACDao().delete(inv, dist.toUpperCase());
            new EMSCACDao().addByObj(aheadList, inv, userid, dist.toUpperCase());
//
//            for (int i = 0; i < aheadList.size(); i++) {
//
//                new EMSCACDao().add(inv, Integer.toString(i + 1), aheadList.get(i).getEMDORDID(),
//                        aheadList.get(i).getEMDPONO(), aheadList.get(i).getEMDSTYLE(),
//                        aheadList.get(i).getEMDCOLOR(), aheadList.get(i).getEMDCUP(),
//                        aheadList.get(i).getEMDSIZES(), aheadList.get(i).getPCS(),
//                        aheadList.get(i).getNET(), aheadList.get(i).getGROSS(),
//                        aheadList.get(i).getBOXTYPE(), userid, aheadList.get(i).getEMDAUTH(), aheadList.get(i).getFlag(), dist.toUpperCase(), aheadList.get(i).getRealFlag());
//
//            }

        } else if (dist.equals("china")) {//****************CHINA
            List<EMSINVD> aheadList = new EMSINVDDao().findByInvDrop(inv, dist.toUpperCase());

            new EMSCACDao().delete(inv, dist.toUpperCase());
            new EMSCACDao().addByObj(aheadList, inv, userid, dist.toUpperCase());
//
//            for (int i = 0; i < aheadList.size(); i++) {
//
//                new EMSCACDao().add(inv, Integer.toString(i + 1), aheadList.get(i).getEMDORDID(),
//                        aheadList.get(i).getEMDPONO(), aheadList.get(i).getEMDSTYLE(),
//                        aheadList.get(i).getEMDCOLOR(), aheadList.get(i).getEMDCUP(),
//                        aheadList.get(i).getEMDSIZES(), aheadList.get(i).getPCS(),
//                        aheadList.get(i).getNET(), aheadList.get(i).getGROSS(),
//                        aheadList.get(i).getBOXTYPE(), userid, aheadList.get(i).getEMDAUTH(), aheadList.get(i).getFlag(), dist.toUpperCase(), aheadList.get(i).getRealFlag());
//
//            }

        } else if (dist.equals("cwx-jpn")) {//****************CWX-JPN
            List<EMSINVD> aheadList = new EMSINVDDao().findByInvDrop(inv, dist.toUpperCase());

            new EMSCACDao().delete(inv, dist.toUpperCase());
            new EMSCACDao().addByObj(aheadList, inv, userid, dist.toUpperCase());
//
//            for (int i = 0; i < aheadList.size(); i++) {
//
//                new EMSCACDao().add(inv, Integer.toString(i + 1), aheadList.get(i).getEMDORDID(),
//                        aheadList.get(i).getEMDPONO(), aheadList.get(i).getEMDSTYLE(),
//                        aheadList.get(i).getEMDCOLOR(), aheadList.get(i).getEMDCUP(),
//                        aheadList.get(i).getEMDSIZES(), aheadList.get(i).getPCS(),
//                        aheadList.get(i).getNET(), aheadList.get(i).getGROSS(),
//                        aheadList.get(i).getBOXTYPE(), userid, aheadList.get(i).getEMDAUTH(), aheadList.get(i).getFlag(), dist.toUpperCase(), aheadList.get(i).getRealFlag());
//
//            }

        }

        new EMSINVHDao().updateComplete(inv, dist, "N");
        new EMSINVHDao().updateRun(inv, dist, "N");

        Calendar e = Calendar.getInstance();
        System.out.print("finish : ");
        System.out.println(e.get(Calendar.HOUR) + ":" + e.get(Calendar.MINUTE) + ":" + e.get(Calendar.SECOND));

        response.setHeader("Refresh", "0;/EMS2/EMS100/dropbox3?inv=" + inv + "&dist=" + dist);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

}
