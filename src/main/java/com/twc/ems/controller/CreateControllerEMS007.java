/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.ems.controller;

import com.twc.ems.dao.EMSTEXTDao;
import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author wien
 */
public class CreateControllerEMS007 extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;
    private static final String PAGE_VIEW = "../views/createEMS007.jsp";

    public CreateControllerEMS007() {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("PROGRAMNAME", "EMS007/C");
        String forward = "";
        try {
            String action = request.getParameter("action");
            if (action == null) {
                forward = PAGE_VIEW;
                request.setAttribute("PROGRAMDESC", "Invoice Text. Display");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        RequestDispatcher view = request.getRequestDispatcher(forward);
        view.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");

        String inv = request.getParameter("inv");
        String date = request.getParameter("date");
        String order = request.getParameter("order");
        String pay = request.getParameter("pay");
        String inco = request.getParameter("inco");
        String unit = request.getParameter("unit");
        String shipF = request.getParameter("shipF");
        String shipT = request.getParameter("shipT");
        String delAdd1 = request.getParameter("delAdd1");
        String delAdd2 = request.getParameter("delAdd2");
        String delAdd3 = request.getParameter("delAdd3");
        String delAdd4 = request.getParameter("delAdd4");
        String midcode = request.getParameter("midcode");
        String manufac1 = request.getParameter("manufac1");
        String manufac2 = request.getParameter("manufac2");
        String manufac3 = request.getParameter("manufac3");

        if (date != null) {
            date = date.toUpperCase();
        }

        String userid = request.getParameter("userid");

        request.setAttribute("PROGRAMNAME", "EMS007/C");
        request.setAttribute("PROGRAMDESC", "Invoice Text. Display");

        String sendMessage = "";
        String forward = "";

        if (new EMSTEXTDao().check(inv).equals("f")) {
            sendMessage = "<script type=\"text/javascript\">\n"
                    + "            var show = function () {\n"
                    + "                $('#myModal').modal('show');\n"
                    + "            };\n"
                    + "\n"
                    + "            window.setTimeout(show, 0);\n"
                    + "\n"
                    + "        </script>";
            forward = PAGE_VIEW;
            request.setAttribute("sendMessage", sendMessage);
            RequestDispatcher view = request.getRequestDispatcher(forward);
            view.forward(request, response);
        } else {
            new EMSTEXTDao().add(inv, date, order, pay, inco, unit, shipF, shipT, userid, delAdd1, delAdd2, delAdd3, delAdd4, midcode, manufac1, manufac2, manufac3);

            response.setHeader("Refresh", "0;/EMS2/EMS007/display");
        }

    }
}
