/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.ems.controller;

import com.twc.ems.dao.EMSTEXTDao;
import com.twc.ems.dao.EMSMASDao;
import com.twc.ems.dao.EMSTEXTDao;
import com.twc.ems.entity.EMSTEXT;
import com.twc.ems.entity.EMSMAS;
import com.twc.ems.entity.EMSTEXT;
import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import javax.servlet.http.HttpSession;

/**
 *
 * @author wien
 */
public class SDisplayControllerEMS007 extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;
    private static final String PAGE_VIEW = "../views/sdisplayEMS007.jsp";

    public SDisplayControllerEMS007() {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("PROGRAMNAME", "EMS007/D");
        String forward = "";
        try {
            String action = request.getParameter("action");
            if (action == null) {
                forward = PAGE_VIEW;
                request.setAttribute("PROGRAMDESC", "Invoice Text. Display");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
//    ********
        String id = request.getParameter("id");

        EMSTEXT p = new EMSTEXTDao().findByCode(id);

        request.setAttribute("inv", p.getEMTXINVNO());
        request.setAttribute("date", p.getEMTXDATE());
        request.setAttribute("order", p.getEMTXORDOF());
        request.setAttribute("pay", p.getEMTXPMT());
        request.setAttribute("inco", p.getEMTXTERM());
        request.setAttribute("unit", p.getEMTXSUNIT());
        request.setAttribute("shipF", p.getEMTXSHFROM());
        request.setAttribute("shipT", p.getEMTXSHTO());
        request.setAttribute("delAdd1", p.getEMTXDELADD1());
        request.setAttribute("delAdd2", p.getEMTXDELADD2());
        request.setAttribute("delAdd3", p.getEMTXDELADD3());
        request.setAttribute("delAdd4", p.getEMTXDELADD4());
        request.setAttribute("midcode", p.getEMTXMIDCODE());
        request.setAttribute("manufac1", p.getEMTXMANUFAC1());
        request.setAttribute("manufac2", p.getEMTXMANUFAC2());
        request.setAttribute("manufac3", p.getEMTXMANUFAC3());

        RequestDispatcher view = request.getRequestDispatcher(forward);
        view.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
