/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.ems.controller;

import com.twc.ems.dao.EMSCACDao;
import com.twc.ems.dao.EMSINVHDao;
import com.twc.ems.dao.EMSMASDao;
import com.twc.ems.entity.EMSCAC;
import com.twc.ems.entity.EMSINVH;
import java.io.IOException;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author wien
 */
public class TrashControllerEMS100 extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;
    private static final String PAGE_VIEW = "../views/trashEMS100.jsp";

    public TrashControllerEMS100() {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("PROGRAMNAME", "EMS100/DIST");
        String forward = "";
        try {
            String action = request.getParameter("action");
            if (action == null) {
                forward = PAGE_VIEW;
                request.setAttribute("PROGRAMDESC", "Invoice Information. Display");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
//    ********

        String inv = request.getParameter("inv");
        String dist = request.getParameter("dist");
        dist = dist.toLowerCase();

        String fail = request.getParameter("fail");

        request.setAttribute("inv", inv);
        request.setAttribute("failMsg", fail);

        EMSINVH p = new EMSINVHDao().findByCode(inv, "0");

        request.setAttribute("EMHINVID", p.getEMHINVID());
        request.setAttribute("EMHCUSID", p.getEMHCUSID());
        request.setAttribute("EMHCUSSHIPID", p.getEMHCUSSHIPID());
        request.setAttribute("EMHSHIPID", p.getEMHSHIPID());
        request.setAttribute("EMHTRANSPID", p.getEMHTRANSPID());
        request.setAttribute("EMHCURRENCY", p.getEMHCURRENCY());
        request.setAttribute("EMHUNIT", p.getEMHUNIT());
        request.setAttribute("EMHSHIPDATE", p.getEMHSHIPDATE());
        request.setAttribute("EMHAMTCTN", p.getEMHAMTCTN());

        List<EMSCAC> aheadList = new EMSCACDao().findByInvDropTrash(inv, dist.toUpperCase());

        if (!aheadList.isEmpty()) {
            request.setAttribute("auth", aheadList.get(0).getEMCAUTH() == null ? "" : aheadList.get(0).getEMCAUTH());
        }
        String thead = "";
        String tfoot = "";

        List<String> sizeList = new EMSCACDao().findSizeList(inv, dist.toUpperCase());
        for (int i = 0; i < sizeList.size(); i++) {
            thead += "<th style=\"color:blue;\">" + sizeList.get(i) + "</th>\n";
            tfoot += "<th></th>\n";
        }

        request.setAttribute("thead", thead);
        request.setAttribute("tfoot", tfoot);

        for (int i = 0; i < aheadList.size(); i++) {
            String tbody = "";
            String tEditSize = "";

            List<String> sizeList2 = new EMSCACDao().findSizeList(inv, dist.toUpperCase());
            for (int j = 0; j < sizeList2.size(); j++) {
                String body = "<td></td>\n";
                String EditSize = "";

                String[] size = aheadList.get(i).getEMCSIZELIST().split(";");
                for (int k = 0; k < size.length; k++) {
                    String si = size[k].split(":")[0];
                    if (si.equalsIgnoreCase(sizeList2.get(j).trim())) {
                        body = "<td title=\"Size : " + sizeList2.get(j).trim() + "\" style=\"text-align: right;\">" + size[k].split(":")[1] + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>\n";
                        EditSize = "<tr style=\"background-color: white;\">\n"
                                + "<td align=\"right\">\n"
                                + "    <font size=\"4\">Size " + sizeList2.get(j).trim() + " : </font>\n"
                                + "</td>\n"
                                + "<td>\n"
                                + "    <input type=\"text\" id=\"" + sizeList2.get(j).trim() + "-edit-size-" + aheadList.get(i).getEMCCTNO() + "\" name=\"edit-size-" + aheadList.get(i).getEMCCTNO() + "\" maxlength=\"4\" value=\"" + size[k].split(":")[1] + "\" onchange=\"sumSize('" + aheadList.get(i).getEMCCTNO() + "','" + aheadList.get(i).getBOXPACK() + "');\" style=\"width: 190px;\">\n"
                                + "</td>\n"
                                + "</tr>\n";
                    }
                }

                tbody += body;
                tEditSize += EditSize;
            }

            aheadList.get(i).setTbody(tbody);
            aheadList.get(i).settEditSize(tEditSize);

        }
        request.setAttribute("aheadList", aheadList);

        if (dist.equalsIgnoreCase("europe")) {//****************EUROPE
            request.setAttribute("distText", dist);

        } else if (dist.equalsIgnoreCase("america")) {//****************AMERICA
            request.setAttribute("distText", dist);
//            request.setAttribute("disnone", "display: none;");

        } else if (dist.equals("brazil")) {//****************BRAZIL
            request.setAttribute("distText", dist);
//            request.setAttribute("disnone", "display: none;");

        } else if (dist.equalsIgnoreCase("usa-cwx")) {//****************USA-CWX
            request.setAttribute("distText", "america");
            request.setAttribute("cwx", "<img src=\"../resources/images/cwx.png\" >");
//            request.setAttribute("disnone", "display: none;");

        } else if (dist.equalsIgnoreCase("asean")) {//****************ASEAN
            request.setAttribute("distText", dist);
//            request.setAttribute("disnone", "display: none;");

        } else if (dist.equalsIgnoreCase("china")) {//****************CHINA
            request.setAttribute("distText", dist);
//            request.setAttribute("disnone", "display: none;");

        } else if (dist.equalsIgnoreCase("cwx-jpn")) {//****************CWX-JPN
            request.setAttribute("distText", "japan");
            request.setAttribute("cwx", "<img src=\"../resources/images/cwx.png\" >");
//            request.setAttribute("disnone", "display: none;");

        }

        request.setAttribute("dist", dist != null ? dist.toUpperCase() : "");

        RequestDispatcher view = request.getRequestDispatcher(forward);
        view.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

}
