/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.ems.controller;

import com.twc.ems.dao.EMSMASDao;
import java.io.IOException;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author wien
 */
public class CopyControllerEMS001 extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;

    public CopyControllerEMS001() {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");

        String zoneOld = request.getParameter("zoneOld");
        String styleOld = request.getParameter("styleOld");
        String cupOld = request.getParameter("cupOld");
        String sizeOld = request.getParameter("sizeOld");

        String uid = request.getParameter("uid");
        if (uid == null) {
            uid = "";
        }

        String zoneNew = request.getParameter("zoneNew");
        String styleNew = request.getParameter("styleNew");
        String cupNew = request.getParameter("cupNew");
        String sizeNew = request.getParameter("sizeNew");

        new EMSMASDao().copy(zoneOld, styleOld, cupOld, sizeOld, zoneNew, styleNew, cupNew, sizeNew, uid);

        response.setHeader("Refresh", "0;/EMS2/EMS001/edit?style=" + styleNew + "&cup=" + cupNew + "&size=" + sizeNew + "&zone=" + zoneNew);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

}
