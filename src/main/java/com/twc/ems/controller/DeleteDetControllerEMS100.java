/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.ems.controller;

import com.twc.ems.dao.EMSINVDDao;
import com.twc.ems.dao.EMSINVHDao;
import com.twc.ems.dao.EMSMASDao;
import com.twc.ems.dao.EMSINVHDao;
import com.twc.ems.dao.EMSINVDDao;
import com.twc.ems.dao.EMSUNITDao;
import com.twc.ems.entity.EMSINVD;
import com.twc.ems.entity.EMSINVH;
import com.twc.ems.entity.EMSMAS;
import com.twc.ems.entity.EMSINVH;
import com.twc.ems.entity.EMSUNIT;
import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import javax.servlet.http.HttpSession;

/**
 *
 * @author wien
 */
public class DeleteDetControllerEMS100 extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;

    public DeleteDetControllerEMS100() {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");

        String inv = request.getParameter("inv");
        String po = request.getParameter("po");
        String size = request.getParameter("size");

        String lotD = request.getParameter("lotD");
        String authD = request.getParameter("authD");
        String brandD = request.getParameter("brandD");
        String typeD = request.getParameter("typeD");
        String styleD = request.getParameter("styleD");
        String colorD = request.getParameter("colorD");
        String codeD = request.getParameter("codeD");
        String cupD = request.getParameter("cupD");

        String nontwc = request.getParameter("nontwc");

        new EMSINVDDao().deleteDet(nontwc, inv, po, size, lotD, authD, brandD, typeD, styleD, colorD, codeD, cupD);

        response.setHeader("Refresh", "0;/EMS2/EMS100/edit?id=" + inv + "&nontwc=" + nontwc);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
