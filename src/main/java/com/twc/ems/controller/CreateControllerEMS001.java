/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.ems.controller;

import com.twc.ems.dao.EMSMASDao;
import com.twc.ems.dao.EMSUNITDao;
import com.twc.ems.entity.EMSUNIT;
import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import javax.servlet.http.HttpSession;

/**
 *
 * @author wien
 */
public class CreateControllerEMS001 extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;
    private static final String PAGE_VIEW = "../views/createEMS001.jsp";

    public CreateControllerEMS001() {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("PROGRAMNAME", "EMS001/C");
        String forward = "";
        try {
            String action = request.getParameter("action");
            if (action == null) {
                forward = PAGE_VIEW;
                request.setAttribute("PROGRAMDESC", "Master Detail. Display");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        EMSUNITDao dao = new EMSUNITDao();
        List<EMSUNIT> unitList = dao.findAll();
        request.setAttribute("unitList", unitList);

        String zone = request.getParameter("zone");
        String style = request.getParameter("style");
        String cup = request.getParameter("cup");
        String size = request.getParameter("size");

        request.setAttribute("zone", zone);
        request.setAttribute("style", style);
        request.setAttribute("cup", cup);
        request.setAttribute("size", size);

        RequestDispatcher view = request.getRequestDispatcher(forward);
        view.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");

        String zone = request.getParameter("zone");
        String style = request.getParameter("style");
        String cup = request.getParameter("cup");
        String size = request.getParameter("size");
        String type = request.getParameter("type");

        String weight = request.getParameter("weight");
        String price = request.getParameter("price");
        String unit = request.getParameter("unit");
        String hs = request.getParameter("hs");
        String ran = request.getParameter("ran");

        if (weight.trim().equals("")) {
            weight = "0";
        }
        if (price.trim().equals("")) {
            price = "0";
        }

        String comp1 = request.getParameter("comp1");
        String comp2 = request.getParameter("comp2");
        String comp3 = request.getParameter("comp3");

        String box6e = request.getParameter("box6e");
        String box6d = request.getParameter("box6d");
        String box4e = request.getParameter("box4e");
        String box4d = request.getParameter("box4d");
        String box2e = request.getParameter("box2e");
        String ppack = request.getParameter("ppack");

        String wei6e = request.getParameter("wei6e");
        String wei6d = request.getParameter("wei6d");
        String wei4e = request.getParameter("wei4e");
        String wei4d = request.getParameter("wei4d");
        String wei2e = request.getParameter("wei2e");

        if (box6e.trim().equals("")) {
            box6e = "0";
        }
        if (box6d.trim().equals("")) {
            box6d = "0";
        }
        if (box4e.trim().equals("")) {
            box4e = "0";
        }
        if (box4d.trim().equals("")) {
            box4d = "0";
        }
        if (box2e.trim().equals("")) {
            box2e = "0";
        }
        if (ppack.trim().equals("")) {
            ppack = "0";
        }

        if (wei6e.trim().equals("")) {
            wei6e = "0";
        }
        if (wei6d.trim().equals("")) {
            wei6d = "0";
        }
        if (wei4e.trim().equals("")) {
            wei4e = "0";
        }
        if (wei4d.trim().equals("")) {
            wei4d = "0";
        }
        if (wei2e.trim().equals("")) {
            wei2e = "0";
        }

        String userid = request.getParameter("userid");

        request.setAttribute("PROGRAMNAME", "EMS001/C");
        request.setAttribute("PROGRAMDESC", "Master Detail. Display");

        String sendMessage = "";
        String forward = "";

        if (new EMSMASDao().check(style, cup, size, zone).equals("f")) {
            sendMessage = "<script type=\"text/javascript\">\n"
                    + "            var show = function () {\n"
                    + "                $('#myModal').modal('show');\n"
                    + "            };\n"
                    + "\n"
                    + "            window.setTimeout(show, 0);\n"
                    + "\n"
                    + "        </script>";
            forward = PAGE_VIEW;
            request.setAttribute("sendMessage", sendMessage);

            EMSUNITDao dao = new EMSUNITDao();
            List<EMSUNIT> unitList = dao.findAll();
            request.setAttribute("unitList", unitList);

            RequestDispatcher view = request.getRequestDispatcher(forward);
            view.forward(request, response);
        } else {
            new EMSMASDao().add(zone, style, cup, size, type, weight, price, unit, hs, ran, comp1, comp2, comp3, box6e, box6d, box4e, box4d, box2e, ppack, wei6e, wei6d, wei4e, wei4d, wei2e, userid);

            response.setHeader("Refresh", "0;/EMS2/EMS001/display?search=true");
        }

    }
}
