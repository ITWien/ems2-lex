/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.ems.controller;

import com.twc.ems.dao.EMSCACDao;
import com.twc.ems.dao.EMSINVDDao;
import com.twc.ems.dao.EMSINVHDao;
import com.twc.ems.dao.EMSMASDao;
import com.twc.ems.dao.EMSINVHDao;
import com.twc.ems.dao.EMSINVDDao;
import com.twc.ems.dao.EMSUNITDao;
import com.twc.ems.entity.EMSCAC;
import com.twc.ems.entity.EMSINVD;
import com.twc.ems.entity.EMSINVH;
import com.twc.ems.entity.EMSMAS;
import com.twc.ems.entity.EMSINVH;
import com.twc.ems.entity.EMSUNIT;
import java.io.IOException;
import java.util.ArrayList;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import javax.servlet.http.HttpSession;

/**
 *
 * @author wien
 */
public class EditCacControllerEMS100 extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;

    public EditCacControllerEMS100() {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");

        String inv = request.getParameter("inv");
        String ctno = request.getParameter("ctno");
        String dist = request.getParameter("dist");

        String po = request.getParameter("po");
        String gw = request.getParameter("gw");
        String pcs = request.getParameter("pcs");
        String boxtype = request.getParameter("boxtype");
        String sizeList = request.getParameter("sizeList");
        String ogsizeList = request.getParameter("ogsizeList");
        String colorList = request.getParameter("colorList");
        String newCupList2 = request.getParameter("cupList");

        EMSCAC p = new EMSCACDao().findCac(inv, ctno, dist.toUpperCase());

//        String newAddCup = "";
//        String sizeListTmp = "";
//
//        if (sizeList != null) {
//            if (sizeList.contains(",")) {
//                String[] sizeOT = sizeList.split(";");
//                for (int i = 0; i < sizeOT.length; i++) {
//                    if (sizeOT[i].contains(",")) {
//                        newAddCup = sizeOT[i].split(",")[0];
//                        sizeListTmp += sizeOT[i].split(",")[1] + ";";
//                    } else {
//                        sizeListTmp += sizeOT[i] + ";";
//                    }
//                }
//                sizeList = sizeListTmp;
//            }
//        }
        String isHaveBoxW = new EMSCACDao().checkBoxW(inv, dist, ctno, boxtype);

        if (new EMSCACDao().editCac(inv, ctno, p.getEMCSTYLE(), p.getEMCCUP(), p.getEMCSIZES(), dist, sizeList, pcs, po, gw, boxtype, isHaveBoxW, colorList)) {

//            String cupList = new EMSCACDao().findCupList(inv, ctno);
//            String cup = new EMSCACDao().findCup(inv, ctno);
//            if (newAddCup.trim().equals("")) {
//                newAddCup = cup;
//            }
//            String[] cups = cupList.split(";");
//
//            String newCupList = "";
//
//            String[] sizes = sizeList.split(";");
//            for (int i = 0; i < sizes.length; i++) {
//                String si = sizes[i].split(":")[0];
//                String pc = sizes[i].split(":")[1];
////                System.out.println(si + " : " + pc);
//
//                int cnt = 0;
//                List<EMSCAC> cl = new ArrayList<EMSCAC>();
//                for (int j = 0; j < cups.length; j++) {
//                    String cu2 = cups[j].split(":")[0];
//                    String si2 = cups[j].split(":")[1].split("%")[1];
//                    String pc2 = cups[j].split(":")[1].split("%")[0];
//                    if (si.equals(si2)) {
//                        EMSCAC cac = new EMSCAC();
//                        cac.setEMCCUP(cu2);
//                        cac.setEMCSIZES(si2);
//                        cac.setEMCPCS(pc2);
//                        cl.add(cac);
//                        cnt++;
//                    }
//                }
//
//                String newAdd = "";
//                if (cnt == 0) {
//                    newAdd += newAddCup + ":" + pc + "%" + si + ";";
//
//                } else if (cnt == 1) {
//                    for (int j = 0; j < cl.size(); j++) {
//                        cl.get(j).setEMCPCS(pc);
//                    }
//                } else {
//                    int pcInt = Integer.parseInt(pc);
//                    int cntPc = 0;
//                    for (int j = 0; j < cl.size(); j++) {
//                        cntPc += Integer.parseInt(cl.get(j).getEMCPCS());
//                    }
//
//                    if (pcInt > cntPc) {
//                        int diffVal = pcInt - cntPc;
//                        int oldVal = Integer.parseInt(cl.get(cl.size() - 1).getEMCPCS());
//                        cl.get(cl.size() - 1).setEMCPCS(Integer.toString(oldVal + diffVal));
//
//                    } else if (pcInt < cntPc) {
//                        int diffVal = pcInt - cntPc;
//                        int oldVal = Integer.parseInt(cl.get(cl.size() - 1).getEMCPCS());
//                        cl.get(cl.size() - 1).setEMCPCS(Integer.toString(oldVal + diffVal));
//
//                        int lastDiff = (oldVal + diffVal);
//                        if (lastDiff <= 0) {
//                            int oldVal2 = Integer.parseInt(cl.get(cl.size() - 2).getEMCPCS());
//                            cl.get(cl.size() - 2).setEMCPCS(Integer.toString(oldVal2 + lastDiff));
//                        }
//                    }
//                }
//
//                for (int j = 0; j < cl.size(); j++) {
//                    if (Integer.parseInt(cl.get(j).getEMCPCS()) > 0) {
//                        newCupList += cl.get(j).getEMCCUP() + ":" + cl.get(j).getEMCPCS() + "%" + cl.get(j).getEMCSIZES() + ";";
//                    }
//                }
//                newCupList += newAdd;
//            }
            newCupList2 = newCupList2.replace("<per>", "%");
            new EMSCACDao().editCupListCac(inv, ctno, newCupList2);

//            edit det
//            for (int i = 0; i < sizeList.split(";").length; i++) {
//                new EMSINVDDao().editDet4(p.getEMCINVID(), p.getEMCPONO(), p.getEMCORDID(),
//                        p.getEMCAUTH(), p.getEMCSTYLE(), p.getEMCCOLOR(), p.getEMCCUP(),
//                        sizeList.split(";")[i].split(":")[0],
//                        sizeList.split(";")[i].split(":")[1] + "-" + ogsizeList.split(";")[i].split(":")[1]);
//            }
            updateNetGrossCac(inv, dist);

            List<EMSCAC> cacList = new EMSCACDao().getCacGebGrp(inv, dist);
            int grpNo = 1;
            String styleGrp = "first";
            for (int i = 0; i < cacList.size(); i++) {
                if (!styleGrp.equals("first") && !styleGrp.equals(cacList.get(i).getEMCSTYLE())) {
                    grpNo++;
                }
                new EMSCACDao().updateGrpNo(inv, dist, cacList.get(i).getEMCCARTONNO(), Integer.toString(grpNo));
                styleGrp = cacList.get(i).getEMCSTYLE();
            }
        } else {
            System.out.println("UPDATE CAC FAILED");
        }

        response.setHeader("Refresh", "0;/EMS2/EMS100/dropbox3?inv=" + inv + "&dist=" + dist);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    public void updateNetGrossCac(String inv, String dist) {
        List<EMSCAC> cacList = new EMSCACDao().getCacList(inv, dist.toUpperCase());

        for (int i = 0; i < cacList.size(); i++) {

            String dataZone = cacList.get(i).getEMCZONE();
            String dataInv = cacList.get(i).getEMCINVID();
            String dataCtno = cacList.get(i).getEMCCTNO();
            String dataStyle = cacList.get(i).getEMCSTYLE();
            String dataBoxType = cacList.get(i).getEMCBOXTYPE();
            String dataGross = new EMSCACDao().getNewGrossCac(dataZone, dataBoxType);
            String realNet = "0";
            String realGross = "0";

            if (cacList.get(i).getEMCCUPLIST() != null) {
                String[] cupCac = cacList.get(i).getEMCCUPLIST().split(";");
                Double sumNet = 0.00;
                for (int j = 0; j < cupCac.length; j++) {

                    String dataCup = cupCac[j].split(":")[0];
                    String dataSize = cupCac[j].split("%")[1];
                    String dataPcs = cupCac[j].split("%")[0].split(":")[1];

                    String dataNet = new EMSCACDao().getNewNetCac(dataZone, dataStyle, dataCup, dataSize, dataPcs);

                    sumNet += Double.parseDouble(dataNet);

                }
                realNet = Double.toString(sumNet);
                realGross = Double.toString(sumNet + Double.parseDouble(dataGross));

            }

            new EMSCACDao().updateNetGrossCac(dataInv, dataCtno, realNet, realGross, dataZone);

        }
    }
}
