/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.ems.controller;

import com.twc.ems.dao.EMSCACDao;
import com.twc.ems.dao.EMSINVDDao;
import com.twc.ems.dao.EMSINVHDao;
import com.twc.ems.dao.EMSMASDao;
import com.twc.ems.dao.EMSINVHDao;
import com.twc.ems.dao.EMSINVDDao;
import com.twc.ems.dao.EMSUNITDao;
import com.twc.ems.entity.EMSCAC;
import com.twc.ems.entity.EMSINVD;
import com.twc.ems.entity.EMSINVH;
import com.twc.ems.entity.EMSMAS;
import com.twc.ems.entity.EMSINVH;
import com.twc.ems.entity.EMSUNIT;
import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import javax.servlet.http.HttpSession;

/**
 *
 * @author wien
 */
public class AddCacControllerEMS100 extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;

    public AddCacControllerEMS100() {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");

        String inv = request.getParameter("add-inv");
        String auth = request.getParameter("add-auth");
        String po = request.getParameter("add-po");
        String order = request.getParameter("add-order");
        String pcs = request.getParameter("add-pcs");
        String boxtype = request.getParameter("add-boxtype");
        String style = request.getParameter("add-style");
        String color = request.getParameter("add-color");
        String cup = request.getParameter("add-cup");
        String size = request.getParameter("add-size");
        String dist = request.getParameter("add-dist");
        String carton = request.getParameter("add-carton");

        if (pcs.trim().equals("")) {
            pcs = "0";
        }

//        String net = request.getParameter("add-net");
//        String gross = request.getParameter("add-gross");
        String userid = request.getParameter("userid2");

        String p = new EMSMASDao().check(style, cup, size, dist);

        String fail = "";

        if (p.equals("f")) {
            new EMSCACDao().clearCartonCac(inv, carton, dist);
            if (new EMSCACDao().addCac(inv, order, po, style, color, cup, size, pcs, boxtype, userid, auth, dist, carton)) {
                if (new EMSINVDDao().checkDet(inv, po, size, order, auth, style, color, cup).equals("f")) {
                    new EMSINVDDao().editDet2(inv, po, size, order, auth, style, color, cup, pcs);
                } else {

                }
                new EMSINVHDao().editCarAmt(inv, dist);
            }
        } else {
            fail = "&fail=ShowFailMsg()";
        }

        List<EMSCAC> cacList = new EMSCACDao().getCacGebGrp(inv, dist);
        int grpNo = 1;
        String styleGrp = "first";
        for (int i = 0; i < cacList.size(); i++) {
            if (!styleGrp.equals("first") && !styleGrp.equals(cacList.get(i).getEMCSTYLE())) {
                grpNo++;
            }
            new EMSCACDao().updateGrpNo(inv, dist, cacList.get(i).getEMCCARTONNO(), Integer.toString(grpNo));
            styleGrp = cacList.get(i).getEMCSTYLE();
        }

        response.setHeader("Refresh", "0;/EMS2/EMS100/dropbox3?inv=" + inv + "&dist=" + dist + fail);
    }
}
