/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.ems.controller;

import com.twc.ems.dao.EMSCUADao;
import com.twc.ems.dao.EMSUNITDao;
import com.twc.ems.entity.EMSUNIT;
import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import javax.servlet.http.HttpSession;

/**
 *
 * @author wien
 */
public class CreateControllerEMS002 extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;
    private static final String PAGE_VIEW = "../views/createEMS002.jsp";

    public CreateControllerEMS002() {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("PROGRAMNAME", "EMS002/C");
        String forward = "";
        try {
            String action = request.getParameter("action");
            if (action == null) {
                forward = PAGE_VIEW;
                request.setAttribute("PROGRAMDESC", "Customer Address. Display");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        RequestDispatcher view = request.getRequestDispatcher(forward);
        view.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");

        String id = request.getParameter("id");

        String name = request.getParameter("name");

        String adr1 = request.getParameter("adr1");
        String adr2 = request.getParameter("adr2");
        String adr3 = request.getParameter("adr3");
        String adr4 = request.getParameter("adr4");

        String userid = request.getParameter("userid");

        request.setAttribute("PROGRAMNAME", "EMS002/C");
        request.setAttribute("PROGRAMDESC", "Customer Address. Display");

        String sendMessage = "";
        String forward = "";

        if (new EMSCUADao().check(id).equals("f")) {
            sendMessage = "<script type=\"text/javascript\">\n"
                    + "            var show = function () {\n"
                    + "                $('#myModal').modal('show');\n"
                    + "            };\n"
                    + "\n"
                    + "            window.setTimeout(show, 0);\n"
                    + "\n"
                    + "        </script>";
            forward = PAGE_VIEW;
            request.setAttribute("sendMessage", sendMessage);
            RequestDispatcher view = request.getRequestDispatcher(forward);
            view.forward(request, response);
        } else {
            new EMSCUADao().add(id, name, adr1, adr2, adr3, adr4, userid);

            response.setHeader("Refresh", "0;/EMS2/EMS002/display");
        }

    }
}
