/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.ems.controller;

import com.twc.ems.dao.EMSUNITDao;
import com.twc.ems.dao.EMSMASDao;
import com.twc.ems.dao.EMSUNITDao;
import com.twc.ems.entity.EMSUNIT;
import com.twc.ems.entity.EMSMAS;
import com.twc.ems.entity.EMSUNIT;
import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import javax.servlet.http.HttpSession;

/**
 *
 * @author wien
 */
public class EditControllerEMS005 extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;
    private static final String PAGE_VIEW = "../views/editEMS005.jsp";

    public EditControllerEMS005() {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("PROGRAMNAME", "EMS005/E");
        String forward = "";
        try {
            String action = request.getParameter("action");
            if (action == null) {
                forward = PAGE_VIEW;
                request.setAttribute("PROGRAMDESC", "Unit of Measurement. Display");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
//    ********
        String id = request.getParameter("id");

        EMSUNIT p = new EMSUNITDao().findByCode(id);

        request.setAttribute("id", p.getEMUMID());

        request.setAttribute("desc", p.getEMUMDESC());

        RequestDispatcher view = request.getRequestDispatcher(forward);
        view.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");

        String id = request.getParameter("id");

        String desc = request.getParameter("desc");

        String userid = request.getParameter("userid");

        request.setAttribute("PROGRAMNAME", "EMS005/E");
        request.setAttribute("PROGRAMDESC", "Unit of Measurement. Display");

        new EMSUNITDao().edit(id, desc, userid);

        response.setHeader("Refresh", "0;/EMS2/EMS005/display");
    }
}
