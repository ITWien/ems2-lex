/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.ems.controller;

import com.twc.ems.dao.EMSINVHDao;
import com.twc.ems.dao.EMSTEXTDao;
import com.twc.ems.entity.EMSTEXT;
import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author wien
 */
public class NonTWCControllerEMS100 extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;

    public NonTWCControllerEMS100() {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");

        String inv = request.getParameter("inv");
        String total = request.getParameter("total");
        String net = request.getParameter("net");
        String gross = request.getParameter("gross");
        String m = request.getParameter("m");
        String di1 = request.getParameter("di1");
        String di2 = request.getParameter("di2");
        String di3 = request.getParameter("di3");

        total = IfBlank(total, "0");
        net = IfBlank(net, "0");
        gross = IfBlank(gross, "0");
        m = IfBlank(m, "0");
        di1 = IfBlank(di1, "0");
        di2 = IfBlank(di2, "0");
        di3 = IfBlank(di3, "0");

        new EMSINVHDao().EditNonTWC(inv.replace(",", ""), total.replace(",", ""), net.replace(",", ""), gross.replace(",", ""), m.replace(",", ""), di1.replace(",", ""), di2.replace(",", ""), di3.replace(",", ""));

        response.setHeader("Refresh", "0;/EMS3/EMS601/print?inv=" + inv + "&nontwc=1");

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    public String IfBlank(String inp, String res) {
        if (inp.trim().equals("")) {
            return res;
        } else {
            return inp;
        }
    }
}
