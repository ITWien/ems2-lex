/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.ems.controller;

import com.twc.ems.dao.EMSINVDDao;
import com.twc.ems.dao.EMSINVHDao;
import com.twc.ems.dao.EMSMASDao;
import com.twc.ems.dao.EMSINVHDao;
import com.twc.ems.dao.EMSINVDDao;
import com.twc.ems.dao.EMSUNITDao;
import com.twc.ems.entity.EMSINVD;
import com.twc.ems.entity.EMSINVH;
import com.twc.ems.entity.EMSMAS;
import com.twc.ems.entity.EMSINVH;
import com.twc.ems.entity.EMSUNIT;
import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import javax.servlet.http.HttpSession;

/**
 *
 * @author wien
 */
public class AddDetControllerEMS100 extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;
    private static final String PAGE_VIEW = "../views/editEMS100.jsp";

    public AddDetControllerEMS100() {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");

        String inv = request.getParameter("inv");
        String po = request.getParameter("add-po");
        String lot = request.getParameter("add-lot");
        String auth = request.getParameter("add-auth");
        String brand = request.getParameter("add-brand");
        String type = request.getParameter("add-type");
        String style = request.getParameter("add-style");
        String color = request.getParameter("add-color");
        String code = request.getParameter("add-code");
        String cup = request.getParameter("add-cup");
        String size = request.getParameter("add-size");
        String cost = request.getParameter("add-cost");
        String amt = request.getParameter("add-amt");

        String nontwc = request.getParameter("nontwc");

        if (amt.trim().equals("")) {
            amt = "0";
        }

        if (cost.trim().equals("")) {
            cost = "0";
        }

        String userid = request.getParameter("userid2");

        String check = new EMSMASDao().check(style, cup, size, inv, nontwc);
        if (check.equals("f")) {
            new EMSINVDDao().addDet(nontwc, inv, po, lot, auth, brand, type, style, color, code, cup, size, cost, amt, userid);
            response.setHeader("Refresh", "0;/EMS2/EMS100/edit?id=" + inv + "&nontwc=" + nontwc);
        } else {
            String zone = new EMSMASDao().checkGetZone(inv, nontwc);
            response.setHeader("Refresh", "0;/EMS2/EMS100/edit?id=" + inv + "&nontwc=" + nontwc + "&noMas=true&zone=" + zone + "&style=" + style + "&cup=" + cup + "&size=" + size);
        }

    }
}
