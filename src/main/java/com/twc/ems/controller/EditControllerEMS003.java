/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.ems.controller;

import com.twc.ems.dao.EMSCUMDao;
import com.twc.ems.dao.EMSMASDao;
import com.twc.ems.dao.EMSUNITDao;
import com.twc.ems.entity.EMSCUM;
import com.twc.ems.entity.EMSMAS;
import com.twc.ems.entity.EMSUNIT;
import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import javax.servlet.http.HttpSession;

/**
 *
 * @author wien
 */
public class EditControllerEMS003 extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;
    private static final String PAGE_VIEW = "../views/editEMS003.jsp";

    public EditControllerEMS003() {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("PROGRAMNAME", "EMS003/E");
        String forward = "";
        try {
            String action = request.getParameter("action");
            if (action == null) {
                forward = PAGE_VIEW;
                request.setAttribute("PROGRAMDESC", "Customer Mark. Display");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
//    ********
        String id = request.getParameter("id");

        EMSCUM p = new EMSCUMDao().findByCode(id);

        request.setAttribute("id", p.getEMCMSHIPID());

        request.setAttribute("desc1", p.getEMCMDESC1());
        request.setAttribute("desc2", p.getEMCMDESC2());
        request.setAttribute("desc3", p.getEMCMDESC3());
        request.setAttribute("desc4", p.getEMCMDESC4());
        request.setAttribute("desc5", p.getEMCMDESC5());

        RequestDispatcher view = request.getRequestDispatcher(forward);
        view.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");

        String id = request.getParameter("id");

        String desc1 = request.getParameter("desc1");
        String desc2 = request.getParameter("desc2");
        String desc3 = request.getParameter("desc3");
        String desc4 = request.getParameter("desc4");
        String desc5 = request.getParameter("desc5");

        String userid = request.getParameter("userid");

        request.setAttribute("PROGRAMNAME", "EMS003/E");
        request.setAttribute("PROGRAMDESC", "Customer Mark. Display");

        new EMSCUMDao().edit(id, desc1, desc2, desc3, desc4, desc5, userid);

        response.setHeader("Refresh", "0;/EMS2/EMS003/display");
    }
}
