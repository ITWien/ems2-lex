/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.ems.controller;

import com.twc.ems.dao.EMSCODEDao;
import com.twc.ems.dao.EMSMASDao;
import com.twc.ems.dao.EMSCODEDao;
import com.twc.ems.entity.EMSCODE;
import com.twc.ems.entity.EMSMAS;
import com.twc.ems.entity.EMSCODE;
import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import javax.servlet.http.HttpSession;

/**
 *
 * @author wien
 */
public class SDisplayControllerEMS008 extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;
    private static final String PAGE_VIEW = "../views/sdisplayEMS008.jsp";

    public SDisplayControllerEMS008() {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("PROGRAMNAME", "EMS008/D");
        String forward = "";
        try {
            String action = request.getParameter("action");
            if (action == null) {
                forward = PAGE_VIEW;
                request.setAttribute("PROGRAMDESC", "Color Code. Display");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
//    ********
        String zone = request.getParameter("zone");
        String id = request.getParameter("id");

        EMSCODE p = new EMSCODEDao().findByCode(id, zone);

        request.setAttribute("zone", p.getEMCDZONE());
        request.setAttribute("id", p.getEMCDCODE());
        request.setAttribute("desc", p.getEMCDDESC());

        RequestDispatcher view = request.getRequestDispatcher(forward);
        view.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
