/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.ems.controller;

import com.twc.ems.dao.EMSCACDao;
import com.twc.ems.dao.EMSINVDDao;
import com.twc.ems.dao.EMSINVHDao;
import com.twc.ems.dao.EMSMASDao;
import com.twc.ems.dao.EMSINVHDao;
import com.twc.ems.dao.EMSINVDDao;
import com.twc.ems.dao.EMSUNITDao;
import com.twc.ems.entity.EMSCAC;
import com.twc.ems.entity.EMSINVD;
import com.twc.ems.entity.EMSINVH;
import com.twc.ems.entity.EMSMAS;
import com.twc.ems.entity.EMSINVH;
import com.twc.ems.entity.EMSUNIT;
import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import javax.servlet.http.HttpSession;

/**
 *
 * @author wien
 */
public class RestoreCacControllerEMS100 extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;

    public RestoreCacControllerEMS100() {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");

        String inv = request.getParameter("inv");
        String ctno = request.getParameter("ctno");
        String dist = request.getParameter("dist");
        String flag = request.getParameter("flag");

        EMSCAC p = new EMSCACDao().findCac(inv, ctno, dist.toUpperCase());
        new EMSINVDDao().editDet3re(p);

        new EMSCACDao().restoreCac(inv, ctno, flag, dist.toUpperCase());

        new EMSINVHDao().editCarAmt(inv, dist);

        response.setHeader("Refresh", "0;/EMS2/EMS100/trash?inv=" + inv + "&dist=" + dist);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
