/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.ems.controller;

import com.twc.ems.dao.EMSDMSDao;
import com.twc.ems.entity.EMSDMS;
import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author wien
 */
public class CopyControllerEMS006 extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;

    public CopyControllerEMS006() {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");

        String ozone = request.getParameter("ozone");
        String nzone = request.getParameter("nzone");
        String obox = request.getParameter("obox");
        String nbox = request.getParameter("nbox");

        String userid = request.getParameter("userid");

        if (new EMSDMSDao().copy(ozone, nzone, obox, nbox, userid)) {
            response.setHeader("Refresh", "0;/EMS2/EMS006/edit?zone=" + nzone + "&box=" + nbox);
        } else {
            response.setHeader("Refresh", "0;/EMS2/EMS006/display");
        }

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
