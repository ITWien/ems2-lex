/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.ems.controller;

import com.twc.ems.dao.EMSINVDDao;
import com.twc.ems.dao.EMSINVHDao;
import com.twc.ems.dao.EMSMASDao;
import com.twc.ems.dao.EMSINVHDao;
import com.twc.ems.dao.EMSUNITDao;
import com.twc.ems.entity.EMSINVD;
import com.twc.ems.entity.EMSINVH;
import com.twc.ems.entity.EMSMAS;
import com.twc.ems.entity.EMSINVH;
import com.twc.ems.entity.EMSUNIT;
import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import javax.servlet.http.HttpSession;

/**
 *
 * @author wien
 */
public class SDisplayControllerEMS100 extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;
    private static final String PAGE_VIEW = "../views/sdisplayEMS100.jsp";

    public SDisplayControllerEMS100() {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("PROGRAMNAME", "EMS100/D");
        String forward = "";
        try {
            String action = request.getParameter("action");
            if (action == null) {
                forward = PAGE_VIEW;
                request.setAttribute("PROGRAMDESC", "Invoice Information. Display");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
//    ********
        String id = request.getParameter("id");
        String nontwc = request.getParameter("nontwc");

        EMSINVH p = new EMSINVHDao().findByCode(id, nontwc);

        request.setAttribute("EMHINVID", p.getEMHINVID());
        request.setAttribute("EMHCUSID", p.getEMHCUSID());
        request.setAttribute("EMHCUSSHIPID", p.getEMHCUSSHIPID());
        request.setAttribute("EMHSHIPID", p.getEMHSHIPID());
        request.setAttribute("EMHTRANSPID", p.getEMHTRANSPID());
        request.setAttribute("EMHCURRENCY", p.getEMHCURRENCY());
        request.setAttribute("EMHUNIT", p.getEMHUNIT());
        request.setAttribute("EMHSHIPDATE", p.getEMHSHIPDATE());
        request.setAttribute("EMHAMTCTN", p.getEMHAMTCTN());

        List<EMSINVD> aheadList = new EMSINVDDao().findByInv(id, nontwc);
        request.setAttribute("aheadList", aheadList);

        RequestDispatcher view = request.getRequestDispatcher(forward);
        view.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//        request.setCharacterEncoding("utf-8");
//
//        String id = request.getParameter("id");
//        String desc = request.getParameter("desc");
//
//        String userid = request.getParameter("userid");
//
////        new EMSINVHDao().edit(id, desc, userid);
//        response.setHeader("Refresh", "0;/EMS2/EMS100/display");
    }
}
