/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.ems.controller;

import com.twc.ems.dao.EMSCACDao;
import com.twc.ems.dao.EMSINVDDao;
import com.twc.ems.dao.EMSINVHDao;
import com.twc.ems.dao.EMSMASDao;
import java.io.IOException;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author wien
 */
public class DeleteControllerEMS100 extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;

    public DeleteControllerEMS100() {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");

        String uid = request.getParameter("uid");
        String id = request.getParameter("id");
        String nontwc = request.getParameter("nontwc");
        String zone = request.getParameter("zone");

        new EMSINVHDao().delete(id, nontwc);
        new EMSINVDDao().delete(id, nontwc);
        if (!nontwc.equals("1")) {
            new EMSCACDao().delete(id, zone);
        }

        response.setHeader("Refresh", "0;/EMS2/EMS100/display?uid=" + uid);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

}
