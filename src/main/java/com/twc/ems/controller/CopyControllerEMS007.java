/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.ems.controller;

import com.twc.ems.dao.EMSTEXTDao;
import com.twc.ems.entity.EMSTEXT;
import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author wien
 */
public class CopyControllerEMS007 extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;

    public CopyControllerEMS007() {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");

        String oinv = request.getParameter("oinv");
        String ninv = request.getParameter("ninv");

        String userid = request.getParameter("userid");

        if (!ninv.trim().equals("")) {
            if (new EMSTEXTDao().copy(oinv, ninv, userid)) {
                response.setHeader("Refresh", "0;/EMS2/EMS007/edit?id=" + ninv);
            } else {
                response.setHeader("Refresh", "0;/EMS2/EMS007/display");
            }
        } else {
            response.setHeader("Refresh", "0;/EMS2/EMS007/display");
        }

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
