/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.ems.controller;

import com.twc.ems.dao.EMSCACDao;
import com.twc.ems.dao.EMSMASDao;
import com.twc.ems.dao.EMSUNITDao;
import com.twc.ems.entity.EMSMAS;
import com.twc.ems.entity.EMSUNIT;
import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import javax.servlet.http.HttpSession;

/**
 *
 * @author wien
 */
public class EditControllerEMS001 extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;
    private static final String PAGE_VIEW = "../views/editEMS001.jsp";

    public EditControllerEMS001() {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("PROGRAMNAME", "EMS001/E");
        String forward = "";
        try {
            String action = request.getParameter("action");
            if (action == null) {
                forward = PAGE_VIEW;
                request.setAttribute("PROGRAMDESC", "Master Detail. Display");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
//    ********
        List<EMSUNIT> unitList = new EMSUNITDao().findAll();
        request.setAttribute("unitList", unitList);

        String zone = request.getParameter("zone");
        String style = request.getParameter("style");
        String cup = request.getParameter("cup");
        String size = request.getParameter("size");

        EMSMAS p = new EMSMASDao().findByCode(style, cup, size, zone);

        request.setAttribute("style", p.getEMMSTYLE());
        request.setAttribute("zone", p.getEMMZONE());
        request.setAttribute("cup", p.getEMMCUP());
        request.setAttribute("size", p.getEMMSIZE());
        request.setAttribute("type", p.getEMMTYPE());

        request.setAttribute("weight", p.getEMMWEIGHT());
        request.setAttribute("price", p.getEMMPRICE());
        request.setAttribute("unit", p.getEMMUNIT());
        request.setAttribute("hs", p.getEMMHSCODE());
        request.setAttribute("ran", p.getEMMRANGE());

        request.setAttribute("comp1", p.getEMMCOMPO1());
        request.setAttribute("comp2", p.getEMMCOMPO2());
        request.setAttribute("comp3", p.getEMMCOMPO3());

        request.setAttribute("box6e", p.getEMMBOXS6E());
        request.setAttribute("box6d", p.getEMMBOXS6D());
        request.setAttribute("box4e", p.getEMMBOXS4E());
        request.setAttribute("box4d", p.getEMMBOXS4D());
        request.setAttribute("box2e", p.getEMMBOXS2E());
        request.setAttribute("ppack", p.getEMMPPACK());

        request.setAttribute("wei6e", p.getEMMWG6E());
        request.setAttribute("wei6d", p.getEMMWG6D());
        request.setAttribute("wei4e", p.getEMMWG4E());
        request.setAttribute("wei4d", p.getEMMWG4D());
        request.setAttribute("wei2e", p.getEMMWG2E());

        RequestDispatcher view = request.getRequestDispatcher(forward);
        view.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");

        String zone = request.getParameter("zone");
        String style = request.getParameter("style");
        String cup = request.getParameter("cup");
        String size = request.getParameter("size");
        String type = request.getParameter("type");

        String weight = request.getParameter("weight");
        String price = request.getParameter("price");
        String unit = request.getParameter("unit");
        String hs = request.getParameter("hs");
        String ran = request.getParameter("ran");

        if (weight.trim().equals("")) {
            weight = "0";
        }
        if (price.trim().equals("")) {
            price = "0";
        }

        String comp1 = request.getParameter("comp1");
        String comp2 = request.getParameter("comp2");
        String comp3 = request.getParameter("comp3");

        String box6e = request.getParameter("box6e");
        String box6d = request.getParameter("box6d");
        String box4e = request.getParameter("box4e");
        String box4d = request.getParameter("box4d");
        String box2e = request.getParameter("box2e");
        String ppack = request.getParameter("ppack");

        String wei6e = request.getParameter("wei6e");
        String wei6d = request.getParameter("wei6d");
        String wei4e = request.getParameter("wei4e");
        String wei4d = request.getParameter("wei4d");
        String wei2e = request.getParameter("wei2e");

        if (box6e.trim().equals("")) {
            box6e = "0";
        }
        if (box6d.trim().equals("")) {
            box6d = "0";
        }
        if (box4e.trim().equals("")) {
            box4e = "0";
        }
        if (box4d.trim().equals("")) {
            box4d = "0";
        }
        if (box2e.trim().equals("")) {
            box2e = "0";
        }
        if (ppack.trim().equals("")) {
            ppack = "0";
        }

        if (wei6e.trim().equals("")) {
            wei6e = "0";
        }
        if (wei6d.trim().equals("")) {
            wei6d = "0";
        }
        if (wei4e.trim().equals("")) {
            wei4e = "0";
        }
        if (wei4d.trim().equals("")) {
            wei4d = "0";
        }
        if (wei2e.trim().equals("")) {
            wei2e = "0";
        }

        String userid = request.getParameter("userid");

        request.setAttribute("PROGRAMNAME", "EMS001/E");
        request.setAttribute("PROGRAMDESC", "Master Detail. Display");

        boolean isUpdate = new EMSMASDao().edit(zone, style, cup, size, type, weight, price, unit, hs, ran, comp1, comp2, comp3, box6e, box6d, box4e, box4d, box2e, ppack, wei6e, wei6d, wei4e, wei4d, wei2e, userid);

        if (isUpdate) {
//            new EMSCACDao().updateReCal(zone, style, cup, size);
        }

        response.setHeader("Refresh", "0;/EMS2/EMS001/display?search=true");
    }
}
