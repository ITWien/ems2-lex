/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.ems.controller;

import com.twc.ems.dao.EMSDWTDao;
import com.twc.ems.dao.EMSMASDao;
import java.io.IOException;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author wien
 */
public class DeleteControllerEMS900 extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;

    public DeleteControllerEMS900() {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");

        String id = request.getParameter("id");
        String action = request.getParameter("action");
        String uid = request.getParameter("uid");

        new EMSDWTDao().delete(id);

        if (action != null) {
            response.setHeader("Refresh", "0;/EMS2/EMS900/" + action);
        } else {
            response.setHeader("Refresh", "0;/EMS2/EMS900/display?uid=" + uid);
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

}
