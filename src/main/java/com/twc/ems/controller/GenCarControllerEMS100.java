/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.ems.controller;

import com.twc.ems.dao.EMSCACDao;
import com.twc.ems.dao.EMSINVHDao;
import com.twc.ems.entity.EMSCAC;
import java.io.IOException;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author wien
 */
public class GenCarControllerEMS100 extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;

    public GenCarControllerEMS100() {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");

        String inv = request.getParameter("inv");
        String dist = request.getParameter("dist");
        if (dist != null) {
            dist = dist.toUpperCase();
        }

        String userid = request.getParameter("userid");

        new EMSCACDao().genCarF1(inv, userid, dist);

        List<EMSCAC> cacList = new EMSCACDao().getCacGebGrp(inv, dist);
        int grpNo = 1;
        String styleGrp = "first";
        for (int i = 0; i < cacList.size(); i++) {
            if (!styleGrp.equals("first") && !styleGrp.equals(cacList.get(i).getEMCSTYLE())) {
                grpNo++;
            }
            new EMSCACDao().updateGrpNo(inv, dist, cacList.get(i).getEMCCARTONNO(), Integer.toString(grpNo));
            styleGrp = cacList.get(i).getEMCSTYLE();
        }

        response.setHeader("Refresh", "0;/EMS2/EMS100/dropbox3?inv=" + inv + "&dist=" + dist);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");

//        String[] ctno = request.getParameterValues("ctno");
        String inv = request.getParameter("invGC");
        String dist = request.getParameter("distGC");
        String numBox = request.getParameter("numBox");
        int numBoxInt = 1;
        if (numBox != null) {
            numBoxInt = Integer.parseInt(numBox);
        }

        if (dist != null) {
            dist = dist.toUpperCase();
        } else {
            dist = "";
        }

        String userid = request.getParameter("userid3");

        if (dist.equalsIgnoreCase("ASEAN") || dist.equalsIgnoreCase("CHINA") || dist.equalsIgnoreCase("CWX-JPN")) {
            List<EMSCAC> aheadList = new EMSCACDao().findByInvDropGenCar(inv, dist);

            if (!aheadList.isEmpty()) {
                new EMSCACDao().CLgenCar(inv, userid, dist);
                int ctno = 1;
                int cnt = 0;
                double net = 0.000;
                for (int i = 0; i < aheadList.size(); i++) {
                    cnt++;
                    if (cnt > numBoxInt) {
                        ctno++;
                        cnt = 1;
                        net = 0.000;
                    }
                    net += Double.parseDouble(aheadList.get(i).getEMCNET());

                    boolean isLast = false;
                    if (cnt == numBoxInt || (i == aheadList.size() - 1)) {
                        isLast = true;
                    }

                    new EMSCACDao().genCarAsean(inv, aheadList.get(i).getEMCCTNO(), Integer.toString(ctno), userid, dist, Double.toString(net), aheadList.get(i).getEMCBOXTYPE(), isLast);
                }
            }
        } else {
            List<EMSCAC> aheadList = new EMSCACDao().findByInvDrop(inv, dist);

            if (!aheadList.isEmpty()) {
                new EMSCACDao().CLgenCar(inv, userid, dist);
                for (int i = 0; i < aheadList.size(); i++) {
                    new EMSCACDao().genCar(inv, aheadList.get(i).getEMCCTNO(), Integer.toString(i + 1), userid, dist);
                }
            }
        }

        new EMSINVHDao().editCarAmt(inv, dist);
        new EMSINVHDao().updateRun(inv, dist, "Y");

        List<EMSCAC> cacList = new EMSCACDao().getCacGebGrp(inv, dist);
        int grpNo = 1;
        String styleGrp = "first";
        for (int i = 0; i < cacList.size(); i++) {
            if (!styleGrp.equals("first") && !styleGrp.equals(cacList.get(i).getEMCSTYLE())) {
                grpNo++;
            }
            new EMSCACDao().updateGrpNo(inv, dist, cacList.get(i).getEMCCARTONNO(), Integer.toString(grpNo));
            styleGrp = cacList.get(i).getEMCSTYLE();
        }

        response.setHeader("Refresh", "0;/EMS2/EMS100/dropbox3?inv=" + inv + "&dist=" + dist);
    }
}
