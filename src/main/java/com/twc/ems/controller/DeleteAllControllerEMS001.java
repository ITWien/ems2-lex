/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.ems.controller;

import com.twc.ems.dao.EMSMASDao;
import java.io.IOException;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author wien
 */
public class DeleteAllControllerEMS001 extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;

    public DeleteAllControllerEMS001() {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");

        String[] selectCk = request.getParameterValues("selectCk");

        if (selectCk != null) {
            for (int i = 0; i < selectCk.length; i++) {

                String zone = selectCk[i].split("<=>")[0];
                String style = selectCk[i].split("<=>")[1];
                String cup = selectCk[i].split("<=>")[2];
                String size = selectCk[i].split("<=>")[3];

                new EMSMASDao().delete(style, cup, size, zone);
            }
        }

        response.setHeader("Refresh", "0;/EMS2/EMS001/display?search=true");
    }

}
