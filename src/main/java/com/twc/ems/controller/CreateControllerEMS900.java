/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.ems.controller;

import com.twc.ems.dao.EMSCODEDao;
import com.twc.ems.dao.EMSCUADao;
import com.twc.ems.dao.EMSCUMDao;
import com.twc.ems.dao.EMSDWTDao;
import com.twc.ems.dao.EMSINVDDao;
import com.twc.ems.dao.EMSINVHDao;
import com.twc.ems.dao.EMSMASDao;
import com.twc.ems.dao.EMSSAP;
import com.twc.ems.dao.EMSTSPDao;
import com.twc.ems.entity.EMSCODE;
import com.twc.ems.entity.EMSCUA;
import com.twc.ems.entity.EMSCUM;
import com.twc.ems.entity.EMSINVD;
import com.twc.ems.entity.EMSINVH;
import com.twc.ems.entity.EMSMAS;
import com.twc.ems.entity.EMSTSP;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.MimeHeaders;
import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPConnectionFactory;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.XML;
import sun.misc.BASE64Encoder;

/**
 *
 * @author wien
 */
public class CreateControllerEMS900 extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;
    private static final String PAGE_VIEW = "../views/createEMS900.jsp";
    static String GG = "";
    static String DELIVERY = "";
    static String DIVISION = "";
    static String EXBUS = "";
    static String MASKID = "";
    static String SALESORG = "";
    static String TRANSID = "";

    public CreateControllerEMS900() {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("PROGRAMNAME", "EMS900/C");
        String forward = "";
        try {
            String action = request.getParameter("action");
            if (action == null) {
                forward = PAGE_VIEW;
                request.setAttribute("PROGRAMDESC", "Download Invoice From SAP. Process");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        List<EMSCUM> shipList = new EMSCUMDao().findAll();
        request.setAttribute("shipList", shipList);

        List<EMSTSP> tspList = new EMSTSPDao().findAll();
        request.setAttribute("tspList", tspList);

        request.setAttribute("compY", "checked");

        RequestDispatcher view = request.getRequestDispatcher(forward);
        view.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            request.setCharacterEncoding("utf-8");

            String zone = request.getParameter("zone");
            String delno = request.getParameter("delno");
            String sale = request.getParameter("sale");
            String ship = request.getParameter("ship");
            String tsp = request.getParameter("tsp");
            String dist = request.getParameter("dist");
            String exp = request.getParameter("exp");
            String comp = request.getParameter("comp");
            if (comp == null) {
                comp = "";
            }

            request.setAttribute("zone", zone);
            request.setAttribute("delno", delno);
            request.setAttribute("sale", sale);
            request.setAttribute("ship", ship);
            request.setAttribute("tsp", tsp);
            request.setAttribute("dist", dist);
            request.setAttribute("exp", exp);

            String nontwc = "0";
            if (comp.equals("Y")) {
                request.setAttribute("compY", "checked");
            } else if (comp.equals("N")) {
                nontwc = "1";
                request.setAttribute("compN", "checked");
            } else {
                nontwc = "2";
                request.setAttribute("compX", "checked");
            }

            String userid = request.getParameter("userid");

            request.setAttribute("PROGRAMNAME", "EMS900/C");
            request.setAttribute("PROGRAMDESC", "Download Invoice From SAP. Process");

            List<EMSCUM> shipList = new EMSCUMDao().findAll();
            request.setAttribute("shipList", shipList);

            List<EMSTSP> tspList = new EMSTSPDao().findAll();
            request.setAttribute("tspList", tspList);

            String sendMessage = "";
            String forward = "";

            String check = new EMSDWTDao().check(delno, sale, ship, tsp, dist, exp, comp, zone);

            if (!check.equals("t")) {
                sendMessage = "window.setTimeout(show, 0);";
                forward = PAGE_VIEW;
                request.setAttribute("sendMessage", sendMessage);
                request.setAttribute("no", check);
                RequestDispatcher view = request.getRequestDispatcher(forward);
                view.forward(request, response);
            } else {

                String javaHomePath = System.getProperty("java.home");
                String keystore = javaHomePath + "/lib/security/cacerts";
                String storepass = "changeit";
                String storetype = "JKS";

                String[][] props = {
                    {"javax.net.ssl.trustStore", keystore,},
                    {"javax.net.ssl.keyStore", keystore,},
                    {"javax.net.ssl.keyStorePassword", storepass,},
                    {"javax.net.ssl.keyStoreType", storetype,},};

                for (int i = 0; i < props.length; i++) {
                    System.getProperties().setProperty(props[i][0], props[i][1]);
                }

                String soapEndpointUrl = "http://10.11.9.11:8000/sap/bc/srt/rfc/sap/Z_WS_TWC_SD_GETEXP?sap-client=500&wsdl=1.1";
                String soapAction = "";

                DELIVERY = delno;
                DIVISION = dist;
                EXBUS = exp;
                MASKID = ship;
                SALESORG = sale;
                TRANSID = tsp;

                String send = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?><SOAP-ENV:Envelope xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xs=\"http://www.w3.org/2001/XMLSchema\">\n"
                        + "    <SOAP-ENV:Header>\n"
                        + "        <sapsess:Session xmlns:sapsess=\"http://www.sap.com/webas/630/soap/features/session/\">\n"
                        + "            <enableSession>true</enableSession>\n"
                        + "        </sapsess:Session>\n"
                        + "    </SOAP-ENV:Header>\n"
                        + "    <SOAP-ENV:Body>\n"
                        + "        <ns1:Z_BAPI_TWC_SD_GETEXP xmlns:ns1='urn:sap-com:document:sap:rfc:functions'>\n"
                        + "            <DELIVERY xsi:type='ns1:char10'>" + DELIVERY + "</DELIVERY>\n"
                        + "            <DIVISION xsi:type='ns1:char2'>" + DIVISION + "</DIVISION>\n"
                        + "            <EXBUS xsi:type='ns1:numeric3'>" + EXBUS + "</EXBUS>\n"
                        + "            <EXP_CUSTOMER xsi:nil='true' xsi:type='ns1:TABLE_OF_ZBAPI_TWC_EXP_CUST'></EXP_CUSTOMER>\n"
                        + "            <EXP_DETAIL xsi:nil='true' xsi:type='ns1:TABLE_OF_ZBAPI_TWC_EXP_DET'></EXP_DETAIL>\n"
                        + "            <EXP_HEADER xsi:nil='true' xsi:type='ns1:TABLE_OF_ZBAPI_TWC_EXP_HEAD'></EXP_HEADER>\n"
                        + "            <MASKID xsi:type='ns1:char3'></MASKID>\n"
                        + "            <RETURN xsi:nil='true' xsi:type='ns1:TABLE_OF_BAPIRETURN'></RETURN>\n"
                        + "            <SALESORG xsi:type='ns1:char4'>" + SALESORG + "</SALESORG>\n"
                        + "            <TRANSID xsi:type='ns1:char3'></TRANSID>\n"
                        + "        </ns1:Z_BAPI_TWC_SD_GETEXP>\n"
                        + "    </SOAP-ENV:Body>\n"
                        + "</SOAP-ENV:Envelope>";

//                System.out.println(send);
                InputStream is = new ByteArrayInputStream(send.getBytes());
                SOAPMessage soapMessage = MessageFactory.newInstance().createMessage(null, is);
                GG = "1";
                Calendar c = Calendar.getInstance();
                int month = c.get(Calendar.MONTH) + 1;

                String m = Integer.toString(month);
                if (m.length() < 2) {
                    m = "0" + m;
                }
                GG = "2";

                String userAndPassword = String.format("%s:%s", "tviewwien", "display" + m);
                String basicAuth = new BASE64Encoder().encode(userAndPassword.getBytes());
                MimeHeaders mimeHeaders = soapMessage.getMimeHeaders();
                mimeHeaders.addHeader("Authorization", "Basic " + basicAuth);
                soapMessage.saveChanges();
                GG = "3";

                // Create SOAP Connection
                SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory.newInstance();
                GG = "4";
                SOAPConnection soapConnection = soapConnectionFactory.createConnection();
                GG = "5";

                // Send SOAP Message to SOAP Server
                SOAPMessage soapResponse = soapConnection.call(soapMessage, soapEndpointUrl);
                GG = "6";

                // Print the SOAP Response
//            System.out.println("Response SOAP Message:");
//            soapResponse.writeTo(System.out);
//            System.out.println();
                soapConnection.close();
                GG = "7";

                ByteArrayOutputStream out = new ByteArrayOutputStream();
                GG = "8";
                soapResponse.writeTo(out);
                GG = "9";
                String strMsg = new String(out.toByteArray());
                GG = "10";
//                System.out.println(strMsg);
                JSONObject json = XML.toJSONObject(strMsg);
                JSONObject Envelope = new JSONObject(json.get("soap-env:Envelope").toString());
                JSONObject Body = new JSONObject(Envelope.get("soap-env:Body").toString());
                JSONObject Z_BAPI_TWC_SD_GETEXPResponse = new JSONObject(Body.get("n0:Z_BAPI_TWC_SD_GETEXPResponse").toString());

                JSONObject RETURN = new JSONObject(Z_BAPI_TWC_SD_GETEXPResponse.get("RETURN").toString());
                JSONObject _return = new JSONObject(RETURN.get("item").toString());

//                System.out.println(_return.get("MESSAGE"));
//                if (_return.get("MESSAGE").toString().contains("successfully")) {
                JSONObject EXP_CUSTOMER = new JSONObject(Z_BAPI_TWC_SD_GETEXPResponse.get("EXP_CUSTOMER").toString());
                char firstCharCUSTOMER = EXP_CUSTOMER.get("item").toString().charAt(0);

                JSONObject EXP_DETAIL = new JSONObject(Z_BAPI_TWC_SD_GETEXPResponse.get("EXP_DETAIL").toString());
                char firstCharDETAIL = EXP_DETAIL.get("item").toString().charAt(0);
                JSONArray DETAIL = new JSONArray(EXP_DETAIL.get("item").toString());

                JSONObject EXP_HEADER = new JSONObject(Z_BAPI_TWC_SD_GETEXPResponse.get("EXP_HEADER").toString());
                JSONObject HEADER = new JSONObject(EXP_HEADER.get("item").toString());
//                }

//                System.out.println("*********finish-in-cont***********");
                //////////////////////////////--START-DOWNLOAD--//////////////////////////////
//            List<EMSINVH> invHeadList = new ArrayList<EMSINVH>();
                List<EMSINVD> invDetailList = new ArrayList<EMSINVD>();
                List<EMSMAS> downloadList = new ArrayList<EMSMAS>();
                List<EMSCODE> downloadList2 = new ArrayList<EMSCODE>();

                EMSCUA customer = new EMSCUA();
                List<EMSCUA> customerList = new ArrayList<EMSCUA>();

                if (firstCharCUSTOMER == '{') {
                    JSONObject objectCUSTOMER = new JSONObject(EXP_CUSTOMER.get("item").toString());
                    customer.setEMCACUSID(Integer.toString(Integer.parseInt(objectCUSTOMER.get("CUSTOMER").toString())));
                    customer.setEMCAADD1(objectCUSTOMER.get("CUST_NAME").toString());
                    customer.setEMCAADD2(objectCUSTOMER.get("CUST_ADDR1").toString());
                    customer.setEMCAADD3(objectCUSTOMER.get("CUST_ADDR2").toString());

                } else if (firstCharCUSTOMER == '[') {
                    JSONArray arrayCUSTOMER = new JSONArray(EXP_CUSTOMER.get("item").toString());
                    for (int i = 0; i < arrayCUSTOMER.length(); i++) {
                        EMSCUA customerItem = new EMSCUA();
                        customerItem.setEMCACUSID(Integer.toString(Integer.parseInt(new JSONObject(arrayCUSTOMER.get(i).toString()).get("CUSTOMER").toString())));
                        customerItem.setEMCAADD1(new JSONObject(arrayCUSTOMER.get(i).toString()).get("CUST_NAME").toString());
                        customerItem.setEMCAADD2(new JSONObject(arrayCUSTOMER.get(i).toString()).get("CUST_ADDR1").toString());
                        customerItem.setEMCAADD3(new JSONObject(arrayCUSTOMER.get(i).toString()).get("CUST_ADDR2").toString());
                        customerList.add(customerItem);
                    }
                }

                EMSINVH head = new EMSINVH();
                head.setEMHINVID(HEADER.get("INVOICE").toString());
                head.setEMHCUSSHIPID(Integer.toString(Integer.parseInt(HEADER.get("SHIPTO").toString())));
                head.setEMHSHIPID(ship);
                head.setEMHTRANSPID(tsp);
                head.setEMHCURRENCY(HEADER.get("CURRENCY").toString());
                head.setEMHUNIT(HEADER.get("UNITNO").toString());
                head.setEMHSHIPDATE(HEADER.get("SHIP_DATE").toString());
                head.setEMHCUSID(Integer.toString(Integer.parseInt(HEADER.get("SOLDTO").toString())));
                head.setEMHZONE(zone);
                head.setEMHNONTWC(nontwc.equals("2") ? "0" : nontwc);
//                System.out.println("Head : " + head.toString());

                for (int i = 0; i < DETAIL.length(); i++) {

                    EMSINVD detail = new EMSINVD();
                    detail.setEMDINVID(new JSONObject(DETAIL.get(i).toString()).get("INVOICE").toString());
                    detail.setEMDORDID(new JSONObject(DETAIL.get(i).toString()).get("BATCH").toString());
                    detail.setEMDSTYLE(new JSONObject(DETAIL.get(i).toString()).get("STYLE").toString());
                    detail.setEMDCOLOR(new JSONObject(DETAIL.get(i).toString()).get("COLOR").toString());
                    String detCup = new JSONObject(DETAIL.get(i).toString()).get("CUP").toString();
                    if (detCup.trim().equals("")) {
                        detCup = "****";
                    }
                    detail.setEMDCUP(detCup);

                    String sizeASEAN = new JSONObject(DETAIL.get(i).toString()).get("SIZE").toString().trim();
                    if (zone.equals("ASEAN")) {
                        if (sizeASEAN.equalsIgnoreCase("X2")) {
                            sizeASEAN = "M";
                        } else if (sizeASEAN.equalsIgnoreCase("X3")) {
                            sizeASEAN = "L";
                        } else if (sizeASEAN.equalsIgnoreCase("X4")) {
                            sizeASEAN = "LL";
                        } else if (sizeASEAN.equalsIgnoreCase("X5")) {
                            sizeASEAN = "EL";
                        } else if (sizeASEAN.equalsIgnoreCase("1")) {
                            sizeASEAN = "65";
                        } else if (sizeASEAN.equalsIgnoreCase("2")) {
                            sizeASEAN = "70";
                        } else if (sizeASEAN.equalsIgnoreCase("3")) {
                            sizeASEAN = "75";
                        } else if (sizeASEAN.equalsIgnoreCase("4")) {
                            sizeASEAN = "80";
                        } else if (sizeASEAN.equalsIgnoreCase("5")) {
                            sizeASEAN = "85";
                        } else if (sizeASEAN.equalsIgnoreCase("6")) {
                            sizeASEAN = "90";
                        } else if (sizeASEAN.equalsIgnoreCase("7")) {
                            sizeASEAN = "95";
                        }
                    }
                    detail.setEMDSIZES(sizeASEAN);

                    detail.setEMDSTYONHD(new JSONObject(DETAIL.get(i).toString()).get("MATERIAL").toString());
                    detail.setEMDPONO(new JSONObject(DETAIL.get(i).toString()).get("PO_NUMBER").toString());
                    detail.setEMDBRAND(new JSONObject(DETAIL.get(i).toString()).get("BRAND").toString());
                    detail.setEMDTYPE(new JSONObject(DETAIL.get(i).toString()).get("MAT_TYPE").toString());
                    detail.setEMDAMOUNT(new JSONObject(DETAIL.get(i).toString()).get("QUANTITY").toString());
                    detail.setEMDDISCOUNT(new JSONObject(DETAIL.get(i).toString()).get("DISCOUNT").toString());
                    detail.setEMDCOST(new JSONObject(DETAIL.get(i).toString()).get("COST").toString());
                    detail.setEMDAUTH(new JSONObject(DETAIL.get(i).toString()).get("AUTHR_NO").toString());
                    detail.setEMDPOSHIP(new JSONObject(DETAIL.get(i).toString()).get("PONUM_SHIP").toString());
                    detail.setPLANT(new JSONObject(DETAIL.get(i).toString()).get("WERKS").toString());
                    invDetailList.add(detail);
//                    System.out.println("Detail : " + detail.toString());

                    //--------------Master Detail------------------
                    EMSMAS dl = new EMSMAS();
                    dl.setEMMZONE(zone);
                    dl.setEMMSTYLE(new JSONObject(DETAIL.get(i).toString()).get("STYLE").toString());
                    dl.setEMMCUP(new JSONObject(DETAIL.get(i).toString()).get("CUP").toString());
                    dl.setEMMSIZE(sizeASEAN);
                    downloadList.add(dl);
                    //-------------Color Code-----------------
                    EMSCODE dl2 = new EMSCODE();
                    dl2.setEMCDZONE(zone);
                    dl2.setEMCDCODE(new JSONObject(DETAIL.get(i).toString()).get("COLOR").toString());
                    downloadList2.add(dl2);
                }
                ////////////////////////////--FINISH-DOWNLOAD--///////////////////////////////
                List<EMSMAS> errorList = new ArrayList<EMSMAS>();
                List<EMSCODE> errorList2 = new ArrayList<EMSCODE>();

                List<String> SerrorList = new ArrayList<String>();
                List<String> SerrorList2 = new ArrayList<String>();

                for (int i = 0; i < downloadList.size(); i++) {
                    List<EMSMAS> checkList = new EMSMASDao()
                            .findAll(downloadList.get(i).getEMMSTYLE(), downloadList.get(i).getEMMCUP(), downloadList.get(i).getEMMSIZE(), zone);

                    if (checkList.isEmpty()) {
                        if (!SerrorList.contains(downloadList.get(i).toString())) {
                            errorList.add(downloadList.get(i));
                            SerrorList.add(downloadList.get(i).toString());
                        }
                    }
                }

                for (int i = 0; i < downloadList2.size(); i++) {
                    List<EMSCODE> checkList2 = new EMSCODEDao().findByCodeCheck(downloadList2.get(i).getEMCDCODE(), zone);

                    if (checkList2.isEmpty()) {
                        if (!SerrorList2.contains(downloadList2.get(i).toString())) {
                            errorList2.add(downloadList2.get(i));
                            SerrorList2.add(downloadList2.get(i).toString());
                        }
                    }
                }

                sendMessage = "window.setTimeout(show2, 0);";
                forward = PAGE_VIEW;
                request.setAttribute("sendMessage", sendMessage);

                String headModal = "No Error !";
                String headModalDet = "";
                if (!errorList.isEmpty() || !errorList2.isEmpty()) {
                    headModal = "Download Failed !";
                    headModalDet = "Data not found in Master Details / Color Code";
                } else {
                    if (new EMSDWTDao().add(delno, sale, ship, tsp, dist, exp, comp, userid, zone, head.getEMHINVID())) {
                        if (new EMSINVHDao().addDownload(head, userid, delno)) {
                            boolean isAddDetail = new EMSINVDDao().addDownload(invDetailList, userid, nontwc);

                            if (firstCharCUSTOMER == '{') {
                                if (new EMSCUADao().check(customer.getEMCACUSID()).equals("f")) {
                                    new EMSCUADao().edit(customer.getEMCACUSID(), customer.getEMCAADD1(), customer.getEMCAADD2(), customer.getEMCAADD3(), "", "", userid);
                                } else {
                                    new EMSCUADao().add(customer.getEMCACUSID(), customer.getEMCAADD1(), customer.getEMCAADD2(), customer.getEMCAADD3(), "", "", userid);
                                }

                            } else if (firstCharCUSTOMER == '[') {
                                for (int i = 0; i < customerList.size(); i++) {
                                    if (new EMSCUADao().check(customerList.get(i).getEMCACUSID()).equals("f")) {
                                        new EMSCUADao().edit(customerList.get(i).getEMCACUSID(), customerList.get(i).getEMCAADD1(), customerList.get(i).getEMCAADD2(), customerList.get(i).getEMCAADD3(), "", "", userid);
                                    } else {
                                        new EMSCUADao().add(customerList.get(i).getEMCACUSID(), customerList.get(i).getEMCAADD1(), customerList.get(i).getEMCAADD2(), customerList.get(i).getEMCAADD3(), "", "", userid);
                                    }
                                }
                            }

                            if (isAddDetail) {
                                //Update Type from Master Detail
                                new EMSINVDDao().updateType(head.getEMHINVID(), nontwc.equals("2") ? "0" : nontwc);
                            }
                        }
                    }
                    response.setHeader("Refresh", "0;/EMS2/EMS900/display?uid=" + userid);
                }
                request.setAttribute("headModal", headModal);
                request.setAttribute("headModalDet", headModalDet);
                request.setAttribute("errorList", errorList);
                request.setAttribute("errorList2", errorList2);

                RequestDispatcher view = request.getRequestDispatcher(forward);
                view.forward(request, response);

            }

        } catch (Exception e) {
            e.printStackTrace();

            String sendMessage = "";
            String forward = "";

            sendMessage = "window.setTimeout(show2, 0);";
            forward = PAGE_VIEW;
            request.setAttribute("sendMessage", sendMessage);

            String headModal = "Download Failed !";
            String headModalDet = GG + " : " + e.getMessage();
            request.setAttribute("headModal", headModal);
            request.setAttribute("headModalDet", headModalDet);

            RequestDispatcher view = request.getRequestDispatcher(forward);
            view.forward(request, response);
        }

    }
}
