/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.ems.controller;

import com.twc.ems.dao.EMSCODEDao;
import com.twc.ems.dao.EMSCUMDao;
import com.twc.ems.dao.EMSDWTDao;
import com.twc.ems.dao.EMSINVDDao;
import com.twc.ems.dao.EMSINVHDao;
import com.twc.ems.dao.EMSMASDao;
import com.twc.ems.dao.EMSSAP;
import com.twc.ems.dao.EMSTSPDao;
import com.twc.ems.entity.EMSCODE;
import com.twc.ems.entity.EMSCUM;
import com.twc.ems.entity.EMSINVD;
import com.twc.ems.entity.EMSINVH;
import com.twc.ems.entity.EMSMAS;
import com.twc.ems.entity.EMSTSP;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.MimeHeaders;
import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPConnectionFactory;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.XML;
import sun.misc.BASE64Encoder;

/**
 *
 * @author wien
 */
public class CreateControllerEMS100 extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;
    private static final String PAGE_VIEW = "../views/createEMS100.jsp";
    static String GG = "";
    static String DELIVERY = "";
    static String DIVISION = "";
    static String EXBUS = "";
    static String MASKID = "";
    static String SALESORG = "";
    static String TRANSID = "";

    public CreateControllerEMS100() {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("PROGRAMNAME", "EMS100/C");
        String forward = "";
        try {
            String action = request.getParameter("action");
            if (action == null) {
                forward = PAGE_VIEW;
                request.setAttribute("PROGRAMDESC", "Invoice Information. Display");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        List<EMSCUM> shipList = new EMSCUMDao().findAll();
        request.setAttribute("shipList", shipList);

        List<EMSTSP> tspList = new EMSTSPDao().findAll();
        request.setAttribute("tspList", tspList);

        request.setAttribute("compY", "checked");

        RequestDispatcher view = request.getRequestDispatcher(forward);
        view.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");

        String zone = request.getParameter("zone");
        String inv = request.getParameter("inv");
        String cusid = request.getParameter("cusid");
        String shipTo = request.getParameter("shipTo");
        String ship = request.getParameter("ship");
        String tsp = request.getParameter("tsp");
        String delno = request.getParameter("delno");
        String cur = request.getParameter("cur");
        String unit = request.getParameter("unit");
        String shipDate = request.getParameter("shipDate");
        String ctnAmt = request.getParameter("ctnAmt");
        String comp = request.getParameter("comp");
        if (comp == null) {
            comp = "";
        }
        if (shipDate != null) {
            shipDate = shipDate.replace("-", "");
        }

        request.setAttribute("zone", zone);
        request.setAttribute("inv", inv);
        request.setAttribute("cusid", cusid);
        request.setAttribute("shipTo", shipTo);
        request.setAttribute("ship", ship);
        request.setAttribute("tsp", tsp);
        request.setAttribute("delno", delno);
        request.setAttribute("cur", cur);
        request.setAttribute("unit", unit);
        request.setAttribute("shipDate", shipDate);
        request.setAttribute("ctnAmt", ctnAmt);

        String nontwc = "0";
        if (comp.equals("Y")) {
            request.setAttribute("compY", "checked");
        } else {
            nontwc = "1";
            request.setAttribute("compN", "checked");
        }

        String userid = request.getParameter("userid");

        request.setAttribute("PROGRAMNAME", "EMS100/C");
        request.setAttribute("PROGRAMDESC", "Invoice Information. Display");

        List<EMSCUM> shipList = new EMSCUMDao().findAll();
        request.setAttribute("shipList", shipList);

        List<EMSTSP> tspList = new EMSTSPDao().findAll();
        request.setAttribute("tspList", tspList);

        String sendMessage = "";
        String forward = "";

        String check = new EMSINVHDao().checkHead(inv, cusid, shipTo, ship, tsp, nontwc, zone);

        if (!check.equals("t")) {
            sendMessage = "window.setTimeout(show, 0);";
            forward = PAGE_VIEW;
            request.setAttribute("sendMessage", sendMessage);
            RequestDispatcher view = request.getRequestDispatcher(forward);
            view.forward(request, response);
        } else {

            EMSINVH head = new EMSINVH();
            head.setEMHINVID(inv);
            head.setEMHCUSID(cusid);
            head.setEMHCUSSHIPID(shipTo);
            head.setEMHSHIPID(ship);
            head.setEMHTRANSPID(tsp);
            head.setEMHNONTWC(nontwc);
            head.setEMHZONE(zone);
            head.setEMHDELNO(delno);
            head.setEMHCURRENCY(cur);
            head.setEMHUNIT(unit);
            head.setEMHSHIPDATE(shipDate);
            head.setEMHAMTCTN(ctnAmt);

            new EMSINVHDao().addHead(head, userid);

        }

        response.setHeader("Refresh", "0;/EMS2/EMS100/edit?id=" + inv + "&nontwc=" + nontwc);

    }
}
