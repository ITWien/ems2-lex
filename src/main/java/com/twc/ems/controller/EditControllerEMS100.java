/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.ems.controller;

import com.twc.ems.dao.EMSCODEDao;
import com.twc.ems.dao.EMSINVDDao;
import com.twc.ems.dao.EMSINVHDao;
import com.twc.ems.dao.EMSMASDao;
import com.twc.ems.dao.EMSINVHDao;
import com.twc.ems.dao.EMSUNITDao;
import com.twc.ems.entity.EMSCODE;
import com.twc.ems.entity.EMSINVD;
import com.twc.ems.entity.EMSINVH;
import com.twc.ems.entity.EMSMAS;
import com.twc.ems.entity.EMSINVH;
import com.twc.ems.entity.EMSUNIT;
import java.io.IOException;
import java.util.ArrayList;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import javax.servlet.http.HttpSession;

/**
 *
 * @author wien
 */
public class EditControllerEMS100 extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;
    private static final String PAGE_VIEW = "../views/editEMS100.jsp";

    public EditControllerEMS100() {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("PROGRAMNAME", "EMS100/E");
        String forward = "";
        try {
            String action = request.getParameter("action");
            if (action == null) {
                forward = PAGE_VIEW;
                request.setAttribute("PROGRAMDESC", "Invoice Information. Display");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
//    ********
        String id = request.getParameter("id");
        String nontwc = request.getParameter("nontwc");
        String noMas = request.getParameter("noMas");

        if (noMas != null) {
            if (noMas.equals("true")) {
                request.setAttribute("noMas", "window.setTimeout(show2, 0);");

                String zone = request.getParameter("zone");
                String style = request.getParameter("style");
                String cup = request.getParameter("cup");
                String size = request.getParameter("size");

                request.setAttribute("zone", zone);
                request.setAttribute("style", style);
                request.setAttribute("cup", cup);
                request.setAttribute("size", size);
            }
        }

        if (nontwc == null) {
            nontwc = "0";
        }
        request.setAttribute("nontwc", nontwc);

        EMSINVH p = new EMSINVHDao().findByCode(id, nontwc);

        request.setAttribute("EMHINVID", p.getEMHINVID());
        request.setAttribute("EMHCUSID", p.getEMHCUSID());
        request.setAttribute("EMHCUSSHIPID", p.getEMHCUSSHIPID());
        request.setAttribute("EMHSHIPID", p.getEMHSHIPID());
        request.setAttribute("EMHTRANSPID", p.getEMHTRANSPID());
        request.setAttribute("EMHCURRENCY", p.getEMHCURRENCY());
        request.setAttribute("EMHUNIT", p.getEMHUNIT());
        request.setAttribute("EMHSHIPDATE", p.getEMHSHIPDATE());
        request.setAttribute("EMHAMTCTN", p.getEMHAMTCTN());

        request.setAttribute("EMHNONTOT", p.getEMHNONTOT());
        request.setAttribute("EMHNONNET", p.getEMHNONNET());
        request.setAttribute("EMHNONGROSS", p.getEMHNONGROSS());
        request.setAttribute("EMHNONM", p.getEMHNONM());
        request.setAttribute("EMHNONDI1", p.getEMHNONDI1());
        request.setAttribute("EMHNONDI2", p.getEMHNONDI2());
        request.setAttribute("EMHNONDI3", p.getEMHNONDI3());

        if (p.getEMHNONTWC().equals("1")) {
            request.setAttribute("disno0", "display:none;");
        } else {
            request.setAttribute("disno1", "display:none;");
        }

        List<EMSINVD> aheadList = new EMSINVDDao().findByInv(id, nontwc);
        request.setAttribute("aheadList", aheadList);

        request.setAttribute("ZONE", p.getEMHZONE());

        List<EMSCODE> codeList = new EMSCODEDao().findAllByZone(p.getEMHZONE());
        request.setAttribute("codeList", codeList);

        RequestDispatcher view = request.getRequestDispatcher(forward);
        view.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
