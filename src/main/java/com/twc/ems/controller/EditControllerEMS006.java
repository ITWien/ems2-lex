/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.ems.controller;

import com.twc.ems.dao.EMSDMSDao;
import com.twc.ems.dao.EMSMASDao;
import com.twc.ems.dao.EMSDMSDao;
import com.twc.ems.entity.EMSDMS;
import com.twc.ems.entity.EMSMAS;
import com.twc.ems.entity.EMSDMS;
import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import javax.servlet.http.HttpSession;

/**
 *
 * @author wien
 */
public class EditControllerEMS006 extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;
    private static final String PAGE_VIEW = "../views/editEMS006.jsp";

    public EditControllerEMS006() {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("PROGRAMNAME", "EMS006/E");
        String forward = "";
        try {
            String action = request.getParameter("action");
            if (action == null) {
                forward = PAGE_VIEW;
                request.setAttribute("PROGRAMDESC", "Carton Dimension. Display");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
//    ********
        String zone = request.getParameter("zone");
        String box = request.getParameter("box");

        EMSDMS p = new EMSDMSDao().findByCode(zone, box);

        request.setAttribute("zone", p.getEMDSZONE());
        request.setAttribute("box", p.getEMDSBOX());
        request.setAttribute("diw", p.getEMDSWEIGHT());
        request.setAttribute("did", p.getEMDSDEEP());
        request.setAttribute("dih", p.getEMDSHEIGHT());
        request.setAttribute("boxw", p.getEMDSBOXW());

        RequestDispatcher view = request.getRequestDispatcher(forward);
        view.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");

        String zone = request.getParameter("zone");
        String box = request.getParameter("box");
        String diw = request.getParameter("diw");
        String did = request.getParameter("did");
        String dih = request.getParameter("dih");
        String boxw = request.getParameter("boxw");

        String userid = request.getParameter("userid");

        new EMSDMSDao().edit(zone, box, diw, did, dih, boxw, userid);

        response.setHeader("Refresh", "0;/EMS2/EMS006/display");
    }
}
