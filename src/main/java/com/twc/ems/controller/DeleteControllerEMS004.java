/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.ems.controller;

import com.twc.ems.dao.EMSTSPDao;
import com.twc.ems.dao.EMSMASDao;
import java.io.IOException;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author wien
 */
public class DeleteControllerEMS004 extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;

    public DeleteControllerEMS004() {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");

        String id = request.getParameter("id");

        new EMSTSPDao().delete(id);

        response.setHeader("Refresh", "0;/EMS2/EMS004/display");
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

}
