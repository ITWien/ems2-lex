/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.ems.controller;

import com.twc.ems.dao.EMSCODEDao;
import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author wien
 */
public class CreateControllerEMS008 extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;
    private static final String PAGE_VIEW = "../views/createEMS008.jsp";

    public CreateControllerEMS008() {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("PROGRAMNAME", "EMS008/C");
        String forward = "";
        try {
            String action = request.getParameter("action");
            if (action == null) {
                forward = PAGE_VIEW;
                request.setAttribute("PROGRAMDESC", "Color Code. Display ");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        String zone = request.getParameter("zone");
        String code = request.getParameter("code");
        String desc = request.getParameter("desc");

        request.setAttribute("zone", zone);
        request.setAttribute("code", code);
        request.setAttribute("desc", desc);

        RequestDispatcher view = request.getRequestDispatcher(forward);
        view.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");

        String zone = request.getParameter("zone");
        String id = request.getParameter("id");
        String desc = request.getParameter("desc");
        String userid = request.getParameter("userid");

        request.setAttribute("PROGRAMNAME", "EMS008/C");
        request.setAttribute("PROGRAMDESC", "Color Code. Display");

        String sendMessage = "";
        String forward = "";

        if (new EMSCODEDao().check(id, zone).equals("f")) {
            sendMessage = "<script type=\"text/javascript\">\n"
                    + "            var show = function () {\n"
                    + "                $('#myModal').modal('show');\n"
                    + "            };\n"
                    + "\n"
                    + "            window.setTimeout(show, 0);\n"
                    + "\n"
                    + "        </script>";
            forward = PAGE_VIEW;
            request.setAttribute("sendMessage", sendMessage);
            RequestDispatcher view = request.getRequestDispatcher(forward);
            view.forward(request, response);
        } else {
            new EMSCODEDao().add(id, zone, desc, userid);

            response.setHeader("Refresh", "0;/EMS2/EMS008/display");
        }

    }
}
