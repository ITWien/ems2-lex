/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.ems.controller;

import com.twc.ems.dao.EMSCACDao;
import com.twc.ems.dao.EMSINVHDao;
import com.twc.ems.dao.EMSMASDao;
import com.twc.ems.entity.EMSCAC;
import com.twc.ems.entity.EMSINVH;
import java.io.IOException;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author wien
 */
public class DropBox2ControllerEMS100 extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;
    private static final String PAGE_VIEW = "../views/drop2EMS100.jsp";

    public DropBox2ControllerEMS100() {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("PROGRAMNAME", "EMS100/DIST");
        String forward = "";
        try {
            String action = request.getParameter("action");
            if (action == null) {
                forward = PAGE_VIEW;
                request.setAttribute("PROGRAMDESC", "Invoice Information. Display");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
//    ********

        String inv = request.getParameter("inv");
        String dist = request.getParameter("dist");
        dist = dist.toLowerCase();

        request.setAttribute("inv", inv);

        EMSINVH p = new EMSINVHDao().findByCode(inv, "0");

        request.setAttribute("EMHINVID", p.getEMHINVID());
        request.setAttribute("EMHCUSID", p.getEMHCUSID());
        request.setAttribute("EMHCUSSHIPID", p.getEMHCUSSHIPID());
        request.setAttribute("EMHSHIPID", p.getEMHSHIPID());
        request.setAttribute("EMHTRANSPID", p.getEMHTRANSPID());
        request.setAttribute("EMHCURRENCY", p.getEMHCURRENCY());
        request.setAttribute("EMHUNIT", p.getEMHUNIT());
        request.setAttribute("EMHSHIPDATE", p.getEMHSHIPDATE());
        request.setAttribute("EMHAMTCTN", p.getEMHAMTCTN());

        List<EMSCAC> aheadList = new EMSCACDao().findByInvDrop2(inv, dist.toUpperCase());

        if (!aheadList.isEmpty()) {
            request.setAttribute("auth", aheadList.get(0).getEMCAUTH() == null ? "" : aheadList.get(0).getEMCAUTH());
        }
        String thead = "";
        String tfoot = "";

        List<String> sizeList = new EMSCACDao().findSizeList(inv, dist.toUpperCase());
        for (int i = 0; i < sizeList.size(); i++) {
            thead += "<th style=\"color:blue;\">" + sizeList.get(i) + "</th>\n";
            tfoot += "<th></th>\n";
        }

        request.setAttribute("thead", thead);
        request.setAttribute("tfoot", tfoot);

        for (int i = 0; i < aheadList.size(); i++) {
            String tbody = "";

            List<String> sizeList2 = new EMSCACDao().findSizeList(inv, dist.toUpperCase());
            for (int j = 0; j < sizeList2.size(); j++) {
                String body = "<td></td>\n";
                body = "<td title=\"Size : " + sizeList2.get(j).trim() + "\" style=\"text-align: right;\">"
                        + "<input ondblclick=\"reValue(this,'reVal-" + aheadList.get(i).getEMCCTNO() + "-" + sizeList2.get(j).trim() + "')\" "
                        + "type=\"text\" ondrop=\"dropinput(event)\" "
                        + "draggable=\"true\" ondragstart=\"drag(event)\" "
                        + "ondragover=\"allowDrop(event)\" style=\"text-align: center; cursor: pointer;\" "
                        + "id=\"" + sizeList2.get(j).trim() + ":dum:sizepcs;" + aheadList.get(i).getEMCCTNO()
                        + ";" + aheadList.get(i).getEMCSIZES()
                        + ";" + aheadList.get(i).getEMCPCS()
                        + ";" + aheadList.get(i).getBOXPACK()
                        + ";" + aheadList.get(i).getEMCSTYLE()
                        + ";" + aheadList.get(i).getEMCCUP()
                        + "\" name=\"sizepcs-" + aheadList.get(i).getEMCCTNO() + "\" "
                        + "readonly "
                        + "value=\"\">"
                        + "<input type=\"hidden\" id=\"reVal-" + aheadList.get(i).getEMCCTNO() + "-" + sizeList2.get(j).trim() + "\" "
                        + "name=\"reVal\" readonly>"
                        + "</td>\n";

                String[] size = aheadList.get(i).getEMCSIZELIST().split(";");
                for (int k = 0; k < size.length; k++) {
                    String si = size[k].split(":")[0];
                    if (si.equalsIgnoreCase(sizeList2.get(j).trim())) {
                        body = "<td title=\"Size : " + sizeList2.get(j).trim() + "\" style=\"text-align: right;\">"
                                + "<input type=\"text\" ondrop=\"dropinput(event)\" ondblclick=\"reValue(this,'reVal-" + aheadList.get(i).getEMCCTNO() + "-" + sizeList2.get(j).trim() + "')\" "
                                + "draggable=\"true\" ondragstart=\"drag(event)\" "
                                + "ondragover=\"allowDrop(event)\" style=\"text-align: center; cursor: pointer;\" "
                                + "id=\"sizepcs;" + aheadList.get(i).getEMCCTNO()
                                + ";" + aheadList.get(i).getEMCSIZES()
                                + ";" + aheadList.get(i).getEMCPCS()
                                + ";" + aheadList.get(i).getBOXPACK()
                                + ";" + aheadList.get(i).getEMCSTYLE()
                                + ";" + aheadList.get(i).getEMCCUP()
                                + "\" name=\"sizepcs-" + aheadList.get(i).getEMCCTNO() + "\" "
                                + "readonly "
                                + "value=\"" + size[k].split(":")[1] + "\">"
                                + "<input type=\"hidden\" id=\"reVal-" + aheadList.get(i).getEMCCTNO() + "-" + sizeList2.get(j).trim() + "\" "
                                + "name=\"reVal\" readonly>"
                                + "</td>\n";
                    }
                }

                tbody += body;
            }

            aheadList.get(i).setTbody(tbody);

        }
        request.setAttribute("aheadList", aheadList);

        if (dist.equalsIgnoreCase("europe")) {//****************EUROPE
            request.setAttribute("distText", dist);

        } else if (dist.equalsIgnoreCase("america") || dist.equalsIgnoreCase("america2")) {//****************AMERICA
            request.setAttribute("distText", dist);
//            request.setAttribute("disnone", "display: none;");

        } else if (dist.equals("brazil")) {//****************BRAZIL
            request.setAttribute("distText", dist);
//            request.setAttribute("disnone", "display: none;");

        } else if (dist.equalsIgnoreCase("usa-cwx")) {//****************USA-CWX
            request.setAttribute("distText", "america");
            request.setAttribute("cwx", "<img src=\"../resources/images/cwx.png\" >");
//            request.setAttribute("disnone", "display: none;");

        } else if (dist.equalsIgnoreCase("asean")) {//****************ASEAN
            request.setAttribute("distText", dist);
//            request.setAttribute("disnone", "display: none;");

        } else if (dist.equalsIgnoreCase("china")) {//****************CHINA
            request.setAttribute("distText", dist);
//            request.setAttribute("disnone", "display: none;");

        } else if (dist.equalsIgnoreCase("cwx-jpn")) {//****************CWX-JPN
            request.setAttribute("distText", "japan");
            request.setAttribute("cwx", "<img src=\"../resources/images/cwx.png\" >");
//            request.setAttribute("disnone", "display: none;");

        }

        request.setAttribute("dist", dist != null ? dist.toUpperCase() : "");

        RequestDispatcher view = request.getRequestDispatcher(forward);
        view.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        request.setCharacterEncoding("utf-8");

        String inv = request.getParameter("inv");
        String dist = request.getParameter("dist");
        String[] ctno = request.getParameterValues("ctno");
        String[] pcs = request.getParameterValues("pcs");
        String[] net = request.getParameterValues("net");
        String[] gross = request.getParameterValues("gross");
        String[] cupList = request.getParameterValues("cupList");
        String[] colorList = request.getParameterValues("colorList");
//        String[] cc = request.getParameterValues("cc");
//
//        System.out.println("ctno : " + ctno.length);
//        System.out.println("pcs : " + pcs.length);
//        System.out.println("net : " + net.length);
//        System.out.println("gross : " + gross.length);
//        System.out.println("cupList : " + cupList.length);
//        System.out.println("colorList : " + colorList.length);
//        System.out.println("cc : " + cc.length);
//        System.out.println("******");
//        for (int i = 0; i < cc.length; i++) {
//            System.out.println(cc[i]);
//        }
//        System.out.println("******");

        String userid = request.getParameter("userid");

        if (pcs != null) {
            for (int i = 0; i < pcs.length; i++) {
                String[] size = request.getParameterValues("sizepcs-" + ctno[i]);
                String sizeList = "";
                List<String> siList = new EMSCACDao().findSizeList(inv, dist.toUpperCase());

                for (int j = 0; j < size.length; j++) {
                    if (!size[j].trim().equals("")) {
                        sizeList += siList.get(j) + ":" + size[j].trim() + ";";
                    }
                }

                String flag = "N";
                if (Integer.parseInt(pcs[i]) == 0) {
                    flag = "W";
                }
                new EMSCACDao().editDropBox2(inv, ctno[i], sizeList, pcs[i], net[i], gross[i], flag, userid, dist.toUpperCase(), cupList[i], colorList[i]);
//            System.out.println(inv + " || " + ctno[i] + " || " + sizeList + " || " + pcs[i] + " || " + net[i] + " || " + gross[i] + " || " + flag);
            }
        }
//        System.out.println("*****************************");

        response.setHeader("Refresh", "0;/EMS2/EMS100/dropbox2?inv=" + inv + "&dist=" + dist);

    }

}
