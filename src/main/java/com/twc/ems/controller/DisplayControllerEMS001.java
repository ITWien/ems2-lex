/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.ems.controller;

import com.twc.ems.dao.EMSMASDao;
import com.twc.ems.entity.EMSMAS;
import java.io.IOException;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author wien
 */
public class DisplayControllerEMS001 extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;
    private static final String PAGE_VIEW = "../views/displayEMS001.jsp";

    public DisplayControllerEMS001() {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("PROGRAMNAME", "EMS001");
        String forward = "";
        try {
            String action = request.getParameter("action");
            if (action == null) {
                forward = PAGE_VIEW;
                request.setAttribute("PROGRAMDESC", "Master Detail. Display");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
//    ********
        String search = request.getParameter("search");
        if (search != null) {
            if (search.equals("true")) {
                request.setAttribute("search", "document.getElementById('searchForm').submit();");
            }
        }

        RequestDispatcher view = request.getRequestDispatcher(forward);
        view.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("PROGRAMNAME", "EMS001");
        String forward = "";
        try {
            String action = request.getParameter("action");
            if (action == null) {
                forward = PAGE_VIEW;
                request.setAttribute("PROGRAMDESC", "Master Detail. Display");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
//    ********
        String zone = request.getParameter("zone");
        String style = request.getParameter("style");
        String cup = request.getParameter("cup");
        String size = request.getParameter("size");

        List<EMSMAS> aheadList = new EMSMASDao().findAll(style, cup, size, zone);
        request.setAttribute("aheadList", aheadList);

        request.setAttribute("zone", zone);
        request.setAttribute("style", style);
        request.setAttribute("cup", cup);
        request.setAttribute("size", size);

        RequestDispatcher view = request.getRequestDispatcher(forward);
        view.forward(request, response);
    }

}
