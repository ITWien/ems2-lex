/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.ems.controller;

import com.twc.ems.dao.EMSDWTDao;
import com.twc.ems.entity.EMSDWT;
import java.io.IOException;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author wien
 */
public class DisplayControllerEMS900 extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;
    private static final String PAGE_VIEW = "../views/displayEMS900.jsp";

    public DisplayControllerEMS900() {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("PROGRAMNAME", "EMS900");
        String forward = "";
        try {
            String action = request.getParameter("action");
            if (action == null) {
                forward = PAGE_VIEW;
                request.setAttribute("PROGRAMDESC", "Download Invoice From SAP. Process");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
//    ********
        String uid = request.getParameter("uid");
        List<EMSDWT> aheadList = new EMSDWTDao().findAll(uid);
        request.setAttribute("aheadList", aheadList);

        RequestDispatcher view = request.getRequestDispatcher(forward);
        view.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

}
