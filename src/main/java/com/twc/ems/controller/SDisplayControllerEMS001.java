/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.ems.controller;

import com.twc.ems.dao.EMSMASDao;
import com.twc.ems.dao.EMSUNITDao;
import com.twc.ems.entity.EMSMAS;
import com.twc.ems.entity.EMSUNIT;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author wien
 */
public class SDisplayControllerEMS001 extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;
    private static final String PAGE_VIEW = "../views/sdisplayEMS001.jsp";
    DecimalFormat formatDou = new DecimalFormat("#,###,###,##0.00");

    public SDisplayControllerEMS001() {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("PROGRAMNAME", "EMS001/D");
        String forward = "";
        try {
            String action = request.getParameter("action");
            if (action == null) {
                forward = PAGE_VIEW;
                request.setAttribute("PROGRAMDESC", "Master Detail. Display");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
//    ********
        List<EMSUNIT> unitList = new EMSUNITDao().findAll();
        request.setAttribute("unitList", unitList);

        String zone = request.getParameter("zone");
        String style = request.getParameter("style");
        String cup = request.getParameter("cup");
        String size = request.getParameter("size");

        EMSMAS p = new EMSMASDao().findByCode(style, cup, size, zone);

        request.setAttribute("style", p.getEMMSTYLE());
        request.setAttribute("zone", p.getEMMZONE());
        request.setAttribute("cup", p.getEMMCUP());
        request.setAttribute("size", p.getEMMSIZE());
        request.setAttribute("type", p.getEMMTYPE());

        request.setAttribute("weight", p.getEMMWEIGHT());
        request.setAttribute("price", p.getEMMPRICE());
        request.setAttribute("unit", p.getEMMUNIT());
        request.setAttribute("hs", p.getEMMHSCODE());
        request.setAttribute("ran", p.getEMMRANGE());

        request.setAttribute("comp1", p.getEMMCOMPO1());
        request.setAttribute("comp2", p.getEMMCOMPO2());
        request.setAttribute("comp3", p.getEMMCOMPO3());

        request.setAttribute("box6e", p.getEMMBOXS6E());
        request.setAttribute("box6d", p.getEMMBOXS6D());
        request.setAttribute("box4e", p.getEMMBOXS4E());
        request.setAttribute("box4d", p.getEMMBOXS4D());
        request.setAttribute("box2e", p.getEMMBOXS2E());
        request.setAttribute("ppack", p.getEMMPPACK());

        request.setAttribute("wei6e", p.getEMMWG6E());
        request.setAttribute("wei6d", p.getEMMWG6D());
        request.setAttribute("wei4e", p.getEMMWG4E());
        request.setAttribute("wei4d", p.getEMMWG4D());
        request.setAttribute("wei2e", p.getEMMWG2E());

        RequestDispatcher view = request.getRequestDispatcher(forward);
        view.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

}
