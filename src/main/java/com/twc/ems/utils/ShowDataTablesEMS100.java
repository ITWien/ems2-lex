/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.ems.utils;

import com.google.gson.Gson;
import com.twc.ems.dao.EMSCACDao;
import com.twc.ems.dao.EMSINVHDao;
import com.twc.ems.entity.EMSCAC;
import com.twc.ems.entity.EMSCODE;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author nutthawoot.noo
 */
public class ShowDataTablesEMS100 extends HttpServlet {

    private Gson gson;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        PrintWriter out = response.getWriter();
        gson = new Gson();
        String json = null;

        String mode = request.getParameter("mode");

        String inv = request.getParameter("inv");
        String ctno = request.getParameter("ctno");
        String dist = request.getParameter("dist");
        String boxtype = request.getParameter("boxtype");

        String style = request.getParameter("style");
        String cup = request.getParameter("cup");
        String size = request.getParameter("size");
        String pcs = request.getParameter("pcs");

        String sizeList = request.getParameter("sizeList");
        String net = request.getParameter("net");
        String gross = request.getParameter("gross");
        String flag = request.getParameter("flag");
        String userid = request.getParameter("userid");
        String cupList = request.getParameter("cupList");
        String colorList = request.getParameter("colorList");

        String sts = request.getParameter("sts");
        String ctnFrom = request.getParameter("ctnFrom");
        String ctnTo = request.getParameter("ctnTo");
        String fgNo = request.getParameter("fgNo");

        try {
            if (mode != null) {
                if (mode.equals("editBoxtypeCac")) {
                    boolean update = new EMSCACDao().editBoxtypeCac(inv, ctno, dist, boxtype, style, cup, size, pcs);
                    json = gson.toJson(update);
                    response.getWriter().write(json);

                } else if (mode.equals("findSizeList")) {
                    List<String> siList = new EMSCACDao().findSizeList(inv, dist.toUpperCase());
                    json = gson.toJson(siList);
                    response.getWriter().write(json);

                } else if (mode.equals("editDropBox2")) {
                    boolean update = new EMSCACDao().editDropBox2(inv, ctno, sizeList, pcs, net, gross, flag, userid, dist.toUpperCase(), cupList, colorList);
                    json = gson.toJson(update);
                    response.getWriter().write(json);

                } else if (mode.equals("updateComplete")) {
                    boolean update = new EMSINVHDao().updateComplete(inv, dist, sts);
                    json = gson.toJson(update);
                    response.getWriter().write(json);

                } else if (mode.equals("updateRun")) {
                    boolean update = new EMSINVHDao().updateRun(inv, dist, sts);
                    json = gson.toJson(update);
                    response.getWriter().write(json);

                } else if (mode.equals("updateFg")) {
                    boolean update = new EMSCACDao().updateFg(inv, dist, ctnFrom, ctnTo, fgNo);
                    json = gson.toJson(update);
                    response.getWriter().write(json);

                } else if (mode.equals("getFgList")) {
                    List<EMSCAC> cacList = new EMSCACDao().getFgList(inv, dist, ctnFrom, ctnTo);
                    json = gson.toJson(cacList);
                    response.getWriter().write(json);

                } else if (mode.equals("updateNetGrossCac")) {
                    List<EMSCAC> cacList = new EMSCACDao().getCacList(inv, dist.toUpperCase());

                    for (int i = 0; i < cacList.size(); i++) {
//                        System.out.println("*************");

                        String dataZone = cacList.get(i).getEMCZONE();
                        String dataInv = cacList.get(i).getEMCINVID();
                        String dataCtno = cacList.get(i).getEMCCTNO();
                        String dataStyle = cacList.get(i).getEMCSTYLE();
                        String dataBoxType = cacList.get(i).getEMCBOXTYPE();
                        String dataGross = new EMSCACDao().getNewGrossCac(dataZone, dataBoxType);
                        String realNet = "0";
                        String realGross = "0";

                        if (cacList.get(i).getEMCCUPLIST() != null) {
                            String[] cupCac = cacList.get(i).getEMCCUPLIST().split(";");
                            Double sumNet = 0.00;
                            for (int j = 0; j < cupCac.length; j++) {

                                String dataCup = cupCac[j].split(":")[0];
                                String dataSize = cupCac[j].split("%")[1];
                                String dataPcs = cupCac[j].split("%")[0].split(":")[1];

                                String dataNet = new EMSCACDao().getNewNetCac(dataZone, dataStyle, dataCup, dataSize, dataPcs);

                                sumNet += Double.parseDouble(dataNet);

//                                System.out.println(dataZone);
//                                System.out.println(dataInv);
//                                System.out.println(dataCtno);
//                                System.out.println(dataStyle);
//                                System.out.println(dataBoxType);
//                                System.out.println(dataCup);
//                                System.out.println(dataSize);
//                                System.out.println(dataPcs);
//                                System.out.println(dataNet);
//                                System.out.println(dataGross);
//                                System.out.println("--------------");
                            }
                            realNet = Double.toString(sumNet);
                            realGross = Double.toString(sumNet + Double.parseDouble(dataGross));
//                            System.out.println(realNet);
//                            System.out.println(realGross);

                        }

//                        System.out.println("*************");
                        new EMSCACDao().updateNetGrossCac(dataInv, dataCtno, realNet, realGross, dataZone);

                    }

                    json = gson.toJson(cacList.size());
                    response.getWriter().write(json);

                } else if (mode.equals("genChild")) {
                    System.out.println("in gen child");
                    new EMSCACDao().deleteChild(inv, dist);
                    List<EMSCAC> cacList = new EMSCACDao().getParentCAC(inv, dist);

                    for (int i = 0; i < cacList.size(); i++) {
                        String cl = cacList.get(i).getEMCCUPLIST();
                        String col = cacList.get(i).getEMCCOLORLIST();
                        String sCtno = cacList.get(i).getEMCCTNO();

                        new EMSCACDao().updateParent(inv, dist, sCtno);

                        String[] c = cl.split(";");
                        String[] co = col.split(";");
                        for (int j = 0; j < c.length; j++) {
                            String sColor = co[j];
                            String sCup = c[j].split(":")[0];
                            String sSize = c[j].split("%")[1];
                            String sPcs = c[j].split(":")[1].split("%")[0];

                            new EMSCACDao().addChild(inv, dist, sCtno, sColor, sCup, sSize, sPcs);
                        }
                    }

                    json = gson.toJson(true);
                    response.getWriter().write(json);

                } else if (mode.equals("getColorMas")) {

                    String invcol = request.getParameter("invcol");

                    List<EMSCODE> color = new EMSCACDao().findColMas(invcol);

                    json = gson.toJson(color);
                    response.getWriter().write(json);
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
