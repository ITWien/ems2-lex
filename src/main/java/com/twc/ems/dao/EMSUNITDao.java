/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.ems.dao;

import com.twc.ems.database.database;
import com.twc.ems.entity.EMSMAS;
import com.twc.ems.entity.EMSUNIT;

import java.sql.*;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author nutthawoot.noo
 */
public class EMSUNITDao extends database {

    DecimalFormat formatDou = new DecimalFormat("#,###,###,##0.00");

    public List<EMSUNIT> findAll() {

        List<EMSUNIT> UAList = new ArrayList<EMSUNIT>();

        String sql = "SELECT [EMUMID]\n"
                + "      ,[EMUMDESC]\n"
                + "      ,(\n"
                + "	  SELECT [USERID]+' : '+SUBSTRING([USERS], 1, CHARINDEX(' ', [USERS])-1)\n"
                + "  FROM [RMShipment].[dbo].[MSSUSER]\n"
                + "  where [USERID] = [EMUMUSER] COLLATE Thai_CI_AS\n"
                + "	  ) AS [EMUMUSER]\n"
                + "  FROM [IEMS].[dbo].[EMSUNIT]";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                EMSUNIT p = new EMSUNIT();
//
                p.setEMUMID(result.getString("EMUMID"));
                p.setEMUMDESC(result.getString("EMUMDESC"));
                p.setEMUMUSER(result.getString("EMUMUSER"));

//
                UAList.add(p);

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public EMSUNIT findByCode(String code) {

        EMSUNIT p = new EMSUNIT();

        String sql = "SELECT [EMUMID]\n"
                + "      ,[EMUMDESC]\n"
                + "  FROM [IEMS].[dbo].[EMSUNIT]"
                + "  WHERE EMUMID = '" + code + "'";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

//
                p.setEMUMID(result.getString("EMUMID"));
                p.setEMUMDESC(result.getString("EMUMDESC"));
//
            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return p;

    }

    public String check(String code) {
        PreparedStatement ps = null;
        ResultSet rs = null;
        String STRRETURN = null;
        try {
            String check = "Select * FROM [IEMS].[dbo].[EMSUNIT] where [EMUMID] = '" + code + "' ";
            ps = connect.prepareStatement(check);
            rs = ps.executeQuery();
            if (rs.next()) {
                STRRETURN = "f";
            } else {
                STRRETURN = "t";
            }
        } catch (Exception exp) {
            exp.printStackTrace();
        }
        return STRRETURN;
    }

    public boolean add(String code, String desc, String userid) {

        boolean result = false;

        String sql = "INSERT INTO EMSUNIT"
                + " ([EMUMCOM]\n"
                + "      ,[EMUMID]\n"
                + "      ,[EMUMDESC]\n"
                + "      ,[EMUMEDT]\n"
                + "      ,[EMUMCDT]\n"
                + "      ,[EMUMUSER])"
                + " VALUES('TWC', '" + code + "', '" + desc + "'"
                + ", CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '" + userid + "')";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public boolean edit(String code, String desc, String uid) {

        boolean result = false;

        String sql = "UPDATE EMSUNIT "
                + "SET EMUMDESC = '" + desc + "'"
                + ", EMUMEDT = CURRENT_TIMESTAMP"
                + ", EMUMUSER = '" + uid + "'"
                + " WHERE EMUMID = '" + code + "'";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public void delete(String code) {

        String sql = "DELETE FROM EMSUNIT WHERE EMUMID = '" + code + "'";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ps.executeUpdate();

        } catch (Exception e) {

            e.printStackTrace();

        }

    }
}
