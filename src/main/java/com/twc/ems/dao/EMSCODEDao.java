/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.ems.dao;

import com.twc.ems.database.database;
import com.twc.ems.entity.EMSMAS;
import com.twc.ems.entity.EMSCODE;

import java.sql.*;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author nutthawoot.noo
 */
public class EMSCODEDao extends database {

    DecimalFormat formatDou = new DecimalFormat("#,###,###,##0.00");

    public List<EMSCODE> findAllByZone(String zone) {

        List<EMSCODE> UAList = new ArrayList<EMSCODE>();

        String sql = "SELECT [EMCDZONE],[EMCDCODE]\n"
                + "      ,[EMCDDESC]\n"
                + "      ,(\n"
                + "	  SELECT [USERID]+' : '+SUBSTRING([USERS], 1, CHARINDEX(' ', [USERS])-1)\n"
                + "         FROM [RMShipment].[dbo].[MSSUSER]\n"
                + "         where [USERID] = [EMCDUSER] COLLATE Thai_CI_AS\n"
                + "	  ) AS [EMCDUSER]\n"
                + "  FROM [IEMS].[dbo].[EMSCODE] "
                + "  WHERE [EMCDZONE] = '" + zone + "' "
                + "  ORDER BY EMCDZONE, EMCDCODE ";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                EMSCODE p = new EMSCODE();
//
                p.setEMCDZONE(result.getString("EMCDZONE"));
                p.setEMCDCODE(result.getString("EMCDCODE"));
                p.setEMCDDESC(result.getString("EMCDDESC"));
                p.setEMCDUSER(result.getString("EMCDUSER"));

//
                UAList.add(p);

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public List<EMSCODE> findAll() {

        List<EMSCODE> UAList = new ArrayList<EMSCODE>();

        String sql = "SELECT [EMCDZONE],[EMCDCODE]\n"
                + "      ,[EMCDDESC]\n"
                + "      ,(\n"
                + "	  SELECT [USERID]+' : '+SUBSTRING([USERS], 1, CHARINDEX(' ', [USERS])-1)\n"
                + "         FROM [RMShipment].[dbo].[MSSUSER]\n"
                + "         where [USERID] = [EMCDUSER] COLLATE Thai_CI_AS\n"
                + "	  ) AS [EMCDUSER]\n"
                + "  FROM [IEMS].[dbo].[EMSCODE] "
                + "  ORDER BY EMCDZONE, EMCDCODE ";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                EMSCODE p = new EMSCODE();
//
                p.setEMCDZONE(result.getString("EMCDZONE"));
                p.setEMCDCODE(result.getString("EMCDCODE"));
                p.setEMCDDESC(result.getString("EMCDDESC"));
                p.setEMCDUSER(result.getString("EMCDUSER"));

//
                UAList.add(p);

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public List<EMSCODE> findByCodeCheck(String code, String zone) {

        List<EMSCODE> pList = new ArrayList<EMSCODE>();

        String sql = "SELECT EMCDZONE,[EMCDCODE]\n"
                + "      ,[EMCDDESC]\n"
                + "  FROM [IEMS].[dbo].[EMSCODE]"
                + "  WHERE EMCDCODE = '" + code + "' AND EMCDZONE = '" + zone + "'";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {
                EMSCODE p = new EMSCODE();
//
                p.setEMCDZONE(result.getString("EMCDZONE"));
                p.setEMCDCODE(result.getString("EMCDCODE"));
                p.setEMCDDESC(result.getString("EMCDDESC"));

                pList.add(p);
//
            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return pList;

    }

    public EMSCODE findByCode(String code, String zone) {

        EMSCODE p = new EMSCODE();

        String sql = "SELECT EMCDZONE,[EMCDCODE]\n"
                + "      ,[EMCDDESC]\n"
                + "  FROM [IEMS].[dbo].[EMSCODE]"
                + "  WHERE EMCDCODE = '" + code + "' AND EMCDZONE = '" + zone + "'";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

//
                p.setEMCDZONE(result.getString("EMCDZONE"));
                p.setEMCDCODE(result.getString("EMCDCODE"));
                p.setEMCDDESC(result.getString("EMCDDESC"));
//
            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return p;

    }

    public String check(String code, String zone) {
        PreparedStatement ps = null;
        ResultSet rs = null;
        String STRRETURN = null;
        try {
            String check = "Select * FROM [IEMS].[dbo].[EMSCODE] where [EMCDCODE] = '" + code + "' and [EMCDZONE] = '" + zone + "' ";
            ps = connect.prepareStatement(check);
            rs = ps.executeQuery();
            if (rs.next()) {
                STRRETURN = "f";
            } else {
                STRRETURN = "t";
            }
        } catch (Exception exp) {
            exp.printStackTrace();
        }
        return STRRETURN;
    }

    public boolean add(String code, String zone, String desc, String userid) {

        boolean result = false;

        String sql = "INSERT INTO EMSCODE"
                + " ([EMCDCOM]\n"
                + "      ,[EMCDZONE]\n"
                + "      ,[EMCDCODE]\n"
                + "      ,[EMCDDESC]\n"
                + "      ,[EMCDCDT]\n"
                + "      ,[EMCDEDT]\n"
                + "      ,[EMCDUSER])"
                + " VALUES('TWC', '" + zone + "', '" + code + "', '" + desc + "'"
                + ", CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '" + userid + "')";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public boolean edit(String code, String zone, String desc, String uid) {

        boolean result = false;

        String sql = "UPDATE EMSCODE "
                + "SET EMCDDESC = '" + desc + "'"
                + ", EMCDEDT = CURRENT_TIMESTAMP"
                + ", EMCDUSER = '" + uid + "'"
                + " WHERE EMCDCODE = '" + code + "' AND EMCDZONE = '" + zone + "'";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public void delete(String code, String zone) {

        String sql = "DELETE FROM EMSCODE WHERE EMCDCODE = '" + code + "' AND EMCDZONE = '" + zone + "'";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ps.executeUpdate();

        } catch (Exception e) {

            e.printStackTrace();

        }

    }
}
