/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.ems.dao;

import com.twc.ems.database.database;
import com.twc.ems.entity.EMSMAS;
import com.twc.ems.entity.EMSTEXT;

import java.sql.*;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author nutthawoot.noo
 */
public class EMSTEXTDao extends database {

    DecimalFormat formatDou = new DecimalFormat("#,###,###,##0.00");

    public boolean copy(String oinv, String ninv, String userid) {

        boolean result = false;

        String sql = "INSERT INTO [IEMS].[dbo].[EMSTEXT] ([EMTXCOM]\n"
                + "      ,[EMTXINVNO]\n"
                + "      ,[EMTXDATE]\n"
                + "      ,[EMTXORDOF]\n"
                + "      ,[EMTXPMT]\n"
                + "      ,[EMTXTERM]\n"
                + "      ,[EMTXSUNIT]\n"
                + "      ,[EMTXSHFROM]\n"
                + "      ,[EMTXSHTO]\n"
                + "      ,[EMTXCDT]\n"
                + "      ,[EMTXEDT]\n"
                + "      ,[EMTXUSER]\n"
                + "      ,[EMTXDELADD1]\n"
                + "      ,[EMTXDELADD2]\n"
                + "      ,[EMTXDELADD3]\n"
                + "      ,[EMTXDELADD4]\n"
                + "      ,[EMTXMIDCODE]\n"
                + "      ,[EMTXMANUFAC1]\n"
                + "      ,[EMTXMANUFAC2]\n"
                + "      ,[EMTXMANUFAC3])\n"
                + "SELECT [EMTXCOM]\n"
                + "      ,'" + ninv + "'\n"
                + "      ,[EMTXDATE]\n"
                + "      ,[EMTXORDOF]\n"
                + "      ,[EMTXPMT]\n"
                + "      ,[EMTXTERM]\n"
                + "      ,[EMTXSUNIT]\n"
                + "      ,[EMTXSHFROM]\n"
                + "      ,[EMTXSHTO]\n"
                + "      ,CURRENT_TIMESTAMP\n"
                + "      ,CURRENT_TIMESTAMP\n"
                + "      ,'" + userid + "'\n"
                + "      ,[EMTXDELADD1]\n"
                + "      ,[EMTXDELADD2]\n"
                + "      ,[EMTXDELADD3]\n"
                + "      ,[EMTXDELADD4]\n"
                + "      ,[EMTXMIDCODE]\n"
                + "      ,[EMTXMANUFAC1]\n"
                + "      ,[EMTXMANUFAC2]\n"
                + "      ,[EMTXMANUFAC3]\n"
                + "  FROM [IEMS].[dbo].[EMSTEXT]\n"
                + "  WHERE [EMTXINVNO] = '" + oinv + "'";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public List<EMSTEXT> findAll() {

        List<EMSTEXT> UAList = new ArrayList<EMSTEXT>();

        String sql = "SELECT [EMTXINVNO]\n"
                + "      ,[EMTXDATE]\n"
                + "      ,[EMTXORDOF]\n"
                + "      ,[EMTXPMT]\n"
                + "      ,[EMTXTERM]\n"
                + "      ,[EMTXSUNIT]\n"
                + "      ,[EMTXSHFROM]\n"
                + "      ,[EMTXSHTO]\n"
                + "	  ,(\n"
                + "	  SELECT [USERID]+' : '+SUBSTRING([USERS], 1, CHARINDEX(' ', [USERS])-1)\n"
                + "         FROM [RMShipment].[dbo].[MSSUSER]\n"
                + "         where [USERID] = [EMTXUSER] COLLATE Thai_CI_AS\n"
                + "	  ) AS [EMTXUSER]\n"
                + "  FROM [IEMS].[dbo].[EMSTEXT]\n"
                + "  order by convert(datetime, [EMTXDATE], 107) desc ";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                EMSTEXT p = new EMSTEXT();
//
                p.setEMTXINVNO(result.getString("EMTXINVNO"));
                p.setEMTXDATE(result.getString("EMTXDATE"));
                p.setEMTXORDOF(result.getString("EMTXORDOF"));
                p.setEMTXPMT(result.getString("EMTXPMT"));
                p.setEMTXTERM(result.getString("EMTXTERM"));
                p.setEMTXSUNIT(result.getString("EMTXSUNIT"));
                p.setEMTXSHFROM(result.getString("EMTXSHFROM"));
                p.setEMTXSHTO(result.getString("EMTXSHTO"));
                p.setEMTXUSER(result.getString("EMTXUSER"));
//
                UAList.add(p);

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public EMSTEXT findByCode(String code) {

        EMSTEXT p = new EMSTEXT();

        String sql = "SELECT [EMTXINVNO]\n"
                + "      ,[EMTXDATE]\n"
                + "      ,[EMTXORDOF]\n"
                + "      ,[EMTXPMT]\n"
                + "      ,[EMTXTERM]\n"
                + "      ,[EMTXSUNIT]\n"
                + "      ,[EMTXSHFROM]\n"
                + "      ,[EMTXSHTO]\n"
                + "      ,[EMTXDELADD1]\n"
                + "      ,[EMTXDELADD2]\n"
                + "      ,[EMTXDELADD3]\n"
                + "      ,[EMTXDELADD4]\n"
                + "      ,[EMTXMIDCODE]\n"
                + "      ,[EMTXMANUFAC1]\n"
                + "      ,[EMTXMANUFAC2]\n"
                + "      ,[EMTXMANUFAC3]\n"
                + "  FROM [IEMS].[dbo].[EMSTEXT]"
                + "  WHERE EMTXINVNO = '" + code + "'";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

//
                p.setEMTXINVNO(result.getString("EMTXINVNO"));
                p.setEMTXDATE(result.getString("EMTXDATE"));
                p.setEMTXORDOF(result.getString("EMTXORDOF"));
                p.setEMTXPMT(result.getString("EMTXPMT"));
                p.setEMTXTERM(result.getString("EMTXTERM"));
                p.setEMTXSUNIT(result.getString("EMTXSUNIT"));
                p.setEMTXSHFROM(result.getString("EMTXSHFROM"));
                p.setEMTXSHTO(result.getString("EMTXSHTO"));
                p.setEMTXDELADD1(result.getString("EMTXDELADD1"));
                p.setEMTXDELADD2(result.getString("EMTXDELADD2"));
                p.setEMTXDELADD3(result.getString("EMTXDELADD3"));
                p.setEMTXDELADD4(result.getString("EMTXDELADD4"));
                p.setEMTXMIDCODE(result.getString("EMTXMIDCODE"));
                p.setEMTXMANUFAC1(result.getString("EMTXMANUFAC1"));
                p.setEMTXMANUFAC2(result.getString("EMTXMANUFAC2"));
                p.setEMTXMANUFAC3(result.getString("EMTXMANUFAC3"));
//
            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return p;

    }

    public String check(String code) {
        PreparedStatement ps = null;
        ResultSet rs = null;
        String STRRETURN = null;
        try {
            String check = "Select * FROM [IEMS].[dbo].[EMSTEXT] where [EMTXINVNO] = '" + code + "' ";
            ps = connect.prepareStatement(check);
            rs = ps.executeQuery();
            if (rs.next()) {
                STRRETURN = "f";
            } else {
                STRRETURN = "t";
            }
        } catch (Exception exp) {
            exp.printStackTrace();
        }
        return STRRETURN;
    }

    public boolean add(String inv, String date, String order, String pay, String inco, String unit, String shipF, String shipT, String userid, String delAdd1, String delAdd2, String delAdd3, String delAdd4, String midcode, String manufac1, String manufac2, String manufac3) {

        boolean result = false;

        String sql = "INSERT INTO EMSTEXT"
                + " ([EMTXCOM]\n"
                + "      ,[EMTXINVNO]\n"
                + "      ,[EMTXDATE]\n"
                + "      ,[EMTXORDOF]\n"
                + "      ,[EMTXPMT]\n"
                + "      ,[EMTXTERM]\n"
                + "      ,[EMTXSUNIT]\n"
                + "      ,[EMTXSHFROM]\n"
                + "      ,[EMTXSHTO]\n"
                + "      ,[EMTXDELADD1]\n"
                + "      ,[EMTXDELADD2]\n"
                + "      ,[EMTXDELADD3]\n"
                + "      ,[EMTXDELADD4]\n"
                + "      ,[EMTXMIDCODE]\n"
                + "      ,[EMTXMANUFAC1]\n"
                + "      ,[EMTXMANUFAC2]\n"
                + "      ,[EMTXMANUFAC3]\n"
                + "      ,[EMTXCDT]\n"
                + "      ,[EMTXEDT]\n"
                + "      ,[EMTXUSER])"
                + " VALUES('TWC'"
                + ", '" + inv + "'"
                + ", '" + date + "'"
                + ", '" + order + "'"
                + ", '" + pay + "'"
                + ", '" + inco + "'"
                + ", '" + unit + "'"
                + ", '" + shipF + "'"
                + ", '" + shipT + "'"
                + ", '" + delAdd1 + "'"
                + ", '" + delAdd2 + "'"
                + ", '" + delAdd3 + "'"
                + ", '" + delAdd4 + "'"
                + ", '" + midcode + "'"
                + ", '" + manufac1 + "'"
                + ", '" + manufac2 + "'"
                + ", '" + manufac3 + "'"
                + ", CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '" + userid + "')";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public boolean edit(String inv, String date, String order, String pay, String inco, String unit, String shipF, String shipT, String userid, String delAdd1, String delAdd2, String delAdd3, String delAdd4, String midcode, String manufac1, String manufac2, String manufac3) {

        boolean result = false;

        String sql = "UPDATE EMSTEXT "
                + "SET EMTXDATE = '" + date + "'"
                + ", EMTXORDOF = '" + order + "'"
                + ", EMTXPMT = '" + pay + "'"
                + ", EMTXTERM = '" + inco + "'"
                + ", EMTXSUNIT = '" + unit + "'"
                + ", EMTXSHFROM = '" + shipF + "'"
                + ", EMTXSHTO = '" + shipT + "'"
                + ", EMTXDELADD1 = '" + delAdd1 + "'"
                + ", EMTXDELADD2 = '" + delAdd2 + "'"
                + ", EMTXDELADD3 = '" + delAdd3 + "'"
                + ", EMTXDELADD4 = '" + delAdd4 + "'"
                + ", EMTXMIDCODE = '" + midcode + "'"
                + ", EMTXMANUFAC1 = '" + manufac1 + "'"
                + ", EMTXMANUFAC2 = '" + manufac2 + "'"
                + ", EMTXMANUFAC3 = '" + manufac3 + "'"
                + ", EMTXEDT = CURRENT_TIMESTAMP"
                + ", EMTXUSER = '" + userid + "'"
                + " WHERE EMTXINVNO = '" + inv + "'";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public void delete(String code) {

        String sql = "DELETE FROM EMSTEXT WHERE EMTXINVNO = '" + code + "'";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ps.executeUpdate();

        } catch (Exception e) {

            e.printStackTrace();

        }

    }
}
