/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.ems.dao;

import com.twc.ems.database.database;
import com.twc.ems.entity.EMSCAC;
import com.twc.ems.entity.EMSINVD;

import java.sql.*;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author nutthawoot.noo
 */
public class EMSINVDDao extends database {

    DecimalFormat formatDou = new DecimalFormat("#,###,###,##0.00");
    DecimalFormat formatDou3 = new DecimalFormat("#,###,###,##0.000");

    public List<String> findSizeList(String inv) {

        List<String> UAList = new ArrayList<String>();

        String sql = "SELECT DISTINCT [EMDSIZES]\n"
                + "  FROM [IEMS].[dbo].[EMSINVD]\n"
                + "  WHERE [EMDINVID] = '" + inv + "' AND [EMDNONTWC] = '0' AND ISNUMERIC([EMDSIZES]) = 1\n"
                + "UNION ALL\n"
                + "SELECT STUFF((\n"
                + "SELECT ',' + [EMDSIZES]\n"
                + "  FROM (\n"
                + "        SELECT DISTINCT [EMDSIZES]\n"
                + "		FROM [IEMS].[dbo].[EMSINVD]\n"
                + "		WHERE [EMDINVID] = '" + inv + "' AND [EMDNONTWC] = '0' AND ISNUMERIC([EMDSIZES]) = 0 AND LEN([EMDSIZES]) = 1\n"
                + "  )TMP\n"
                + "  ORDER BY [EMDSIZES] DESC\n"
                + "  FOR XML PATH('')), 1, 1, '')\n"
                + "UNION ALL\n"
                + "SELECT DISTINCT [EMDSIZES]\n"
                + "  FROM [IEMS].[dbo].[EMSINVD]\n"
                + "  WHERE [EMDINVID] = '" + inv + "' AND [EMDNONTWC] = '0' AND ISNUMERIC([EMDSIZES]) = 0 AND LEN([EMDSIZES]) > 1";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                String a = "";
                if (result.getString("EMDSIZES") != null) {
                    a = result.getString("EMDSIZES");
                    String[] sa = a.split(",");
                    for (int i = 0; i < sa.length; i++) {
                        UAList.add(sa[i]);
                    }
                }

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public List<EMSINVD> findByInvDrop(String inv, String zone) {

        List<EMSINVD> UAList = new ArrayList<EMSINVD>();

        String sql = "SELECT \n"
                + "XXX2.EMDINVID,	XXX2.EMDPONO,	XXX2.EMDORDID,	XXX2.EMDAUTH,	XXX2.EMDBRAND,	\n"
                + "XXX2.EMDTYPE,	XXX2.EMDSTYLE,	XXX2.EMDCOLOR,	XXX2.EMDCODE,	XXX2.EMCDDESC,	\n"
                + "XXX2.EMDCUP,	XXX2.EMDSIZES,	XXX2.EMDCOST,	XXX2.EMDAMOUNT,	XXX2.BOXPACK,	\n"
                + "XXX2.PPACK,	XXX2.BOXTYPE, CASE WHEN XXX2.BOXWEIGHT2 = '0.00' THEN XXX2.BOXWEIGHT ELSE XXX2.BOXWEIGHT2 END AS [BOXWEIGHT], XXX2.[WEIGHT]\n"
                + "from(\n"
                + "SELECT *\n"
                + ",(\n"
                + "  SELECT isnull([EMDSBOXW],0) FROM [IEMS].[dbo].[EMSDMS] \n"
                + "  WHERE [EMDSZONE] = '" + zone + "'\n"
                + "  AND [EMDSBOX] like '%'+BOXTYPE+'%'\n"
                + ") AS BOXWEIGHT\n"
                + "FROM(\n"
                + "SELECT [EMDINVID]\n"
                + "      ,[EMDPONO]\n"
                + "	  ,[EMDORDID]\n"
                + "	  ,[EMDAUTH]\n"
                + "	  ,[EMDBRAND]\n"
                + "      ,[EMDTYPE]\n"
                + "      ,[EMDSTYLE]\n"
                + "      ,[EMDCOLOR]\n"
                + "      ,isnull([EMDCODE],'') as EMDCODE\n"
                + "	  ,isnull(EMDCODE+' : '+[EMCDDESC],'') as EMCDDESC\n"
                + "      ,[EMDCUP]\n"
                + "      ,[EMDSIZES]\n"
                + "	  ,FORMAT([EMDCOST],'#,#0.00') as [EMDCOST]\n"
                + "      ,FORMAT([EMDAMOUNT],'#,#0') as [EMDAMOUNT]\n"
                + ",(\n"
                + "SELECT case when isnull([EMMBOXS6E],0) = 0 \n"
                + "       then \n"
                + "	   (\n"
                + "			case when isnull([EMMBOXS6D],0) = 0 \n"
                + "			then \n"
                + "			(\n"
                + "				case when isnull([EMMBOXS4E],0) = 0 \n"
                + "				then \n"
                + "				(\n"
                + "					case when isnull([EMMBOXS4D],0) = 0 \n"
                + "					then \n"
                + "					(\n"
                + "						case when isnull([EMMBOXS2E],0) = 0 \n"
                + "						then \n"
                + "						FORMAT(0,'#,#0')\n"
                + "						else FORMAT([EMMBOXS2E],'#,#0') end\n"
                + "					)  \n"
                + "					else FORMAT([EMMBOXS4D],'#,#0') end\n"
                + "				) \n"
                + "				else FORMAT([EMMBOXS4E],'#,#0') end\n"
                + "			)\n"
                + "			else FORMAT([EMMBOXS6D],'#,#0') end\n"
                + "	   )\n"
                + "	   else FORMAT([EMMBOXS6E],'#,#0') end AS BOXPACK\n"
                + "  FROM [IEMS].[dbo].[EMSMAS]\n"
                + "  where [EMMSTYLE] = [EMDSTYLE]\n"
                + "  and [EMMCUP] = [EMDCUP]\n"
                + "  and [EMMSIZE] = [EMDSIZES] and [EMMZONE] = '" + zone + "'\n"
                + ") AS BOXPACK\n"
                + ",(\n"
                + "SELECT ISNULL(FORMAT([EMMPPACK],'#0'),0)\n"
                + "  FROM [IEMS].[dbo].[EMSMAS]\n"
                + "  where [EMMSTYLE] = [EMDSTYLE]\n"
                + "  and [EMMCUP] = [EMDCUP]\n"
                + "  and [EMMSIZE] = [EMDSIZES] and [EMMZONE] = '" + zone + "'\n"
                + ") AS PPACK\n"
                + ",(\n"
                + "SELECT case when isnull([EMMBOXS6E],0) = 0 \n"
                + "       then \n"
                + "	   (\n"
                + "			case when isnull([EMMBOXS6D],0) = 0 \n"
                + "			then \n"
                + "			(\n"
                + "				case when isnull([EMMBOXS4E],0) = 0 \n"
                + "				then \n"
                + "				(\n"
                + "					case when isnull([EMMBOXS4D],0) = 0 \n"
                + "					then \n"
                + "					(\n"
                + "						case when isnull([EMMBOXS2E],0) = 0 \n"
                + "						then \n"
                + "						'' \n"
                + "						else '2E' end\n"
                + "					)  \n"
                + "					else '4D' end\n"
                + "				) \n"
                + "				else '4E' end\n"
                + "			)\n"
                + "			else '6D' end\n"
                + "	   )\n"
                + "	   else '6E' end AS BOXPACK\n"
                + "  FROM [IEMS].[dbo].[EMSMAS]\n"
                + "  where [EMMSTYLE] = [EMDSTYLE]\n"
                + "  and [EMMCUP] = [EMDCUP]\n"
                + "  and [EMMSIZE] = [EMDSIZES] and [EMMZONE] = '" + zone + "'\n"
                + ") AS BOXTYPE\n"
                + ",(\n"
                + "  select [EMMWEIGHT]\n"
                + "  FROM [IEMS].[dbo].[EMSMAS]\n"
                + "  where [EMMSTYLE] = [EMDSTYLE]\n"
                + "  and [EMMCUP] = [EMDCUP]\n"
                + "  and [EMMSIZE] = [EMDSIZES] and [EMMZONE] = '" + zone + "'\n"
                + ") AS [WEIGHT]\n"
                + ",(\n"
                + "SELECT case when isnull([EMMWG6E],0) = 0 \n"
                + "       then \n"
                + "	   (\n"
                + "			case when isnull([EMMWG6D],0) = 0 \n"
                + "			then \n"
                + "			(\n"
                + "				case when isnull([EMMWG4E],0) = 0 \n"
                + "				then \n"
                + "				(\n"
                + "					case when isnull([EMMWG4D],0) = 0 \n"
                + "					then \n"
                + "					(\n"
                + "						case when isnull([EMMWG2E],0) = 0 \n"
                + "						then \n"
                + "						FORMAT(0,'#,#0.00')\n"
                + "						else FORMAT([EMMWG2E],'#,#0.00') end\n"
                + "					)  \n"
                + "					else FORMAT([EMMWG4D],'#,#0.00') end\n"
                + "				) \n"
                + "				else FORMAT([EMMWG4E],'#,#0.00') end\n"
                + "			)\n"
                + "			else FORMAT([EMMWG6D],'#,#0.00') end\n"
                + "	   )\n"
                + "	   else FORMAT([EMMWG6E],'#,#0.00') end AS WG\n"
                + "  FROM [IEMS].[dbo].[EMSMAS]\n"
                + "  where [EMMSTYLE] = [EMDSTYLE]\n"
                + "  and [EMMCUP] = [EMDCUP]\n"
                + "  and [EMMSIZE] = [EMDSIZES] and [EMMZONE] = '" + zone + "'\n"
                + ") AS BOXWEIGHT2\n"
                + "FROM [IEMS].[dbo].[EMSINVD]\n"
                + "LEFT JOIN [EMSCODE] ON [EMSCODE].[EMCDCODE] = [EMSINVD].[EMDCODE] AND [EMCDZONE] = '" + zone + "'\n"
                + "WHERE [EMDINVID] = '" + inv + "' AND [EMDNONTWC] = '0'\n"
                + "AND isnull(EMDSTS,'') != 'D'\n"
                + ")XXX)XXX2\n"
                + "ORDER BY [EMDINVID],[EMDPONO],[EMDORDID],[EMDAUTH],[EMDBRAND],[EMDTYPE],[EMDSTYLE],[EMDCOLOR],[EMDCUP],[EMDSIZES]";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                int amt = 0;
                if (result.getString("EMDAMOUNT") != null) {
                    amt = Integer.parseInt(result.getString("EMDAMOUNT").replace(",", ""));
                }

                int bp = 0;
                if (result.getString("BOXPACK") != null) {
                    bp = Integer.parseInt(result.getString("BOXPACK").replace(",", ""));
                }

                for (int i = 0; i < (amt / bp); i++) {
                    EMSINVD p = new EMSINVD();

                    p.setEMDINVID(result.getString("EMDINVID"));
                    p.setEMDPONO(result.getString("EMDPONO"));
                    p.setEMDORDID(result.getString("EMDORDID"));
                    p.setEMDAUTH(result.getString("EMDAUTH"));
                    p.setEMDBRAND(result.getString("EMDBRAND"));
                    p.setEMDTYPE(result.getString("EMDTYPE"));
                    p.setEMDSTYLE(result.getString("EMDSTYLE"));
                    p.setEMDCOLOR(result.getString("EMDCOLOR"));
                    p.setEMDCUP(result.getString("EMDCUP"));
                    p.setEMDSIZES(result.getString("EMDSIZES"));
                    p.setEMDCOST(result.getString("EMDCOST"));
                    p.setEMDAMOUNT(result.getString("EMDAMOUNT"));
                    p.setBOXTYPE(result.getString("BOXTYPE"));
                    p.setPCS(Integer.toString(bp));
                    p.setFlag("Y");
                    p.setRealFlag("Y");

                    Double net = 0.00;
                    Double weight = 0.00;
                    if (result.getString("WEIGHT") != null) {
                        weight = Double.parseDouble(result.getString("WEIGHT"));
                    }

                    net = bp * weight;
                    p.setNET(formatDou.format(net));

                    Double gross = 0.00;
                    Double boxweight = 0.00;
                    if (result.getString("BOXWEIGHT") != null) {
                        boxweight = Double.parseDouble(result.getString("BOXWEIGHT"));
                    }

                    gross = net + boxweight;
                    p.setGROSS(formatDou.format(gross));

                    UAList.add(p);
                }

                if ((amt % bp) != 0) {
                    int ppack = Integer.parseInt(result.getString("PPACK"));
                    if ((amt % bp) > ppack && ppack != 0) {
                        EMSINVD p = new EMSINVD();

                        p.setEMDINVID(result.getString("EMDINVID"));
                        p.setEMDPONO(result.getString("EMDPONO"));
                        p.setEMDORDID(result.getString("EMDORDID"));
                        p.setEMDAUTH(result.getString("EMDAUTH"));
                        p.setEMDBRAND(result.getString("EMDBRAND"));
                        p.setEMDTYPE(result.getString("EMDTYPE"));
                        p.setEMDSTYLE(result.getString("EMDSTYLE"));
                        p.setEMDCOLOR(result.getString("EMDCOLOR"));
                        p.setEMDCUP(result.getString("EMDCUP"));
                        p.setEMDSIZES(result.getString("EMDSIZES"));
                        p.setEMDCOST(result.getString("EMDCOST"));
                        p.setEMDAMOUNT(result.getString("EMDAMOUNT"));
                        p.setBOXTYPE(result.getString("BOXTYPE"));
                        p.setPCS(Integer.toString((amt % bp)));
                        p.setFlag("Y");
                        p.setRealFlag("N");

                        Double net = 0.00;
                        Double weight = 0.00;
                        if (result.getString("WEIGHT") != null) {
                            weight = Double.parseDouble(result.getString("WEIGHT"));
                        }

                        net = (amt % bp) * weight;
                        p.setNET(formatDou.format(net));

                        Double gross = 0.00;
                        Double boxweight = 0.00;
                        if (result.getString("BOXWEIGHT") != null) {
                            boxweight = Double.parseDouble(result.getString("BOXWEIGHT"));
                        }

                        gross = net + boxweight;
                        p.setGROSS(formatDou.format(gross));

                        UAList.add(p);
                    } else {
                        EMSINVD p = new EMSINVD();

                        p.setEMDINVID(result.getString("EMDINVID"));
                        p.setEMDPONO(result.getString("EMDPONO"));
                        p.setEMDORDID(result.getString("EMDORDID"));
                        p.setEMDAUTH(result.getString("EMDAUTH"));
                        p.setEMDBRAND(result.getString("EMDBRAND"));
                        p.setEMDTYPE(result.getString("EMDTYPE"));
                        p.setEMDSTYLE(result.getString("EMDSTYLE"));
                        p.setEMDCOLOR(result.getString("EMDCOLOR"));
                        p.setEMDCUP(result.getString("EMDCUP"));
                        p.setEMDSIZES(result.getString("EMDSIZES"));
                        p.setEMDCOST(result.getString("EMDCOST"));
                        p.setEMDAMOUNT(result.getString("EMDAMOUNT"));
                        p.setBOXTYPE(result.getString("BOXTYPE"));
                        p.setPCS(Integer.toString((amt % bp)));
                        p.setFlag("N");
                        p.setRealFlag("N");

                        Double net = 0.00;
                        Double weight = 0.00;
                        if (result.getString("WEIGHT") != null) {
                            weight = Double.parseDouble(result.getString("WEIGHT"));
                        }

                        net = (amt % bp) * weight;
                        p.setNET(formatDou.format(net));

                        Double gross = 0.00;
                        Double boxweight = 0.00;
                        if (result.getString("BOXWEIGHT") != null) {
                            boxweight = Double.parseDouble(result.getString("BOXWEIGHT"));
                        }

                        gross = net + boxweight;
                        p.setGROSS(formatDou.format(gross));

                        UAList.add(p);
                    }
                }

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public boolean editDet(String nontwc, String inv, String opo, String po, String size, String amt, String cost, String code, String userid,
            String lotD, String authD, String brandD, String typeD, String styleD, String colorD, String codeD, String cupD) {

        boolean result = false;

        String sql = "UPDATE [IEMS].[dbo].[EMSINVD]\n"
                + "SET [EMDPONO] = '" + po + "'\n"
                + "      ,[EMDAMOUNT] = '" + amt + "'\n"
                + "      ,[EMDCOST] = '" + cost + "'\n"
                + "      ,[EMDCODE] = '" + code + "'\n"
                + "  WHERE [EMDINVID] = '" + inv + "'\n"
                + "  AND [EMDPONO] = '" + opo + "'\n"
                + "  AND [EMDORDID] = '" + lotD + "'\n"
                + "  AND [EMDAUTH] = '" + authD + "'\n"
                + "  AND [EMDBRAND] = '" + brandD + "'\n"
                + "  AND [EMDTYPE] = '" + typeD + "'\n"
                + "  AND [EMDSTYLE] = '" + styleD + "'\n"
                + "  AND [EMDCOLOR] = '" + colorD + "'\n"
                + "  AND isnull([EMDCODE],'') = '" + codeD + "'\n"
                + "  AND [EMDCUP] = '" + cupD + "'\n"
                + "  AND [EMDNONTWC] = '" + nontwc + "'\n"
                + "  AND [EMDSIZES] = '" + size + "'";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public boolean updateType(String inv, String nontwc) {

        boolean result = false;

        String sql = "UPDATE [EMSINVD]\n"
                + "SET EMDTYPE = (SELECT TOP 1 [EMMTYPE]\n"
                + "            FROM [IEMS].[dbo].[EMSMAS]\n"
                + "            WHERE [EMMZONE]=(SELECT TOP 1 EMHZONE FROM [EMSINVH] WHERE [EMHINVID]=[EMDINVID] AND [EMHNONTWC]=[EMDNONTWC])\n"
                + "            AND [EMMSTYLE]=[EMDSTYLE]\n"
                + "            AND [EMMCUP]=[EMDCUP]\n"
                + "            AND [EMMSIZE]=[EMDSIZES])\n"
                + "  WHERE [EMDINVID] = '" + inv + "' AND [EMDNONTWC] = '" + nontwc + "'";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public List<EMSINVD> findByInv(String inv, String nontwc) {

        List<EMSINVD> UAList = new ArrayList<EMSINVD>();

        String sql = "SELECT [EMDINVID]\n"
                + "      ,[EMDPONO]\n"
                + "	  ,[EMDORDID]\n"
                + "	  ,[EMDAUTH]\n"
                + "	  ,[EMDBRAND]\n"
                + "      ,[EMDTYPE]\n"
                + "      ,[EMDSTYLE]\n"
                + "      ,[EMDCOLOR]\n"
                + "      ,isnull([EMDCODE],'') as EMDCODE\n"
                + "	  ,isnull(EMDCODE+' : '+[EMCDDESC],'') as EMCDDESC\n"
                + "      ,[EMDCUP]\n"
                + "      ,[EMDSIZES]\n"
                + "	  ,FORMAT([EMDCOST],'#,#0.00') as [EMDCOST]\n"
                + "      ,FORMAT([EMDAMOUNT],'#,#0') as [EMDAMOUNT]\n"
                + "  FROM [IEMS].[dbo].[EMSINVD]\n"
                + "  LEFT JOIN [EMSINVH] ON [EMSINVH].[EMHINVID] = [EMSINVD].[EMDINVID] AND [EMSINVH].[EMHNONTWC] = [EMSINVD].[EMDNONTWC]\n"
                + "  LEFT JOIN [EMSCODE] ON [EMSCODE].[EMCDCODE] = [EMSINVD].[EMDCODE] AND [EMSCODE].[EMCDZONE] = [EMSINVH].[EMHZONE]\n"
                + "  WHERE [EMDINVID] = '" + inv + "' AND [EMDNONTWC] = '" + nontwc + "'\n"
                + "  AND isnull(EMDSTS,'') != 'D'\n"
                + "  ORDER BY [EMDINVID],[EMDPONO],[EMDORDID],[EMDAUTH],[EMDBRAND],[EMDTYPE],[EMDSTYLE],[EMDCOLOR],[EMDCUP],[EMDSIZES]";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            String po = "ST";
            String order = "ST";
            String auth = "ST";
            String brand = "ST";
            String type = "ST";
            String style = "ST";
            String color = "ST";
            String code = "ST";
            String cup = "ST";

            while (result.next()) {

                EMSINVD p = new EMSINVD();

                p.setEMDINVID(result.getString("EMDINVID"));

                if (po.equals("ST")) {
                    p.setEMDPONO(result.getString("EMDPONO"));
                    po = result.getString("EMDPONO");
                } else if (po.equals(result.getString("EMDPONO"))) {
                    p.setEMDPONO("<b style=\"opacity: 0;\">" + result.getString("EMDPONO") + "</b>");
                    po = result.getString("EMDPONO");
                } else {
                    p.setEMDPONO(result.getString("EMDPONO"));
                    po = result.getString("EMDPONO");

                    order = "ST";
                    auth = "ST";
                    brand = "ST";
                    type = "ST";
                    style = "ST";
                    color = "ST";
                    code = "ST";
                    cup = "ST";
                }

                if (order.equals("ST")) {
                    p.setEMDORDID(result.getString("EMDORDID"));
                    order = result.getString("EMDORDID");
                } else if (order.equals(result.getString("EMDORDID"))) {
                    p.setEMDORDID("<b style=\"opacity: 0;\">" + result.getString("EMDORDID") + "</b>");
                    order = result.getString("EMDORDID");
                } else {
                    p.setEMDORDID(result.getString("EMDORDID"));
                    order = result.getString("EMDORDID");

                    auth = "ST";
                    brand = "ST";
                    type = "ST";
                    style = "ST";
                    color = "ST";
                    code = "ST";
                    cup = "ST";
                }

                if (auth.equals("ST")) {
                    p.setEMDAUTH(result.getString("EMDAUTH"));
                    auth = result.getString("EMDAUTH");
                } else if (auth.equals(result.getString("EMDAUTH"))) {
                    p.setEMDAUTH("<b style=\"opacity: 0;\">" + result.getString("EMDAUTH") + "</b>");
                    auth = result.getString("EMDAUTH");
                } else {
                    p.setEMDAUTH(result.getString("EMDAUTH"));
                    auth = result.getString("EMDAUTH");

                    brand = "ST";
                    type = "ST";
                    style = "ST";
                    color = "ST";
                    code = "ST";
                    cup = "ST";
                }

                if (brand.equals("ST")) {
                    p.setEMDBRAND(result.getString("EMDBRAND"));
                    brand = result.getString("EMDBRAND");
                } else if (brand.equals(result.getString("EMDBRAND"))) {
                    p.setEMDBRAND("<b style=\"opacity: 0;\">" + result.getString("EMDBRAND") + "</b>");
                    brand = result.getString("EMDBRAND");
                } else {
                    p.setEMDBRAND(result.getString("EMDBRAND"));
                    brand = result.getString("EMDBRAND");

                    type = "ST";
                    style = "ST";
                    color = "ST";
                    code = "ST";
                    cup = "ST";
                }

                if (type.equals("ST")) {
                    p.setEMDTYPE(result.getString("EMDTYPE"));
                    type = result.getString("EMDTYPE");
                } else if (type.equals(result.getString("EMDTYPE"))) {
                    p.setEMDTYPE("<b style=\"opacity: 0;\">" + result.getString("EMDTYPE") + "</b>");
                    type = result.getString("EMDTYPE");
                } else {
                    p.setEMDTYPE(result.getString("EMDTYPE"));
                    type = result.getString("EMDTYPE");

                    style = "ST";
                    color = "ST";
                    code = "ST";
                    cup = "ST";
                }

                if (style.equals("ST")) {
                    p.setEMDSTYLE(result.getString("EMDSTYLE"));
                    style = result.getString("EMDSTYLE");
                } else if (style.equals(result.getString("EMDSTYLE"))) {
                    p.setEMDSTYLE("<b style=\"opacity: 0;\">" + result.getString("EMDSTYLE") + "</b>");
                    style = result.getString("EMDSTYLE");
                } else {
                    p.setEMDSTYLE(result.getString("EMDSTYLE"));
                    style = result.getString("EMDSTYLE");

                    color = "ST";
                    code = "ST";
                    cup = "ST";
                }

                if (color.equals("ST")) {
                    p.setEMDCOLOR(result.getString("EMDCOLOR"));
                    color = result.getString("EMDCOLOR");
                } else if (color.equals(result.getString("EMDCOLOR"))) {
                    p.setEMDCOLOR("<b style=\"opacity: 0;\">" + result.getString("EMDCOLOR") + "</b>");
                    color = result.getString("EMDCOLOR");
                } else {
                    p.setEMDCOLOR(result.getString("EMDCOLOR"));
                    color = result.getString("EMDCOLOR");

                    code = "ST";
                    cup = "ST";
                }

                if (code.equals("ST")) {
                    p.setEMDCODE(result.getString("EMCDDESC"));
                    code = result.getString("EMCDDESC");
                } else if (code.equals(result.getString("EMCDDESC"))) {
                    p.setEMDCODE("<b style=\"opacity: 0;\">" + result.getString("EMCDDESC") + "</b>");
                    code = result.getString("EMCDDESC");
                } else {
                    p.setEMDCODE(result.getString("EMCDDESC"));
                    code = result.getString("EMCDDESC");

                    cup = "ST";
                }

                if (cup.equals("ST")) {
                    p.setEMDCUP(result.getString("EMDCUP"));
                    cup = result.getString("EMDCUP");
                } else if (cup.equals(result.getString("EMDCUP"))) {
                    p.setEMDCUP("<b style=\"opacity: 0;\">" + result.getString("EMDCUP") + "</b>");
                    cup = result.getString("EMDCUP");
                } else {
                    p.setEMDCUP(result.getString("EMDCUP"));
                    cup = result.getString("EMDCUP");
                }

                p.setEMDPONOdata(result.getString("EMDPONO"));
                p.setEMDCODEdata(result.getString("EMDCODE"));
                p.setEMDCODEdata2(result.getString("EMCDDESC"));
                p.setEMDORDIDdata(result.getString("EMDORDID"));
                p.setEMDAUTHdata(result.getString("EMDAUTH"));
                p.setEMDBRANDdata(result.getString("EMDBRAND"));
                p.setEMDTYPEdata(result.getString("EMDTYPE"));
                p.setEMDSTYLEdata(result.getString("EMDSTYLE"));
                p.setEMDCOLORdata(result.getString("EMDCOLOR"));
                p.setEMDCUPdata(result.getString("EMDCUP"));
                p.setEMDSIZES(result.getString("EMDSIZES"));
                p.setEMDCOST(result.getString("EMDCOST"));
                p.setEMDAMOUNT(result.getString("EMDAMOUNT"));

                UAList.add(p);

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public EMSINVD findByCode(String code) {

        EMSINVD p = new EMSINVD();

        String sql = "SELECT [EMHINVID]\n"
                + "      ,[EMHCUSID]\n"
                + "      ,[EMHCUSSHIPID]\n"
                + "      ,[EMHSHIPID]\n"
                + "      ,[EMHTRANSPID]+' : '+[EMTPDESC] AS [EMHTRANSPID]\n"
                + "      ,[EMHCURRENCY]\n"
                + "      ,[EMHUNIT]\n"
                + "      ,(substring([EMHSHIPDATE],1,4)+'-'+substring([EMHSHIPDATE],5,2)+'-'+substring([EMHSHIPDATE],7,2)) as [EMHSHIPDATE]\n"
                + "      ,[EMHAMTCTN]\n"
                + "	  ,(\n"
                + "	    SELECT [USERID]+' : '+SUBSTRING([USERS], 1, CHARINDEX(' ', [USERS])-1)\n"
                + "        FROM [RMShipment].[dbo].[MSSUSER]\n"
                + "        where [USERID] = [EMHUSER] COLLATE Thai_CI_AS\n"
                + "       ) AS [EMHUSER]\n"
                + "  FROM [IEMS].[dbo].[EMSINVD]\n"
                + "  LEFT JOIN [EMSTSP] ON [EMSTSP].[EMTPID] = [EMSINVD].[EMHTRANSPID]\n"
                + "  WHERE EMHINVID = '" + code + "'";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

//                p.setEMHINVID(result.getString("EMHINVID"));
//                p.setEMHCUSID(result.getString("EMHCUSID"));
//                p.setEMHCUSSHIPID(result.getString("EMHCUSSHIPID"));
//                p.setEMHSHIPID(result.getString("EMHSHIPID"));
//                p.setEMHTRANSPID(result.getString("EMHTRANSPID"));
//                p.setEMHCURRENCY(result.getString("EMHCURRENCY"));
//                p.setEMHUNIT(result.getString("EMHUNIT"));
//                p.setEMHSHIPDATE(result.getString("EMHSHIPDATE"));
//                p.setEMHAMTCTN(result.getString("EMHAMTCTN"));
//                p.setEMHUSER(result.getString("EMHUSER"));
            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return p;

    }

    public String check(String code) {
        PreparedStatement ps = null;
        ResultSet rs = null;
        String STRRETURN = null;
        try {
            String check = "Select * FROM [IEMS].[dbo].[EMSINVD] where [EMSDWDELNO] = '" + code + "' ";
            ps = connect.prepareStatement(check);
            rs = ps.executeQuery();
            if (rs.next()) {
                STRRETURN = "f";
            } else {
                STRRETURN = "t";
            }
        } catch (Exception exp) {
            exp.printStackTrace();
        }
        return STRRETURN;
    }

    public boolean add(String delno, String sale, String ship, String tsp, String dist, String exp, String userid) {

        boolean result = false;

        String sql = "INSERT INTO EMSINVD"
                + " ([EMSDWCOM]\n"
                + "      ,[EMSDWDELNO]\n"
                + "      ,[EMSINVDRDT]\n"
                + "      ,[EMSDWSALEORG]\n"
                + "      ,[EMSDWSHIPMARK]\n"
                + "      ,[EMSINVDRANSPORT]\n"
                + "      ,[EMSDWDISTCH]\n"
                + "      ,[EMSDWEXP]\n"
                + "      ,[EMSDWEDT]\n"
                + "      ,[EMSDWCDT]\n"
                + "      ,[EMSDWUSER])"
                + " VALUES('TWC'"
                + ", '" + delno + "'"
                + ", CURRENT_TIMESTAMP"
                + ", '" + sale + "'"
                + ", '" + ship + "'"
                + ", '" + tsp + "'"
                + ", '" + dist + "'"
                + ", '" + exp + "'"
                + ", CURRENT_TIMESTAMP"
                + ", CURRENT_TIMESTAMP"
                + ", '" + userid + "')";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public boolean addDownload(List<EMSINVD> invDetailList, String userid, String nontwc) {

        boolean result = false;

        String sql = "INSERT INTO EMSINVD"
                + " ([EMDCOM]\n"
                + "      ,[EMDINVID]\n"
                + "      ,[EMDORDID]\n"
                + "      ,[EMDLOT]\n"
                + "      ,[EMDPONO]\n"
                + "      ,[EMDSTYLE]\n"
                + "      ,[EMDCOLOR]\n"
                + "      ,[EMDCUP]\n"
                + "      ,[EMDSIZES]\n"
                + "      ,[EMDNONTWC]\n"
                + "      ,[EMDSTYONHD]\n"
                + "      ,[EMDAUTH]\n"
                + "      ,[EMDBRAND]\n"
                + "      ,[EMDTYPE]\n"
                + "      ,[EMDAMOUNT]\n"
                + "      ,[EMDDISCOUNT]\n"
                + "      ,[EMDCOST]\n"
                + "      ,[EMDPOSHIP]\n"
                + "      ,[EMDEDT]\n"
                + "      ,[EMDCDT]\n"
                + "      ,[EMDUSER])\n";
        int idx = 0;
        for (int i = 0; i < invDetailList.size(); i++) {

            if (nontwc.equals("1")) {
                if (!invDetailList.get(i).getPLANT().trim().equals("1000")) {
                    if (idx == 0) {
                        sql += "SELECT 'TWC',"
                                + "'" + invDetailList.get(i).getEMDINVID() + "',"
                                + "'" + invDetailList.get(i).getEMDORDID() + "',"
                                + "'',"
                                + "'" + invDetailList.get(i).getEMDPONO() + "',"
                                + "'" + invDetailList.get(i).getEMDSTYLE() + "',"
                                + "'" + invDetailList.get(i).getEMDCOLOR() + "',"
                                + "'" + invDetailList.get(i).getEMDCUP() + "',"
                                + "'" + invDetailList.get(i).getEMDSIZES() + "',"
                                + "'" + nontwc + "',"
                                + "'" + invDetailList.get(i).getEMDSTYONHD() + "',"
                                + "'" + invDetailList.get(i).getEMDAUTH() + "',"
                                + "'" + invDetailList.get(i).getEMDBRAND() + "',"
                                + "'" + invDetailList.get(i).getEMDTYPE() + "',"
                                + "'" + invDetailList.get(i).getEMDAMOUNT() + "',"
                                + "'" + invDetailList.get(i).getEMDDISCOUNT() + "',"
                                + "'" + invDetailList.get(i).getEMDCOST() + "',"
                                + "'" + invDetailList.get(i).getEMDPOSHIP() + "',"
                                + "CURRENT_TIMESTAMP,CURRENT_TIMESTAMP,'" + userid + "'\n";
                    } else {
                        sql += "UNION ALL\n"
                                + "SELECT 'TWC',"
                                + "'" + invDetailList.get(i).getEMDINVID() + "',"
                                + "'" + invDetailList.get(i).getEMDORDID() + "',"
                                + "'',"
                                + "'" + invDetailList.get(i).getEMDPONO() + "',"
                                + "'" + invDetailList.get(i).getEMDSTYLE() + "',"
                                + "'" + invDetailList.get(i).getEMDCOLOR() + "',"
                                + "'" + invDetailList.get(i).getEMDCUP() + "',"
                                + "'" + invDetailList.get(i).getEMDSIZES() + "',"
                                + "'" + nontwc + "',"
                                + "'" + invDetailList.get(i).getEMDSTYONHD() + "',"
                                + "'" + invDetailList.get(i).getEMDAUTH() + "',"
                                + "'" + invDetailList.get(i).getEMDBRAND() + "',"
                                + "'" + invDetailList.get(i).getEMDTYPE() + "',"
                                + "'" + invDetailList.get(i).getEMDAMOUNT() + "',"
                                + "'" + invDetailList.get(i).getEMDDISCOUNT() + "',"
                                + "'" + invDetailList.get(i).getEMDCOST() + "',"
                                + "'" + invDetailList.get(i).getEMDPOSHIP() + "',"
                                + "CURRENT_TIMESTAMP,CURRENT_TIMESTAMP,'" + userid + "'\n";
                    }
                    idx++;
                }
            } else if (nontwc.equals("0")) {
                if (invDetailList.get(i).getPLANT().trim().equals("1000")) {
                    if (idx == 0) {
                        sql += "SELECT 'TWC',"
                                + "'" + invDetailList.get(i).getEMDINVID() + "',"
                                + "'" + invDetailList.get(i).getEMDORDID() + "',"
                                + "'',"
                                + "'" + invDetailList.get(i).getEMDPONO() + "',"
                                + "'" + invDetailList.get(i).getEMDSTYLE() + "',"
                                + "'" + invDetailList.get(i).getEMDCOLOR() + "',"
                                + "'" + invDetailList.get(i).getEMDCUP() + "',"
                                + "'" + invDetailList.get(i).getEMDSIZES() + "',"
                                + "'" + nontwc + "',"
                                + "'" + invDetailList.get(i).getEMDSTYONHD() + "',"
                                + "'" + invDetailList.get(i).getEMDAUTH() + "',"
                                + "'" + invDetailList.get(i).getEMDBRAND() + "',"
                                + "'" + invDetailList.get(i).getEMDTYPE() + "',"
                                + "'" + invDetailList.get(i).getEMDAMOUNT() + "',"
                                + "'" + invDetailList.get(i).getEMDDISCOUNT() + "',"
                                + "'" + invDetailList.get(i).getEMDCOST() + "',"
                                + "'" + invDetailList.get(i).getEMDPOSHIP() + "',"
                                + "CURRENT_TIMESTAMP,CURRENT_TIMESTAMP,'" + userid + "'\n";
                    } else {
                        sql += "UNION ALL\n"
                                + "SELECT 'TWC',"
                                + "'" + invDetailList.get(i).getEMDINVID() + "',"
                                + "'" + invDetailList.get(i).getEMDORDID() + "',"
                                + "'',"
                                + "'" + invDetailList.get(i).getEMDPONO() + "',"
                                + "'" + invDetailList.get(i).getEMDSTYLE() + "',"
                                + "'" + invDetailList.get(i).getEMDCOLOR() + "',"
                                + "'" + invDetailList.get(i).getEMDCUP() + "',"
                                + "'" + invDetailList.get(i).getEMDSIZES() + "',"
                                + "'" + nontwc + "',"
                                + "'" + invDetailList.get(i).getEMDSTYONHD() + "',"
                                + "'" + invDetailList.get(i).getEMDAUTH() + "',"
                                + "'" + invDetailList.get(i).getEMDBRAND() + "',"
                                + "'" + invDetailList.get(i).getEMDTYPE() + "',"
                                + "'" + invDetailList.get(i).getEMDAMOUNT() + "',"
                                + "'" + invDetailList.get(i).getEMDDISCOUNT() + "',"
                                + "'" + invDetailList.get(i).getEMDCOST() + "',"
                                + "'" + invDetailList.get(i).getEMDPOSHIP() + "',"
                                + "CURRENT_TIMESTAMP,CURRENT_TIMESTAMP,'" + userid + "'\n";
                    }
                    idx++;
                }
            } else {
//                System.out.println(invDetailList.get(i).getPLANT());
                if (idx == 0) {
                    sql += "SELECT 'TWC',"
                            + "'" + invDetailList.get(i).getEMDINVID() + "',"
                            + "'" + invDetailList.get(i).getEMDORDID() + "',"
                            + "'',"
                            + "'" + invDetailList.get(i).getEMDPONO() + "',"
                            + "'" + invDetailList.get(i).getEMDSTYLE() + "',"
                            + "'" + invDetailList.get(i).getEMDCOLOR() + "',"
                            + "'" + invDetailList.get(i).getEMDCUP() + "',"
                            + "'" + invDetailList.get(i).getEMDSIZES() + "',"
                            + "'0',"
                            + "'" + invDetailList.get(i).getEMDSTYONHD() + "',"
                            + "'" + invDetailList.get(i).getEMDAUTH() + "',"
                            + "'" + invDetailList.get(i).getEMDBRAND() + "',"
                            + "'" + invDetailList.get(i).getEMDTYPE() + "',"
                            + "'" + invDetailList.get(i).getEMDAMOUNT() + "',"
                            + "'" + invDetailList.get(i).getEMDDISCOUNT() + "',"
                            + "'" + invDetailList.get(i).getEMDCOST() + "',"
                            + "'" + invDetailList.get(i).getEMDPOSHIP() + "',"
                            + "CURRENT_TIMESTAMP,CURRENT_TIMESTAMP,'" + userid + "'\n";
                } else {
                    sql += "UNION ALL\n"
                            + "SELECT 'TWC',"
                            + "'" + invDetailList.get(i).getEMDINVID() + "',"
                            + "'" + invDetailList.get(i).getEMDORDID() + "',"
                            + "'',"
                            + "'" + invDetailList.get(i).getEMDPONO() + "',"
                            + "'" + invDetailList.get(i).getEMDSTYLE() + "',"
                            + "'" + invDetailList.get(i).getEMDCOLOR() + "',"
                            + "'" + invDetailList.get(i).getEMDCUP() + "',"
                            + "'" + invDetailList.get(i).getEMDSIZES() + "',"
                            + "'0',"
                            + "'" + invDetailList.get(i).getEMDSTYONHD() + "',"
                            + "'" + invDetailList.get(i).getEMDAUTH() + "',"
                            + "'" + invDetailList.get(i).getEMDBRAND() + "',"
                            + "'" + invDetailList.get(i).getEMDTYPE() + "',"
                            + "'" + invDetailList.get(i).getEMDAMOUNT() + "',"
                            + "'" + invDetailList.get(i).getEMDDISCOUNT() + "',"
                            + "'" + invDetailList.get(i).getEMDCOST() + "',"
                            + "'" + invDetailList.get(i).getEMDPOSHIP() + "',"
                            + "CURRENT_TIMESTAMP,CURRENT_TIMESTAMP,'" + userid + "'\n";
                }
                idx++;
            }
        }

//        System.out.println("Add Detail Sql : " + sql);
        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public boolean addDet(String nontwc, String inv, String po, String lot, String auth, String brand, String type, String style,
            String color, String code, String cup, String size, String cost, String amt, String userid) {

        boolean result = false;

        String sql = "INSERT INTO EMSINVD"
                + " ([EMDCOM]\n"
                + "      ,[EMDINVID]\n"
                + "      ,[EMDORDID]\n"
                + "      ,[EMDLOT]\n"
                + "      ,[EMDPONO]\n"
                + "      ,[EMDSTYLE]\n"
                + "      ,[EMDCOLOR]\n"
                + "      ,[EMDCUP]\n"
                + "      ,[EMDSIZES]\n"
                + "      ,[EMDAUTH]\n"
                + "      ,[EMDBRAND]\n"
                + "      ,[EMDTYPE]\n"
                + "      ,[EMDAMOUNT]\n"
                + "      ,[EMDCOST]\n"
                + "      ,[EMDEDT]\n"
                + "      ,[EMDCDT]\n"
                + "      ,[EMDUSER]\n"
                + "      ,[EMDNONTWC]\n"
                + "      ,[EMDCODE])"
                + " VALUES('TWC'"
                + ", '" + inv + "'"
                + ", '" + lot + "'"
                + ", ''"
                + ", '" + po + "'"
                + ", '" + style + "'"
                + ", '" + color + "'"
                + ", '" + cup + "'"
                + ", '" + size + "'"
                + ", '" + auth + "'"
                + ", '" + brand + "'"
                + ", ? "
                + ", '" + amt + "'"
                + ", '" + cost + "'"
                + ", CURRENT_TIMESTAMP"
                + ", CURRENT_TIMESTAMP"
                + ", '" + userid + "'"
                + ", '" + nontwc + "'"
                + ", '" + code + "')";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);
            ps.setString(1, type);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public boolean edit(String code, String desc, String uid) {

        boolean result = false;

        String sql = "UPDATE EMSINVD "
                + "SET EMUMDESC = '" + desc + "'"
                + ", EMUMEDT = CURRENT_TIMESTAMP"
                + ", EMUMUSER = '" + uid + "'"
                + " WHERE EMUMID = '" + code + "'";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public void delete(String code, String nontwc) {

        String sql = "DELETE FROM EMSINVD WHERE [EMDINVID] = '" + code + "' AND [EMDNONTWC] = '" + nontwc + "'";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ps.executeUpdate();

        } catch (Exception e) {

            e.printStackTrace();

        }

    }

    public void deleteDet(String nontwc, String inv, String po, String size,
            String lotD, String authD, String brandD, String typeD, String styleD, String colorD, String codeD, String cupD) {

        String sql = "DELETE EMSINVD \n"
                + "WHERE [EMDINVID] = '" + inv + "'\n"
                + "  AND [EMDPONO] = '" + po + "'\n"
                + "  AND [EMDORDID] = '" + lotD + "'\n"
                + "  AND [EMDAUTH] = '" + authD + "'\n"
                + "  AND [EMDBRAND] = '" + brandD + "'\n"
                + "  AND [EMDTYPE] = '" + typeD + "'\n"
                + "  AND [EMDSTYLE] = '" + styleD + "'\n"
                + "  AND [EMDCOLOR] = '" + colorD + "'\n"
                + "  AND isnull([EMDCODE],'') = '" + codeD + "'\n"
                + "  AND [EMDCUP] = '" + cupD + "'\n"
                + "  AND [EMDNONTWC] = '" + nontwc + "'\n"
                + "  AND [EMDSIZES] = '" + size + "'";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ps.executeUpdate();

        } catch (Exception e) {

            e.printStackTrace();

        }

    }

    public String checkDet(String inv, String po, String size,
            String order, String auth, String style, String color, String cup) {
        PreparedStatement ps = null;
        ResultSet rs = null;
        String STRRETURN = null;
        try {
            String check = "SELECT * FROM EMSINVD "
                    + "WHERE [EMDINVID] = '" + inv + "'\n"
                    + "  AND [EMDPONO] = '" + po + "'\n"
                    + "  AND [EMDORDID] = '" + order + "'\n"
                    + "  AND [EMDAUTH] = '" + auth + "'\n"
                    + "  AND [EMDSTYLE] = '" + style + "'\n"
                    + "  AND [EMDCOLOR] = '" + color + "'\n"
                    + "  AND [EMDCUP] = '" + cup + "'\n"
                    + "  AND [EMDSIZES] = '" + size + "'";
//        System.out.println(sql);
            ps = connect.prepareStatement(check);
            rs = ps.executeQuery();
            if (rs.next()) {
                STRRETURN = "f";
            } else {
                STRRETURN = "t";
            }
        } catch (Exception exp) {
            exp.printStackTrace();
        }
        return STRRETURN;
    }

    public boolean editDet2(String inv, String po, String size,
            String order, String auth, String style, String color, String cup, String pcs) {

        boolean result = false;

        String sql = "UPDATE EMSINVD \n"
                + "SET EMDAMOUNT = isnull(EMDAMOUNT,0) + " + pcs + " \n"
                + "WHERE [EMDINVID] = '" + inv + "'\n"
                + "  AND [EMDPONO] = '" + po + "'\n"
                + "  AND [EMDORDID] = '" + order + "'\n"
                + "  AND [EMDAUTH] = '" + auth + "'\n"
                + "  AND [EMDSTYLE] = '" + style + "'\n"
                + "  AND [EMDCOLOR] = '" + color + "'\n"
                + "  AND [EMDCUP] = '" + cup + "'\n"
                + "  AND [EMDSIZES] = '" + size + "'";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public boolean editDet3re(EMSCAC p) {

        boolean result = false;

        String sql = "UPDATE EMSINVD \n"
                + "SET EMDAMOUNT = isnull(EMDAMOUNT,0) + " + p.getEMCPCS() + " \n"
                + "WHERE [EMDINVID] = '" + p.getEMCINVID() + "'\n"
                + "  AND [EMDPONO] = '" + p.getEMCPONO() + "'\n"
                + "  AND [EMDORDID] = '" + p.getEMCORDID() + "'\n"
                + "  AND [EMDAUTH] = '" + p.getEMCAUTH() + "'\n"
                + "  AND [EMDSTYLE] = '" + p.getEMCSTYLE() + "'\n"
                + "  AND [EMDCOLOR] = '" + p.getEMCCOLOR() + "'\n"
                + "  AND [EMDCUP] = '" + p.getEMCCUP() + "'\n"
                + "  AND [EMDSIZES] = '" + p.getEMCSIZES() + "'";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public boolean editDet3(EMSCAC p) {

        boolean result = false;

        String sql = "UPDATE EMSINVD \n"
                + "SET EMDAMOUNT = isnull(EMDAMOUNT,0) - " + p.getEMCPCS() + " \n"
                + "WHERE [EMDINVID] = '" + p.getEMCINVID() + "'\n"
                + "  AND [EMDPONO] = '" + p.getEMCPONO() + "'\n"
                + "  AND [EMDORDID] = '" + p.getEMCORDID() + "'\n"
                + "  AND [EMDAUTH] = '" + p.getEMCAUTH() + "'\n"
                + "  AND [EMDSTYLE] = '" + p.getEMCSTYLE() + "'\n"
                + "  AND [EMDCOLOR] = '" + p.getEMCCOLOR() + "'\n"
                + "  AND [EMDCUP] = '" + p.getEMCCUP() + "'\n"
                + "  AND [EMDSIZES] = '" + p.getEMCSIZES() + "'";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public boolean editDet4(String inv, String po, String order, String auth, String style,
            String color, String cup, String size, String pcs) {

        boolean result = false;

        String sql = "UPDATE EMSINVD \n"
                + "SET EMDAMOUNT = isnull(EMDAMOUNT,0) + (" + pcs + ") \n"
                + "WHERE [EMDINVID] = '" + inv + "'\n"
                + "  AND [EMDPONO] = '" + po + "'\n"
                + "  AND [EMDORDID] = '" + order + "'\n"
                + "  AND [EMDAUTH] = '" + auth + "'\n"
                + "  AND [EMDSTYLE] = '" + style + "'\n"
                + "  AND [EMDCOLOR] = '" + color + "'\n"
                + "  AND [EMDCUP] = '" + cup + "'\n"
                + "  AND [EMDSIZES] = '" + size + "'";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }
        return result;

    }

}
