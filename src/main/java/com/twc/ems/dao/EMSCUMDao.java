/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.ems.dao;

import com.twc.ems.database.database;
import com.twc.ems.entity.EMSCUM;

import java.sql.*;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author nutthawoot.noo
 */
public class EMSCUMDao extends database {

    DecimalFormat formatDou = new DecimalFormat("#,###,###,##0.00");

    public List<EMSCUM> findAll() {

        List<EMSCUM> UAList = new ArrayList<EMSCUM>();

        String sql = "SELECT [EMCMSHIPID]\n"
                + "      ,[EMCMDESC1]\n"
                + "      ,[EMCMDESC2]\n"
                + "      ,[EMCMDESC3]\n"
                + "      ,(\n"
                + "	  SELECT [USERID]+' : '+SUBSTRING([USERS], 1, CHARINDEX(' ', [USERS])-1)\n"
                + "  FROM [RMShipment].[dbo].[MSSUSER]\n"
                + "  where [USERID] = [EMCMUSER] COLLATE Thai_CI_AS\n"
                + "	  ) AS [EMCMUSER]\n"
                + "  FROM [IEMS].[dbo].[EMSCUM]";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                EMSCUM p = new EMSCUM();
//
                p.setEMCMSHIPID(result.getString("EMCMSHIPID"));
                p.setEMCMDESC1(result.getString("EMCMDESC1"));
                p.setEMCMDESC2(result.getString("EMCMDESC2"));
                p.setEMCMDESC3(result.getString("EMCMDESC3"));
                p.setEMCMUSER(result.getString("EMCMUSER"));

//
                UAList.add(p);

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public EMSCUM findByCode(String id) {

        EMSCUM p = new EMSCUM();

        String sql = "SELECT [EMCMSHIPID]\n"
                + "      ,[EMCMDESC1]\n"
                + "      ,[EMCMDESC2]\n"
                + "      ,[EMCMDESC3]\n"
                + "      ,[EMCMDESC4]\n"
                + "      ,[EMCMDESC5]\n"
                + "  FROM [IEMS].[dbo].[EMSCUM]\n"
                + "  where [EMCMSHIPID] = '" + id + "'";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                p.setEMCMSHIPID(result.getString("EMCMSHIPID"));
                p.setEMCMDESC1(result.getString("EMCMDESC1"));
                p.setEMCMDESC2(result.getString("EMCMDESC2"));
                p.setEMCMDESC3(result.getString("EMCMDESC3"));
                p.setEMCMDESC4(result.getString("EMCMDESC4"));
                p.setEMCMDESC5(result.getString("EMCMDESC5"));
            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return p;

    }

    public String check(String id) {
        PreparedStatement ps = null;
        ResultSet rs = null;
        String STRRETURN = null;
        try {
            String check = "SELECT * FROM [IEMS].[dbo].[EMSCUM]\n"
                    + "  where [EMCMSHIPID] = '" + id + "'";
            ps = connect.prepareStatement(check);
            rs = ps.executeQuery();
            if (rs.next()) {
                STRRETURN = "f";
            } else {
                STRRETURN = "t";
            }
        } catch (Exception exp) {
            exp.printStackTrace();
        }
        return STRRETURN;
    }

    public boolean add(String id, String desc1, String desc2, String desc3, String desc4, String desc5, String userid) {

        boolean result = false;

        String sql = "INSERT INTO EMSCUM"
                + " ([EMCMCOM]\n"
                + "      ,[EMCMSHIPID]\n"
                + "      ,[EMCMDESC1]\n"
                + "      ,[EMCMDESC2]\n"
                + "      ,[EMCMDESC3]\n"
                + "      ,[EMCMDESC4]\n"
                + "      ,[EMCMDESC5]\n"
                + "      ,[EMCMEDT]\n"
                + "      ,[EMCMCDT]\n"
                + "      ,[EMCMUSER])"
                + " VALUES('TWC', '" + id + "', '" + desc1 + "', '" + desc2 + "'"
                + ", '" + desc3 + "'"
                + ", '" + desc4 + "'"
                + ", '" + desc5 + "'"
                + ", CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '" + userid + "')";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public boolean copy(String shipMark, String newShipMark, String userid) {

        boolean result = false;

        String sql = "INSERT INTO EMSCUM\n"
                + " ([EMCMCOM]\n"
                + "      ,[EMCMSHIPID]\n"
                + "      ,[EMCMDESC1]\n"
                + "      ,[EMCMDESC2]\n"
                + "      ,[EMCMDESC3]\n"
                + "      ,[EMCMDESC4]\n"
                + "      ,[EMCMDESC5]\n"
                + "      ,[EMCMEDT]\n"
                + "      ,[EMCMCDT]\n"
                + "      ,[EMCMUSER])\n"
                + "SELECT [EMCMCOM]\n"
                + "      ,'" + newShipMark + "'\n"
                + "      ,[EMCMDESC1]\n"
                + "      ,[EMCMDESC2]\n"
                + "      ,[EMCMDESC3]\n"
                + "      ,[EMCMDESC4]\n"
                + "      ,[EMCMDESC5]\n"
                + "      ,CURRENT_TIMESTAMP\n"
                + "      ,CURRENT_TIMESTAMP\n"
                + "      ,'" + userid + "'\n"
                + "FROM EMSCUM\n"
                + "WHERE EMCMSHIPID = '" + shipMark + "'\n";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public boolean edit(String id, String desc1, String desc2, String desc3, String desc4, String desc5, String userid) {

        boolean result = false;

        String sql = "UPDATE EMSCUM "
                + "SET EMCMDESC1 = '" + desc1 + "'"
                + ",EMCMDESC2 = '" + desc2 + "'"
                + ",EMCMDESC3 = '" + desc3 + "'"
                + ",EMCMDESC4 = '" + desc4 + "'"
                + ",EMCMDESC5 = '" + desc5 + "'"
                + ",EMCMEDT = CURRENT_TIMESTAMP"
                + ",EMCMUSER = '" + userid + "'"
                + " where [EMCMSHIPID] = '" + id + "'";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public void delete(String id) {

        String sql = "DELETE FROM EMSCUM "
                + "where [EMCMSHIPID] = '" + id + "'";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ps.executeUpdate();

        } catch (Exception e) {

            e.printStackTrace();

        }

    }
}
