/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.ems.dao;

import com.twc.ems.database.database;
import com.twc.ems.entity.EMSTSP;

import java.sql.*;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author nutthawoot.noo
 */
public class EMSTSPDao extends database {

    DecimalFormat formatDou = new DecimalFormat("#,###,###,##0.00");

    public List<EMSTSP> findAll() {

        List<EMSTSP> UAList = new ArrayList<EMSTSP>();

        String sql = "SELECT [EMTPID]\n"
                + "      ,[EMTPDESC]\n"
                + "      ,(\n"
                + "	  SELECT [USERID]+' : '+SUBSTRING([USERS], 1, CHARINDEX(' ', [USERS])-1)\n"
                + "  FROM [RMShipment].[dbo].[MSSUSER]\n"
                + "  where [USERID] = [EMTPUSER] COLLATE Thai_CI_AS\n"
                + "	  ) AS [EMTPUSER]\n"
                + "  FROM [IEMS].[dbo].[EMSTSP]";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                EMSTSP p = new EMSTSP();
//
                p.setEMTPID(result.getString("EMTPID"));
                p.setEMTPDESC(result.getString("EMTPDESC"));
                p.setEMTPUSER(result.getString("EMTPUSER"));

//
                UAList.add(p);

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public EMSTSP findByCode(String id) {

        EMSTSP p = new EMSTSP();

        String sql = "SELECT [EMTPID]\n"
                + "      ,[EMTPDESC]\n"
                + "  FROM [IEMS].[dbo].[EMSTSP]\n"
                + "  where EMTPID = '" + id + "'";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                p.setEMTPID(result.getString("EMTPID"));
                p.setEMTPDESC(result.getString("EMTPDESC"));
            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return p;

    }

    public String check(String id) {
        PreparedStatement ps = null;
        ResultSet rs = null;
        String STRRETURN = null;
        try {
            String check = "SELECT * FROM [IEMS].[dbo].[EMSTSP]\n"
                    + "  where [EMTPID] = '" + id + "'";
            ps = connect.prepareStatement(check);
            rs = ps.executeQuery();
            if (rs.next()) {
                STRRETURN = "f";
            } else {
                STRRETURN = "t";
            }
        } catch (Exception exp) {
            exp.printStackTrace();
        }
        return STRRETURN;
    }

    public boolean add(String id, String desc, String userid) {

        boolean result = false;

        String sql = "INSERT INTO EMSTSP"
                + " ([EMTPCOM]\n"
                + "      ,[EMTPID]\n"
                + "      ,[EMTPDESC]\n"
                + "      ,[EMTPEDT]\n"
                + "      ,[EMTPCDT]\n"
                + "      ,[EMTPUSER])"
                + " VALUES('TWC', '" + id + "', '" + desc + "'"
                + ", CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '" + userid + "')";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public boolean edit(String id, String desc, String userid) {

        boolean result = false;

        String sql = "UPDATE EMSTSP "
                + "SET EMTPDESC = '" + desc + "'"
                + ",EMTPEDT = CURRENT_TIMESTAMP"
                + ",EMTPUSER = '" + userid + "'"
                + " where [EMTPID] = '" + id + "'";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public void delete(String id) {

        String sql = "DELETE FROM EMSTSP "
                + "where [EMTPID] = '" + id + "'";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ps.executeUpdate();

        } catch (Exception e) {

            e.printStackTrace();

        }

    }
}
