/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.ems.dao;

import com.twc.ems.database.database;
import com.twc.ems.entity.EMSINVH;

import java.sql.*;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author nutthawoot.noo
 */
public class EMSINVHDao extends database {

    DecimalFormat formatDou = new DecimalFormat("#,###,###,##0.00");

    public List<EMSINVH> findAll(String uid) {

        List<EMSINVH> UAList = new ArrayList<EMSINVH>();

        if (uid == null) {
            uid = "K90270";
        }

        String sql = "SELECT [EMHINVID]\n"
                + "      ,[EMHDELNO]\n"
                + "      ,[EMHCUSID]\n"
                + "      ,[EMHCUSSHIPID]\n"
                + "      ,[EMHSHIPID]\n"
                + "      ,[EMHTRANSPID]+' : '+[EMTPDESC] AS [EMHTRANSPID]\n"
                + "      ,[EMHCURRENCY]\n"
                + "      ,[EMHUNIT]\n"
                + "      ,FORMAT([EMHCDT],'yyyy-MM-dd') AS EMHCDT\n"
                + "      ,(substring([EMHSHIPDATE],1,4)+'-'+substring([EMHSHIPDATE],5,2)+'-'+substring([EMHSHIPDATE],7,2)) as [EMHSHIPDATE]\n"
                + "      ,[EMHAMTCTN]\n"
                + "      ,[EMHNONTWC] AS NONTWC\n"
                + "      ,case when [EMHNONTWC] = 0 then 'TWC' when [EMHNONTWC] = 1 then 'NON TWC' else null end AS [EMHNONTWC]\n"
                + "	  ,(\n"
                + "	    SELECT [USERID]+' : '+SUBSTRING([USERS], 1, CHARINDEX(' ', [USERS])-1)\n"
                + "        FROM [RMShipment].[dbo].[MSSUSER]\n"
                + "        where [USERID] = [EMHUSER] COLLATE Thai_CI_AS\n"
                + "       ) AS [EMHUSER]\n"
                + "       ,EMHZONE AS EMCZONE\n"
                + "  FROM [IEMS].[dbo].[EMSINVH]\n"
                + "  LEFT JOIN [EMSTSP] ON [EMSTSP].[EMTPID] = [EMSINVH].[EMHTRANSPID]\n"
//                + "  WHERE (SELECT [MSWHSE]\n"
//                + "     FROM [RMShipment].[dbo].[MSSUSER]\n"
//                + "     where [USERID] = [EMHUSER] COLLATE Thai_CI_AS) LIKE\n"
//                + "	 case when (SELECT [MSWHSE]\n"
//                + "         FROM [RMShipment].[dbo].[MSSUSER]\n"
//                + "         where [USERID] = '" + uid + "' COLLATE Thai_CI_AS) = 'WX3' then 'WX3'\n"
//                + "     else '%%' end\n"
                + "  ORDER BY EMHCDT DESC\n";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                EMSINVH p = new EMSINVH();
//
                p.setEMHINVID(result.getString("EMHINVID"));
                p.setEMHDELNO(result.getString("EMHDELNO"));
                p.setEMHCUSID(result.getString("EMHCUSID"));
                p.setEMHCUSSHIPID(result.getString("EMHCUSSHIPID"));
                p.setEMHSHIPID(result.getString("EMHSHIPID"));
                p.setEMHTRANSPID(result.getString("EMHTRANSPID"));
                p.setEMHCURRENCY(result.getString("EMHCURRENCY"));
                p.setEMHUNIT(result.getString("EMHUNIT"));
                p.setEMHSHIPDATE(result.getString("EMHSHIPDATE"));
                p.setEMHAMTCTN(result.getString("EMHAMTCTN"));
                p.setEMHUSER(result.getString("EMHUSER"));
                p.setEMHNONTWC(result.getString("EMHNONTWC"));
                p.setEMHZONE(result.getString("EMCZONE"));
                p.setNONTWC(result.getString("NONTWC"));
                p.setEMHCDT(result.getString("EMHCDT"));

//
                UAList.add(p);

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public EMSINVH findByCode(String code, String nontwc) {

        EMSINVH p = new EMSINVH();

        String sql = "SELECT [EMHINVID]\n"
                + "      ,[EMHCUSID]\n"
                + "      ,[EMHCUSSHIPID]\n"
                + "      ,[EMHSHIPID]\n"
                + "      ,[EMHTRANSPID]+' : '+[EMTPDESC] AS [EMHTRANSPID]\n"
                + "      ,[EMHCURRENCY]\n"
                + "      ,[EMHUNIT]\n"
                + "      ,[EMHZONE]\n"
                + "      ,(substring([EMHSHIPDATE],1,4)+'-'+substring([EMHSHIPDATE],5,2)+'-'+substring([EMHSHIPDATE],7,2)) as [EMHSHIPDATE]\n"
                + "      ,[EMHAMTCTN]\n"
                + "	  ,(\n"
                + "	    SELECT [USERID]+' : '+SUBSTRING([USERS], 1, CHARINDEX(' ', [USERS])-1)\n"
                + "        FROM [RMShipment].[dbo].[MSSUSER]\n"
                + "        where [USERID] = [EMHUSER] COLLATE Thai_CI_AS\n"
                + "       ) AS [EMHUSER]\n"
                + "      ,ISNULL(EMHNONTWC,'') AS EMHNONTWC\n"
                + "      ,FORMAT([EMHNONTOT],'#,#0.000') AS [EMHNONTOT]\n"
                + "      ,FORMAT([EMHNONNET],'#,#0.000') AS [EMHNONNET]\n"
                + "      ,FORMAT([EMHNONGROSS],'#,#0.000') AS [EMHNONGROSS]\n"
                + "      ,FORMAT([EMHNONM],'#,#0.000') AS [EMHNONM]\n"
                + "      ,FORMAT([EMHNONDI1],'#,#0.000') AS [EMHNONDI1]\n"
                + "      ,FORMAT([EMHNONDI2],'#,#0.000') AS [EMHNONDI2]\n"
                + "      ,FORMAT([EMHNONDI3],'#,#0.000') AS [EMHNONDI3]\n"
                + "  FROM [IEMS].[dbo].[EMSINVH]\n"
                + "  LEFT JOIN [EMSTSP] ON [EMSTSP].[EMTPID] = [EMSINVH].[EMHTRANSPID]\n"
                + "  WHERE EMHINVID = '" + code + "' AND [EMHNONTWC] = '" + nontwc + "'";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                p.setEMHINVID(result.getString("EMHINVID"));
                p.setEMHCUSID(result.getString("EMHCUSID"));
                p.setEMHCUSSHIPID(result.getString("EMHCUSSHIPID"));
                p.setEMHSHIPID(result.getString("EMHSHIPID"));
                p.setEMHTRANSPID(result.getString("EMHTRANSPID"));
                p.setEMHCURRENCY(result.getString("EMHCURRENCY"));
                p.setEMHUNIT(result.getString("EMHUNIT"));
                p.setEMHSHIPDATE(result.getString("EMHSHIPDATE"));
                p.setEMHAMTCTN(result.getString("EMHAMTCTN"));
                p.setEMHUSER(result.getString("EMHUSER"));
                p.setEMHNONTWC(result.getString("EMHNONTWC"));

                p.setEMHNONTOT(result.getString("EMHNONTOT"));
                p.setEMHNONNET(result.getString("EMHNONNET"));
                p.setEMHNONGROSS(result.getString("EMHNONGROSS"));
                p.setEMHNONM(result.getString("EMHNONM"));
                p.setEMHNONDI1(result.getString("EMHNONDI1"));
                p.setEMHNONDI2(result.getString("EMHNONDI2"));
                p.setEMHNONDI3(result.getString("EMHNONDI3"));

                p.setEMHZONE(result.getString("EMHZONE"));

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return p;

    }

    public String checkHead(String inv, String cusid, String shipTo, String ship, String tsp, String nontwc, String zone) {
        PreparedStatement ps = null;
        ResultSet rs = null;
        String STRRETURN = null;
        try {
            String check = "SELECT *\n"
                    + "  FROM [IEMS].[dbo].[EMSINVH]\n"
                    + "  WHERE [EMHINVID] = '" + inv + "'\n"
                    + "  AND [EMHCUSID] = '" + cusid + "'\n"
                    + "  AND [EMHCUSSHIPID] = '" + shipTo + "'\n"
                    + "  AND [EMHSHIPID] = '" + ship + "'\n"
                    + "  AND [EMHTRANSPID] = '" + tsp + "'\n"
                    + "  AND [EMHNONTWC] = '" + nontwc + "'\n"
                    + "  AND [EMHZONE] = '" + zone + "'";
            ps = connect.prepareStatement(check);
            rs = ps.executeQuery();
            if (rs.next()) {
                STRRETURN = "f";
            } else {
                STRRETURN = "t";
            }
        } catch (Exception exp) {
            exp.printStackTrace();
        }
        return STRRETURN;
    }

    public String check(String code) {
        PreparedStatement ps = null;
        ResultSet rs = null;
        String STRRETURN = null;
        try {
            String check = "Select * FROM [IEMS].[dbo].[EMSINVH] where [EMSDWDELNO] = '" + code + "' ";
            ps = connect.prepareStatement(check);
            rs = ps.executeQuery();
            if (rs.next()) {
                STRRETURN = "f";
            } else {
                STRRETURN = "t";
            }
        } catch (Exception exp) {
            exp.printStackTrace();
        }
        return STRRETURN;
    }

    public boolean add(String delno, String sale, String ship, String tsp, String dist, String exp, String userid) {

        boolean result = false;

        String sql = "INSERT INTO EMSINVH"
                + " ([EMSDWCOM]\n"
                + "      ,[EMSDWDELNO]\n"
                + "      ,[EMSINVHRDT]\n"
                + "      ,[EMSDWSALEORG]\n"
                + "      ,[EMSDWSHIPMARK]\n"
                + "      ,[EMSINVHRANSPORT]\n"
                + "      ,[EMSDWDISTCH]\n"
                + "      ,[EMSDWEXP]\n"
                + "      ,[EMSDWEDT]\n"
                + "      ,[EMSDWCDT]\n"
                + "      ,[EMSDWUSER])"
                + " VALUES('TWC'"
                + ", '" + delno + "'"
                + ", CURRENT_TIMESTAMP"
                + ", '" + sale + "'"
                + ", '" + ship + "'"
                + ", '" + tsp + "'"
                + ", '" + dist + "'"
                + ", '" + exp + "'"
                + ", CURRENT_TIMESTAMP"
                + ", CURRENT_TIMESTAMP"
                + ", '" + userid + "')";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public boolean addHead(EMSINVH head, String userid) {

        boolean result = false;

        String sql = "INSERT INTO EMSINVH"
                + " ([EMHCOM]\n"
                + "      ,[EMHINVID]\n"
                + "      ,[EMHCUSID]\n"
                + "      ,[EMHCUSSHIPID]\n"
                + "      ,[EMHSHIPID]\n"
                + "      ,[EMHTRANSPID]\n"
                + "      ,[EMHNONTWC]\n"
                + "      ,[EMHZONE]\n"
                + "      ,[EMHDELNO]\n"
                + "      ,[EMHCURRENCY]\n"
                + "      ,[EMHUNIT]\n"
                + "      ,[EMHSHIPDATE]\n"
                + "      ,[EMHAMTCTN]\n"
                + "      ,[EMHEDT]\n"
                + "      ,[EMHCDT]\n"
                + "      ,[EMHUSER])"
                + " VALUES('TWC'"
                + ", '" + head.getEMHINVID() + "'"
                + ", '" + head.getEMHCUSID() + "'"
                + ", '" + head.getEMHCUSSHIPID() + "'"
                + ", '" + head.getEMHSHIPID() + "'"
                + ", '" + head.getEMHTRANSPID() + "'"
                + ", '" + head.getEMHNONTWC() + "'"
                + ", '" + head.getEMHZONE() + "'"
                + ", '" + head.getEMHDELNO() + "'"
                + ", '" + head.getEMHCURRENCY() + "'"
                + ", '" + head.getEMHUNIT() + "'"
                + ", '" + head.getEMHSHIPDATE() + "'"
                + ", '" + head.getEMHAMTCTN() + "'"
                + ", CURRENT_TIMESTAMP"
                + ", CURRENT_TIMESTAMP"
                + ", '" + userid + "')";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public boolean addDownload(EMSINVH head, String userid, String delno) {

        boolean result = false;

        String sql = "INSERT INTO EMSINVH"
                + " ([EMHCOM]\n"
                + "      ,[EMHINVID]\n"
                + "      ,[EMHCUSID]\n"
                + "      ,[EMHCUSSHIPID]\n"
                + "      ,[EMHSHIPID]\n"
                + "      ,[EMHTRANSPID]\n"
                + "      ,[EMHNONTWC]\n"
                + "      ,[EMHCURRENCY]\n"
                + "      ,[EMHUNIT]\n"
                + "      ,[EMHSHIPDATE]\n"
                + "      ,[EMHEDT]\n"
                + "      ,[EMHCDT]\n"
                + "      ,[EMHUSER]\n"
                + "      ,[EMHDELNO]\n"
                + "      ,[EMHZONE])"
                + " VALUES('TWC'"
                + ", '" + head.getEMHINVID() + "'"
                + ", '" + head.getEMHCUSID() + "'"
                + ", '" + head.getEMHCUSSHIPID() + "'"
                + ", '" + head.getEMHSHIPID() + "'"
                + ", '" + head.getEMHTRANSPID() + "'"
                + ", '" + head.getEMHNONTWC() + "'"
                + ", '" + head.getEMHCURRENCY() + "'"
                + ", '" + head.getEMHUNIT() + "'"
                + ", '" + head.getEMHSHIPDATE() + "'"
                + ", CURRENT_TIMESTAMP"
                + ", CURRENT_TIMESTAMP"
                + ", '" + userid + "'"
                + ", '" + delno + "'"
                + ", '" + head.getEMHZONE() + "')";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public boolean editCarAmt(String inv, String dist) {

        boolean result = false;

        String sql = "UPDATE EMSINVH "
                + "SET EMHAMTCTN = (SELECT CASE WHEN (SELECT TOP 1 [EMHTRANSPID] FROM [EMSINVH] WHERE EMHINVID = '" + inv + "' AND EMHZONE = '" + dist + "') = '02' \n"
                + " OR (SELECT TOP 1 [EMHTRANSPID] FROM [EMSINVH] WHERE EMHINVID = '" + inv + "' AND EMHZONE = '" + dist + "') = '03' \n"
                + "  THEN 'A' ELSE 'B' END +\n"
                + "  CAST(MIN([EMCCARTONNO]) AS NVARCHAR)+'-'+\n"
                + "  CASE WHEN (SELECT TOP 1 [EMHTRANSPID] FROM [EMSINVH] WHERE EMHINVID = '" + inv + "' AND EMHZONE = '" + dist + "') = '02' \n"
                + " OR (SELECT TOP 1 [EMHTRANSPID] FROM [EMSINVH] WHERE EMHINVID = '" + inv + "' AND EMHZONE = '" + dist + "') = '03' \n"
                + "  THEN 'A' ELSE 'B' END\n"
                + "  +CAST(MAX([EMCCARTONNO]) AS NVARCHAR)\n"
                + "  FROM EMSCAC\n"
                + "  WHERE [EMCINVID] = '" + inv + "' AND [EMCZONE] = '" + dist + "')"
                + ", EMHEDT = CURRENT_TIMESTAMP"
                + " WHERE EMHINVID = '" + inv + "' AND EMHZONE = '" + dist + "'";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public boolean updateComplete(String inv, String dist, String sts) {

        boolean result = false;

        String sql = "UPDATE EMSINVH \n"
                + "SET EMHSTS = '" + sts + "'\n"
                + "WHERE EMHINVID = '" + inv + "' AND EMHZONE = '" + dist + "'";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public boolean updateRun(String inv, String dist, String sts) {

        boolean result = false;

        String sql = "UPDATE EMSINVH \n"
                + "SET EMHRUN = '" + sts + "'\n"
                + "WHERE EMHINVID = '" + inv + "' AND EMHZONE = '" + dist + "'";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public boolean getComplete(String inv, String dist) {

        boolean res = false;

        String sql = "SELECT isnull(EMHSTS, 'N') as EMHSTS \n"
                + "FROM EMSINVH \n"
                + "WHERE EMHINVID = '" + inv + "' AND EMHZONE = '" + dist + "'";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                if (result.getString("EMHSTS").equals("Y")) {
                    res = true;
                }

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return res;

    }

    public boolean getRun(String inv, String dist) {

        boolean res = false;

        String sql = "SELECT isnull(EMHRUN, 'N') as EMHRUN \n"
                + "FROM EMSINVH \n"
                + "WHERE EMHINVID = '" + inv + "' AND EMHZONE = '" + dist + "'";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                if (result.getString("EMHRUN").equals("Y")) {
                    res = true;
                }

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return res;

    }

    public boolean edit(String code, String desc, String uid) {

        boolean result = false;

        String sql = "UPDATE EMSINVH "
                + "SET EMUMDESC = '" + desc + "'"
                + ", EMUMEDT = CURRENT_TIMESTAMP"
                + ", EMUMUSER = '" + uid + "'"
                + " WHERE EMUMID = '" + code + "'";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public boolean EditNonTWC(String inv, String total, String net, String gross, String m, String di1, String di2, String di3) {

        boolean result = false;

        String sql = "UPDATE [EMSINVH] "
                + "SET [EMHNONTOT] = '" + total + "'"
                + ", [EMHEDT] = CURRENT_TIMESTAMP"
                + ", [EMHNONNET] = '" + net + "'"
                + ", [EMHNONGROSS] = '" + gross + "'"
                + ", [EMHNONM] = '" + m + "'"
                + ", [EMHNONDI1] = '" + di1 + "'"
                + ", [EMHNONDI2] = '" + di2 + "'"
                + ", [EMHNONDI3] = '" + di3 + "'"
                + " WHERE [EMHINVID] = '" + inv + "' AND [EMHNONTWC] = '1' ";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public void delete(String code, String nontwc) {

        String sql = "DELETE FROM EMSINVH WHERE [EMHINVID] = '" + code + "' AND [EMHNONTWC] = '" + nontwc + "'";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ps.executeUpdate();

        } catch (Exception e) {

            e.printStackTrace();

        }

    }
}
