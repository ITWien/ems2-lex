/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.ems.dao;

import com.twc.ems.database.database;
import com.twc.ems.entity.EMSMAS;

import java.sql.*;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author nutthawoot.noo
 */
public class EMSMASDao extends database {

    DecimalFormat formatDou = new DecimalFormat("#,###,###,##0.00");

    public List<EMSMAS> findAll(String style, String cup, String size, String zone) {

        List<EMSMAS> UAList = new ArrayList<EMSMAS>();

        String ZN = "";
        String ST = "";
        String CP = "";
        String SZ = "";

        if (!zone.trim().equals("")) {
            if (zone.equals("AMERICA") || zone.equals("AMERICA2")) {
                ZN = "  and EMMZONE = '" + zone + "'\n";
            } else {
                ZN = "  and EMMZONE like '%" + zone + "%'\n";
            }
        }
        if (!style.trim().equals("")) {
            ST = "  and EMMSTYLE like '%" + style + "%'\n";
        }
        if (!cup.trim().equals("")) {
            CP = "  and EMMCUP like '%" + cup + "%'\n";
        }
        if (!size.trim().equals("")) {
            SZ = "  and EMMSIZE like '%" + size + "%'";
        }

        String sql = "SELECT EMMZONE,[EMMSTYLE]\n"
                + "      ,[EMMCUP]\n"
                + "      ,[EMMSIZE]\n"
                + "      ,[EMMTYPE]\n"
                + "      ,[EMMCOMPO1]\n"
                + "      ,[EMMPRICE]\n"
                + "      ,[EMMWEIGHT]\n"
                + "      ,[EMMPPACK]\n"
                + "      ,case when EMMWG6E = 0 then null else EMMWG6E end as [EMMWG6E]\n"
                + "      ,case when EMMWG6D = 0 then null else EMMWG6D end as [EMMWG6D]\n"
                + "      ,case when EMMWG4E = 0 then null else EMMWG4E end as [EMMWG4E]\n"
                + "      ,case when EMMWG4D = 0 then null else EMMWG4D end as [EMMWG4D]\n"
                + "      ,case when EMMWG2E = 0 then null else EMMWG2E end as [EMMWG2E]\n"
                + "      ,case when EMMBOXS6E = 0 then null else EMMBOXS6E end as [EMMBOXS6E]\n"
                + "      ,case when EMMBOXS6D = 0 then null else EMMBOXS6D end as [EMMBOXS6D]\n"
                + "      ,case when EMMBOXS4E = 0 then null else EMMBOXS4E end as [EMMBOXS4E]\n"
                + "      ,case when EMMBOXS4D = 0 then null else EMMBOXS4D end as [EMMBOXS4D]\n"
                + "      ,case when EMMBOXS2E = 0 then null else EMMBOXS2E end as [EMMBOXS2E]\n"
                + "      ,(\n"
                + "	  SELECT [USERID]+' : '+SUBSTRING([USERS], 1, CHARINDEX(' ', [USERS])-1)\n"
                + "  FROM [RMShipment].[dbo].[MSSUSER]\n"
                + "  where [USERID] = [EMMUSER] COLLATE Thai_CI_AS\n"
                + "	  ) AS [EMMUSER]\n"
                + "  FROM [IEMS].[dbo].[EMSMAS]\n"
                + "  where [EMMCOM] = 'TWC'\n"
                + ZN
                + ST
                + CP
                + SZ;

//        System.out.println(sql);
        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                EMSMAS p = new EMSMAS();
//
                p.setEMMZONE(result.getString("EMMZONE"));
                p.setEMMSTYLE(result.getString("EMMSTYLE"));
                p.setEMMCUP(result.getString("EMMCUP"));
                p.setEMMSIZE(result.getString("EMMSIZE"));
                p.setEMMTYPE(result.getString("EMMTYPE"));
                p.setEMMCOMPO1(result.getString("EMMCOMPO1"));
                p.setEMMPRICE(result.getString("EMMPRICE"));
                p.setEMMWEIGHT(result.getString("EMMWEIGHT"));
                p.setEMMWG6E(result.getString("EMMWG6E"));
                p.setEMMWG6D(result.getString("EMMWG6D"));
                p.setEMMWG4E(result.getString("EMMWG4E"));
                p.setEMMWG4D(result.getString("EMMWG4D"));
                p.setEMMWG2E(result.getString("EMMWG2E"));
                p.setEMMUSER(result.getString("EMMUSER"));
                p.setEMMBOXS6E(result.getString("EMMBOXS6E"));
                p.setEMMBOXS6D(result.getString("EMMBOXS6D"));
                p.setEMMBOXS4E(result.getString("EMMBOXS4E"));
                p.setEMMBOXS4D(result.getString("EMMBOXS4D"));
                p.setEMMBOXS2E(result.getString("EMMBOXS2E"));
                p.setEMMPPACK(result.getString("EMMPPACK"));

//
                UAList.add(p);

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public EMSMAS findByCode(String style, String cup, String size, String zone) {

        EMSMAS p = new EMSMAS();

        String sql = "SELECT [EMMSTYLE]\n"
                + "      ,[EMMCUP]\n"
                + "      ,[EMMZONE]\n"
                + "      ,[EMMSIZE]\n"
                + "      ,[EMMTYPE]\n"
                + "      ,[EMMCOMPO1]\n"
                + "      ,[EMMCOMPO2]\n"
                + "      ,[EMMCOMPO3]\n"
                + "      ,[EMMWEIGHT]\n"
                + "      ,[EMMPRICE]\n"
                + "      ,[EMMUNIT]\n"
                + "      ,[EMMHSCODE]\n"
                + "      ,[EMMRANGE]\n"
                + "      ,[EMMBOXS6E]\n"
                + "      ,[EMMBOXS6D]\n"
                + "      ,[EMMBOXS4E]\n"
                + "      ,[EMMBOXS4D]\n"
                + "      ,[EMMBOXS2E]\n"
                + "      ,[EMMPPACK]\n"
                + "      ,[EMMWG6E]\n"
                + "      ,[EMMWG6D]\n"
                + "      ,[EMMWG4E]\n"
                + "      ,[EMMWG4D]\n"
                + "      ,[EMMWG2E]\n"
                + "  FROM [IEMS].[dbo].[EMSMAS]\n"
                + "  where [EMMSTYLE] = '" + style + "'\n"
                + "  and [EMMZONE] = '" + zone + "'\n"
                + "  and [EMMCUP] = '" + cup + "'\n"
                + "  and [EMMSIZE] = '" + size + "'";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                p.setEMMSTYLE(result.getString("EMMSTYLE"));
                p.setEMMZONE(result.getString("EMMZONE"));
                p.setEMMCUP(result.getString("EMMCUP"));
                p.setEMMSIZE(result.getString("EMMSIZE"));
                p.setEMMTYPE(result.getString("EMMTYPE"));

                p.setEMMWEIGHT(result.getString("EMMWEIGHT"));
                p.setEMMPRICE(result.getString("EMMPRICE"));
                p.setEMMUNIT(result.getString("EMMUNIT"));
                p.setEMMHSCODE(result.getString("EMMHSCODE"));
                p.setEMMRANGE(result.getString("EMMRANGE"));

                p.setEMMCOMPO1(result.getString("EMMCOMPO1"));
                p.setEMMCOMPO2(result.getString("EMMCOMPO2"));
                p.setEMMCOMPO3(result.getString("EMMCOMPO3"));

                p.setEMMBOXS6E(result.getString("EMMBOXS6E"));
                p.setEMMBOXS6D(result.getString("EMMBOXS6D"));
                p.setEMMBOXS4E(result.getString("EMMBOXS4E"));
                p.setEMMBOXS4D(result.getString("EMMBOXS4D"));
                p.setEMMBOXS2E(result.getString("EMMBOXS2E"));
                p.setEMMPPACK(result.getString("EMMPPACK"));

                p.setEMMWG6E(result.getString("EMMWG6E"));
                p.setEMMWG6D(result.getString("EMMWG6D"));
                p.setEMMWG4E(result.getString("EMMWG4E"));
                p.setEMMWG4D(result.getString("EMMWG4D"));
                p.setEMMWG2E(result.getString("EMMWG2E"));

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return p;

    }

    public String checkGetZone(String inv, String nontwc) {
        PreparedStatement ps = null;
        ResultSet rs = null;
        String STRRETURN = null;
        try {
            String check = "SELECT TOP 1 [EMHZONE]\n"
                    + "  FROM [IEMS].[dbo].[EMSINVH]\n"
                    + "  WHERE [EMHINVID] = '" + inv + "' AND EMHNONTWC = '" + nontwc + "'";
//            System.out.println(check);
            ps = connect.prepareStatement(check);
            rs = ps.executeQuery();
            if (rs.next()) {
                STRRETURN = rs.getString("EMHZONE");
            } else {
                STRRETURN = "t";
            }
        } catch (Exception exp) {
            exp.printStackTrace();
        }
        return STRRETURN;
    }

    public String check(String style, String cup, String size, String inv, String nontwc) {
        PreparedStatement ps = null;
        ResultSet rs = null;
        String STRRETURN = null;
        try {
            String check = "SELECT * FROM [IEMS].[dbo].[EMSMAS] \n"
                    + "where [EMMSTYLE] = '" + style + "' and [EMMCUP] = '" + cup + "' and [EMMSIZE] = '" + size + "' "
                    + "and [EMMZONE] = (SELECT TOP 1 [EMHZONE]\n"
                    + "  FROM [IEMS].[dbo].[EMSINVH]\n"
                    + "  WHERE [EMHINVID] = '" + inv + "' AND EMHNONTWC = '" + nontwc + "')";
//            System.out.println(check);
            ps = connect.prepareStatement(check);
            rs = ps.executeQuery();
            if (rs.next()) {
                STRRETURN = "f";
            } else {
                STRRETURN = "t";
            }
        } catch (Exception exp) {
            exp.printStackTrace();
        }
        return STRRETURN;
    }

    public String check(String style, String cup, String size, String zone) {
        PreparedStatement ps = null;
        ResultSet rs = null;
        String STRRETURN = null;
        try {
            String check = "SELECT * FROM [IEMS].[dbo].[EMSMAS] \n"
                    + "where [EMMSTYLE] = '" + style + "' and [EMMCUP] = '" + cup + "' and [EMMSIZE] = '" + size + "' and [EMMZONE] = '" + zone + "'";
//            System.out.println(check);
            ps = connect.prepareStatement(check);
            rs = ps.executeQuery();
            if (rs.next()) {
                STRRETURN = "f";
            } else {
                STRRETURN = "t";
            }
        } catch (Exception exp) {
            exp.printStackTrace();
        }
        return STRRETURN;
    }

    public boolean add(String zone, String style, String cup, String size, String type, String weight, String price, String unit, String hs, String ran, String comp1, String comp2, String comp3, String box6, String box6d, String box4, String box4d, String box2, String ppack, String w6e, String w6d, String w4e, String w4d, String w2e, String userid) {

        boolean result = false;

        String sql = "INSERT INTO EMSMAS"
                + " ([EMMCOM]\n"
                + "      ,[EMMZONE]\n"
                + "      ,[EMMSTYLE]\n"
                + "      ,[EMMCUP]\n"
                + "      ,[EMMSIZE]\n"
                + "      ,[EMMTYPE]\n"
                + "      ,[EMMCOMPO1]\n"
                + "      ,[EMMCOMPO2]\n"
                + "      ,[EMMCOMPO3]\n"
                + "      ,[EMMWEIGHT]\n"
                + "      ,[EMMPRICE]\n"
                + "      ,[EMMUNIT]\n"
                + "      ,[EMMHSCODE]\n"
                + "      ,[EMMRANGE]\n"
                + "      ,[EMMBOXS6E]\n"
                + "      ,[EMMBOXS6D]\n"
                + "      ,[EMMBOXS4E]\n"
                + "      ,[EMMBOXS4D]\n"
                + "      ,[EMMBOXS2E]\n"
                + "      ,[EMMPPACK]\n"
                + "      ,[EMMWG6E]\n"
                + "      ,[EMMWG6D]\n"
                + "      ,[EMMWG4E]\n"
                + "      ,[EMMWG4D]\n"
                + "      ,[EMMWG2E]\n"
                + "      ,[EMMEDT]\n"
                + "      ,[EMMCDT]\n"
                + "      ,[EMMUSER])"
                + " VALUES('TWC', '" + zone + "', '" + style + "', '" + cup + "', '" + size + "'"
                + ", '" + type + "', '" + comp1 + "', '" + comp2 + "', '" + comp3 + "'"
                + ", '" + weight + "', '" + price + "', '" + unit + "', '" + hs + "', '" + ran + "', '" + box6 + "'"
                + ", '" + box6d + "', '" + box4 + "', '" + box4d + "', '" + box2 + "', '" + ppack + "'"
                + ", '" + w6e + "', '" + w6d + "', '" + w4e + "', '" + w4d + "', '" + w2e + "'"
                + ", CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '" + userid + "')";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public boolean copy(String zone, String style, String cup, String size, String zoneNew, String styleNew, String cupNew, String sizeNew, String uid) {

        boolean result = false;

        String sql = "INSERT INTO EMSMAS"
                + " ([EMMCOM]\n"
                + "      ,[EMMZONE]\n"
                + "      ,[EMMSTYLE]\n"
                + "      ,[EMMCUP]\n"
                + "      ,[EMMSIZE]\n"
                + "      ,[EMMTYPE]\n"
                + "      ,[EMMCOMPO1]\n"
                + "      ,[EMMCOMPO2]\n"
                + "      ,[EMMCOMPO3]\n"
                + "      ,[EMMWEIGHT]\n"
                + "      ,[EMMPRICE]\n"
                + "      ,[EMMUNIT]\n"
                + "      ,[EMMHSCODE]\n"
                + "      ,[EMMRANGE]\n"
                + "      ,[EMMBOXS6E]\n"
                + "      ,[EMMBOXS6D]\n"
                + "      ,[EMMBOXS4E]\n"
                + "      ,[EMMBOXS4D]\n"
                + "      ,[EMMBOXS2E]\n"
                + "      ,[EMMWG6E]\n"
                + "      ,[EMMWG6D]\n"
                + "      ,[EMMWG4E]\n"
                + "      ,[EMMWG4D]\n"
                + "      ,[EMMWG2E]\n"
                + "      ,[EMMPPACK]\n"
                + "      ,[EMMEDT]\n"
                + "      ,[EMMCDT]\n"
                + "      ,[EMMUSER])\n"
                + "SELECT [EMMCOM]\n"
                + "      ,'" + zoneNew + "'\n"
                + "      ,'" + styleNew + "'\n"
                + "      ,'" + cupNew + "'\n"
                + "      ,'" + sizeNew + "'\n"
                + "      ,[EMMTYPE]\n"
                + "      ,[EMMCOMPO1]\n"
                + "      ,[EMMCOMPO2]\n"
                + "      ,[EMMCOMPO3]\n"
                + "      ,[EMMWEIGHT]\n"
                + "      ,[EMMPRICE]\n"
                + "      ,[EMMUNIT]\n"
                + "      ,[EMMHSCODE]\n"
                + "      ,[EMMRANGE]\n"
                + "      ,[EMMBOXS6E]\n"
                + "      ,[EMMBOXS6D]\n"
                + "      ,[EMMBOXS4E]\n"
                + "      ,[EMMBOXS4D]\n"
                + "      ,[EMMBOXS2E]\n"
                + "      ,[EMMWG6E]\n"
                + "      ,[EMMWG6D]\n"
                + "      ,[EMMWG4E]\n"
                + "      ,[EMMWG4D]\n"
                + "      ,[EMMWG2E]\n"
                + "      ,[EMMPPACK]\n"
                + "      ,CURRENT_TIMESTAMP\n"
                + "      ,CURRENT_TIMESTAMP\n"
                + "      ,'" + uid + "'\n"
                + "  FROM [IEMS].[dbo].[EMSMAS]\n"
                + "  WHERE [EMMZONE] = '" + zone + "'\n"
                + "  AND [EMMSTYLE] = '" + style + "'\n"
                + "  AND [EMMCUP] = '" + cup + "'\n"
                + "  AND [EMMSIZE] = '" + size + "'\n";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public boolean copyAll(String zoneOld, String styleOld, String zoneNew, String styleNew, String uid) {

        boolean result = false;

        String sql = "INSERT INTO EMSMAS"
                + " ([EMMCOM]\n"
                + "      ,[EMMZONE]\n"
                + "      ,[EMMSTYLE]\n"
                + "      ,[EMMCUP]\n"
                + "      ,[EMMSIZE]\n"
                + "      ,[EMMTYPE]\n"
                + "      ,[EMMCOMPO1]\n"
                + "      ,[EMMCOMPO2]\n"
                + "      ,[EMMCOMPO3]\n"
                + "      ,[EMMWEIGHT]\n"
                + "      ,[EMMPRICE]\n"
                + "      ,[EMMUNIT]\n"
                + "      ,[EMMHSCODE]\n"
                + "      ,[EMMRANGE]\n"
                + "      ,[EMMBOXS6E]\n"
                + "      ,[EMMBOXS6D]\n"
                + "      ,[EMMBOXS4E]\n"
                + "      ,[EMMBOXS4D]\n"
                + "      ,[EMMBOXS2E]\n"
                + "      ,[EMMWG6E]\n"
                + "      ,[EMMWG6D]\n"
                + "      ,[EMMWG4E]\n"
                + "      ,[EMMWG4D]\n"
                + "      ,[EMMWG2E]\n"
                + "      ,[EMMPPACK]\n"
                + "      ,[EMMEDT]\n"
                + "      ,[EMMCDT]\n"
                + "      ,[EMMUSER])\n"
                + "SELECT [EMMCOM]\n"
                + "      ,'" + zoneNew + "'\n"
                + "      ,'" + styleNew + "'\n"
                + "      ,[EMMCUP]\n"
                + "      ,[EMMSIZE]\n"
                + "      ,[EMMTYPE]\n"
                + "      ,[EMMCOMPO1]\n"
                + "      ,[EMMCOMPO2]\n"
                + "      ,[EMMCOMPO3]\n"
                + "      ,[EMMWEIGHT]\n"
                + "      ,[EMMPRICE]\n"
                + "      ,[EMMUNIT]\n"
                + "      ,[EMMHSCODE]\n"
                + "      ,[EMMRANGE]\n"
                + "      ,[EMMBOXS6E]\n"
                + "      ,[EMMBOXS6D]\n"
                + "      ,[EMMBOXS4E]\n"
                + "      ,[EMMBOXS4D]\n"
                + "      ,[EMMBOXS2E]\n"
                + "      ,[EMMWG6E]\n"
                + "      ,[EMMWG6D]\n"
                + "      ,[EMMWG4E]\n"
                + "      ,[EMMWG4D]\n"
                + "      ,[EMMWG2E]\n"
                + "      ,[EMMPPACK]\n"
                + "      ,CURRENT_TIMESTAMP\n"
                + "      ,CURRENT_TIMESTAMP\n"
                + "      ,'" + uid + "'\n"
                + "  FROM [IEMS].[dbo].[EMSMAS]\n"
                + "  WHERE [EMMZONE] = '" + zoneOld + "'\n"
                + "  AND [EMMSTYLE] = '" + styleOld + "'\n";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public boolean edit(String zone, String style, String cup, String size, String type, String weight, String price, String unit, String hs, String ran, String comp1, String comp2, String comp3, String box6, String box6d, String box4, String box4d, String box2, String ppack, String w6e, String w6d, String w4e, String w4d, String w2e, String userid) {

        boolean result = false;

        String sql = "UPDATE EMSMAS "
                + "SET EMMTYPE = ?"
                + ",EMMCOMPO1 = ?"
                + ",EMMCOMPO2 = ?"
                + ",EMMCOMPO3 = ?"
                + ",EMMWEIGHT = ?"
                + ",EMMPRICE = ?"
                + ",EMMUNIT = ?"
                + ",EMMHSCODE = ?"
                + ",EMMRANGE = ?"
                + ",EMMBOXS6E = ?"
                + ",EMMBOXS6D = ?"
                + ",EMMBOXS4E = ?"
                + ",EMMBOXS4D = ?"
                + ",EMMBOXS2E = ?"
                + ",EMMPPACK = ?"
                + ",EMMWG6E = ?"
                + ",EMMWG6D = ?"
                + ",EMMWG4E = ?"
                + ",EMMWG4D = ?"
                + ",EMMWG2E = ?"
                + ",EMMEDT = CURRENT_TIMESTAMP"
                + ",EMMUSER = ?"
                + " where [EMMSTYLE] = ?"
                + "  and [EMMZONE] = ?"
                + "  and [EMMCUP] = ?"
                + "  and [EMMSIZE] = ?";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ps.setString(1, type);
            ps.setString(2, comp1);
            ps.setString(3, comp2);
            ps.setString(4, comp3);
            ps.setString(5, weight);
            ps.setString(6, price);
            ps.setString(7, unit);
            ps.setString(8, hs);
            ps.setString(9, ran);
            ps.setString(10, box6);
            ps.setString(11, box6d);
            ps.setString(12, box4);
            ps.setString(13, box4d);
            ps.setString(14, box2);
            ps.setString(15, ppack);
            ps.setString(16, w6e);
            ps.setString(17, w6d);
            ps.setString(18, w4e);
            ps.setString(19, w4d);
            ps.setString(20, w2e);
            ps.setString(21, userid);
            ps.setString(22, style);
            ps.setString(23, zone);
            ps.setString(24, cup);
            ps.setString(25, size);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public void delete(String style, String cup, String size, String zone) {

        String sql = "DELETE FROM EMSMAS "
                + "where [EMMSTYLE] = '" + style + "'\n"
                + "  and [EMMZONE] = '" + zone + "'\n"
                + "  and [EMMCUP] = '" + cup + "'\n"
                + "  and [EMMSIZE] = '" + size + "'";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ps.executeUpdate();

        } catch (Exception e) {

            e.printStackTrace();

        }

    }
}
