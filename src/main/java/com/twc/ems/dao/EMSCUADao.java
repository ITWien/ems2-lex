/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.ems.dao;

import com.twc.ems.database.database;
import com.twc.ems.entity.EMSCUA;

import java.sql.*;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author nutthawoot.noo
 */
public class EMSCUADao extends database {

    DecimalFormat formatDou = new DecimalFormat("#,###,###,##0.00");

    public List<EMSCUA> findAll() {

        List<EMSCUA> UAList = new ArrayList<EMSCUA>();

        String sql = "SELECT [EMCACUSID]\n"
                + "      ,[EMCAADD1]\n"
                + "      ,[EMCAADD2]\n"
                + "      ,[EMCAADD3]\n"
                + "      ,(\n"
                + "	  SELECT [USERID]+' : '+SUBSTRING([USERS], 1, CHARINDEX(' ', [USERS])-1)\n"
                + "  FROM [RMShipment].[dbo].[MSSUSER]\n"
                + "  where [USERID] = [EMCAUSER] COLLATE Thai_CI_AS\n"
                + "	  ) AS [EMCAUSER]\n"
                + "  FROM [IEMS].[dbo].[EMSCUA]";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                EMSCUA p = new EMSCUA();
//
                p.setEMCACUSID(result.getString("EMCACUSID"));
                p.setEMCAADD1(result.getString("EMCAADD1"));
                p.setEMCAADD2(result.getString("EMCAADD2"));
                p.setEMCAADD3(result.getString("EMCAADD3"));
                p.setEMCAUSER(result.getString("EMCAUSER"));

//
                UAList.add(p);

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public EMSCUA findByCode(String id) {

        EMSCUA p = new EMSCUA();

        String sql = "SELECT [EMCACUSID]\n"
                + "      ,[EMCAADD1]\n"
                + "      ,[EMCAADD2]\n"
                + "      ,[EMCAADD3]\n"
                + "      ,[EMCAADD4]\n"
                + "      ,[EMCAADD5]\n"
                + "  FROM [IEMS].[dbo].[EMSCUA]\n"
                + "  where [EMCACUSID] = '" + id + "'";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                p.setEMCACUSID(result.getString("EMCACUSID"));
                p.setEMCAADD1(result.getString("EMCAADD1"));
                p.setEMCAADD2(result.getString("EMCAADD2"));
                p.setEMCAADD3(result.getString("EMCAADD3"));
                p.setEMCAADD4(result.getString("EMCAADD4"));
                p.setEMCAADD5(result.getString("EMCAADD5"));

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return p;

    }

    public String check(String id) {
        PreparedStatement ps = null;
        ResultSet rs = null;
        String STRRETURN = null;
        try {
            String check = "SELECT * FROM [IEMS].[dbo].[EMSCUA]\n"
                    + "  where [EMCACUSID] = '" + id + "'";
            ps = connect.prepareStatement(check);
            rs = ps.executeQuery();
            if (rs.next()) {
                STRRETURN = "f";
            } else {
                STRRETURN = "t";
            }
        } catch (Exception exp) {
            exp.printStackTrace();
        }
        return STRRETURN;
    }

    public boolean add(String id, String name, String adr1, String adr2, String adr3, String adr4, String userid) {

        boolean result = false;

        String sql = "INSERT INTO EMSCUA"
                + " ([EMCACOM]\n"
                + "      ,[EMCACUSID]\n"
                + "      ,[EMCAADD1]\n"
                + "      ,[EMCAADD2]\n"
                + "      ,[EMCAADD3]\n"
                + "      ,[EMCAADD4]\n"
                + "      ,[EMCAADD5]\n"
                + "      ,[EMCAEDT]\n"
                + "      ,[EMCACDT]\n"
                + "      ,[EMCAUSER])"
                + " VALUES('TWC', '" + id + "', '" + name + "', '" + adr1 + "'"
                + ", '" + adr2 + "'"
                + ", '" + adr3 + "'"
                + ", '" + adr4 + "'"
                + ", CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '" + userid + "')";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public boolean edit(String id, String name, String adr1, String adr2, String adr3, String adr4, String userid) {

        boolean result = false;

        String sql = "UPDATE EMSCUA "
                + "SET EMCAADD1 = '" + name + "'"
                + ",EMCAADD2 = '" + adr1 + "'"
                + ",EMCAADD3 = '" + adr2 + "'"
                + ",EMCAADD4 = '" + adr3 + "'"
                + ",EMCAADD5 = '" + adr4 + "'"
                + ",EMCAEDT = CURRENT_TIMESTAMP"
                + ",EMCAUSER = '" + userid + "'"
                + " where [EMCACUSID] = '" + id + "'";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public void delete(String id) {

        String sql = "DELETE FROM EMSCUA "
                + "where [EMCACUSID] = '" + id + "'";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ps.executeUpdate();

        } catch (Exception e) {

            e.printStackTrace();

        }

    }
}
