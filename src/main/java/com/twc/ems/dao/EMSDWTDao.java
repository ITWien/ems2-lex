/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.ems.dao;

import com.twc.ems.database.database;
import com.twc.ems.entity.EMSDWT;

import java.sql.*;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author nutthawoot.noo
 */
public class EMSDWTDao extends database {

    DecimalFormat formatDou = new DecimalFormat("#,###,###,##0.00");

    public List<EMSDWT> findAll(String uid) {

        List<EMSDWT> UAList = new ArrayList<EMSDWT>();

        if (uid == null) {
            uid = "93147";
        }

        String sql = "SELECT [EMSDWQNO],[EMSDWDELNO],EMSDWZONE,EMSDWINVNO\n"
                + "      ,FORMAT([EMSDWTRDT],'dd-MM-yyyy') AS [EMSDWTRDT]\n"
                + "      ,[EMSDWSALEORG]\n"
                + "      ,[EMSDWSHIPMARK]\n"
                + "      ,[EMSDWTRANSPORT]+' : '+[EMTPDESC] AS [EMSDWTRANSPORT]\n"
                + "      ,[EMSDWDISTCH]\n"
                + "      ,[EMSDWEXP]\n"
                + "      ,[EMSDWNONTWC]\n"
                + "	  ,(\n"
                + "	    SELECT [USERID]+' : '+SUBSTRING([USERS], 1, CHARINDEX(' ', [USERS])-1)\n"
                + "        FROM [RMShipment].[dbo].[MSSUSER]\n"
                + "        where [USERID] = [EMSDWUSER] COLLATE Thai_CI_AS\n"
                + "	  ) AS [EMSDWUSER]\n"
                + "  FROM [IEMS].[dbo].[EMSDWT]\n"
                + "  LEFT JOIN [EMSTSP] ON [EMSTSP].[EMTPID] = [EMSDWT].[EMSDWTRANSPORT]\n"
                + "  WHERE (SELECT MSWHSE\n"
                + "        FROM [RMShipment].[dbo].[MSSUSER]\n"
                + "        where [USERID] = [EMSDWUSER] COLLATE Thai_CI_AS) LIKE \n"
                + "		case when (SELECT MSWHSE\n"
                + "        FROM [RMShipment].[dbo].[MSSUSER]\n"
                + "        where [USERID] = '" + uid + "' COLLATE Thai_CI_AS) = 'WX3' then 'WX3'\n"
                + "		else '%%' end\n";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                EMSDWT p = new EMSDWT();
//
                p.setEMSDWQNO(result.getString("EMSDWQNO"));
                p.setEMSDWZONE(result.getString("EMSDWZONE"));
                p.setEMSDWINVNO(result.getString("EMSDWINVNO"));
                p.setEMSDWDELNO(result.getString("EMSDWDELNO"));
                p.setEMSDWTRDT(result.getString("EMSDWTRDT"));
                p.setEMSDWSALEORG(result.getString("EMSDWSALEORG"));
                p.setEMSDWSHIPMARK(result.getString("EMSDWSHIPMARK"));
                p.setEMSDWTRANSPORT(result.getString("EMSDWTRANSPORT"));
                p.setEMSDWDISTCH(result.getString("EMSDWDISTCH"));
                p.setEMSDWEXP(result.getString("EMSDWEXP"));
                p.setEMSDWNONTWC(result.getString("EMSDWNONTWC"));
                p.setEMSDWUSER(result.getString("EMSDWUSER"));

//
                UAList.add(p);

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public EMSDWT findByCode(String code) {

        EMSDWT p = new EMSDWT();

        String sql = "SELECT [EMSDWQNO]\n"
                + "      ,[EMSDWDELNO]\n"
                + "      ,[EMSDWSALEORG]\n"
                + "      ,[EMSDWSHIPMARK]\n"
                + "      ,[EMSDWTRANSPORT]\n"
                + "      ,[EMSDWDISTCH]\n"
                + "      ,[EMSDWEXP]\n"
                + "      ,[EMSDWNONTWC]\n"
                + "      ,[EMSDWZONE]\n"
                + "  FROM [IEMS].[dbo].[EMSDWT]\n"
                + "  where [EMSDWQNO] = '" + code + "'";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                p.setEMSDWQNO(result.getString("EMSDWQNO"));
                p.setEMSDWZONE(result.getString("EMSDWZONE"));
                p.setEMSDWDELNO(result.getString("EMSDWDELNO"));
                p.setEMSDWSALEORG(result.getString("EMSDWSALEORG"));
                p.setEMSDWSHIPMARK(result.getString("EMSDWSHIPMARK"));
                p.setEMSDWTRANSPORT(result.getString("EMSDWTRANSPORT"));
                p.setEMSDWDISTCH(result.getString("EMSDWDISTCH"));
                p.setEMSDWEXP(result.getString("EMSDWEXP"));
                p.setEMSDWNONTWC(result.getString("EMSDWNONTWC"));
            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return p;

    }

    public String check(String delno, String sale, String ship, String tsp, String dist, String exp, String comp, String zone) {
        PreparedStatement ps = null;
        ResultSet rs = null;
        String STRRETURN = null;
        try {
            String check = "Select * FROM [IEMS].[dbo].[EMSDWT] "
                    + "where [EMSDWDELNO] = '" + delno + "' "
                    + "and [EMSDWSALEORG] = '" + sale + "' "
                    + "and [EMSDWSHIPMARK] = '" + ship + "' "
                    + "and [EMSDWTRANSPORT] = '" + tsp + "' "
                    + "and [EMSDWDISTCH] = '" + dist + "' "
                    + "and [EMSDWEXP] = '" + exp + "' "
                    + "and [EMSDWNONTWC] = '" + comp + "' "
                    + "and [EMSDWZONE] = '" + zone + "' ";
            ps = connect.prepareStatement(check);
            rs = ps.executeQuery();
            if (rs.next()) {
                STRRETURN = rs.getString("EMSDWQNO");
            } else {
                STRRETURN = "t";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return STRRETURN;
    }

    public boolean add(String delno, String sale, String ship, String tsp, String dist, String exp, String comp, String userid, String zone, String inv) {

        boolean result = false;

        String sql = "INSERT INTO EMSDWT"
                + " ([EMSDWCOM]\n"
                + "      ,[EMSDWQNO]\n"
                + "      ,[EMSDWDELNO]\n"
                + "      ,[EMSDWTRDT]\n"
                + "      ,[EMSDWSALEORG]\n"
                + "      ,[EMSDWSHIPMARK]\n"
                + "      ,[EMSDWTRANSPORT]\n"
                + "      ,[EMSDWDISTCH]\n"
                + "      ,[EMSDWEXP]\n"
                + "      ,[EMSDWEDT]\n"
                + "      ,[EMSDWCDT]\n"
                + "      ,[EMSDWUSER]\n"
                + "      ,[EMSDWZONE]\n"
                + "      ,[EMSDWINVNO]\n"
                + "      ,[EMSDWNONTWC])"
                + " VALUES('TWC'"
                + ", (SELECT ISNULL(MAX(EMSDWQNO)+1, 1) FROM EMSDWT)"
                + ", '" + delno + "'"
                + ", CURRENT_TIMESTAMP"
                + ", '" + sale + "'"
                + ", '" + ship + "'"
                + ", '" + tsp + "'"
                + ", '" + dist + "'"
                + ", '" + exp + "'"
                + ", CURRENT_TIMESTAMP"
                + ", CURRENT_TIMESTAMP"
                + ", '" + userid + "'"
                + ", '" + zone + "'"
                + ", '" + inv + "'"
                + ", '" + comp + "')";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public boolean edit(String code, String desc, String uid) {

        boolean result = false;

        String sql = "UPDATE EMSDWT "
                + "SET EMUMDESC = '" + desc + "'"
                + ", EMUMEDT = CURRENT_TIMESTAMP"
                + ", EMUMUSER = '" + uid + "'"
                + " WHERE EMUMID = '" + code + "'";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public void delete(String code) {

        String sql = "DELETE FROM EMSDWT WHERE EMSDWQNO = '" + code + "'";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ps.executeUpdate();

        } catch (Exception e) {

            e.printStackTrace();

        }

    }
}
