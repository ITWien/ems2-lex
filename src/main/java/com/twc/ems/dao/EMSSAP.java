/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.ems.dao;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.MimeHeaders;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPConnectionFactory;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPPart;
import sun.misc.BASE64Encoder;

/**
 *
 * @author nutthawoot.noo
 */
public class EMSSAP {

    static String DELIVERY = "";
    static String DIVISION = "";
    static String EXBUS = "";
    static String MASKID = "";
    static String SALESORG = "";
    static String TRANSID = "";

    public SOAPMessage getSOAPMessage(String delno, String dist, String exp, String ship, String sale, String tsp) {

        String soapEndpointUrl = "http://twcprd.wacoal.co.th:8000/sap/bc/srt/rfc/sap/Z_WS_TWC_SD_GETEXP?sap-client=500&wsdl=1.1";
        String soapAction = "";

        DELIVERY = delno;
        DIVISION = dist;
        EXBUS = exp;
        MASKID = ship;
        SALESORG = sale;
        TRANSID = tsp;

        SOAPMessage sm = callSoapWebService(soapEndpointUrl, soapAction);
        return sm;
    }

    private static void createSoapEnvelope(SOAPMessage soapMessage) throws SOAPException {
        SOAPPart soapPart = soapMessage.getSOAPPart();

        String myNamespace = "myNamespace";
        String myNamespaceURI = "urn:sap-com:document:sap:rfc:functions";

        // SOAP Envelope
        SOAPEnvelope envelope = soapPart.getEnvelope();
        envelope.addNamespaceDeclaration(myNamespace, myNamespaceURI);

        /*
            Constructed SOAP Request Message:
            <SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:myNamespace="https://www.w3schools.com/xml/">
                <SOAP-ENV:Header/>
                <SOAP-ENV:Body>
                    <myNamespace:CelsiusToFahrenheit>
                        <myNamespace:Celsius>100</myNamespace:Celsius>
                    </myNamespace:CelsiusToFahrenheit>
                </SOAP-ENV:Body>
            </SOAP-ENV:Envelope>
         */
        // SOAP Body
        SOAPBody soapBody = envelope.getBody();

        SOAPElement soapBodyElem = soapBody.addChildElement("Z_BAPI_TWC_SD_GETEXP", myNamespace);

        SOAPElement soapBodyElem1 = soapBodyElem.addChildElement("DELIVERY", myNamespace);
        soapBodyElem1.addTextNode("1101801145");

        SOAPElement soapBodyElem2 = soapBodyElem.addChildElement("DIVISION", myNamespace);
        soapBodyElem2.addTextNode("10");

        SOAPElement soapBodyElem3 = soapBodyElem.addChildElement("EXBUS", myNamespace);
        soapBodyElem3.addTextNode("603");

        SOAPElement soapBodyElem7 = soapBodyElem.addChildElement("EXP_CUSTOMER", myNamespace);
        soapBodyElem7.addTextNode("");

        SOAPElement soapBodyElem8 = soapBodyElem.addChildElement("EXP_DETAIL", myNamespace);
        soapBodyElem8.addTextNode("");

        SOAPElement soapBodyElem9 = soapBodyElem.addChildElement("EXP_HEADER", myNamespace);
        soapBodyElem9.addTextNode("");

        SOAPElement soapBodyElem4 = soapBodyElem.addChildElement("MASKID", myNamespace);
        soapBodyElem4.addTextNode("");

        SOAPElement soapBodyElem10 = soapBodyElem.addChildElement("RETURN", myNamespace);
        soapBodyElem10.addTextNode("");

        SOAPElement soapBodyElem5 = soapBodyElem.addChildElement("SALESORG", myNamespace);
        soapBodyElem5.addTextNode("1000");

        SOAPElement soapBodyElem6 = soapBodyElem.addChildElement("TRANSID", myNamespace);
        soapBodyElem6.addTextNode("");
    }

    private static SOAPMessage callSoapWebService(String soapEndpointUrl, String soapAction) {

        SOAPMessage sm = null;
        try {
            // Create SOAP Connection
            SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory.newInstance();
            SOAPConnection soapConnection = soapConnectionFactory.createConnection();

            // Send SOAP Message to SOAP Server
            SOAPMessage soapResponse = soapConnection.call(createSOAPRequest(soapAction), soapEndpointUrl);

            // Print the SOAP Response
//            System.out.println("Response SOAP Message:");
//            soapResponse.writeTo(System.out);
//            System.out.println();
            soapConnection.close();

            sm = soapResponse;
        } catch (Exception e) {
//            System.err.println("\nError occurred while sending SOAP Request to Server!\nMake sure you have the correct endpoint URL and SOAPAction!\n");
            e.printStackTrace();
        }
        return sm;
    }

    private static SOAPMessage createSOAPRequest(String soapAction) throws Exception {
//        MessageFactory messageFactory = MessageFactory.newInstance();
//        SOAPMessage soapMessage = messageFactory.createMessage();

        String send = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?><SOAP-ENV:Envelope xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xs=\"http://www.w3.org/2001/XMLSchema\">\n"
                + "    <SOAP-ENV:Header>\n"
                + "        <sapsess:Session xmlns:sapsess=\"http://www.sap.com/webas/630/soap/features/session/\">\n"
                + "            <enableSession>true</enableSession>\n"
                + "        </sapsess:Session>\n"
                + "    </SOAP-ENV:Header>\n"
                + "    <SOAP-ENV:Body>\n"
                + "        <ns1:Z_BAPI_TWC_SD_GETEXP xmlns:ns1='urn:sap-com:document:sap:rfc:functions'>\n"
                + "            <DELIVERY xsi:type='ns1:char10'>" + DELIVERY + "</DELIVERY>\n"
                + "            <DIVISION xsi:type='ns1:char2'>" + DIVISION + "</DIVISION>\n"
                + "            <EXBUS xsi:type='ns1:numeric3'>" + EXBUS + "</EXBUS>\n"
                + "            <EXP_CUSTOMER xsi:nil='true' xsi:type='ns1:TABLE_OF_ZBAPI_TWC_EXP_CUST'></EXP_CUSTOMER>\n"
                + "            <EXP_DETAIL xsi:nil='true' xsi:type='ns1:TABLE_OF_ZBAPI_TWC_EXP_DET'></EXP_DETAIL>\n"
                + "            <EXP_HEADER xsi:nil='true' xsi:type='ns1:TABLE_OF_ZBAPI_TWC_EXP_HEAD'></EXP_HEADER>\n"
                + "            <MASKID xsi:type='ns1:char3'>" + MASKID + "</MASKID>\n"
                + "            <RETURN xsi:nil='true' xsi:type='ns1:TABLE_OF_BAPIRETURN'></RETURN>\n"
                + "            <SALESORG xsi:type='ns1:char4'>" + SALESORG + "</SALESORG>\n"
                + "            <TRANSID xsi:type='ns1:char3'>" + TRANSID + "</TRANSID>\n"
                + "        </ns1:Z_BAPI_TWC_SD_GETEXP>\n"
                + "    </SOAP-ENV:Body>\n"
                + "</SOAP-ENV:Envelope>";

        InputStream is = new ByteArrayInputStream(send.getBytes());
        SOAPMessage soapMessage = MessageFactory.newInstance().createMessage(null, is);

        Calendar c = Calendar.getInstance();
        int month = c.get(Calendar.MONTH) + 1;

        String m = Integer.toString(month);
        if (m.length() < 2) {
            m = "0" + m;
        }

//        System.out.println("------+++++++++--------- month : " + m);
        String userAndPassword = String.format("%s:%s", "tviewwien", "display" + m);
        String basicAuth = new BASE64Encoder().encode(userAndPassword.getBytes());
        MimeHeaders mimeHeaders = soapMessage.getMimeHeaders();
        mimeHeaders.addHeader("Authorization", "Basic " + basicAuth);
//
//        createSoapEnvelope(soapMessage);
//
//        MimeHeaders headers = soapMessage.getMimeHeaders();
//        headers.addHeader("SOAPAction", soapAction);
//
//        soapMessage.saveChanges();
//
        /* Print the request message, just for debugging purposes */
//        System.out.println("Request SOAP Message:");
//        soapMessage.writeTo(System.out);
//        System.out.println("\n");

        return soapMessage;
    }

}
