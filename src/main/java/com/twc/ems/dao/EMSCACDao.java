/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.ems.dao;

import com.twc.ems.database.database;
import com.twc.ems.entity.EMSCAC;
import com.twc.ems.entity.EMSCODE;
import com.twc.ems.entity.EMSINVD;

import java.sql.*;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author nutthawoot.noo
 */
public class EMSCACDao extends database {

    DecimalFormat formatDou = new DecimalFormat("#,###,###,##0.00");
    DecimalFormat formatDou3 = new DecimalFormat("#,###,###,##0.000");

    public boolean updateReCal(String zone, String style, String cup, String size) {

        boolean result = false;

        String sql = "UPDATE [EMSCAC]\n"
                + "SET [EMCNET] = (SELECT top 1 cast(isnull((isnull([EMMWEIGHT],0) * [EMCPCS]),0) as decimal(16,2)) \n"
                + "  FROM [IEMS].[dbo].[EMSMAS]\n"
                + "  where [EMMSTYLE] = [EMCSTYLE]\n"
                + "      and [EMMCUP] = [EMCCUP]\n"
                + "      and [EMMSIZE] = [EMCSIZES] and [EMMZONE] = [EMCZONE])\n"
                + "	  ,[EMCGROSS] = (SELECT top 1 cast(isnull((isnull([EMMWEIGHT],0) * [EMCPCS]),0) as decimal(16,2)) +\n"
                + "  cast(case when isnull([EMMWG6E],0) = 0 \n"
                + "       then \n"
                + "	   (\n"
                + "			case when isnull([EMMWG6D],0) = 0 \n"
                + "			then \n"
                + "			(\n"
                + "				case when isnull([EMMWG4E],0) = 0 \n"
                + "				then \n"
                + "				(\n"
                + "					case when isnull([EMMWG4D],0) = 0 \n"
                + "					then \n"
                + "					(\n"
                + "						case when isnull([EMMWG2E],0) = 0 \n"
                + "						then \n"
                + "						(\n"
                + "							SELECT FORMAT(ISNULL([EMDSBOXW],0),'#,#0.00') AS [EMDSBOXW]\n"
                + "							FROM [IEMS].[dbo].[EMSDMS]\n"
                + "							where [EMDSZONE] = [EMCZONE]\n"
                + "							and [EMDSBOX] like '%'+[EMCBOXTYPE]+'%'\n"
                + "						)\n"
                + "						else FORMAT([EMMWG2E],'#,#0.00') end\n"
                + "					)  \n"
                + "					else FORMAT([EMMWG4D],'#,#0.00') end\n"
                + "				) \n"
                + "				else FORMAT([EMMWG4E],'#,#0.00') end\n"
                + "			)\n"
                + "			else FORMAT([EMMWG6D],'#,#0.00') end\n"
                + "	   )\n"
                + "	   else FORMAT([EMMWG6E],'#,#0.00') end as decimal(16,2))\n"
                + "  FROM [IEMS].[dbo].[EMSMAS]\n"
                + "  where [EMMSTYLE] = [EMCSTYLE]\n"
                + "      and [EMMCUP] = [EMCCUP]\n"
                + "      and [EMMSIZE] = [EMCSIZES] and [EMMZONE] = [EMCZONE])\n"
                + "	  ,[EMCEDT] = CURRENT_TIMESTAMP\n"
                + "where [EMCZONE]='" + zone + "'\n"
                + "  and [EMCSTYLE]='" + style + "'\n"
                + "  and [EMCCUP]='" + cup + "'\n"
                + "  and [EMCSIZES]='" + size + "'";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public boolean checkCAC(String inv, String dist) {
        PreparedStatement ps = null;
        ResultSet rs = null;
        boolean STRRETURN = false;
        try {
            String check = "SELECT *\n"
                    + "FROM [IEMS].[dbo].[EMSCAC]\n"
                    + "WHERE EMCINVID = '" + inv + "' AND EMCZONE = '" + dist + "'";
            ps = connect.prepareStatement(check);
            rs = ps.executeQuery();
            if (rs.next()) {
                STRRETURN = true;
            } else {
                STRRETURN = false;
            }
        } catch (Exception exp) {
            exp.printStackTrace();
        }
        return STRRETURN;
    }

    public List<String> findSizeList(String inv, String dist) {

        List<String> UAList = new ArrayList<String>();

        String sql = "SELECT STUFF((\n"
                + "SELECT ',' + [EMCSIZES]\n"
                + "FROM(\n"
                + "SELECT DISTINCT [EMCSIZES]\n"
                + "  FROM [IEMS].[dbo].[EMSCAC]\n"
                + "  WHERE [EMCINVID] = '" + inv + "' AND EMCZONE = '" + dist + "' AND ISNUMERIC([EMCSIZES]) = 1\n"
                + ")TMP2\n"
                + "  ORDER BY [EMCSIZES]\n"
                + "  FOR XML PATH('')), 1, 1, '') AS EMCSIZES\n"
                + "UNION ALL\n"
                + "SELECT STUFF((\n"
                + "SELECT ',' + [EMCSIZES]\n"
                + "  FROM (\n"
                + "        SELECT DISTINCT [EMCSIZES]\n"
                + "		FROM [IEMS].[dbo].[EMSCAC]\n"
                + "		WHERE [EMCINVID] = '" + inv + "' AND EMCZONE = '" + dist + "' AND ISNUMERIC([EMCSIZES]) = 0 AND LEN([EMCSIZES]) = 1\n"
                + "  )TMP\n"
                + "  ORDER BY [EMCSIZES] DESC\n"
                + "  FOR XML PATH('')), 1, 1, '')\n"
                + "UNION ALL\n"
                + "SELECT DISTINCT [EMCSIZES]\n"
                + "  FROM [IEMS].[dbo].[EMSCAC]\n"
                + "  WHERE [EMCINVID] = '" + inv + "' AND EMCZONE = '" + dist + "' AND ISNUMERIC([EMCSIZES]) = 0 AND LEN([EMCSIZES]) > 1";
//        System.out.println(sql);
//        System.out.println("***********************");

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                String a = "";
                if (result.getString("EMCSIZES") != null) {
                    a = result.getString("EMCSIZES");
                    String[] sa = a.split(",");
                    for (int i = 0; i < sa.length; i++) {
                        UAList.add(sa[i]);
                    }
                }

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public List<EMSCAC> findByInvDropGenCar(String inv, String dist) {

        List<EMSCAC> UAList = new ArrayList<EMSCAC>();

        String sql = "SELECT \n"
                + "CASE WHEN CNT > (CNTSTYLE - (CNTSTYLE % 4)) THEN 'YES' ELSE 'NO' END AS [BIT]\n"
                + ",(CNTSTYLE - (CNTSTYLE % 4)) AS HD\n"
                + ",(CNTSTYLE % 4) AS [MOD]\n"
                + ",*\n"
                + "FROM(\n"
                + "SELECT \n"
                + "ROW_NUMBER() OVER(PARTITION BY EMCSTYLE ORDER BY [EMCSTYLE],LENSIZE,[EMCCTNO]) AS CNT\n"
                + ",(SELECT COUNT([EMCSTYLE])\n"
                + "  FROM [IEMS].[dbo].[EMSCAC]\n"
                + "  WHERE [EMCINVID] = TMP.[EMCINVID]\n"
                + "  AND EMCFLAG != 'W'\n"
                + "  AND EMCZONE = TMP.[EMCZONE]\n"
                + "  AND EMCSTYLE = TMP.EMCSTYLE\n"
                + "  AND isnull(EMCSTS,'') != 'D') AS CNTSTYLE\n"
                + ",* \n"
                + ",(\n"
                + "SELECT case when isnull([EMMBOXS6E],0) = 0 \n"
                + "       then \n"
                + "	   (\n"
                + "			case when isnull([EMMBOXS6D],0) = 0 \n"
                + "			then \n"
                + "			(\n"
                + "				case when isnull([EMMBOXS4E],0) = 0 \n"
                + "				then \n"
                + "				(\n"
                + "					case when isnull([EMMBOXS4D],0) = 0 \n"
                + "					then \n"
                + "					(\n"
                + "						case when isnull([EMMBOXS2E],0) = 0 \n"
                + "						then \n"
                + "						0\n"
                + "						else format([EMMBOXS2E],'#0') end\n"
                + "					)  \n"
                + "					else format([EMMBOXS4D],'#0') end\n"
                + "				) \n"
                + "				else format([EMMBOXS4E],'#0') end\n"
                + "			)\n"
                + "			else format([EMMBOXS6D],'#0') end\n"
                + "	   )\n"
                + "	   else format([EMMBOXS6E],'#0') end AS BOXPACK\n"
                + "  FROM [IEMS].[dbo].[EMSMAS]\n"
                + "  where [EMMSTYLE] = TMP.[EMCSTYLE]\n"
                + "  and [EMMCUP] = TMP.[EMCCUP]\n"
                + "  and [EMMSIZE] = TMP.[EMCSIZES] and [EMMZONE] = TMP.[EMCZONE]\n"
                + ") AS BOXPACK\n"
                + "FROM(\n"
                + "SELECT [EMCZONE]\n"
                + "      ,[EMCINVID]\n"
                + "      ,[EMCCTNO]\n"
                + "      ,[EMCCARTONNO]\n"
                + "      ,[EMCORDID]\n"
                + "      ,[EMCPONO]\n"
                + "      ,[EMCSTYLE]\n"
                + "      ,[EMCCOLOR]\n"
                + "      ,[EMCCUP]\n"
                + "      ,[EMCSIZES]\n"
                + "      ,FORMAT([EMCPCS],'#,#0') AS [EMCPCS]\n"
                + "      ,[EMCNET]\n"
                + "      ,[EMCGROSS]\n"
                + "      ,[EMCBOXTYPE]\n"
                + "      ,[EMCAUTH]\n"
                + "      ,ISNULL(EMCFLAG,'') AS [EMCFLAG]\n"
                + "      ,ISNULL(EMCSIZELIST,'') AS EMCSIZELIST\n"
                + "	  ,LEN(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(EMCSIZELIST, 'X', ''), '0', ''), '1', ''), '2', ''), '3', ''), '4', ''), '5', ''), '6', ''), '7', ''), '8', ''), '9', ''), 'S', ''), 'M', ''), 'L', ''), 'A', ''), 'B', ''), 'C', ''), 'D', ''), 'E', ''), 'F', ''), 'G', ''), 'H', ''), 'I', ''), 'J', ''), 'K', ''), 'N', ''), 'O', ''), 'P', ''), 'Q', ''), 'R', ''), 'T', ''), 'U', ''), 'V', ''), 'W', ''), 'Y', ''), 'Z', ''), ':', '')) AS LENSIZE\n"
                + "  FROM [IEMS].[dbo].[EMSCAC]\n"
                + "  WHERE [EMCINVID] = '" + inv + "'\n"
                + "  AND EMCFLAG != 'W'\n"
                + "  AND EMCZONE = '" + dist + "'\n"
                + "  AND isnull(EMCSTS,'') != 'D'\n"
                + "  and ([EMCPARENT] is null or [EMCPARENT] = 'Y')\n"
                + ")TMP)TMP2\n"
                + "ORDER BY [BIT],[EMCSTYLE],LENSIZE,[EMCCTNO]";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            String carton = "";

            while (result.next()) {

                EMSCAC p = new EMSCAC();

                p.setEMCINVID(result.getString("EMCINVID"));
                p.setEMCCTNO(result.getString("EMCCTNO"));

                if (carton.trim().equals("")) {
                    p.setEMCCARTONNO(result.getString("EMCCARTONNO"));
                    carton = result.getString("EMCCARTONNO") == null ? "" : result.getString("EMCCARTONNO").trim();
                } else {
                    if (carton.trim().equals(result.getString("EMCCARTONNO") == null ? "" : result.getString("EMCCARTONNO").trim())) {
                        p.setEMCCARTONNO("<span style=\"opacity: 0;\">" + result.getString("EMCCARTONNO") + "</span>");
                        carton = result.getString("EMCCARTONNO") == null ? "" : result.getString("EMCCARTONNO").trim();
                    } else {
                        p.setEMCCARTONNO(result.getString("EMCCARTONNO"));
                        carton = result.getString("EMCCARTONNO") == null ? "" : result.getString("EMCCARTONNO").trim();
                    }
                }

                p.setEMCORDID(result.getString("EMCORDID"));
                p.setEMCPONO(result.getString("EMCPONO"));
                p.setEMCSTYLE(result.getString("EMCSTYLE"));
                p.setEMCCOLOR(result.getString("EMCCOLOR"));
                p.setEMCCUP(result.getString("EMCCUP"));
                p.setEMCSIZES(result.getString("EMCSIZES"));
                p.setEMCPCS(result.getString("EMCPCS"));
                p.setEMCNET(result.getString("EMCNET"));
                p.setEMCGROSS(result.getString("EMCGROSS"));
                p.setEMCBOXTYPE(result.getString("EMCBOXTYPE"));
                p.setEMCAUTH(result.getString("EMCAUTH"));
                p.setEMCSIZELIST(result.getString("EMCSIZELIST"));
                p.setBOXPACK(result.getString("BOXPACK"));
                p.setEMCFLAG(result.getString("EMCFLAG"));

//                if (!result.getString("EMCFLAG").equals("I")) {
//                    p.setEMCFLAG("display: none;");
//                } else {
//                    p.setEMCFLAG("");
//                }
                UAList.add(p);

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public List<EMSCAC> getCacGebGrp(String inv, String dist) {

        List<EMSCAC> UAList = new ArrayList<EMSCAC>();

        String sql = "SELECT [EMCCARTONNO]\n"
                + "      ,[EMCSTYLE]+[EMCCUPLIST] AS EMCSTYLE\n"
                + "  FROM [IEMS].[dbo].[EMSCAC]\n"
                + "  WHERE [EMCINVID]='" + inv + "'\n"
                + "  AND [EMCZONE]='" + dist + "'\n"
                + "  AND [EMCCARTONNO] IS NOT NULL\n"
                + "  AND [EMCSTYLE]+[EMCCUPLIST] IS NOT NULL\n"
                + "ORDER BY [EMCCARTONNO],[EMCSTYLE],[EMCCUPLIST]";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                EMSCAC p = new EMSCAC();

                p.setEMCCARTONNO(result.getString("EMCCARTONNO"));
                p.setEMCSTYLE(result.getString("EMCSTYLE"));

                UAList.add(p);

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public List<EMSCAC> getCacList(String inv, String dist) {

        List<EMSCAC> UAList = new ArrayList<EMSCAC>();

        String sql = "SELECT [EMCZONE]\n"
                + "      ,[EMCINVID]\n"
                + "	 ,[EMCCTNO]\n"
                + "      ,[EMCSTYLE]\n"
                + "	 ,[EMCBOXTYPE]\n"
                + "      ,[EMCCUPLIST]\n"
                + "  FROM [IEMS].[dbo].[EMSCAC]\n"
                + "  where [EMCINVID]='" + inv + "'\n"
                + "  and [EMCZONE]='" + dist + "'\n"
                + "  and (LEN(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE([EMCCUPLIST], 'X', ''), '*', ''), '0', ''), '1', ''), '2', ''), '3', ''), '4', ''), '5', ''), '6', ''), '7', ''), '8', ''), '9', ''), 'S', ''), 'M', ''), 'L', ''), 'A', ''), 'B', ''), 'C', ''), 'D', ''), 'E', ''), 'F', ''), 'G', ''), 'H', ''), 'I', ''), 'J', ''), 'K', ''), 'N', ''), 'O', ''), 'P', ''), 'Q', ''), 'R', ''), 'T', ''), 'U', ''), 'V', ''), 'W', ''), 'Y', ''), 'Z', ''), ':', ''), '%', ''))>1 \n"
                + "  ) \n";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                EMSCAC p = new EMSCAC();

                p.setEMCZONE(result.getString("EMCZONE"));
                p.setEMCINVID(result.getString("EMCINVID"));
                p.setEMCCTNO(result.getString("EMCCTNO"));
                p.setEMCSTYLE(result.getString("EMCSTYLE"));
                p.setEMCBOXTYPE(result.getString("EMCBOXTYPE"));
                p.setEMCCUPLIST(result.getString("EMCCUPLIST"));

                UAList.add(p);

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public List<EMSCAC> getFgList(String inv, String dist, String ctnFrom, String ctnTo) {

        List<EMSCAC> UAList = new ArrayList<EMSCAC>();

        String sql = "SELECT [EMCZONE]\n"
                + "      ,[EMCINVID]\n"
                + "	 ,[EMCCARTONNO]\n"
                + "      ,[EMCFGNO]\n"
                + "  FROM [IEMS].[dbo].[EMSCAC]\n"
                + "  where [EMCINVID]='" + inv + "'\n"
                + "  and [EMCZONE]='" + dist + "'\n"
                + "  and ([EMCCARTONNO] between '" + ctnFrom + "' and '" + ctnTo + "') \n";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                EMSCAC p = new EMSCAC();

                p.setEMCZONE(result.getString("EMCZONE"));
                p.setEMCINVID(result.getString("EMCINVID"));
                p.setEMCCARTONNO(result.getString("EMCCARTONNO"));
                p.setEMCFGNO(result.getString("EMCFGNO"));

                UAList.add(p);

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public String getNewNetCac(String dataZone, String dataStyle, String dataCup, String dataSize, String dataPcs) {

        String net = "0";

        String sql = "SELECT top 1 cast(isnull((isnull([EMMWEIGHT],0) * " + dataPcs + "),0) as decimal(18,2)) as NET\n"
                + "  FROM [IEMS].[dbo].[EMSMAS]\n"
                + "  where [EMMSTYLE] = '" + dataStyle + "'\n"
                + "  and [EMMCUP] = '" + dataCup + "'\n"
                + "  and [EMMSIZE] = '" + dataSize + "' and [EMMZONE] = '" + dataZone + "'";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                net = result.getString("NET");

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return net;

    }

    public String getNewGrossCac(String dataZone, String dataBoxType) {

        String gross = "0";

        String sql = "SELECT cast(ISNULL([EMDSBOXW],0) as decimal(18,2)) AS [GROSS]\n"
                + "FROM [IEMS].[dbo].[EMSDMS]\n"
                + "where [EMDSZONE] = '" + dataZone + "'\n"
                + "and [EMDSBOX] like '%'+'" + dataBoxType + "'+'%'";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                gross = result.getString("GROSS");

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return gross;

    }

    public List<EMSCAC> findByInvDropTrash(String inv, String dist) {

        List<EMSCAC> UAList = new ArrayList<EMSCAC>();

        String sql = "SELECT * \n"
                + ",(\n"
                + "SELECT case when isnull([EMMBOXS6E],0) = 0 \n"
                + "       then \n"
                + "	   (\n"
                + "			case when isnull([EMMBOXS6D],0) = 0 \n"
                + "			then \n"
                + "			(\n"
                + "				case when isnull([EMMBOXS4E],0) = 0 \n"
                + "				then \n"
                + "				(\n"
                + "					case when isnull([EMMBOXS4D],0) = 0 \n"
                + "					then \n"
                + "					(\n"
                + "						case when isnull([EMMBOXS2E],0) = 0 \n"
                + "						then \n"
                + "						0\n"
                + "						else format([EMMBOXS2E],'#0') end\n"
                + "					)  \n"
                + "					else format([EMMBOXS4D],'#0') end\n"
                + "				) \n"
                + "				else format([EMMBOXS4E],'#0') end\n"
                + "			)\n"
                + "			else format([EMMBOXS6D],'#0') end\n"
                + "	   )\n"
                + "	   else format([EMMBOXS6E],'#0') end AS BOXPACK\n"
                + "  FROM [IEMS].[dbo].[EMSMAS]\n"
                + "  where [EMMSTYLE] = TMP.[EMCSTYLE]\n"
                + "  and [EMMCUP] = TMP.[EMCCUP]\n"
                + "  and [EMMSIZE] = TMP.[EMCSIZES] and [EMMZONE] = '" + dist + "'\n"
                + ") AS BOXPACK\n"
                + "FROM(\n"
                + "SELECT [EMCINVID]\n"
                + "      ,[EMCCTNO]\n"
                + "      ,[EMCCARTONNO]\n"
                + "      ,[EMCORDID]\n"
                + "      ,[EMCPONO]\n"
                + "      ,[EMCSTYLE]\n"
                + "      ,[EMCCOLOR]\n"
                + "      ,[EMCCUP]\n"
                + "      ,[EMCSIZES]\n"
                + "      ,FORMAT([EMCPCS],'#,#0') AS [EMCPCS]\n"
                + "      ,[EMCNET]\n"
                + "      ,[EMCGROSS]\n"
                + "      ,[EMCBOXTYPE]\n"
                + "      ,[EMCAUTH]\n"
                + "      ,ISNULL(EMCFLAG,'') AS [EMCFLAG]\n"
                + "      ,ISNULL(EMCSIZELIST,'') AS EMCSIZELIST\n"
                + "	  ,LEN(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(EMCSIZELIST, 'X', ''), '0', ''), '1', ''), '2', ''), '3', ''), '4', ''), '5', ''), '6', ''), '7', ''), '8', ''), '9', ''), 'S', ''), 'M', ''), 'L', ''), 'A', ''), 'B', ''), 'C', ''), 'D', ''), 'E', ''), 'F', ''), 'G', ''), 'H', ''), 'I', ''), 'J', ''), 'K', ''), 'N', ''), 'O', ''), 'P', ''), 'Q', ''), 'R', ''), 'T', ''), 'U', ''), 'V', ''), 'W', ''), 'Y', ''), 'Z', ''), ':', '')) AS LENSIZE\n"
                + "  FROM [IEMS].[dbo].[EMSCAC]\n"
                + "  WHERE [EMCINVID] = '" + inv + "'\n"
                + "  AND EMCFLAG != 'W'\n"
                + "  AND EMCZONE = '" + dist + "'\n"
                + "  AND isnull(EMCSTS,'') = 'D'\n"
                + ")TMP\n"
                + "ORDER BY EMCCARTONNO,[EMCSTYLE],LENSIZE,[EMCCTNO]";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            String carton = "";

            while (result.next()) {

                EMSCAC p = new EMSCAC();

                p.setEMCINVID(result.getString("EMCINVID"));
                p.setEMCCTNO(result.getString("EMCCTNO"));

                String EMCCARTONNO = "";
                if (result.getString("EMCCARTONNO") != null) {
                    EMCCARTONNO = result.getString("EMCCARTONNO").trim();
                }

                if (carton.trim().equals("")) {
                    p.setEMCCARTONNO("<b style=\"opacity: 1;\">" + EMCCARTONNO + "</b>");
                    carton = EMCCARTONNO;
                } else {
                    if (carton.trim().equals(EMCCARTONNO)) {
                        p.setEMCCARTONNO("<span style=\"opacity: 0.3;\">" + EMCCARTONNO + "</span>");
                        carton = EMCCARTONNO;
                    } else {
                        p.setEMCCARTONNO("<b style=\"opacity: 1;\">" + EMCCARTONNO + "</b>");
                        carton = EMCCARTONNO;
                    }
                }

                p.setEMCORDID(result.getString("EMCORDID"));
                p.setEMCPONO(result.getString("EMCPONO"));
                p.setEMCSTYLE(result.getString("EMCSTYLE"));
                p.setEMCCOLOR(result.getString("EMCCOLOR"));
                p.setEMCCUP(result.getString("EMCCUP"));
                p.setEMCSIZES(result.getString("EMCSIZES"));
                p.setEMCPCS(result.getString("EMCPCS"));
                p.setEMCNET(result.getString("EMCNET"));
                p.setEMCGROSS(result.getString("EMCGROSS"));
                p.setEMCBOXTYPE(result.getString("EMCBOXTYPE"));
                p.setEMCAUTH(result.getString("EMCAUTH"));
                p.setEMCSIZELIST(result.getString("EMCSIZELIST"));
                p.setBOXPACK(result.getString("BOXPACK"));
                p.setEMCFLAG(result.getString("EMCFLAG"));

//                if (!result.getString("EMCFLAG").equals("I")) {
//                    p.setEMCFLAG("display: none;");
//                } else {
//                    p.setEMCFLAG("");
//                }
                UAList.add(p);

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public List<EMSCAC> findByInvDrop(String inv, String dist) {

        List<EMSCAC> UAList = new ArrayList<EMSCAC>();

        String sql = "SELECT * \n"
                + ",(\n"
                + "SELECT case when isnull([EMMBOXS6E],0) = 0 \n"
                + "       then \n"
                + "	   (\n"
                + "			case when isnull([EMMBOXS6D],0) = 0 \n"
                + "			then \n"
                + "			(\n"
                + "				case when isnull([EMMBOXS4E],0) = 0 \n"
                + "				then \n"
                + "				(\n"
                + "					case when isnull([EMMBOXS4D],0) = 0 \n"
                + "					then \n"
                + "					(\n"
                + "						case when isnull([EMMBOXS2E],0) = 0 \n"
                + "						then \n"
                + "						0\n"
                + "						else format([EMMBOXS2E],'#0') end\n"
                + "					)  \n"
                + "					else format([EMMBOXS4D],'#0') end\n"
                + "				) \n"
                + "				else format([EMMBOXS4E],'#0') end\n"
                + "			)\n"
                + "			else format([EMMBOXS6D],'#0') end\n"
                + "	   )\n"
                + "	   else format([EMMBOXS6E],'#0') end AS BOXPACK\n"
                + "  FROM [IEMS].[dbo].[EMSMAS]\n"
                + "  where [EMMSTYLE] = TMP.[EMCSTYLE]\n"
                + "  and [EMMCUP] = TMP.[EMCCUP]\n"
                + "  and [EMMSIZE] = TMP.[EMCSIZES] and [EMMZONE] = '" + dist + "'\n"
                + ") AS BOXPACK\n"
                + "FROM(\n"
                + "SELECT [EMCINVID]\n"
                + "      ,[EMCCTNO]\n"
                + "      ,[EMCCARTONNO]\n"
                + "      ,[EMCORDID]\n"
                + "      ,[EMCPONO]\n"
                + "      ,[EMCFGNO]\n"
                + "      ,[EMCSTYLE]\n"
                + "      ,[EMCCOLOR]\n"
                + "      ,[EMCCUP]\n"
                + "      ,[EMCSIZES]\n"
                + "      ,FORMAT([EMCPCS],'#,#0') AS [EMCPCS]\n"
                + "      ,FORMAT([EMCPCS],'00000') AS [EMCPCS_OD]\n"
                + "      ,[EMCNET]\n"
                + "      ,(case when EMCGROSS = 0 then '' else format(EMCGROSS,'#,#0.00') end) as [EMCGROSS]\n"
                + "      ,[EMCBOXTYPE]\n"
                + "      ,[EMCAUTH]\n"
                + "      ,ISNULL(EMCFLAG,'') AS [EMCFLAG]\n"
                + "      ,ISNULL(EMCSIZELIST,'') AS EMCSIZELIST\n"
                + "	  ,LEN(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(EMCSIZELIST, 'X', ''), '0', ''), '1', ''), '2', ''), '3', ''), '4', ''), '5', ''), '6', ''), '7', ''), '8', ''), '9', ''), 'S', ''), 'M', ''), 'L', ''), 'A', ''), 'B', ''), 'C', ''), 'D', ''), 'E', ''), 'F', ''), 'G', ''), 'H', ''), 'I', ''), 'J', ''), 'K', ''), 'N', ''), 'O', ''), 'P', ''), 'Q', ''), 'R', ''), 'T', ''), 'U', ''), 'V', ''), 'W', ''), 'Y', ''), 'Z', ''), ':', '')) AS LENSIZE\n"
                + "      ,ISNULL(EMCCUPLIST,'') AS EMCCUPLIST\n"
                + "      ,ISNULL(EMCCOLORLIST,'') AS EMCCOLORLIST\n"
                + "	  ,LEN(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(EMCCUPLIST, 'X', ''), '*', ''), '0', ''), '1', ''), '2', ''), '3', ''), '4', ''), '5', ''), '6', ''), '7', ''), '8', ''), '9', ''), 'S', ''), 'M', ''), 'L', ''), 'A', ''), 'B', ''), 'C', ''), 'D', ''), 'E', ''), 'F', ''), 'G', ''), 'H', ''), 'I', ''), 'J', ''), 'K', ''), 'N', ''), 'O', ''), 'P', ''), 'Q', ''), 'R', ''), 'T', ''), 'U', ''), 'V', ''), 'W', ''), 'Y', ''), 'Z', ''), ':', ''), '%', '')) AS LENCUP\n"
                + "  FROM [IEMS].[dbo].[EMSCAC]\n"
                + "  WHERE [EMCINVID] = '" + inv + "'\n"
                + "  AND EMCFLAG != 'W'\n"
                + "  AND EMCZONE = '" + dist + "'\n"
                + "  AND isnull(EMCSTS,'') != 'D'\n"
                + "  and ([EMCPARENT] is null or [EMCPARENT] = 'Y')\n"
                //                + "  AND EMCCTNO in ('118','7')\n"
                + ")TMP\n"
                + "ORDER BY [EMCAUTH],[EMCSTYLE],[EMCCOLOR],LENCUP,[EMCCUP],LENSIZE,[EMCSIZES],[EMCPCS_OD] desc ";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            String carton = "";

            while (result.next()) {

                EMSCAC p = new EMSCAC();

                p.setEMCINVID(result.getString("EMCINVID"));
                p.setEMCCTNO(result.getString("EMCCTNO"));

                String EMCCARTONNO = "";
                if (result.getString("EMCCARTONNO") != null) {
                    EMCCARTONNO = result.getString("EMCCARTONNO").trim();
                }

                if (carton.trim().equals("")) {
                    p.setEMCCARTONNO("<b style=\"opacity: 1;\">" + EMCCARTONNO + "</b>");
                    carton = EMCCARTONNO;
                } else {
                    if (carton.trim().equals(EMCCARTONNO)) {
                        p.setEMCCARTONNO("<span style=\"opacity: 0.3;\">" + EMCCARTONNO + "</span>");
                        carton = EMCCARTONNO;
                    } else {
                        p.setEMCCARTONNO("<b style=\"opacity: 1;\">" + EMCCARTONNO + "</b>");
                        carton = EMCCARTONNO;
                    }
                }

                p.setEMCORDID(result.getString("EMCORDID"));
                p.setEMCCARTONNOdata(result.getString("EMCCARTONNO"));
                p.setEMCPONO(result.getString("EMCPONO"));
                p.setEMCFGNO(result.getString("EMCFGNO"));
                p.setEMCSTYLE(result.getString("EMCSTYLE"));
                p.setEMCCOLOR(result.getString("EMCCOLOR"));
                p.setEMCCUP(result.getString("EMCCUP"));
                p.setEMCSIZES(result.getString("EMCSIZES"));
                p.setEMCPCS(result.getString("EMCPCS"));
                p.setEMCNET(result.getString("EMCNET"));
                p.setEMCGROSS(result.getString("EMCGROSS"));
                p.setEMCBOXTYPE(result.getString("EMCBOXTYPE"));
                p.setEMCAUTH(result.getString("EMCAUTH"));
                p.setEMCSIZELIST(result.getString("EMCSIZELIST"));
                p.setBOXPACK(result.getString("BOXPACK"));
                p.setEMCFLAG(result.getString("EMCFLAG"));
                p.setEMCCUPLIST(result.getString("EMCCUPLIST"));
                p.setEMCCOLORLIST(result.getString("EMCCOLORLIST"));

//                if (!result.getString("EMCFLAG").equals("I")) {
//                    p.setEMCFLAG("display: none;");
//                } else {
//                    p.setEMCFLAG("");
//                }
                UAList.add(p);

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public String findCupList(String inv, String ctno) {

        String cuplist = "";

        String sql = "SELECT top 1 [EMCCUPLIST]\n"
                + "  FROM [IEMS].[dbo].[EMSCAC]\n"
                + "  where [EMCINVID]='" + inv + "'\n"
                + "  and [EMCCTNO]='" + ctno + "'";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                cuplist = result.getString("EMCCUPLIST");

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return cuplist;

    }

    public String findCup(String inv, String ctno) {

        String cup = "";

        String sql = "SELECT top 1 [EMCCUP]\n"
                + "  FROM [IEMS].[dbo].[EMSCAC]\n"
                + "  where [EMCINVID]='" + inv + "'\n"
                + "  and [EMCCTNO]='" + ctno + "'";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                cup = result.getString("EMCCUP");

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return cup;

    }

    public List<EMSCAC> findByInvDropAsean(String inv, String dist) {

        List<EMSCAC> UAList = new ArrayList<EMSCAC>();

        String sql = "SELECT * \n"
                + ",(\n"
                + "SELECT case when isnull([EMMBOXS6E],0) = 0 \n"
                + "       then \n"
                + "	   (\n"
                + "			case when isnull([EMMBOXS6D],0) = 0 \n"
                + "			then \n"
                + "			(\n"
                + "				case when isnull([EMMBOXS4E],0) = 0 \n"
                + "				then \n"
                + "				(\n"
                + "					case when isnull([EMMBOXS4D],0) = 0 \n"
                + "					then \n"
                + "					(\n"
                + "						case when isnull([EMMBOXS2E],0) = 0 \n"
                + "						then \n"
                + "						0\n"
                + "						else format([EMMBOXS2E],'#0') end\n"
                + "					)  \n"
                + "					else format([EMMBOXS4D],'#0') end\n"
                + "				) \n"
                + "				else format([EMMBOXS4E],'#0') end\n"
                + "			)\n"
                + "			else format([EMMBOXS6D],'#0') end\n"
                + "	   )\n"
                + "	   else format([EMMBOXS6E],'#0') end AS BOXPACK\n"
                + "  FROM [IEMS].[dbo].[EMSMAS]\n"
                + "  where [EMMSTYLE] = TMP.[EMCSTYLE]\n"
                + "  and [EMMCUP] = TMP.[EMCCUP]\n"
                + "  and [EMMSIZE] = TMP.[EMCSIZES] and [EMMZONE] = '" + dist + "'\n"
                + ") AS BOXPACK\n"
                + "FROM(\n"
                + "SELECT [EMCINVID]\n"
                + "      ,[EMCCTNO]\n"
                + "      ,[EMCCARTONNO]\n"
                + "      ,[EMCORDID]\n"
                + "      ,[EMCPONO]\n"
                + "      ,[EMCFGNO]\n"
                + "      ,[EMCSTYLE]\n"
                + "      ,[EMCCOLOR]\n"
                + "      ,[EMCCUP]\n"
                + "      ,[EMCSIZES]\n"
                + "      ,FORMAT([EMCPCS],'#,#0') AS [EMCPCS]\n"
                + "      ,[EMCNET]\n"
                + "      ,(case when EMCGROSS = 0 then '' else format(EMCGROSS,'#,#0.00') end) as [EMCGROSS]\n"
                + "      ,[EMCBOXTYPE]\n"
                + "      ,[EMCAUTH]\n"
                + "      ,ISNULL(EMCFLAG,'') AS [EMCFLAG]\n"
                + "      ,ISNULL(EMCSIZELIST,'') AS EMCSIZELIST\n"
                + "	  ,LEN(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(EMCSIZELIST, 'X', ''), '0', ''), '1', ''), '2', ''), '3', ''), '4', ''), '5', ''), '6', ''), '7', ''), '8', ''), '9', ''), 'S', ''), 'M', ''), 'L', ''), 'A', ''), 'B', ''), 'C', ''), 'D', ''), 'E', ''), 'F', ''), 'G', ''), 'H', ''), 'I', ''), 'J', ''), 'K', ''), 'N', ''), 'O', ''), 'P', ''), 'Q', ''), 'R', ''), 'T', ''), 'U', ''), 'V', ''), 'W', ''), 'Y', ''), 'Z', ''), ':', '')) AS LENSIZE\n"
                + "      ,ISNULL(EMCCUPLIST,'') AS EMCCUPLIST\n"
                + "      ,ISNULL(EMCCOLORLIST,'') AS EMCCOLORLIST\n"
                + "	  ,LEN(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(EMCCUPLIST, 'X', ''), '*', ''), '0', ''), '1', ''), '2', ''), '3', ''), '4', ''), '5', ''), '6', ''), '7', ''), '8', ''), '9', ''), 'S', ''), 'M', ''), 'L', ''), 'A', ''), 'B', ''), 'C', ''), 'D', ''), 'E', ''), 'F', ''), 'G', ''), 'H', ''), 'I', ''), 'J', ''), 'K', ''), 'N', ''), 'O', ''), 'P', ''), 'Q', ''), 'R', ''), 'T', ''), 'U', ''), 'V', ''), 'W', ''), 'Y', ''), 'Z', ''), ':', ''), '%', '')) AS LENCUP\n"
                + "  FROM [IEMS].[dbo].[EMSCAC]\n"
                + "  WHERE [EMCINVID] = '" + inv + "'\n"
                + "  AND EMCFLAG != 'W'\n"
                + "  AND EMCZONE = '" + dist + "'\n"
                + "  AND isnull(EMCSTS,'') != 'D'\n"
                + "  and ([EMCPARENT] is null or [EMCPARENT] = 'Y')\n"
                + ")TMP\n"
                + "ORDER BY EMCCARTONNO,[EMCAUTH],[EMCSTYLE],[EMCCOLOR],LENCUP,[EMCCUP],LENSIZE,[EMCSIZES]";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            String carton = "";

            while (result.next()) {

                EMSCAC p = new EMSCAC();

                p.setEMCINVID(result.getString("EMCINVID"));
                p.setEMCCTNO(result.getString("EMCCTNO"));

                String EMCCARTONNO = "";
                if (result.getString("EMCCARTONNO") != null) {
                    EMCCARTONNO = result.getString("EMCCARTONNO").trim();
                }

                if (carton.trim().equals("")) {
                    p.setEMCCARTONNO("<b style=\"opacity: 1;\">" + EMCCARTONNO + "</b>");
                    carton = EMCCARTONNO;
                } else {
                    if (carton.trim().equals(EMCCARTONNO)) {
                        p.setEMCCARTONNO("<span style=\"opacity: 0.3;\">" + EMCCARTONNO + "</span>");
                        carton = EMCCARTONNO;
                    } else {
                        p.setEMCCARTONNO("<b style=\"opacity: 1;\">" + EMCCARTONNO + "</b>");
                        carton = EMCCARTONNO;
                    }
                }

                p.setEMCORDID(result.getString("EMCORDID"));
                p.setEMCCARTONNOdata(result.getString("EMCCARTONNO"));
                p.setEMCPONO(result.getString("EMCPONO"));
                p.setEMCFGNO(result.getString("EMCFGNO"));
                p.setEMCSTYLE(result.getString("EMCSTYLE"));
                p.setEMCCOLOR(result.getString("EMCCOLOR"));
                p.setEMCCUP(result.getString("EMCCUP"));
                p.setEMCSIZES(result.getString("EMCSIZES"));
                p.setEMCPCS(result.getString("EMCPCS"));
                p.setEMCNET(result.getString("EMCNET"));
                p.setEMCGROSS(result.getString("EMCGROSS"));
                p.setEMCBOXTYPE(result.getString("EMCBOXTYPE"));
                p.setEMCAUTH(result.getString("EMCAUTH"));
                p.setEMCSIZELIST(result.getString("EMCSIZELIST"));
                p.setBOXPACK(result.getString("BOXPACK"));
                p.setEMCFLAG(result.getString("EMCFLAG"));
                p.setEMCCUPLIST(result.getString("EMCCUPLIST"));
                p.setEMCCOLORLIST(result.getString("EMCCOLORLIST"));

//                if (!result.getString("EMCFLAG").equals("I")) {
//                    p.setEMCFLAG("display: none;");
//                } else {
//                    p.setEMCFLAG("");
//                }
                UAList.add(p);

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public List<EMSCAC> findCartonDeleteByInvDrop(String inv, String dist) {

        List<EMSCAC> UAList = new ArrayList<EMSCAC>();

        String sql = "SELECT DISTINCT [EMCINVID]\n"
                + "      ,[EMCCTNO]\n"
                + "	  ,[EMCCARTONNO]\n"
                + "  FROM [IEMS].[dbo].[EMSCAC]\n"
                + "  WHERE [EMCINVID] = '" + inv + "' \n"
                + "  AND [EMCZONE] = '" + dist + "'\n"
                + "  AND [EMCSTS] = 'D'\n"
                + "  ORDER BY [EMCCARTONNO]";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                EMSCAC p = new EMSCAC();

                p.setEMCINVID(result.getString("EMCINVID"));
                p.setEMCCTNO(result.getString("EMCCTNO"));
                p.setEMCCARTONNO(result.getString("EMCCARTONNO"));

                UAList.add(p);

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public List<EMSCAC> findByInvDrop2(String inv, String dist) {

        List<EMSCAC> UAList = new ArrayList<EMSCAC>();

        String sql = "SELECT *, FORMAT(BOXPACK,'#,#0') AS BOXPACKINT\n"
                + "FROM(\n"
                + "SELECT [EMCINVID]\n"
                + "      ,[EMCCTNO]\n"
                + "      ,[EMCCARTONNO]\n"
                + "      ,[EMCORDID]\n"
                + "      ,[EMCPONO]\n"
                + "      ,[EMCSTYLE]\n"
                + "      ,[EMCCOLOR]\n"
                + "      ,[EMCCUP]\n"
                + "      ,[EMCSIZES]\n"
                + "      ,FORMAT([EMCPCS],'#,#0') AS [EMCPCS]\n"
                + "      ,[EMCNET]\n"
                + "      ,[EMCGROSS]\n"
                + "      ,[EMCBOXTYPE]\n"
                + "      ,[EMCAUTH]\n"
                + "      ,ISNULL(EMCSIZELIST,'') AS EMCSIZELIST\n"
                + "      ,ISNULL(EMCCUPLIST,'') AS EMCCUPLIST\n"
                + "      ,ISNULL(EMCCOLORLIST,'') AS EMCCOLORLIST\n"
                + ",(\n"
                + "SELECT case when isnull([EMMBOXS6E],0) = 0 \n"
                + "       then \n"
                + "	   (\n"
                + "			case when isnull([EMMBOXS6D],0) = 0 \n"
                + "			then \n"
                + "			(\n"
                + "				case when isnull([EMMBOXS4E],0) = 0 \n"
                + "				then \n"
                + "				(\n"
                + "					case when isnull([EMMBOXS4D],0) = 0 \n"
                + "					then \n"
                + "					(\n"
                + "						case when isnull([EMMBOXS2E],0) = 0 \n"
                + "						then \n"
                + "						0\n"
                + "						else [EMMBOXS2E] end\n"
                + "					)  \n"
                + "					else [EMMBOXS4D] end\n"
                + "				) \n"
                + "				else [EMMBOXS4E] end\n"
                + "			)\n"
                + "			else [EMMBOXS6D] end\n"
                + "	   )\n"
                + "	   else [EMMBOXS6E] end AS BOXPACK\n"
                + "  FROM [IEMS].[dbo].[EMSMAS]\n"
                + "  where [EMMSTYLE] = [EMCSTYLE]\n"
                + "  and [EMMCUP] = [EMCCUP]\n"
                + "  and [EMMSIZE] = [EMCSIZES] and [EMMZONE] = '" + dist + "'\n"
                + ") AS BOXPACK\n"
                + ",(\n"
                + "  SELECT case when isnull([EMMWG6E],0) = 0 \n"
                + "       then \n"
                + "	   (\n"
                + "			case when isnull([EMMWG6D],0) = 0 \n"
                + "			then \n"
                + "			(\n"
                + "				case when isnull([EMMWG4E],0) = 0 \n"
                + "				then \n"
                + "				(\n"
                + "					case when isnull([EMMWG4D],0) = 0 \n"
                + "					then \n"
                + "					(\n"
                + "						case when isnull([EMMWG2E],0) = 0 \n"
                + "						then \n"
                + "						(\n"
                + "							SELECT FORMAT(ISNULL([EMDSBOXW],0),'#,#0.00') AS [EMDSBOXW]\n"
                + "							FROM [IEMS].[dbo].[EMSDMS]\n"
                + "							where [EMDSZONE] = '" + dist + "'\n"
                + "							and [EMDSBOX] like '%'+[EMCBOXTYPE]+'%'\n"
                + "						) \n"
                + "						else FORMAT([EMMWG2E],'#,#0.00') end\n"
                + "					)  \n"
                + "					else FORMAT([EMMWG4D],'#,#0.00') end\n"
                + "				) \n"
                + "				else FORMAT([EMMWG4E],'#,#0.00') end\n"
                + "			)\n"
                + "			else FORMAT([EMMWG6D],'#,#0.00') end\n"
                + "	   )\n"
                + "	   else FORMAT([EMMWG6E],'#,#0.00') end AS [EMMWEIGHT]\n"
                + "  FROM [IEMS].[dbo].[EMSMAS]\n"
                + "  where [EMMSTYLE] = [EMCSTYLE]\n"
                + "  and [EMMCUP] = [EMCCUP]\n"
                + "  and [EMMSIZE] = [EMCSIZES] and [EMMZONE] = '" + dist + "'\n"
                + ") AS [BOXWEIGHT]\n"
                + ",(\n"
                + "  SELECT TOP 1 ISNULL([EMMWEIGHT],0) AS [EMMWEIGHT]\n"
                + "  FROM [IEMS].[dbo].[EMSMAS]\n"
                + "  where [EMMSTYLE] = [EMCSTYLE]\n"
                + "  and [EMMCUP] = [EMCCUP]\n"
                + "  and [EMMSIZE] = [EMCSIZES] and [EMMZONE] = '" + dist + "'\n"
                + ") AS [WEIGHT]\n"
                + "  FROM [IEMS].[dbo].[EMSCAC]\n"
                + "  WHERE [EMCINVID] = '" + inv + "'\n"
                + "  AND EMCZONE = '" + dist + "'\n"
                + "  AND EMCFLAG = 'N'\n"
                + "  AND isnull(EMCSTS,'') != 'D'\n"
                + "  and ([EMCPARENT] is null or [EMCPARENT] = 'Y')\n"
                + ")TTT\n"
                //                + "WHERE TTT.BOXPACK != TTT.EMCPCS"
                + "ORDER BY EMCCTNO,EMCSTYLE";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                EMSCAC p = new EMSCAC();

                p.setEMCINVID(result.getString("EMCINVID"));
                p.setEMCCTNO(result.getString("EMCCTNO"));
                p.setEMCCARTONNO(result.getString("EMCCARTONNO"));
                p.setEMCORDID(result.getString("EMCORDID"));
                p.setEMCPONO(result.getString("EMCPONO"));
                p.setEMCSTYLE(result.getString("EMCSTYLE"));
                p.setEMCCOLOR(result.getString("EMCCOLOR"));
                p.setEMCCUP(result.getString("EMCCUP"));
                p.setEMCSIZES(result.getString("EMCSIZES"));
                p.setEMCPCS(result.getString("EMCPCS"));
                p.setEMCNET(result.getString("EMCNET"));
                p.setEMCGROSS(result.getString("EMCGROSS"));
                p.setEMCBOXTYPE(result.getString("EMCBOXTYPE"));
                p.setEMCAUTH(result.getString("EMCAUTH"));
                p.setBOXPACK(Integer.toString(Integer.parseInt(result.getString("BOXPACKINT") == null ? "0" : result.getString("BOXPACKINT"))));
                p.setWeight(result.getString("WEIGHT"));
                p.setBoxweight(result.getString("BOXWEIGHT"));
                p.setEMCSIZELIST(result.getString("EMCSIZELIST"));
                p.setEMCCUPLIST(result.getString("EMCCUPLIST"));
                p.setEMCCOLORLIST(result.getString("EMCCOLORLIST"));

                String cupList = result.getString("EMCCUPLIST");
                if (cupList == null) {
                    cupList = "";
                }

                String cupListShow = "";
                String[] cupoShow = cupList.split(";");
                for (int i = 0; i < cupoShow.length; i++) {
                    String cup = cupoShow[i].split(":")[0];
                    String size = cupoShow[i].split(":")[1].split("%")[1];
                    String pcs = cupoShow[i].split(":")[1].split("%")[0];
                    cupListShow += cup + ", " + size + ", " + pcs + ";";
                }

                p.setEMCCUPLIST2(cupListShow.replace(";", "&#013;"));

                String[] cupo = cupList.split(";");
                String cupolist = "";
                for (int i = 0; i < cupo.length; i++) {
                    String comma = ", ";
                    if (i == 0) {
                        comma = "";
                    }
                    cupolist += comma + cupo[i].split(":")[0];
                }
                p.setEMCCUPOLIST(cupolist);

                UAList.add(p);

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public boolean updateGrpNo(String inv, String dist, String carton, String grpNo) {

        boolean result = false;

        String sql = "UPDATE [EMSCAC]\n"
                + "SET [EMCGRPNO] = '" + grpNo + "'\n"
                + "  WHERE [EMCINVID] = '" + inv + "'\n"
                + "  AND [EMCZONE] = '" + dist + "'\n"
                + "  AND [EMCCARTONNO] = '" + carton + "'";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public boolean editDet(String inv, String opo, String po, String size, String amt, String code, String userid) {

        boolean result = false;

        String sql = "UPDATE [IEMS].[dbo].[EMSCAC]\n"
                + "SET [EMDPONO] = '" + po + "'\n"
                + "      ,[EMDAMOUNT] = '" + amt + "'\n"
                + "      ,[EMDCODE] = '" + code + "'\n"
                + "  WHERE [EMDINVID] = '" + inv + "'\n"
                + "  AND [EMDPONO] = '" + opo + "'\n"
                + "  AND [EMDSIZES] = '" + size + "'";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public List<EMSCAC> findByInv(String inv) {

        List<EMSCAC> UAList = new ArrayList<EMSCAC>();

        String sql = "SELECT [EMDINVID]\n"
                + "      ,[EMDPONO]\n"
                + "	  ,[EMDORDID]\n"
                + "	  ,[EMDAUTH]\n"
                + "	  ,[EMDBRAND]\n"
                + "      ,[EMDTYPE]\n"
                + "      ,[EMDSTYLE]\n"
                + "      ,[EMDCOLOR]\n"
                + "      ,isnull([EMDCODE],'') as EMDCODE\n"
                + "	  ,isnull(EMDCODE+' : '+[EMCDDESC],'') as EMCDDESC\n"
                + "      ,[EMDCUP]\n"
                + "      ,[EMDSIZES]\n"
                + "	  ,FORMAT([EMDCOST],'#,#0.00') as [EMDCOST]\n"
                + "      ,FORMAT([EMDAMOUNT],'#,#0') as [EMDAMOUNT]\n"
                + "  FROM [IEMS].[dbo].[EMSCAC]\n"
                + "  LEFT JOIN [EMSCODE] ON [EMSCODE].[EMCDCODE] = [EMSCAC].[EMDCODE]\n"
                + "  WHERE [EMDINVID] = '" + inv + "'\n"
                + "  ORDER BY [EMDINVID],[EMDPONO],[EMDORDID],[EMDAUTH],[EMDBRAND],[EMDTYPE],[EMDSTYLE],[EMDCOLOR],[EMDCUP],[EMDSIZES]";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            String po = "ST";
            String order = "ST";
            String auth = "ST";
            String brand = "ST";
            String type = "ST";
            String style = "ST";
            String color = "ST";
            String code = "ST";
            String cup = "ST";

            while (result.next()) {

//                EMSCAC p = new EMSCAC();
//
//                p.setEMDINVID(result.getString("EMDINVID"));
//
//                if (po.equals("ST")) {
//                    p.setEMDPONO(result.getString("EMDPONO"));
//                    po = result.getString("EMDPONO");
//                } else if (po.equals(result.getString("EMDPONO"))) {
//                    p.setEMDPONO("<b style=\"opacity: 0;\">" + result.getString("EMDPONO") + "</b>");
//                    po = result.getString("EMDPONO");
//                } else {
//                    p.setEMDPONO(result.getString("EMDPONO"));
//                    po = result.getString("EMDPONO");
//
//                    order = "ST";
//                    auth = "ST";
//                    brand = "ST";
//                    type = "ST";
//                    style = "ST";
//                    color = "ST";
//                    code = "ST";
//                    cup = "ST";
//                }
//
//                if (order.equals("ST")) {
//                    p.setEMDORDID(result.getString("EMDORDID"));
//                    order = result.getString("EMDORDID");
//                } else if (order.equals(result.getString("EMDORDID"))) {
//                    p.setEMDORDID("<b style=\"opacity: 0;\">" + result.getString("EMDORDID") + "</b>");
//                    order = result.getString("EMDORDID");
//                } else {
//                    p.setEMDORDID(result.getString("EMDORDID"));
//                    order = result.getString("EMDORDID");
//
//                    auth = "ST";
//                    brand = "ST";
//                    type = "ST";
//                    style = "ST";
//                    color = "ST";
//                    code = "ST";
//                    cup = "ST";
//                }
//
//                if (auth.equals("ST")) {
//                    p.setEMDAUTH(result.getString("EMDAUTH"));
//                    auth = result.getString("EMDAUTH");
//                } else if (auth.equals(result.getString("EMDAUTH"))) {
//                    p.setEMDAUTH("<b style=\"opacity: 0;\">" + result.getString("EMDAUTH") + "</b>");
//                    auth = result.getString("EMDAUTH");
//                } else {
//                    p.setEMDAUTH(result.getString("EMDAUTH"));
//                    auth = result.getString("EMDAUTH");
//
//                    brand = "ST";
//                    type = "ST";
//                    style = "ST";
//                    color = "ST";
//                    code = "ST";
//                    cup = "ST";
//                }
//
//                if (brand.equals("ST")) {
//                    p.setEMDBRAND(result.getString("EMDBRAND"));
//                    brand = result.getString("EMDBRAND");
//                } else if (brand.equals(result.getString("EMDBRAND"))) {
//                    p.setEMDBRAND("<b style=\"opacity: 0;\">" + result.getString("EMDBRAND") + "</b>");
//                    brand = result.getString("EMDBRAND");
//                } else {
//                    p.setEMDBRAND(result.getString("EMDBRAND"));
//                    brand = result.getString("EMDBRAND");
//
//                    type = "ST";
//                    style = "ST";
//                    color = "ST";
//                    code = "ST";
//                    cup = "ST";
//                }
//
//                if (type.equals("ST")) {
//                    p.setEMDTYPE(result.getString("EMDTYPE"));
//                    type = result.getString("EMDTYPE");
//                } else if (type.equals(result.getString("EMDTYPE"))) {
//                    p.setEMDTYPE("<b style=\"opacity: 0;\">" + result.getString("EMDTYPE") + "</b>");
//                    type = result.getString("EMDTYPE");
//                } else {
//                    p.setEMDTYPE(result.getString("EMDTYPE"));
//                    type = result.getString("EMDTYPE");
//
//                    style = "ST";
//                    color = "ST";
//                    code = "ST";
//                    cup = "ST";
//                }
//
//                if (style.equals("ST")) {
//                    p.setEMDSTYLE(result.getString("EMDSTYLE"));
//                    style = result.getString("EMDSTYLE");
//                } else if (style.equals(result.getString("EMDSTYLE"))) {
//                    p.setEMDSTYLE("<b style=\"opacity: 0;\">" + result.getString("EMDSTYLE") + "</b>");
//                    style = result.getString("EMDSTYLE");
//                } else {
//                    p.setEMDSTYLE(result.getString("EMDSTYLE"));
//                    style = result.getString("EMDSTYLE");
//
//                    color = "ST";
//                    code = "ST";
//                    cup = "ST";
//                }
//
//                if (color.equals("ST")) {
//                    p.setEMDCOLOR(result.getString("EMDCOLOR"));
//                    color = result.getString("EMDCOLOR");
//                } else if (color.equals(result.getString("EMDCOLOR"))) {
//                    p.setEMDCOLOR("<b style=\"opacity: 0;\">" + result.getString("EMDCOLOR") + "</b>");
//                    color = result.getString("EMDCOLOR");
//                } else {
//                    p.setEMDCOLOR(result.getString("EMDCOLOR"));
//                    color = result.getString("EMDCOLOR");
//
//                    code = "ST";
//                    cup = "ST";
//                }
//
//                if (code.equals("ST")) {
//                    p.setEMDCODE(result.getString("EMCDDESC"));
//                    code = result.getString("EMCDDESC");
//                } else if (code.equals(result.getString("EMCDDESC"))) {
//                    p.setEMDCODE("<b style=\"opacity: 0;\">" + result.getString("EMCDDESC") + "</b>");
//                    code = result.getString("EMCDDESC");
//                } else {
//                    p.setEMDCODE(result.getString("EMCDDESC"));
//                    code = result.getString("EMCDDESC");
//
//                    cup = "ST";
//                }
//
//                if (cup.equals("ST")) {
//                    p.setEMDCUP(result.getString("EMDCUP"));
//                    cup = result.getString("EMDCUP");
//                } else if (cup.equals(result.getString("EMDCUP"))) {
//                    p.setEMDCUP("<b style=\"opacity: 0;\">" + result.getString("EMDCUP") + "</b>");
//                    cup = result.getString("EMDCUP");
//                } else {
//                    p.setEMDCUP(result.getString("EMDCUP"));
//                    cup = result.getString("EMDCUP");
//                }
//
//                p.setEMDPONOdata(result.getString("EMDPONO"));
//                p.setEMDCODEdata(result.getString("EMDCODE"));
//                p.setEMDCODEdata2(result.getString("EMCDDESC"));
////                p.setEMDORDID(result.getString("EMDORDID"));
////                p.setEMDAUTH(result.getString("EMDAUTH"));
////                p.setEMDBRAND(result.getString("EMDBRAND"));
////                p.setEMDTYPE(result.getString("EMDTYPE"));
////                p.setEMDSTYLE(result.getString("EMDSTYLE"));
////                p.setEMDCOLOR(result.getString("EMDCOLOR"));
////                p.setEMDCUP(result.getString("EMDCUP"));
//                p.setEMDSIZES(result.getString("EMDSIZES"));
//                p.setEMDCOST(result.getString("EMDCOST"));
//                p.setEMDAMOUNT(result.getString("EMDAMOUNT"));
//
//                UAList.add(p);
            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public EMSCAC findByCode(String code) {

        EMSCAC p = new EMSCAC();

        String sql = "SELECT [EMHINVID]\n"
                + "      ,[EMHCUSID]\n"
                + "      ,[EMHCUSSHIPID]\n"
                + "      ,[EMHSHIPID]\n"
                + "      ,[EMHTRANSPID]+' : '+[EMTPDESC] AS [EMHTRANSPID]\n"
                + "      ,[EMHCURRENCY]\n"
                + "      ,[EMHUNIT]\n"
                + "      ,(substring([EMHSHIPDATE],1,4)+'-'+substring([EMHSHIPDATE],5,2)+'-'+substring([EMHSHIPDATE],7,2)) as [EMHSHIPDATE]\n"
                + "      ,[EMHAMTCTN]\n"
                + "	  ,(\n"
                + "	    SELECT [USERID]+' : '+SUBSTRING([USERS], 1, CHARINDEX(' ', [USERS])-1)\n"
                + "        FROM [RMShipment].[dbo].[MSSUSER]\n"
                + "        where [USERID] = [EMHUSER] COLLATE Thai_CI_AS\n"
                + "       ) AS [EMHUSER]\n"
                + "  FROM [IEMS].[dbo].[EMSCAC]\n"
                + "  LEFT JOIN [EMSTSP] ON [EMSTSP].[EMTPID] = [EMSCAC].[EMHTRANSPID]\n"
                + "  WHERE EMHINVID = '" + code + "'";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

//                p.setEMHINVID(result.getString("EMHINVID"));
//                p.setEMHCUSID(result.getString("EMHCUSID"));
//                p.setEMHCUSSHIPID(result.getString("EMHCUSSHIPID"));
//                p.setEMHSHIPID(result.getString("EMHSHIPID"));
//                p.setEMHTRANSPID(result.getString("EMHTRANSPID"));
//                p.setEMHCURRENCY(result.getString("EMHCURRENCY"));
//                p.setEMHUNIT(result.getString("EMHUNIT"));
//                p.setEMHSHIPDATE(result.getString("EMHSHIPDATE"));
//                p.setEMHAMTCTN(result.getString("EMHAMTCTN"));
//                p.setEMHUSER(result.getString("EMHUSER"));
            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return p;

    }

    public String check(String code) {
        PreparedStatement ps = null;
        ResultSet rs = null;
        String STRRETURN = null;
        try {
            String check = "Select * FROM [IEMS].[dbo].[EMSCAC] where [EMSDWDELNO] = '" + code + "' ";
            ps = connect.prepareStatement(check);
            rs = ps.executeQuery();
            if (rs.next()) {
                STRRETURN = "f";
            } else {
                STRRETURN = "t";
            }
        } catch (Exception exp) {
            exp.printStackTrace();
        }
        return STRRETURN;
    }

    public boolean add(String inv, String line, String order, String po, String style, String color,
            String cup, String size, String pcs, String net, String gross, String boxtype, String userid, String auth, String flag, String dist, String realFlag) {

        String GROSS = ", '" + gross + "'";
        String BOXTYPE = ", '" + boxtype + "'";

//        if (realFlag.equals("N")) {
        String BT = "(SELECT case when isnull([EMMBOXS2E],0) = 0 or isnull([EMMBOXS2E],0) < " + pcs + "\n"
                + "       then \n"
                + "	   (\n"
                + "			case when isnull([EMMBOXS4D],0) = 0 or isnull([EMMBOXS4D],0) < " + pcs + "\n"
                + "			then \n"
                + "			(\n"
                + "				case when isnull([EMMBOXS4E],0) = 0 or isnull([EMMBOXS4E],0) < " + pcs + "\n"
                + "				then \n"
                + "				(\n"
                + "					case when isnull([EMMBOXS6D],0) = 0 or isnull([EMMBOXS6D],0) < " + pcs + "\n"
                + "					then \n"
                + "					(\n"
                + "						case when isnull([EMMBOXS6E],0) = 0 or isnull([EMMBOXS6E],0) < " + pcs + "\n"
                + "						then \n"
                + "						''\n"
                + "						else '6E' end\n"
                + "					)  \n"
                + "					else '6D' end\n"
                + "				) \n"
                + "				else '4E' end\n"
                + "			)\n"
                + "			else '4D' end\n"
                + "	   )\n"
                + "	   else '2E' end AS BOXPACK\n"
                + "  FROM [IEMS].[dbo].[EMSMAS]\n"
                + "  where [EMMSTYLE] = '" + style + "'\n"
                + "  and [EMMCUP] = '" + cup + "'\n"
                + "  and [EMMSIZE] = '" + size + "' and [EMMZONE] = '" + dist + "')";

        GROSS = ", (SELECT top 1 cast(isnull((isnull([EMMWEIGHT],0) * " + pcs + "),0) as decimal(16,2)) +\n"
                + "  cast(case when isnull([EMMWG2E],0) = 0 or isnull([EMMBOXS2E],0) < " + pcs + "\n"
                + "       then \n"
                + "	   (\n"
                + "			case when isnull([EMMWG4D],0) = 0 or isnull([EMMBOXS4D],0) < " + pcs + "\n"
                + "			then \n"
                + "			(\n"
                + "				case when isnull([EMMWG4E],0) = 0 or isnull([EMMBOXS4E],0) < " + pcs + "\n"
                + "				then \n"
                + "				(\n"
                + "					case when isnull([EMMWG6D],0) = 0 or isnull([EMMBOXS6D],0) < " + pcs + "\n"
                + "					then \n"
                + "					(\n"
                + "						case when isnull([EMMWG6E],0) = 0 or isnull([EMMBOXS6E],0) < " + pcs + "\n"
                + "						then \n"
                + "						(\n"
                + "							SELECT FORMAT(ISNULL([EMDSBOXW],0),'#,#0.00') AS [EMDSBOXW]\n"
                + "							FROM [IEMS].[dbo].[EMSDMS]\n"
                + "							where [EMDSZONE] = '" + dist + "'\n"
                + "							and [EMDSBOX] like '%'+" + BT + "+'%'\n"
                + "						)\n"
                + "						else FORMAT([EMMWG6E],'#,#0.00') end\n"
                + "					)  \n"
                + "					else FORMAT([EMMWG6D],'#,#0.00') end\n"
                + "				) \n"
                + "				else FORMAT([EMMWG4E],'#,#0.00') end\n"
                + "			)\n"
                + "			else FORMAT([EMMWG4D],'#,#0.00') end\n"
                + "	   )\n"
                + "	   else FORMAT([EMMWG2E],'#,#0.00') end as decimal(16,2))\n"
                + "  FROM [IEMS].[dbo].[EMSMAS]\n"
                + "  where [EMMSTYLE] = '" + style + "'\n"
                + "  and [EMMCUP] = '" + cup + "'\n"
                + "  and [EMMSIZE] = '" + size + "' and [EMMZONE] = '" + dist + "')";

        BOXTYPE = ", " + BT;
//        }

        boolean result = false;

        String sql = "INSERT INTO EMSCAC"
                + " ([EMCCOM]\n"
                + "      ,[EMCINVID]\n"
                + "      ,[EMCCTNO]\n"
                + "      ,[EMCORDID]\n"
                + "      ,[EMCPONO]\n"
                + "      ,[EMCSTYLE]\n"
                + "      ,[EMCCOLOR]\n"
                + "      ,[EMCCUP]\n"
                + "      ,[EMCSIZES]\n"
                + "      ,[EMCPCS]\n"
                + "      ,[EMCNET]\n"
                + "      ,[EMCGROSS]\n"
                + "      ,[EMCBOXTYPE]\n"
                + "      ,[EMCEDT]\n"
                + "      ,[EMCCDT]\n"
                + "      ,[EMCUSER]\n"
                + "      ,[EMCAUTH]\n"
                + "      ,[EMCFLAG]\n"
                + "      ,[EMCSIZELIST],[EMCCUPLIST],[EMCCOLORLIST],[EMCZONE])"
                + " VALUES('TWC'"
                + ", '" + inv + "'"
                + ", '" + line + "'"
                + ", '" + order + "'"
                + ", '" + po + "'"
                + ", '" + style + "'"
                + ", '" + color + "'"
                + ", '" + cup + "'"
                + ", '" + size + "'"
                + ", '" + pcs + "'"
                + ", (SELECT cast(isnull((isnull([EMMWEIGHT],0) * " + pcs + "),0) as decimal(16,2))\n"
                + "  FROM [IEMS].[dbo].[EMSMAS]\n"
                + "  where [EMMSTYLE] = '" + style + "'\n"
                + "      and [EMMCUP] = '" + cup + "'\n"
                + "      and [EMMSIZE] = '" + size + "' AND [EMMZONE] = '" + dist + "')\n"
                + GROSS
                + BOXTYPE
                + ", CURRENT_TIMESTAMP"
                + ", CURRENT_TIMESTAMP"
                + ", '" + userid + "'"
                + ", '" + auth + "'"
                + ", '" + flag + "'"
                + ", '" + size + ":" + pcs + ";'"
                + ", '" + cup + ":" + pcs + "%" + size + ";'"
                + ", '" + color + ";'"
                + ", '" + dist + "')";

//            System.out.println(sql);
        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public boolean addByObj(List<EMSINVD> aheadList, String inv, String userid, String dist) {

        String SELECT = "";

        for (int i = 0; i < aheadList.size(); i++) {
            String line = Integer.toString(i + 1);
            String order = aheadList.get(i).getEMDORDID();
            String po = aheadList.get(i).getEMDPONO();
            String style = aheadList.get(i).getEMDSTYLE();
            String color = aheadList.get(i).getEMDCOLOR();
            String cup = aheadList.get(i).getEMDCUP();
            String size = aheadList.get(i).getEMDSIZES();
            String pcs = aheadList.get(i).getPCS();
            String net = aheadList.get(i).getNET();
            String gross = aheadList.get(i).getGROSS();
            String boxtype = aheadList.get(i).getBOXTYPE();
            String auth = aheadList.get(i).getEMDAUTH();
            String flag = aheadList.get(i).getFlag();
            String realFlag = aheadList.get(i).getRealFlag();

            String GROSS = ", '" + gross + "'";
            String BOXTYPE = ", '" + boxtype + "'";

//        if (realFlag.equals("N")) {
            String BT = "(SELECT case when isnull([EMMBOXS2E],0) = 0 or isnull([EMMBOXS2E],0) < " + pcs + "\n"
                    + "       then \n"
                    + "	   (\n"
                    + "			case when isnull([EMMBOXS4D],0) = 0 or isnull([EMMBOXS4D],0) < " + pcs + "\n"
                    + "			then \n"
                    + "			(\n"
                    + "				case when isnull([EMMBOXS4E],0) = 0 or isnull([EMMBOXS4E],0) < " + pcs + "\n"
                    + "				then \n"
                    + "				(\n"
                    + "					case when isnull([EMMBOXS6D],0) = 0 or isnull([EMMBOXS6D],0) < " + pcs + "\n"
                    + "					then \n"
                    + "					(\n"
                    + "						case when isnull([EMMBOXS6E],0) = 0 or isnull([EMMBOXS6E],0) < " + pcs + "\n"
                    + "						then \n"
                    + "						''\n"
                    + "						else '6E' end\n"
                    + "					)  \n"
                    + "					else '6D' end\n"
                    + "				) \n"
                    + "				else '4E' end\n"
                    + "			)\n"
                    + "			else '4D' end\n"
                    + "	   )\n"
                    + "	   else '2E' end AS BOXPACK\n"
                    + "  FROM [IEMS].[dbo].[EMSMAS]\n"
                    + "  where [EMMSTYLE] = '" + style + "'\n"
                    + "  and [EMMCUP] = '" + cup + "'\n"
                    + "  and [EMMSIZE] = '" + size + "' and [EMMZONE] = '" + dist + "')";

            GROSS = ", (SELECT top 1 cast(isnull((isnull([EMMWEIGHT],0) * " + pcs + "),0) as decimal(16,2)) +\n"
                    + "  cast(case when isnull([EMMWG2E],0) = 0 or isnull([EMMBOXS2E],0) < " + pcs + "\n"
                    + "       then \n"
                    + "	   (\n"
                    + "			case when isnull([EMMWG4D],0) = 0 or isnull([EMMBOXS4D],0) < " + pcs + "\n"
                    + "			then \n"
                    + "			(\n"
                    + "				case when isnull([EMMWG4E],0) = 0 or isnull([EMMBOXS4E],0) < " + pcs + "\n"
                    + "				then \n"
                    + "				(\n"
                    + "					case when isnull([EMMWG6D],0) = 0 or isnull([EMMBOXS6D],0) < " + pcs + "\n"
                    + "					then \n"
                    + "					(\n"
                    + "						case when isnull([EMMWG6E],0) = 0 or isnull([EMMBOXS6E],0) < " + pcs + "\n"
                    + "						then \n"
                    + "						(\n"
                    + "							SELECT FORMAT(ISNULL([EMDSBOXW],0),'#,#0.00') AS [EMDSBOXW]\n"
                    + "							FROM [IEMS].[dbo].[EMSDMS]\n"
                    + "							where [EMDSZONE] = '" + dist + "'\n"
                    + "							and [EMDSBOX] like '%'+" + BT + "+'%'\n"
                    + "						)\n"
                    + "						else FORMAT([EMMWG6E],'#,#0.00') end\n"
                    + "					)  \n"
                    + "					else FORMAT([EMMWG6D],'#,#0.00') end\n"
                    + "				) \n"
                    + "				else FORMAT([EMMWG4E],'#,#0.00') end\n"
                    + "			)\n"
                    + "			else FORMAT([EMMWG4D],'#,#0.00') end\n"
                    + "	   )\n"
                    + "	   else FORMAT([EMMWG2E],'#,#0.00') end as decimal(16,2))\n"
                    + "  FROM [IEMS].[dbo].[EMSMAS]\n"
                    + "  where [EMMSTYLE] = '" + style + "'\n"
                    + "  and [EMMCUP] = '" + cup + "'\n"
                    + "  and [EMMSIZE] = '" + size + "' and [EMMZONE] = '" + dist + "')";

            BOXTYPE = ", " + BT;
//        }

            String union = "";
            if (i == (aheadList.size() - 1)) {
                union = "";
            }

            SELECT += "INSERT INTO EMSCAC\n"
                    + " ([EMCCOM]\n"
                    + "      ,[EMCINVID]\n"
                    + "      ,[EMCCTNO]\n"
                    + "      ,[EMCORDID]\n"
                    + "      ,[EMCPONO]\n"
                    + "      ,[EMCSTYLE]\n"
                    + "      ,[EMCCOLOR]\n"
                    + "      ,[EMCCUP]\n"
                    + "      ,[EMCSIZES]\n"
                    + "      ,[EMCPCS]\n"
                    + "      ,[EMCNET]\n"
                    + "      ,[EMCGROSS]\n"
                    + "      ,[EMCBOXTYPE]\n"
                    + "      ,[EMCEDT]\n"
                    + "      ,[EMCCDT]\n"
                    + "      ,[EMCUSER]\n"
                    + "      ,[EMCAUTH]\n"
                    + "      ,[EMCFLAG]\n"
                    + "      ,[EMCSIZELIST],[EMCCUPLIST],[EMCCOLORLIST],[EMCZONE])\n"
                    + " SELECT 'TWC'"
                    + ", '" + inv + "'"
                    + ", '" + line + "'"
                    + ", '" + order + "'"
                    + ", '" + po + "'"
                    + ", '" + style + "'"
                    + ", '" + color + "'"
                    + ", '" + cup + "'"
                    + ", '" + size + "'"
                    + ", '" + pcs + "'"
                    + ", (SELECT cast(isnull((isnull([EMMWEIGHT],0) * " + pcs + "),0) as decimal(16,2))\n"
                    + "  FROM [IEMS].[dbo].[EMSMAS]\n"
                    + "  where [EMMSTYLE] = '" + style + "'\n"
                    + "      and [EMMCUP] = '" + cup + "'\n"
                    + "      and [EMMSIZE] = '" + size + "' AND [EMMZONE] = '" + dist + "')\n"
                    + GROSS
                    + BOXTYPE
                    + ", CURRENT_TIMESTAMP"
                    + ", CURRENT_TIMESTAMP"
                    + ", '" + userid + "'"
                    + ", '" + auth + "'"
                    + ", '" + flag + "'"
                    + ", '" + size + ":" + pcs + ";'"
                    + ", '" + cup + ":" + pcs + "%" + size + ";'"
                    + ", '" + color + ";'"
                    + ", '" + dist + "' " + union + " \n";
        }

        boolean result = false;

        String sql = SELECT;

//        System.out.println(sql);
        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public boolean addCac(String inv, String order, String po, String style, String color,
            String cup, String size, String pcs, String boxtype, String userid, String auth, String dist, String carton) {

        boolean result = false;

        if (carton.trim().equals("")) {
            carton = "(SELECT max([EMCCARTONNO])+1 FROM [IEMS].[dbo].[EMSCAC] where EMCINVID='" + inv + "')";
        } else {
            carton = "'" + carton + "'";
        }

        String sql = "INSERT INTO EMSCAC"
                + " ([EMCCOM]\n"
                + "      ,[EMCINVID]\n"
                + "      ,[EMCCTNO]\n"
                + "      ,[EMCORDID]\n"
                + "      ,[EMCPONO]\n"
                + "      ,[EMCSTYLE]\n"
                + "      ,[EMCCOLOR]\n"
                + "      ,[EMCCUP]\n"
                + "      ,[EMCSIZES]\n"
                + "      ,[EMCPCS]\n"
                + "      ,[EMCNET]\n"
                + "      ,[EMCGROSS]\n"
                + "      ,[EMCBOXTYPE]\n"
                + "      ,[EMCEDT]\n"
                + "      ,[EMCCDT]\n"
                + "      ,[EMCUSER]\n"
                + "      ,[EMCAUTH]\n"
                + "      ,[EMCSIZELIST]\n"
                + "      ,[EMCFLAG]\n"
                + "      ,[EMCCARTONNO],EMCZONE,[EMCCUPLIST])\n"
                + " VALUES('TWC'"
                + ", '" + inv + "'"
                + ", (SELECT max([EMCCTNO])+1 FROM [IEMS].[dbo].[EMSCAC] where EMCINVID='" + inv + "')"
                + ", '" + order + "'"
                + ", '" + po + "'"
                + ", '" + style + "'"
                + ", '" + color + "'"
                + ", '" + cup + "'"
                + ", '" + size + "'"
                + ", '" + pcs + "'"
                + ", (SELECT top 1 cast(isnull((isnull([EMMWEIGHT],0) * " + pcs + "),0) as decimal(16,2))\n"
                + "  FROM [IEMS].[dbo].[EMSMAS]\n"
                + "  where [EMMSTYLE] = '" + style + "'\n"
                + "      and [EMMCUP] = '" + cup + "'\n"
                + "      and [EMMSIZE] = '" + size + "' and [EMMZONE] = '" + dist + "')"
                + ", (SELECT top 1 cast(isnull((isnull([EMMWEIGHT],0) * " + pcs + "),0) as decimal(16,2)) +\n"
                + "  cast(case when isnull([EMMWG6E],0) = 0 \n"
                + "       then \n"
                + "	   (\n"
                + "			case when isnull([EMMWG6D],0) = 0 \n"
                + "			then \n"
                + "			(\n"
                + "				case when isnull([EMMWG4E],0) = 0 \n"
                + "				then \n"
                + "				(\n"
                + "					case when isnull([EMMWG4D],0) = 0 \n"
                + "					then \n"
                + "					(\n"
                + "						case when isnull([EMMWG2E],0) = 0 \n"
                + "						then \n"
                + "						(\n"
                + "							SELECT FORMAT(ISNULL([EMDSBOXW],0),'#,#0.00') AS [EMDSBOXW]\n"
                + "							FROM [IEMS].[dbo].[EMSDMS]\n"
                + "							where [EMDSZONE] = '" + dist + "'\n"
                + "							and [EMDSBOX] like '%'+'" + boxtype + "'+'%'\n"
                + "						)\n"
                + "						else FORMAT([EMMWG2E],'#,#0.00') end\n"
                + "					)  \n"
                + "					else FORMAT([EMMWG4D],'#,#0.00') end\n"
                + "				) \n"
                + "				else FORMAT([EMMWG4E],'#,#0.00') end\n"
                + "			)\n"
                + "			else FORMAT([EMMWG6D],'#,#0.00') end\n"
                + "	   )\n"
                + "	   else FORMAT([EMMWG6E],'#,#0.00') end as decimal(16,2))\n"
                + "  FROM [IEMS].[dbo].[EMSMAS]\n"
                + "  where [EMMSTYLE] = '" + style + "'\n"
                + "      and [EMMCUP] = '" + cup + "'\n"
                + "      and [EMMSIZE] = '" + size + "' and [EMMZONE] = '" + dist + "')"
                + ", '" + boxtype + "'"
                + ", CURRENT_TIMESTAMP"
                + ", CURRENT_TIMESTAMP"
                + ", '" + userid + "'"
                + ", '" + auth + "'"
                + ", '" + size + ":" + pcs + ";'"
                + ", 'I'"
                + ", " + carton
                + ", '" + dist + "', '" + cup + ":" + pcs + "%" + size + ";')";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public boolean edit(String code, String desc, String uid) {

        boolean result = false;

        String sql = "UPDATE EMSCAC "
                + "SET EMUMDESC = '" + desc + "'"
                + ", EMUMEDT = CURRENT_TIMESTAMP"
                + ", EMUMUSER = '" + uid + "'"
                + " WHERE EMUMID = '" + code + "'";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public boolean genCarAsean(String inv, String ctno, String car, String uid, String dist, String net, String boxtype, boolean isLast) {

        boolean result = false;

        String GROSS = "0";
        if (isLast) {
            GROSS = "(SELECT TOP 1 ISNULL([EMDSBOXW],0) + " + net + "\n"
                    + "  FROM [IEMS].[dbo].[EMSDMS]\n"
                    + "  WHERE [EMDSZONE] = '" + dist + "'\n"
                    + "  AND REPLACE([EMDSBOX],'BOX','') = '" + boxtype + "')";
        }

        String sql = "UPDATE EMSCAC "
                + "SET EMCCARTONNO = '" + car + "'"
                + ", EMCGROSS = " + GROSS
                + ", EMCEDT = CURRENT_TIMESTAMP"
                + ", EMCUSER = '" + uid + "'"
                + " WHERE EMCINVID = '" + inv + "' AND EMCCTNO = '" + ctno + "' AND EMCZONE = '" + dist + "'";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public boolean genCar(String inv, String ctno, String car, String uid, String dist) {

        boolean result = false;

        String sql = "UPDATE EMSCAC "
                + "SET EMCCARTONNO = '" + car + "'"
                + ", EMCEDT = CURRENT_TIMESTAMP"
                + ", EMCUSER = '" + uid + "'"
                + " WHERE EMCINVID = '" + inv + "' AND EMCCTNO = '" + ctno + "' AND EMCZONE = '" + dist + "'";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public boolean updateNetGrossCac(String inv, String ctno, String net, String gross, String dist) {

        boolean result = false;

        String sql = "UPDATE EMSCAC "
                + "SET EMCNET = '" + net + "' "
                + ", EMCGROSS = '" + gross + "' "
                + ", [EMCCUP] = substring([EMCCUPLIST],0,charindex(':',[EMCCUPLIST])) "
                + ", [EMCSIZES] = substring([EMCCUPLIST],charindex('%',[EMCCUPLIST])+1,charindex(';',[EMCCUPLIST])-(charindex('%',[EMCCUPLIST])+1)) "
                + " WHERE EMCINVID = '" + inv + "' AND EMCCTNO = '" + ctno + "' AND EMCZONE = '" + dist + "'";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public boolean CLgenCar(String inv, String uid, String dist) {

        boolean result = false;

        String sql = "UPDATE EMSCAC "
                + "SET EMCCARTONNO = null"
                + ", EMCEDT = CURRENT_TIMESTAMP"
                + ", EMCUSER = '" + uid + "'"
                + " WHERE EMCINVID = '" + inv + "' AND EMCZONE = '" + dist + "' ";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public boolean genCarF1(String inv, String uid, String dist) {

        boolean result = false;

        String sql = "UPDATE EMSCAC "
                + "SET EMCCARTONNO = EMCCTNO"
                + ", EMCEDT = CURRENT_TIMESTAMP"
                + ", EMCUSER = '" + uid + "'"
                + " WHERE EMCINVID = '" + inv + "' AND EMCZONE = '" + dist + "' ";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public boolean editDropBox2(String inv, String ctno, String sizeList, String pcs, String net, String gross, String flag, String uid, String dist, String cupList, String colorList) {

        boolean result = false;

        String sql = "UPDATE EMSCAC \n"
                + " SET [EMCSIZELIST] = '" + sizeList + "'\n"
                + "      ,[EMCPCS] = '" + pcs + "'\n"
                + "      ,[EMCNET] = '" + net + "'\n"
                + "      ,[EMCGROSS] = '" + gross + "'\n"
                + "	 ,[EMCFLAG] = '" + flag + "'\n"
                + "	 ,[EMCCUPLIST] = '" + cupList + "'\n"
                + "	 ,[EMCCOLORLIST] = '" + colorList + "'\n"
                + "	 ,[EMCUSER] = '" + uid + "'\n"
                + " WHERE EMCINVID = '" + inv + "' \n"
                + " AND EMCCTNO = '" + ctno + "' "
                + " AND EMCZONE = '" + dist + "' ";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public String checkBoxW(String inv, String dist, String ctno, String boxtype) {
        PreparedStatement ps = null;
        ResultSet rs = null;
        String STRRETURN = null;
        try {
            String check = "SELECT [EMCINVID],[EMCCTNO],[EMCZONE],[EMCSTYLE],CUP,SIZE,[EMCBOXTYPE]\n"
                    + ",(case when '" + boxtype + "' = '6E' then isnull([EMMWG6E],0) else 0 end\n"
                    + "+case when '" + boxtype + "' = '6D' then isnull([EMMWG6D],0) else 0 end\n"
                    + "+case when '" + boxtype + "' = '4E' then isnull([EMMWG4E],0) else 0 end\n"
                    + "+case when '" + boxtype + "' = '4D' then isnull([EMMWG4D],0) else 0 end\n"
                    + "+case when '" + boxtype + "' = '2E' then isnull([EMMWG2E],0) else 0 end) as BOXW\n"
                    + ",case when (case when '" + boxtype + "' = '6E' then isnull([EMMWG6E],0) else 0 end\n"
                    + "+case when '" + boxtype + "' = '6D' then isnull([EMMWG6D],0) else 0 end\n"
                    + "+case when '" + boxtype + "' = '4E' then isnull([EMMWG4E],0) else 0 end\n"
                    + "+case when '" + boxtype + "' = '4D' then isnull([EMMWG4D],0) else 0 end\n"
                    + "+case when '" + boxtype + "' = '2E' then isnull([EMMWG2E],0) else 0 end) > 0 then 'YES' else 'NO' end as IS_HAVE_BOXW\n"
                    + "FROM(\n"
                    + "SELECT [EMCCOM]\n"
                    + "      ,[EMCINVID]\n"
                    + "      ,[EMCCTNO]\n"
                    + "      ,[EMCORDID]\n"
                    + "      ,[EMCPONO]\n"
                    + "      ,[EMCSTYLE]\n"
                    + "      ,[EMCCOLOR]\n"
                    + "      ,[EMCCUP]\n"
                    + "      ,[EMCSIZES]\n"
                    + "      ,[EMCPCS]\n"
                    + "      ,[EMCNET]\n"
                    + "      ,[EMCGROSS]\n"
                    + "      ,[EMCBOXTYPE]\n"
                    + "      ,[EMCEDT]\n"
                    + "      ,[EMCCDT]\n"
                    + "      ,[EMCUSER]\n"
                    + "      ,[EMCAUTH]\n"
                    + "      ,[EMCSIZELIST]\n"
                    + "      ,[EMCFLAG]\n"
                    + "      ,[EMCCARTONNO]\n"
                    + "      ,[EMCSTS]\n"
                    + "      ,[EMCZONE]\n"
                    + "      ,[EMCCUPLIST]\n"
                    + "	  ,SUBSTRING([EMCCUPLIST], 1, CHARINDEX(':', [EMCCUPLIST])-1) as CUP\n"
                    + "	  ,SUBSTRING([EMCCUPLIST], CHARINDEX(':', [EMCCUPLIST])+1, CHARINDEX('%', [EMCCUPLIST])-CHARINDEX(':', [EMCCUPLIST])-1) as PCS\n"
                    + "	  ,SUBSTRING([EMCCUPLIST], CHARINDEX('%', [EMCCUPLIST])+1, CHARINDEX(';', [EMCCUPLIST])-CHARINDEX('%', [EMCCUPLIST])-1) as SIZE\n"
                    + "  FROM [IEMS].[dbo].[EMSCAC]\n"
                    + "  where [EMCZONE]='" + dist + "'\n"
                    + "  and [EMCINVID]='" + inv + "'\n"
                    + "  and [EMCCTNO]='" + ctno + "'\n"
                    + "  and LEN(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE([EMCCUPLIST], 'X', ''), '*', ''), '0', ''), '1', ''), '2', ''), '3', ''), '4', ''), '5', ''), '6', ''), '7', ''), '8', ''), '9', ''), 'S', ''), 'M', ''), 'L', ''), 'A', ''), 'B', ''), 'C', ''), 'D', ''), 'E', ''), 'F', ''), 'G', ''), 'H', ''), 'I', ''), 'J', ''), 'K', ''), 'N', ''), 'O', ''), 'P', ''), 'Q', ''), 'R', ''), 'T', ''), 'U', ''), 'V', ''), 'W', ''), 'Y', ''), 'Z', ''), ':', ''), '%', ''))=1\n"
                    + ") TMP \n"
                    + "LEFT JOIN EMSMAS ON [EMMZONE] = [EMCZONE]\n"
                    + "				and [EMMSTYLE] = [EMCSTYLE]\n"
                    + "				and [EMMCUP] = CUP\n"
                    + "				and [EMMSIZE] = SIZE\n";
//            System.out.println(check);
            ps = connect.prepareStatement(check);
            rs = ps.executeQuery();
            if (rs.next()) {
                STRRETURN = rs.getString("IS_HAVE_BOXW");
            } else {
                STRRETURN = "t";
            }
        } catch (Exception exp) {
            exp.printStackTrace();
        }
        return STRRETURN;
    }

    public boolean editCac(String inv, String ctno, String style, String cup, String size, String dist,
            String sizeList, String pcs, String po, String gw, String boxtype, String isHaveBoxW, String colorList) {

        boolean result = false;

        String BOXW = "cast((\n"
                + "	SELECT FORMAT(ISNULL([EMDSBOXW],0),'#,#0.00') AS [EMDSBOXW]\n"
                + "	FROM [IEMS].[dbo].[EMSDMS]\n"
                + "	where [EMDSZONE] = '" + dist + "'\n"
                + "	and [EMDSBOX] like '%'+'" + boxtype + "'+'%'\n"
                + ") as decimal(16,2))\n";

        if (isHaveBoxW.equals("YES")) {
            BOXW = "cast(case when isnull([EMMWG6E],0) = 0 \n"
                    + "       then \n"
                    + "	   (\n"
                    + "			case when isnull([EMMWG6D],0) = 0 \n"
                    + "			then \n"
                    + "			(\n"
                    + "				case when isnull([EMMWG4E],0) = 0 \n"
                    + "				then \n"
                    + "				(\n"
                    + "					case when isnull([EMMWG4D],0) = 0 \n"
                    + "					then \n"
                    + "					(\n"
                    + "						case when isnull([EMMWG2E],0) = 0 \n"
                    + "						then \n"
                    + "						(\n"
                    + "							SELECT FORMAT(ISNULL([EMDSBOXW],0),'#,#0.00') AS [EMDSBOXW]\n"
                    + "							FROM [IEMS].[dbo].[EMSDMS]\n"
                    + "							where [EMDSZONE] = '" + dist + "'\n"
                    + "							and [EMDSBOX] like '%'+'" + boxtype + "'+'%'\n"
                    + "						)\n"
                    + "						else FORMAT([EMMWG2E],'#,#0.00') end\n"
                    + "					)  \n"
                    + "					else FORMAT([EMMWG4D],'#,#0.00') end\n"
                    + "				) \n"
                    + "				else FORMAT([EMMWG4E],'#,#0.00') end\n"
                    + "			)\n"
                    + "			else FORMAT([EMMWG6D],'#,#0.00') end\n"
                    + "	   )\n"
                    + "	   else FORMAT([EMMWG6E],'#,#0.00') end as decimal(16,2))\n";
        }

        String sql = "UPDATE EMSCAC \n"
                + " SET [EMCSIZELIST] = '" + sizeList + "'\n"
                + "      ,[EMCPCS] = '" + pcs + "'\n"
                + "      ,[EMCPONO] = '" + po + "'\n"
                + "      ,[EMCGROSS] = (SELECT top 1 cast(isnull((isnull([EMMWEIGHT],0) * " + pcs + "),0) as decimal(16,2)) +\n"
                + BOXW
                + "  FROM [IEMS].[dbo].[EMSMAS]\n"
                + "  where [EMMSTYLE] = '" + style + "'\n"
                + "      and [EMMCUP] = '" + cup + "'\n"
                + "      and [EMMSIZE] = '" + size + "' and [EMMZONE] = '" + dist + "')\n"
                + ",[EMCNET] = (SELECT cast(isnull((isnull([EMMWEIGHT],0) * " + pcs + "),0) as decimal(16,2))\n"
                + "  FROM [IEMS].[dbo].[EMSMAS]\n"
                + "  where [EMMSTYLE] = '" + style + "'\n"
                + "      and [EMMCUP] = '" + cup + "'\n"
                + "      and [EMMSIZE] = '" + size + "' AND [EMMZONE] = '" + dist + "')\n"
                + ",[EMCBOXTYPE] = '" + boxtype + "'\n"
                + ",[EMCCOLORLIST] = '" + colorList + "'\n"
                + " WHERE EMCINVID = '" + inv + "' \n"
                + " AND EMCCTNO = '" + ctno + "' "
                + " AND EMCZONE = '" + dist + "' ";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }
        return result;

    }

    public boolean editCupListCac(String inv, String ctno, String cupList) {

        boolean result = false;

        String sql = "update [IEMS].[dbo].[EMSCAC]\n"
                + "set [EMCCUPLIST] = '" + cupList + "'\n"
                + "  where [EMCINVID]='" + inv + "'\n"
                + "  and [EMCCTNO]='" + ctno + "'";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }
        return result;

    }

    public boolean editBoxtypeCac(String inv, String ctno, String dist, String boxtype, String style, String cup, String size, String pcs) {

        boolean result = false;

        String sql = "UPDATE EMSCAC \n"
                + " SET [EMCBOXTYPE] = '" + boxtype + "'\n"
                + "     ,[EMCGROSS] = (SELECT top 1 cast(isnull((isnull([EMMWEIGHT],0) * " + pcs + "),0) as decimal(16,2)) +\n"
                + "  cast(case when isnull([EMMWG6E],0) = 0 \n"
                + "       then \n"
                + "	   (\n"
                + "			case when isnull([EMMWG6D],0) = 0 \n"
                + "			then \n"
                + "			(\n"
                + "				case when isnull([EMMWG4E],0) = 0 \n"
                + "				then \n"
                + "				(\n"
                + "					case when isnull([EMMWG4D],0) = 0 \n"
                + "					then \n"
                + "					(\n"
                + "						case when isnull([EMMWG2E],0) = 0 \n"
                + "						then \n"
                + "						(\n"
                + "							SELECT FORMAT(ISNULL([EMDSBOXW],0),'#,#0.00') AS [EMDSBOXW]\n"
                + "							FROM [IEMS].[dbo].[EMSDMS]\n"
                + "							where [EMDSZONE] = '" + dist + "'\n"
                + "							and [EMDSBOX] like '%'+'" + boxtype + "'+'%'\n"
                + "						)\n"
                + "						else FORMAT([EMMWG2E],'#,#0.00') end\n"
                + "					)  \n"
                + "					else FORMAT([EMMWG4D],'#,#0.00') end\n"
                + "				) \n"
                + "				else FORMAT([EMMWG4E],'#,#0.00') end\n"
                + "			)\n"
                + "			else FORMAT([EMMWG6D],'#,#0.00') end\n"
                + "	   )\n"
                + "	   else FORMAT([EMMWG6E],'#,#0.00') end as decimal(16,2))\n"
                + "  FROM [IEMS].[dbo].[EMSMAS]\n"
                + "  where [EMMSTYLE] = '" + style + "'\n"
                + "      and [EMMCUP] = '" + cup + "'\n"
                + "      and [EMMSIZE] = '" + size + "' and [EMMZONE] = '" + dist + "')\n"
                + " WHERE EMCINVID = '" + inv + "' \n"
                + " AND EMCCTNO = '" + ctno + "' \n"
                + " AND EMCZONE = '" + dist + "' ";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }
        return result;

    }

    public boolean updateFg(String inv, String dist, String ctnFrom, String ctnTo, String fgNo) {

        boolean result = false;

        String sql = "UPDATE EMSCAC \n"
                + " SET [EMCFGNO] = '" + fgNo + "'\n"
                + " WHERE EMCINVID = '" + inv + "' \n"
                + " AND (EMCCARTONNO between '" + ctnFrom + "' and '" + ctnTo + "') \n"
                + " AND EMCZONE = '" + dist + "' ";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }
        return result;

    }

    public void delete(String code, String dist) {

        String sql = "DELETE FROM EMSCAC WHERE [EMCINVID] = '" + code + "' AND [EMCZONE] = '" + dist + "'";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ps.executeUpdate();

        } catch (Exception e) {

            e.printStackTrace();

        }

    }

    public void clearCartonCac(String inv, String carton, String dist) {

        String sql = "UPDATE EMSCAC SET [EMCCARTONNO] = null WHERE [EMCINVID] = '" + inv + "' AND [EMCCARTONNO] = '" + carton + "' AND [EMCZONE] = '" + dist + "'";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ps.executeUpdate();

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

    }

    public void deleteCac(String inv, String ctno, String flag, String dist) {

        String sql = "UPDATE EMSCAC SET EMCSTS = 'D' WHERE [EMCINVID] = '" + inv + "' AND [EMCCTNO] = '" + ctno + "' AND [EMCZONE] = '" + dist + "'";

        if (flag.trim().equals("I")) {
            sql = "DELETE FROM EMSCAC WHERE [EMCINVID] = '" + inv + "' AND [EMCCTNO] = '" + ctno + "' AND [EMCZONE] = '" + dist + "'";
        }

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ps.executeUpdate();

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

    }

    public void restoreCac(String inv, String ctno, String flag, String dist) {

        String sql = "UPDATE EMSCAC SET EMCSTS = null WHERE [EMCINVID] = '" + inv + "' AND [EMCCTNO] = '" + ctno + "' AND [EMCZONE] = '" + dist + "'";

//        if (flag.trim().equals("I")) {
//            sql = "DELETE FROM EMSCAC WHERE [EMCINVID] = '" + inv + "' AND [EMCCTNO] = '" + ctno + "' AND [EMCZONE] = '" + dist + "'";
//        }
        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ps.executeUpdate();

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

    }

    public EMSCAC findCac(String inv, String ctno, String dist) {

        EMSCAC p = new EMSCAC();

        String sql = "SELECT [EMCINVID]\n"
                + "      ,[EMCPONO]\n"
                + "      ,[EMCORDID]\n"
                + "      ,[EMCAUTH]\n"
                + "      ,[EMCSTYLE]\n"
                + "      ,[EMCCOLOR]\n"
                + "      ,[EMCCUP]\n"
                + "      ,[EMCSIZES]\n"
                + "      ,FORMAT(isnull([EMCPCS],0),'#0') as [EMCPCS] "
                + "FROM EMSCAC "
                + "WHERE [EMCINVID] = '" + inv + "' AND [EMCCTNO] = '" + ctno + "' AND [EMCZONE] = '" + dist + "'";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                p.setEMCINVID(result.getString("EMCINVID"));
                p.setEMCPONO(result.getString("EMCPONO"));
                p.setEMCORDID(result.getString("EMCORDID"));
                p.setEMCAUTH(result.getString("EMCAUTH"));
                p.setEMCSTYLE(result.getString("EMCSTYLE"));
                p.setEMCCOLOR(result.getString("EMCCOLOR"));
                p.setEMCCUP(result.getString("EMCCUP"));
                p.setEMCSIZES(result.getString("EMCSIZES"));
                p.setEMCPCS(result.getString("EMCPCS"));
            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return p;

    }

    public List<EMSCAC> getParentCAC(String inv, String dist) {

        List<EMSCAC> UAList = new ArrayList<EMSCAC>();

        String sql = "SELECT [EMCINVID]\n"
                + "      ,[EMCCTNO]\n"
                + "      ,[EMCSTYLE]\n"
                + "      ,[EMCBOXTYPE]\n"
                + "      ,[EMCSIZELIST]\n"
                + "      ,[EMCCARTONNO]\n"
                + "      ,[EMCZONE]\n"
                + "      ,[EMCCUPLIST]\n"
                + "      ,[EMCCOLORLIST]\n"
                + "  FROM [IEMS].[dbo].[EMSCAC]\n"
                + "  where [EMCINVID] = '" + inv + "'\n"
                + "  and [EMCZONE] = '" + dist + "'\n"
                + "  and [EMCCUPLIST] is not null\n"
                + "  and [EMCCUPLIST] != '' \n"
                + "  and [EMCFLAG] = 'N'";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                EMSCAC p = new EMSCAC();

                p.setEMCINVID(result.getString("EMCINVID"));
                p.setEMCCTNO(result.getString("EMCCTNO"));
                p.setEMCSTYLE(result.getString("EMCSTYLE"));
                p.setEMCBOXTYPE(result.getString("EMCBOXTYPE"));
                p.setEMCSIZELIST(result.getString("EMCSIZELIST"));
                p.setEMCCARTONNO(result.getString("EMCCARTONNO"));
                p.setEMCZONE(result.getString("EMCZONE"));
                p.setEMCCUPLIST(result.getString("EMCCUPLIST"));
                p.setEMCCOLORLIST(result.getString("EMCCOLORLIST"));

                String cupList = p.getEMCCUPLIST();
                int cntChar = 0;
                for (int i = 0; i < cupList.length(); i++) {
                    if (cupList.charAt(i) == ';') {
                        cntChar++;
                    }
                }
                if (cntChar > 1) {
                    UAList.add(p);
                }

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public boolean updateParent(String inv, String dist, String ctno) {

        boolean result = false;

        String sql = "UPDATE [EMSCAC]\n"
                + "SET [EMCPARENT] = 'Y'\n"
                + "  WHERE [EMCINVID] = '" + inv + "'\n"
                + "  AND [EMCZONE] = '" + dist + "'\n"
                + "  AND [EMCCTNO] = '" + ctno + "'";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public boolean deleteChild(String inv, String dist) {

        boolean result = false;

        String sql = "DELETE [EMSCAC]\n"
                + "  WHERE [EMCINVID] = '" + inv + "'\n"
                + "  AND [EMCZONE] = '" + dist + "'\n"
                + "  AND [EMCPARENT] = 'N'\n"
                + "\n"
                + "UPDATE [EMSCAC]\n"
                + "SET [EMCPARENT] = null\n"
                + "  WHERE [EMCINVID] = '" + inv + "'\n"
                + "  AND [EMCZONE] = '" + dist + "'\n"
                + "  AND [EMCPARENT] = 'Y'\n"
                + "\n"
                + "UPDATE [EMSCAC]\n"
                + "SET [EMCCHILDCTN] = [EMCCTNO]\n"
                + "  WHERE [EMCINVID] = '" + inv + "'\n"
                + "  AND [EMCZONE] = '" + dist + "'\n";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public boolean addChild(String inv, String dist, String ctno, String color, String cup, String size, String pcs) {

        String cuplist = cup + ":" + pcs + "%" + size + ";";

        boolean result = false;

        String sql = "INSERT INTO [EMSCAC] ([EMCCOM]\n"
                + "      ,[EMCINVID]\n"
                + "      ,[EMCCTNO]\n"
                + "      ,[EMCORDID]\n"
                + "      ,[EMCPONO]\n"
                + "      ,[EMCSTYLE]\n"
                + "      ,[EMCCOLOR]\n"
                + "      ,[EMCCUP]\n"
                + "      ,[EMCSIZES]\n"
                + "      ,[EMCPCS]\n"
                + "      ,[EMCNET]\n"
                + "      ,[EMCGROSS]\n"
                + "      ,[EMCBOXTYPE]\n"
                + "      ,[EMCEDT]\n"
                + "      ,[EMCCDT]\n"
                + "      ,[EMCUSER]\n"
                + "      ,[EMCAUTH]\n"
                + "      ,[EMCSIZELIST]\n"
                + "      ,[EMCFLAG]\n"
                + "      ,[EMCCARTONNO]\n"
                + "      ,[EMCSTS]\n"
                + "      ,[EMCZONE]\n"
                + "      ,[EMCCUPLIST]\n"
                + "      ,[EMCGRPNO]\n"
                + "      ,[EMCCOLORLIST]\n"
                + "      ,[EMCFGNO]\n"
                + "      ,[EMCCHILDCTN]\n"
                + "      ,[EMCSTYLELIST]\n"
                + "      ,[EMCPARENT])\n"
                + " SELECT [EMCCOM]\n"
                + "      ,[EMCINVID]\n"
                + "      ,'0'\n"
                + "      ,[EMCORDID]\n"
                + "      ,[EMCPONO]\n"
                + "      ,[EMCSTYLE]\n"
                + "      ,'" + color + "'\n"
                + "      ,'" + cup + "'\n"
                + "      ,'" + size + "'\n"
                + "      ,'" + pcs + "'\n"
                + "      ,(SELECT top 1 cast(isnull((isnull([EMMWEIGHT],0) * " + pcs + "),0) as decimal(16,2)) \n"
                + "          FROM [IEMS].[dbo].[EMSMAS]\n"
                + "          where [EMMSTYLE] = [EMCSTYLE]\n"
                + "          and [EMMCUP] = '" + cup + "'\n"
                + "          and [EMMSIZE] = '" + size + "' and [EMMZONE] = [EMCZONE])\n"
                + "      ,[EMCGROSS]\n"
                + "      ,[EMCBOXTYPE]\n"
                + "      ,current_timestamp\n"
                + "      ,current_timestamp\n"
                + "      ,[EMCUSER]\n"
                + "      ,[EMCAUTH]\n"
                + "      ,null\n"
                + "      ,[EMCFLAG]\n"
                + "      ,[EMCCARTONNO]\n"
                + "      ,[EMCSTS]\n"
                + "      ,[EMCZONE]\n"
                + "      ,'" + cuplist + "'\n"
                //                + "      ,null\n"
                + "      ,[EMCGRPNO]\n"
                + "      ,'"+color+";'\n"
//                + "      ,null\n"
                + "      ,[EMCFGNO]\n"
                + "      ,[EMCCTNO]\n"
                + "      ,null\n"
                + "      ,'N'\n"
                + "  FROM [IEMS].[dbo].[EMSCAC]\n"
                + "  where [EMCINVID] = '" + inv + "'\n"
                + "  and [EMCZONE] = '" + dist + "'\n"
                + "  and [EMCCTNO] = '" + ctno + "'";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public List<EMSCODE> findColMas(String invcol) {

        List<EMSCODE> list = new ArrayList<EMSCODE>();

        String sql = "SELECT DISTINCT [EMCDZONE]\n"
                + "      ,[EMCDCODE]\n"
                + "      ,[EMCDDESC]\n"
                + "      ,[EMCDCDT]\n"
                + "      ,[EMCDEDT]\n"
                + "      ,[EMCDUSER]\n"
                + "  FROM [IEMS].[dbo].[EMSCODE] WHERE EMCDZONE = (SELECT TOP 1 EMHZONE FROM [IEMS].[dbo].[EMSINVH] WHERE EMHINVID = '" + invcol + "')\n"
                + "  ORDER BY EMCDCODE";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {
                EMSCODE p = new EMSCODE();
                p.setEMCDZONE(result.getString("EMCDZONE"));
                p.setEMCDCODE(result.getString("EMCDCODE"));
                p.setEMCDDESC(result.getString("EMCDDESC"));
                list.add(p);
            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return list;

    }

}
