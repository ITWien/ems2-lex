/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.ems.dao;

import com.twc.ems.database.database;
import com.twc.ems.entity.EMSMAS;
import com.twc.ems.entity.EMSDMS;

import java.sql.*;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author nutthawoot.noo
 */
public class EMSDMSDao extends database {

    DecimalFormat formatDou = new DecimalFormat("#,###,###,##0.00");

    public List<EMSDMS> findAll() {

        List<EMSDMS> UAList = new ArrayList<EMSDMS>();

        String sql = "SELECT [EMDSZONE]\n"
                + "      ,[EMDSBOX]\n"
                + "      ,FORMAT([EMDSWEIGHT],'#,#0') AS [EMDSWEIGHT]\n"
                + "      ,FORMAT([EMDSDEEP],'#,#0') AS [EMDSDEEP]\n"
                + "      ,FORMAT([EMDSHEIGHT],'#,#0') AS [EMDSHEIGHT]\n"
                + "      ,[EMDSBOXW]\n"
                + "	  ,(\n"
                + "	     SELECT [USERID]+' : '+SUBSTRING([USERS], 1, CHARINDEX(' ', [USERS])-1)\n"
                + "         FROM [RMShipment].[dbo].[MSSUSER]\n"
                + "         where [USERID] = [EMDSUSER] COLLATE Thai_CI_AS\n"
                + "	  ) AS [EMDSUSER]\n"
                + "  FROM [IEMS].[dbo].[EMSDMS]";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                EMSDMS p = new EMSDMS();
//
                p.setEMDSZONE(result.getString("EMDSZONE"));
                p.setEMDSBOX(result.getString("EMDSBOX"));
                p.setEMDSWEIGHT(result.getString("EMDSWEIGHT"));
                p.setEMDSDEEP(result.getString("EMDSDEEP"));
                p.setEMDSHEIGHT(result.getString("EMDSHEIGHT"));
                p.setEMDSBOXW(result.getString("EMDSBOXW"));
                p.setEMDSUSER(result.getString("EMDSUSER"));

//
                UAList.add(p);

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public EMSDMS findByCode(String zone, String box) {

        EMSDMS p = new EMSDMS();

        String sql = "SELECT [EMDSZONE]\n"
                + "      ,[EMDSBOX]\n"
                + "      ,FORMAT([EMDSWEIGHT],'#,#0') AS [EMDSWEIGHT]\n"
                + "      ,FORMAT([EMDSDEEP],'#,#0') AS [EMDSDEEP]\n"
                + "      ,FORMAT([EMDSHEIGHT],'#,#0') AS [EMDSHEIGHT]\n"
                + "      ,[EMDSBOXW]\n"
                + "	  ,(\n"
                + "	     SELECT [USERID]+' : '+SUBSTRING([USERS], 1, CHARINDEX(' ', [USERS])-1)\n"
                + "         FROM [RMShipment].[dbo].[MSSUSER]\n"
                + "         where [USERID] = [EMDSUSER] COLLATE Thai_CI_AS\n"
                + "	  ) AS [EMDSUSER]\n"
                + "  FROM [IEMS].[dbo].[EMSDMS]"
                + "  where [EMDSZONE] = '" + zone + "' and [EMDSBOX] = '" + box + "'";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                p.setEMDSZONE(result.getString("EMDSZONE"));
                p.setEMDSBOX(result.getString("EMDSBOX"));
                p.setEMDSWEIGHT(result.getString("EMDSWEIGHT"));
                p.setEMDSDEEP(result.getString("EMDSDEEP"));
                p.setEMDSHEIGHT(result.getString("EMDSHEIGHT"));
                p.setEMDSBOXW(result.getString("EMDSBOXW"));
            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return p;

    }

    public String check(String zone, String box) {
        PreparedStatement ps = null;
        ResultSet rs = null;
        String STRRETURN = null;
        try {
            String check = "Select * FROM [IEMS].[dbo].[EMSDMS] where [EMDSZONE] = '" + zone + "' and [EMDSBOX] = '" + box + "'";
            ps = connect.prepareStatement(check);
            rs = ps.executeQuery();
            if (rs.next()) {
                STRRETURN = "f";
            } else {
                STRRETURN = "t";
            }
        } catch (Exception exp) {
            exp.printStackTrace();
        }
        return STRRETURN;
    }

    public boolean add(String zone, String box, String diw, String did, String dih, String boxw, String userid) {

        boolean result = false;

        String sql = "INSERT INTO EMSDMS"
                + " ([EMDSCOM]\n"
                + "      ,[EMDSZONE]\n"
                + "      ,[EMDSBOX]\n"
                + "      ,[EMDSWEIGHT]\n"
                + "      ,[EMDSDEEP]\n"
                + "      ,[EMDSHEIGHT]\n"
                + "      ,[EMDSBOXW]\n"
                + "      ,[EMDSCDATE]\n"
                + "      ,[EMDSEDATE]\n"
                + "      ,[EMDSUSER])"
                + " VALUES('TWC'"
                + ", '" + zone + "'"
                + ", '" + box + "'"
                + ", '" + diw + "'"
                + ", '" + did + "'"
                + ", '" + dih + "'"
                + ", '" + boxw + "'"
                + ", CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '" + userid + "')";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public boolean copy(String ozone, String nzone, String obox, String nbox, String userid) {

        boolean result = false;

        String sql = "INSERT INTO EMSDMS"
                + " ([EMDSCOM]\n"
                + "      ,[EMDSZONE]\n"
                + "      ,[EMDSBOX]\n"
                + "      ,[EMDSWEIGHT]\n"
                + "      ,[EMDSDEEP]\n"
                + "      ,[EMDSHEIGHT]\n"
                + "      ,[EMDSBOXW]\n"
                + "      ,[EMDSCDATE]\n"
                + "      ,[EMDSEDATE]\n"
                + "      ,[EMDSUSER])\n"
                + " SELECT TOP 1 [EMDSCOM]\n"
                + "      ,'" + nzone + "'\n"
                + "      ,'" + nbox + "'\n"
                + "      ,[EMDSWEIGHT]\n"
                + "      ,[EMDSDEEP]\n"
                + "      ,[EMDSHEIGHT]\n"
                + "      ,[EMDSBOXW]\n"
                + "      ,CURRENT_TIMESTAMP\n"
                + "      ,CURRENT_TIMESTAMP\n"
                + "      ,'" + userid + "'\n"
                + " FROM EMSDMS \n"
                + " WHERE EMDSZONE = '" + ozone + "' AND EMDSBOX = '" + obox + "' \n";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public boolean edit(String zone, String box, String diw, String did, String dih, String boxw, String userid) {

        boolean result = false;

        String sql = "UPDATE EMSDMS "
                + "SET EMDSWEIGHT = '" + diw + "'"
                + ", EMDSDEEP = '" + did + "'"
                + ", EMDSHEIGHT = '" + dih + "'"
                + ", EMDSBOXW = '" + boxw + "'"
                + ", EMDSEDATE = CURRENT_TIMESTAMP"
                + ", EMDSUSER = '" + userid + "'"
                + " where [EMDSZONE] = '" + zone + "' and [EMDSBOX] = '" + box + "'";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public void delete(String zone, String box) {

        String sql = "DELETE FROM EMSDMS where [EMDSZONE] = '" + zone + "' and [EMDSBOX] = '" + box + "' ";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ps.executeUpdate();

        } catch (Exception e) {

            e.printStackTrace();

        }

    }
}
