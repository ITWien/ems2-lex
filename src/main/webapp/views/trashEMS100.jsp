<%@ include file="../fragments/taglibs.jsp" %>
<%@ include file="../fragments/imports.jsp" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.sql.*;" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>DISTRIBUTION MODE</title>
        <!-- css :: vendors -->
        <jsp:include page = "../fragments/css.jsp" />
        <!-- additional custom :: my-style -->
        <link rel="stylesheet" href="../resources/assets/styles/myStyles.css">

        <!-- js :: vendors -->
        <jsp:include page = "../fragments/script.jsp" />
        <!-- additional custom :: my-script -->
        <script src="../resources/assets/scripts/myScripts.js" async></script>
        <script src="../resources/assets/scripts/toggleLoad.js" async></script>
        <script type="text/javascript"> /* add something code to your script */</script>
        ${sendMessage}
        <script>
            var show1 = function () {
                $('#myModal1').modal('show');
            };
            function validateForm() {
                var x = document.forms["frm"]["id"].value;
                if (x === "") {
                    window.setTimeout(show1, 0);
                    return false;
                }
            }
        </script>
        <style>
            input[type=text], select {
                width: 100%;
                padding: 5px 5px;
                margin: 8px 0;
                display: inline-block;
                border: 1px solid #ccc;
                border-radius: 4px;
                box-sizing: border-box;
                height: 35px;
            }

            input[type=submit] {
                width: 100%;
                background-color: #2bd14a;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            input[type=submit]:hover {
                background-color: #45a049;
            }

            input[type=button] {
                width: 100%;
                background-color: #ef604a;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            input[type=button]:hover {
                background-color: #e51e00;
            }

            button[name=ok] {
                width: 25%;
                background-color: #008cff;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            button[name=ok]:hover {
                background-color: #008cff;
            }
        </style>
        <script>
            $(document).ready(function () {
                $('#showTable').DataTable({
                    "paging": false,
                    "ordering": false,
                    "lengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
                    "bSortClasses": false,
//                    "order": [[0, "desc"]],
                    "columnDefs": [
//                        {orderable: false, targets: [12]},
//                        {"width": "15%", "targets": 12},
//                        {"width": "15%", "targets": 11}
                    ]
                });
                // Setup - add a text input to each footer cell
                $('#showTable tfoot th').each(function () {
                    var title = $(this).text();
                    $(this).html('<input type="text" />');
                });
                // DataTable
                var table = $('#showTable').DataTable();
                // Apply the search
                table.columns().every(function () {
                    var that = this;
                    $('input', this.footer()).on('keyup change', function () {
                        if (that.search() !== this.value) {
                            that
                                    .search(this.value)
                                    .draw();
                        }
                    });
                });
            });
        </script>
        <style>
            body {font-family: Arial, Helvetica, sans-serif;}

            /* The Modal (background) */
            .modal {
                display: none; /* Hidden by default */
                position: fixed; /* Stay in place */
                z-index: 1; /* Sit on top */
                padding-top: 100px; /* Location of the box */
                left: 0;
                top: 0;
                width: 100%; /* Full width */
                height: 100%; /* Full height */
                overflow: auto; /* Enable scroll if needed */
                background-color: rgb(0,0,0); /* Fallback color */
                background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
            }

            /* Modal Content */
            .modal-content {
                background-color: #fefefe;
                margin: auto;
                padding: 20px;
                border: 1px solid #888;
                width: fit-content;
                height: 320px;
            }

            /* The Close Button */
            .close {
                color: #aaaaaa;
                float: right;
                font-size: 28px;
                font-weight: bold;
            }

            .close:hover,
            .close:focus {
                color: #000;
                text-decoration: none;
                cursor: pointer;
            }
        </style>
        <style>
            img {
                border: 1px solid #ddd; /* Gray border */
                border-radius: 4px;  /* Rounded border */
                padding: 5px; /* Some padding */
                width: 100px; /* Set a small width */
            }
        </style>
        <script>
            function GenCar() {
//                document.getElementById("genc").action = "genCar";
                document.getElementById("genc").submit();

            }
        </script>
        <style>
            body {font-family: Arial, Helvetica, sans-serif;}

            /* The Modal (background) */
            .modal {
                display: none; /* Hidden by default */
                position: fixed; /* Stay in place */
                z-index: 1; /* Sit on top */
                padding-top: 100px; /* Location of the box */
                left: 0;
                top: 0;
                width: 100%; /* Full width */
                height: 100%; /* Full height */
                overflow: auto; /* Enable scroll if needed */
                background-color: rgb(0,0,0); /* Fallback color */
                background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
            }

            /* Modal Content */
            .modal-content {
                background-color: #fefefe;
                margin: auto;
                padding: 20px;
                border: 1px solid #888;
                width: fit-content;
                height: 320px;
            }

            /* The Close Button */
            .close {
                color: #aaaaaa;
                float: right;
                font-size: 28px;
                font-weight: bold;
            }

            .close:hover,
            .close:focus {
                color: #000;
                text-decoration: none;
                cursor: pointer;
            }
        </style>
        <script>
            function sumSize(line, mx) {
                var input = document.getElementsByTagName('input');
                var tmpSL = "";
                var pcs = 0;
                var max = parseInt(mx);
                for (var i = 0; i < input.length; i++) {
                    if (input[i].name === "edit-size-" + line) {
                        var size = input[i].id.toString().split("-")[0];
                        tmpSL += size + ":" + input[i].value + ";";
                        pcs += parseInt(input[i].value);
                    }
                }
                var newSize = document.getElementById('new-edit-size-' + line).value;
                var newSizeVal = document.getElementById('new-edit-sizeVal-' + line).value;
                if (newSize.toString().trim() !== "" && newSizeVal.toString().trim() !== "") {
                    tmpSL += newSize + ":" + newSizeVal + ";";
                    pcs += parseInt(newSizeVal);
                }

                document.getElementById('edit-sizeList-' + line).value = tmpSL;
                document.getElementById('edit-pcs-' + line).value = pcs;

                if (pcs > max) {
                    document.getElementById('confirm-' + line).className += " disabled";
                    document.getElementById('msg-' + line).style.display = '';
                } else {
                    document.getElementById('confirm-' + line).className = document.getElementById('confirm-' + line).className.toString().replace(/ disabled/g, '');
                    document.getElementById('msg-' + line).style.display = 'none';
                }

            }

            function ShowFailMsg() {
                document.getElementById('myModal-9').style.display = 'block';
            }
        </script>
    </head>    
    <body onload="document.getElementById('userid').value = sessionStorage.getItem('uid');
            document.getElementById('userid2').value = sessionStorage.getItem('uid');
            document.getElementById('userid3').value = sessionStorage.getItem('uid');
          ${failMsg}"> 
        <div id="wrapper">

            <!-- /#sidebar-wrapper -->
            <%@ include file="../fragments/sidebar.jsp" %>

            <!-- Page Content -->
            <!-- nav-head-custom -->
            <%@ include file="../fragments/nav_head.jsp" %>

            <div class="container-fluid">
                <!--                <div id="wrapper-top" align="left">
                <%-- PART 2 --%>
                <div class="row">
                    <div class="col-lg-12">
                        <div id="set-height" style="height:415px;margin-top:0px;">
                            <div id="sidebar-wrapper-top" class="">
                                <b class="page-header" style="padding-left:5px;font-size:18px;">
                <%-- <hr style="margin:0px;margin-top:10px;margin-bottom:10px;"> --%>
            </b>
        </div>
    </div>
</div>
</div> -->

                <%-- PART 3 --%> 
                <form action="genCar" method="post" id="genc">
                    <input type="hidden" name="invGC" value="${EMHINVID}">
                    <input type="hidden" name="distGC" value="${dist}">
                    <input type="hidden" id="userid3" name="userid3">
                </form>
                <form action="dropbox" method="POST" name="frm" id="frm" onsubmit="return validateForm()">
                    <input type="hidden" id="userid" name="userid">   
                    <input type="hidden" id="dist" name="dist" value="${dist}">  
                    <br>
                    <div style="border: 2px solid #ccc; border-radius: 5px; padding: 20px;">
                        <div style="width: 100%; height: 50px; background-color: #e0e0e0;">
                            <br>
                            <h4><b>DISTRIBUTION MODE</b></h4>
                        </div>
                        <div style="border-style: solid; border-width: thin; border-color: #e0e0e0;">
                            <table style="margin: 20px; width: 100%;">
                                <tr>
                                    <th style="width: 8%;">
                                        CUSTOMER ID :
                                    </th>
                                    <td style="width: 10%;">
                                        <input type="text" value="${EMHCUSID}" style=" width: 87%;" readonly>
                                    </td>
                                    <th style="width: 8%;">
                                        SHIP MARK ID :
                                    </th>
                                    <td style="width: 10%;">
                                        <input type="text" value="${EMHSHIPID}" style=" width: 87%;" readonly>
                                    </td>
                                    <th style="width: 10%;">
                                        TRANSPORTATION ID :
                                    </th>
                                    <td style="width: 10%;">
                                        <input type="text" value="${EMHTRANSPID}" style=" width: 87%;" readonly>
                                    </td>
                                    <th style="width: 8%;">
                                        SHIP DATE :
                                    </th>
                                    <td style="width: 10%;">
                                        <input type="text" value="${EMHSHIPDATE}" style=" width: 87%;" readonly>
                                    </td>
                                    <th style="width: 10%;">
                                        INVOICE NO. :
                                    </th>
                                    <th style="width: 10%;">
                                        <input type="text" name="inv" value="${EMHINVID}" style=" width: 87%;" readonly>
                                    </th>
                                </tr>
                                <tr>
                                    <th style="width: 8%;">
                                        SHIP TO :
                                    </th>
                                    <td style="width: 10%;">
                                        <input type="text" value="${EMHCUSSHIPID}" style=" width: 87%;" readonly>
                                    </td>
                                    <th style="width: 8%;">
                                        CURRENCY :
                                    </th>
                                    <td style="width: 10%;">
                                        <input type="text" value="${EMHCURRENCY}" style=" width: 87%;" readonly>
                                    </td>
                                    <th style="width: 10%;">
                                        UNIT :
                                    </th>
                                    <td style="width: 10%;">
                                        <input type="text" value="${EMHUNIT}" style=" width: 87%;" readonly>
                                    </td>
                                    <th style="width: 8%;">
                                        CARTON AMOUNT :
                                    </th>
                                    <td style="width: 10%;">
                                        <input type="text" value="${EMHAMTCTN}" style=" width: 87%;" readonly>
                                    </td>
                                    <th style="width: 10%;">
                                    </th>
                                    <td style="width: 10%;">
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <br>
                        <table style=" width: 100%;">
                            <tr>
                                <td style=" width: 50%; text-align: left;">
                                    <h1>Deleted Lines <i class="fa fa-trash-o" aria-hidden="true" style=" font-size: 50px; color: #bf2121;"></i></h1>
                                </td>
                                <td style="text-align: right; width: 50%; ${disnone}">
                                    <input style="width: 100px;" type="button" value="Back" onclick="window.location.href = '/EMS2/EMS100/edit?id=${inv}'"/>
                                    &nbsp;&nbsp;&nbsp;&nbsp;
                                    <a href="dropbox3?inv=${EMHINVID}&dist=${dist}"><i class="fa fa-cube" aria-hidden="true" style=" font-size: 50px; color: #001384;"></i></a>
                                    &nbsp;&nbsp;&nbsp;&nbsp;
                                    <img src="../resources/images/${distText}.png" >
                                    ${cwx}<br>
                                    <b>${dist}</b>
                                </td>
                            </tr>
                        </table>
                        <table id="showTable" class="display" style="width:100%; border: 1px solid #ccc; border-radius: 4px;">
                            <thead>
                                <tr>
                                    <th>LINE</th>
                                    <th>CTN NO.</th>
                                    <th>ORDER</th>
                                    <th>AUTH</th>
                                    <th>STYLE</th>
                                    <th>COLOR</th>
                                    <th>CUP</th>
                                        ${thead}
                                    <th>PO</th>
                                    <th>PCS</th>
                                    <th>NET WEIGHT</th>
                                    <th>GROSS WEIGHT</th>
                                    <th>BOX TYPE</th>
                                    <th>Option 
                                        &nbsp;&nbsp;&nbsp;
                                        <!--<a onclick="document.getElementById('myModal-8').style.display = 'block';" style="cursor: pointer;"><i class="fa fa-plus" aria-hidden="true" style=" font-size: 30px; color: #001384;"></i></a>-->
                                    </th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                        ${tfoot}
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </tfoot>
                            <tbody>
                                <c:forEach varStatus="i" items="${aheadList}" var="x">
                                    <tr>
                                        <td style="text-align: center;">${x.EMCCTNO}<input type="hidden" name="ctno" value="${x.EMCCTNO}"></td>
                                        <td style="text-align: center;">${x.EMCCARTONNO}</td>
                                        <td style="text-align: center;">${x.EMCORDID}</td>
                                        <td>${x.EMCAUTH}</td>
                                        <td>${x.EMCSTYLE}</td>
                                        <td>${x.EMCCOLOR}</td>
                                        <td>${x.EMCCUP}</td>
                                        ${x.tbody}
                                        <td>${x.EMCPONO}</td>
                                        <td style="text-align: right;">${x.EMCPCS}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                        <td style="text-align: right;">${x.EMCNET}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                        <td style="text-align: right;">${x.EMCGROSS}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                        <td style="text-align: center;">${x.EMCBOXTYPE}</td>
                                        <td>
                                            <a onclick="document.getElementById('myModal-3-${x.EMCCTNO}').style.display = 'block';" style="cursor: pointer;"><i class="fa fa-undo" aria-hidden="true" style=" font-size: 30px; color: #2bd14a;"></i></a>
                                            <div id="myModal-3-${x.EMCCTNO}" class="modal">
                                                <!-- Modal content -->
                                                <div class="modal-content" style=" height: 300px; width: 500px;">
                                                    <span id="span-3-${x.EMCCTNO}" class="close" onclick="document.getElementById('myModal-3-${x.EMCCTNO}').style.display = 'none';">&times;</span>
                                                    <p><b><font size="4"></font></b></p>
                                                    <table width="100%">
                                                        <tr style="background-color: white;">
                                                            <td align="center">
                                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                <b style="color: #00399b;">
                                                                    <font size="5">Do you want to restore ?</font><hr>
                                                                    <font size="4">Invoice No. : ${EMHINVID}</font><br>
                                                                    <font size="4">Line : ${x.EMCCTNO}</font>
                                                                </b>
                                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                <br><br><br>
                                                                <a class="btn btn btn-outline btn-danger" onclick="document.getElementById('myModal-3-${x.EMCCTNO}').style.display = 'none';">
                                                                    <i class="fa fa-times-circle-o" style="font-size:20px;"></i> Cancel</a>  
                                                                &nbsp;
                                                                <a class="btn btn btn-outline btn-success" onclick="window.location.href = 'restoreCac?inv=${EMHINVID}&ctno=${x.EMCCTNO}&dist=${dist}&flag=${x.EMCFLAG}'">
                                                                    <i class="fa fa-check-circle-o" style="font-size:20px;"></i> Confirm</a>                    
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                </c:forEach>
                            </tbody>
                        </table>
                        <center>
                            <input style="width: 100px;" type="button" value="Back" onclick="window.location.href = '/EMS2/EMS100/edit?id=${inv}'"/>
                        </center>
                        <br>
                    </div>
                </form>
                <form action="addCac" method="post" id="addForm">
                    <input type="hidden" id="add-inv" name="add-inv" value="${EMHINVID}">  
                    <input type="hidden" id="add-auth" name="add-auth" value="${auth}">  
                    <input type="hidden" id="add-dist" name="add-dist" value="${dist}">
                    <input type="hidden" id="userid2" name="userid2">
                    <div id="myModal-8" class="modal">
                        <!-- Modal content -->
                        <div class="modal-content" style=" height: 350px; width: 850px;">
                            <span id="span-8" class="close" onclick="document.getElementById('myModal-8').style.display = 'none';">&times;</span>
                            <p><b><font size="4"></font></b></p>
                            <b style="color: #00399b;">
                                <font size="5">CREATE MODE</font>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <font size="4">INVOICE NO. : ${EMHINVID}</font>
                                <hr>
                            </b>
                            <table>
                                <tr>
                                    <!--<th>LINE</th>-->
                                    <!--<th>CTN NO.</th>-->
                                    <th>ORDER</th>
                                    <th>STYLE</th>
                                    <th>COLOR</th>
                                    <th>CUP</th>
                                    <!--<th>SIZE</th>-->
                                    <!--<th>PO</th>-->
                                    <!--<th>PCS</th>-->
                                    <!--<th>NET WEIGHT</th>-->
                                    <!--<th>GROSS WEIGHT</th>-->
                                    <!--<th>BOX TYPE</th>-->
                                </tr>
                                <tr>
                                    <!--<td><input type="text" id="add-line" name="add-line" style="width: 95%;"></td>-->
                                    <!--<td><input type="text" id="add-ctno" name="add-ctno" style="width: 95%;"></td>-->
                                    <td><input type="text" id="add-oder" name="add-order" maxlength="16" style="width: 190px;">&nbsp;&nbsp;</td>
                                    <td><input type="text" id="add-style" name="add-style" maxlength="10" style="width: 190px;">&nbsp;&nbsp;</td>
                                    <td><input type="text" id="add-color" name="add-color" maxlength="3" style="width: 190px;">&nbsp;&nbsp;</td>
                                    <td><input type="text" id="add-cup" name="add-cup" maxlength="4" style="width: 190px;">&nbsp;&nbsp;</td>
                                    <!--<td><input type="text" id="add-size" name="add-size" maxlength="4" style="width: 95%;"></td>-->
                                    <!--<td><input type="text" id="add-po" name="add-po" maxlength="15" style="width: 95%;"></td>-->
                                    <!--<td><input type="text" id="add-pcs" name="add-pcs" maxlength="18" style="width: 95%;"></td>-->
                                    <!--<td><input type="text" id="add-net" name="add-net" maxlength="18" style="text-align: right; width: 95%;"></td>-->
                                    <!--<td><input type="text" id="add-gross" name="add-gross" maxlength="18" style="text-align: right; width: 95%;"></td>-->
                                    <!--                                    <td>
                                                                            <select id="add-boxtype" name="add-boxtype">
                                                                                <option value=""></option>
                                                                                <option value="6E">6E</option>
                                                                                <option value="6D">6D</option>
                                                                                <option value="4E">4E</option>
                                                                                <option value="4D">4D</option>
                                                                                <option value="2E">2E</option>
                                                                            </select>
                                                                        </td>-->
                                </tr>
                            </table>
                            <br>
                            <table>
                                <tr>
                                    <!--<th>LINE</th>-->
                                    <!--<th>CTN NO.</th>-->
                                    <!--                                    <th>ORDER</th>
                                                                        <th>STYLE</th>
                                                                        <th>COLOR</th>
                                                                        <th>CUP</th>-->
                                    <th>SIZE</th>
                                    <th>PO</th>
                                    <th>PCS</th>
                                    <!--<th>NET WEIGHT</th>-->
                                    <!--<th>GROSS WEIGHT</th>-->
                                    <th>BOX TYPE</th>
                                </tr>
                                <tr>
                                    <!--<td><input type="text" id="add-line" name="add-line" style="width: 95%;"></td>-->
                                    <!--<td><input type="text" id="add-ctno" name="add-ctno" style="width: 95%;"></td>-->
                                    <!--<td><input type="text" id="add-oder" name="add-order" maxlength="16" style="width: 95%;"></td>-->
                                    <!--<td><input type="text" id="add-style" name="add-style" maxlength="10" style="width: 95%;"></td>-->
                                    <!--<td><input type="text" id="add-color" name="add-color" maxlength="3" style="width: 95%;"></td>-->
                                    <!--<td><input type="text" id="add-cup" name="add-cup" maxlength="4" style="width: 95%;"></td>-->
                                    <td><input type="text" id="add-size" name="add-size" maxlength="4" style="width: 190px;">&nbsp;&nbsp;</td>
                                    <td><input type="text" id="add-po" name="add-po" maxlength="15" style="width: 190px;">&nbsp;&nbsp;</td>
                                    <td><input type="text" id="add-pcs" name="add-pcs" maxlength="18" style="width: 190px;">&nbsp;&nbsp;</td>
                                    <!--<td><input type="text" id="add-net" name="add-net" maxlength="18" style="text-align: right; width: 95%;"></td>-->
                                    <!--<td><input type="text" id="add-gross" name="add-gross" maxlength="18" style="text-align: right; width: 95%;"></td>-->
                                    <td>
                                        <select id="add-boxtype" name="add-boxtype" style="width: 190px;">
                                            <option value=""></option>
                                            <option value="6E">6E</option>
                                            <option value="6D">6D</option>
                                            <option value="4E">4E</option>
                                            <option value="4D">4D</option>
                                            <option value="2E">2E</option>
                                        </select>&nbsp;&nbsp;
                                    </td>
                                </tr>
                            </table>
                            <br>
                            <center>
                                <a class="btn btn btn-outline btn-danger" onclick="document.getElementById('myModal-8').style.display = 'none';">
                                    <i class="fa fa-times-circle-o" style="font-size:20px;"></i> Cancel</a>  
                                &nbsp;
                                <a class="btn btn btn-outline btn-success" onclick="document.getElementById('addForm').submit();">
                                    <i class="fa fa-check-circle-o" style="font-size:20px;"></i> Confirm</a>  
                            </center>
                        </div>
                    </div>
                </form>
                <div id="myModal-9" class="modal">
                    <!-- Modal content -->
                    <div class="modal-content" style=" height: 230px; width: 550px;">
                        <span id="span-9" class="close" onclick="document.getElementById('myModal-9').style.display = 'none';">&times;</span>
                        <p><b><font size="4"></font></b></p>
                        <center>
                            <b style="color: #e51e00;">
                                <font size="5">CREATE FAIL !</font>
                                <hr>
                                <font size="4">No Style, Cup, Size in Master !</font>
                            </b>
                            <br><br><br>
                            <a class="btn btn btn-outline btn-primary" style="width:150px;" onclick="document.getElementById('myModal-9').style.display = 'none';">
                                OK 
                            </a>  
                        </center>
                    </div>
                </div>
                <!--End Part 3-->
                <!--</div>  end #wrapper-top -->
            </div> <!-- end .container-fluid -->
        </div> <!-- end #wrapper -->


    </body>

</html>