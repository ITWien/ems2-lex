<%@ include file="../fragments/taglibs.jsp" %>
<%@ include file="../fragments/imports.jsp" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.sql.*;" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>DISTRIBUTION MODE</title>
        <!-- css :: vendors -->
        <jsp:include page = "../fragments/css.jsp" />
        <!-- additional custom :: my-style -->
        <link rel="stylesheet" href="../resources/assets/styles/myStyles.css">

        <!-- js :: vendors -->
        <jsp:include page = "../fragments/script.jsp" />
        <!-- additional custom :: my-script -->
        <script src="../resources/assets/scripts/myScripts.js" async></script>
        <script src="../resources/assets/scripts/toggleLoad.js" async></script>
        <script type="text/javascript"> /* add something code to your script */</script>
        ${sendMessage}
        <script>
            var show1 = function () {
                $('#myModal1').modal('show');
            };
            function validateForm() {
                var x = document.forms["frm"]["id"].value;
                if (x === "") {
                    window.setTimeout(show1, 0);
                    return false;
                }
            }
        </script>
        <style>
            input[type=text], select {
                width: 100%;
                padding: 5px 5px;
                margin: 8px 0;
                display: inline-block;
                border: 1px solid #ccc;
                border-radius: 4px;
                box-sizing: border-box;
            }

            input[type=submit] {
                width: 100%;
                background-color: #2bd14a;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            input[type=submit]:hover {
                background-color: #45a049;
            }

            input[type=button] {
                width: 100%;
                background-color: #ef604a;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            input[type=button]:hover {
                background-color: #e51e00;
            }

            button[name=ok] {
                width: 25%;
                background-color: #008cff;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            button[name=ok]:hover {
                background-color: #008cff;
            }
        </style>
        <script>
            $(document).ready(function () {
                $('#showTable').DataTable({
                    "paging": false,
                    "ordering": false,
                    "lengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
                    "bSortClasses": false,
//                    "order": [[0, "desc"]],
                    "columnDefs": [
//                        {orderable: false, targets: [12]},
//                        {"width": "15%", "targets": 12},
//                        {"width": "15%", "targets": 11}
                    ]
                });
                // Setup - add a text input to each footer cell
                $('#showTable tfoot th').each(function () {
                    var title = $(this).text();
                    $(this).html('<input type="text" />');
                });
                // DataTable
                var table = $('#showTable').DataTable();
                // Apply the search
                table.columns().every(function () {
                    var that = this;
                    $('input', this.footer()).on('keyup change', function () {
                        if (that.search() !== this.value) {
                            that
                                    .search(this.value)
                                    .draw();
                        }
                    });
                });
            });
        </script>
        <style>
            body {font-family: Arial, Helvetica, sans-serif;}

            /* The Modal (background) */
            .modal {
                display: none; /* Hidden by default */
                position: fixed; /* Stay in place */
                z-index: 1; /* Sit on top */
                padding-top: 100px; /* Location of the box */
                left: 0;
                top: 0;
                width: 100%; /* Full width */
                height: 100%; /* Full height */
                overflow: auto; /* Enable scroll if needed */
                background-color: rgb(0,0,0); /* Fallback color */
                background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
            }

            /* Modal Content */
            .modal-content {
                background-color: #fefefe;
                margin: auto;
                padding: 20px;
                border: 1px solid #888;
                width: fit-content;
                height: 320px;
            }

            /* The Close Button */
            .close {
                color: #aaaaaa;
                float: right;
                font-size: 28px;
                font-weight: bold;
            }

            .close:hover,
            .close:focus {
                color: #000;
                text-decoration: none;
                cursor: pointer;
            }
        </style>
        <style>
            img {
                border: 1px solid #ddd; /* Gray border */
                border-radius: 4px;  /* Rounded border */
                padding: 5px; /* Some padding */
                width: 100px; /* Set a small width */
            }
        </style>
    </head>    
    <body onload="document.getElementById('userid').value = sessionStorage.getItem('uid');"> 
        <div id="wrapper">

            <!-- /#sidebar-wrapper -->
            <%@ include file="../fragments/sidebar.jsp" %>

            <!-- Page Content -->
            <!-- nav-head-custom -->
            <%@ include file="../fragments/nav_head.jsp" %>

            <div class="container-fluid">
                <!--                <div id="wrapper-top" align="left">
                <%-- PART 2 --%>
                <div class="row">
                    <div class="col-lg-12">
                        <div id="set-height" style="height:415px;margin-top:0px;">
                            <div id="sidebar-wrapper-top" class="">
                                <b class="page-header" style="padding-left:5px;font-size:18px;">
                <%-- <hr style="margin:0px;margin-top:10px;margin-bottom:10px;"> --%>
            </b>
        </div>
    </div>
</div>
</div> -->

                <%-- PART 3 --%> 
                <form action="dropbox" method="POST" name="frm" onsubmit="return validateForm()">
                    <input type="hidden" id="userid" name="userid">   
                    <br>
                    <div style="border: 2px solid #ccc; border-radius: 5px; padding: 20px;">
                        <div style="width: 100%; height: 50px; background-color: #e0e0e0;">
                            <br>
                            <h4><b>DISTRIBUTION MODE</b></h4>
                        </div>
                        <div style="border-style: solid; border-width: thin; border-color: #e0e0e0;">
                            <table style="margin: 20px; width: 100%;">
                                <tr>
                                    <th style="width: 8%;">
                                        CUSTOMER ID :
                                    </th>
                                    <td style="width: 10%;">
                                        <input type="text" value="${EMHCUSID}" style=" width: 87%;" readonly>
                                    </td>
                                    <th style="width: 8%;">
                                        SHIP MARK ID :
                                    </th>
                                    <td style="width: 10%;">
                                        <input type="text" value="${EMHSHIPID}" style=" width: 87%;" readonly>
                                    </td>
                                    <th style="width: 10%;">
                                        TRANSPORTATION ID :
                                    </th>
                                    <td style="width: 10%;">
                                        <input type="text" value="${EMHTRANSPID}" style=" width: 87%;" readonly>
                                    </td>
                                    <th style="width: 8%;">
                                        SHIP DATE :
                                    </th>
                                    <td style="width: 10%;">
                                        <input type="text" value="${EMHSHIPDATE}" style=" width: 87%;" readonly>
                                    </td>
                                    <th style="width: 10%;">
                                        INVOICE NO. :
                                    </th>
                                    <th style="width: 10%;">
                                        <input type="text" value="${EMHINVID}" style=" width: 87%;" readonly>
                                    </th>
                                </tr>
                                <tr>
                                    <th style="width: 8%;">
                                        SHIP TO :
                                    </th>
                                    <td style="width: 10%;">
                                        <input type="text" value="${EMHCUSSHIPID}" style=" width: 87%;" readonly>
                                    </td>
                                    <th style="width: 8%;">
                                        CURRENCY :
                                    </th>
                                    <td style="width: 10%;">
                                        <input type="text" value="${EMHCURRENCY}" style=" width: 87%;" readonly>
                                    </td>
                                    <th style="width: 10%;">
                                        UNIT :
                                    </th>
                                    <td style="width: 10%;">
                                        <input type="text" value="${EMHUNIT}" style=" width: 87%;" readonly>
                                    </td>
                                    <th style="width: 8%;">
                                        CARTON AMOUNT :
                                    </th>
                                    <td style="width: 10%;">
                                        <input type="text" value="${EMHAMTCTN}" style=" width: 87%;" readonly>
                                    </td>
                                    <th style="width: 10%;">
                                    </th>
                                    <td style="width: 10%;">
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <br>
                        <table style=" width: 100%;">
                            <tr>
                                <td style=" width: 50%; text-align: right;">

                                </td>
                                <td style="text-align: right; width: 50%; ${disnone}">
                                    <input style="width: 100px;" type="button" value="Back" onclick="window.location.href = '/EMS2/EMS100/edit?id=${inv}'"/>
                                    &nbsp;&nbsp;&nbsp;&nbsp;
                                    <a href="dropbox2?inv=${EMHINVID}&dist=${dist}"><i class="fa fa-dropbox" aria-hidden="true" style=" font-size: 50px; color: #001384;"></i></a>
                                    &nbsp;&nbsp;&nbsp;&nbsp;
                                    <a href="genCar?inv=${EMHINVID}&dist=${dist}"><i class="fa fa-list-ol" aria-hidden="true" style=" font-size: 50px; color: #0d8533;"></i></a>
                                    &nbsp;&nbsp;&nbsp;&nbsp;
                                    <img src="../resources/images/${distText}.png" >
                                    ${cwx}<br>
                                    <b>${dist}</b>
                                </td>
                            </tr>
                        </table>
                        <table id="showTable" class="display" style="width:100%; border: 1px solid #ccc; border-radius: 4px;">
                            <thead>
                                <tr>
                                    <th>LINE</th>
                                    <th>CTN NO.</th>
                                    <th>ORDER</th>
                                    <th>STYLE</th>
                                    <th>COLOR</th>
                                    <th>CUP</th>
                                        ${thead}
                                    <th>PO</th>
                                    <th>PCS</th>
                                    <th>NET WEIGHT</th>
                                    <th>GROSS WEIGHT</th>
                                    <th>BOX TYPE</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                        ${tfoot}
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </tfoot>
                            <tbody>
                                <c:forEach varStatus="i" items="${aheadList}" var="x">
                                    <tr>
                                        <td style="text-align: center;">${i.count}</td>
                                        <td style="text-align: center;"></td>
                                        <td style="text-align: center;">${x.EMDORDID}</td>
                                        <td>${x.EMDSTYLE}</td>
                                        <td>${x.EMDCOLOR}</td>
                                        <td>${x.EMDCUP}</td>
                                        ${x.tbody}
                                        <td>${x.EMDPONO}</td>
                                        <td style="text-align: right;">${x.PCS}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                        <td style="text-align: right;">${x.NET}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                        <td style="text-align: right;">${x.GROSS}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                        <td style="text-align: center;">${x.BOXTYPE}</td>
                                    </tr>
                                </c:forEach>
                            </tbody>
                        </table>
                        <center>
                            <input style="width: 100px;" type="button" value="Back" onclick="window.location.href = '/EMS2/EMS100/edit?id=${inv}'"/>
                        </center>
                        <br>
                    </div>
                </form>
                <!--End Part 3-->
                <!--</div>  end #wrapper-top -->
            </div> <!-- end .container-fluid -->
        </div> <!-- end #wrapper -->


    </body>

</html>