<%@ include file="../fragments/taglibs.jsp" %>
<%@ include file="../fragments/imports.jsp" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.sql.*;" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>DISPLAY MODE</title>
        <!-- css :: vendors -->
        <jsp:include page = "../fragments/css.jsp" />
        <!-- additional custom :: my-style -->
        <link rel="stylesheet" href="../resources/assets/styles/myStyles.css">

        <!-- js :: vendors -->
        <jsp:include page = "../fragments/script.jsp" />
        <!-- additional custom :: my-script -->
        <script src="../resources/assets/scripts/myScripts.js" async></script>
        <script src="../resources/assets/scripts/toggleLoad.js" async></script>
        <script type="text/javascript"> /* add something code to your script */</script>
        ${sendMessage}
        <script>
            var show1 = function () {
                $('#myModal1').modal('show');
            };

            function validateForm() {
                var x = document.forms["frm"]["inv"].value;


                if (x === "") {
                    window.setTimeout(show1, 0);
                    return false;
                }
            }
        </script>
        <style>
            input[type=text], select, input[type=date] {
                width: 100%;
                padding: 5px 5px;
                margin: 8px 0;
                display: inline-block;
                border: 1px solid #ccc;
                border-radius: 4px;
                box-sizing: border-box;
            }

            input[type=submit] {
                width: 100%;
                background-color: #2bd14a;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            input[type=submit]:hover {
                background-color: #45a049;
            }

            input[type=button] {
                width: 100%;
                background-color: #ef604a;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            input[type=button]:hover {
                background-color: #e51e00;
            }

            button[name=ok] {
                width: 25%;
                background-color: #008cff;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            button[name=ok]:hover {
                background-color: #008cff;
            }
        </style>
    </head>    
    <body onload="document.getElementById('userid').value = sessionStorage.getItem('uid');"> 
        <div id="wrapper">

            <!-- /#sidebar-wrapper -->
            <%@ include file="../fragments/sidebar.jsp" %>

            <!-- Page Content -->
            <!-- nav-head-custom -->
            <%@ include file="../fragments/nav_head.jsp" %>

            <div class="container-fluid">
                <!--                <div id="wrapper-top" align="left">
                <%-- PART 2 --%>
                <div class="row">
                    <div class="col-lg-12">
                        <div id="set-height" style="height:415px;margin-top:0px;">
                            <div id="sidebar-wrapper-top" class="">
                                <b class="page-header" style="padding-left:5px;font-size:18px;">
                <%-- <hr style="margin:0px;margin-top:10px;margin-bottom:10px;"> --%>
            </b>
        </div>
    </div>
</div>
</div> -->

                <%-- PART 3 --%> 
                <form action="sdisplay" method="POST" name="frm" onsubmit="return validateForm()">
                    <input type="hidden" id="userid" name="userid">   
                    <br>
                    <div style="width: 100%; height: 50px; background-color: #e0e0e0;">
                        <br>
                        <h4><b>DISPLAY MODE</b></h4>
                    </div>
                    <div style="border-style: solid; border-width: thin; border-color: #e0e0e0;">
                        <table style="margin: 20px;">
                            <tr>
                                <th style="width: 200px;">
                                    INVOICE NO. :
                                </th>
                                <th style="width: 87%;">
                                    <input type="text" id="inv" name="inv" value="${inv}" style=" width: 20%; background-color: #e0e0e0;" onkeyup="this.value = this.value.toUpperCase();" readonly>
                                </th>
                            </tr>
                            <tr>
                                <th style="width: 200px;">
                                    DATE :
                                </th>
                                <th style="width: 87%;">
                                    <div id="inputDateDiv" style="position:relative;">
                                        <input type="text" id="date" name="date" value="${date}" style=" width: 20%;" onchange="this.value = this.value.toUpperCase();" readonly>
                                    </div>
                                    <script type="text/javascript">
                                        $(function () {
                                            $('#date').datetimepicker({
                                                format: 'MMM D, YYYY'
                                            });
                                        });
                                    </script>
                                </th>
                            </tr>
                            <tr>
                                <th style="width: 200px;">
                                    ORDER OF :
                                </th>
                                <th style="width: 87%;">
                                    <input type="text" id="order" name="order" value="${order}" onkeyup="this.value = this.value.toUpperCase();" readonly>
                                </th>
                            </tr>
                            <tr>
                                <th style="width: 200px;">
                                    PAYMENT CONDITION :
                                </th>
                                <th style="width: 87%;">
                                    <input type="text" id="pay" name="pay" value="${pay}" onkeyup="this.value = this.value.toUpperCase();" readonly>
                                </th>
                            </tr>
                            <tr>
                                <th style="width: 200px;">
                                    INCOTERMS :
                                </th>
                                <th style="width: 87%;">
                                    <input type="text" id="inco" name="inco" value="${inco}" onkeyup="this.value = this.value.toUpperCase();" readonly>
                                </th>
                            </tr>
                            <tr>
                                <th style="width: 200px;">
                                    UNIT FOR SALES :
                                </th>
                                <th style="width: 87%;">
                                    <input type="text" id="unit" name="unit" value="${unit}" onkeyup="this.value = this.value.toUpperCase();" readonly>
                                </th>
                            </tr>
                            <tr>
                                <th style="width: 200px;">
                                    SHIPPED FROM :
                                </th>
                                <th style="width: 87%;">
                                    <input type="text" id="shipF" name="shipF" value="${shipF}" onkeyup="this.value = this.value.toUpperCase();" readonly>
                                </th>
                            </tr>
                            <tr>
                                <th style="width: 200px;">
                                    SHIPPED TO :
                                </th>
                                <th style="width: 87%;">
                                    <input type="text" id="shipT" name="shipT" value="${shipT}" onkeyup="this.value = this.value.toUpperCase();" readonly>
                                </th>
                            </tr>
                            <tr>
                                <th style="width: 200px;">
                                    DELIVERY ADDRESS :
                                </th>
                                <th style="width: 87%;">
                                    <input type="text" id="delAdd1" name="delAdd1" value="${delAdd1}" onkeyup="this.value = this.value.toUpperCase();" readonly>
                                </th>
                            </tr>
                            <tr>
                                <th style="width: 200px;">
                                </th>
                                <th style="width: 87%;">
                                    <input type="text" id="delAdd2" name="delAdd2" value="${delAdd2}" onkeyup="this.value = this.value.toUpperCase();" readonly>
                                </th>
                            </tr>
                            <tr>
                                <th style="width: 200px;">
                                </th>
                                <th style="width: 87%;">
                                    <input type="text" id="delAdd3" name="delAdd3" value="${delAdd3}" onkeyup="this.value = this.value.toUpperCase();" readonly>
                                </th>
                            </tr>
                            <tr>
                                <th style="width: 200px;">
                                </th>
                                <th style="width: 87%;">
                                    <input type="text" id="delAdd4" name="delAdd4" value="${delAdd4}" onkeyup="this.value = this.value.toUpperCase();" readonly>
                                </th>
                            </tr>
                            <tr>
                                <th style="width: 200px;">
                                    MID CODE : 
                                </th>
                                <th style="width: 87%;">
                                    <input type="text" id="midcode" name="midcode" value="${midcode}" style="width:30%;" onkeyup="this.value = this.value.toUpperCase();" readonly>
                                </th>
                            </tr>
                            <tr>
                                <th style="width: 200px;">
                                    MANUFACTURER : 
                                </th>
                                <th style="width: 87%;">
                                    <input type="text" id="manufac1" name="manufac1" value="${manufac1}" style="width:30%;" readonly> &nbsp;
                                    <input type="text" id="manufac2" name="manufac2" value="${manufac2}" style="width:34%;" readonly> &nbsp;
                                    <input type="text" id="manufac3" name="manufac3" value="${manufac3}" style="width:34%;" readonly>
                                </th>
                            </tr>
                        </table>
                        <br>
                        <center>
                            <input style="width: 100px;" type="button" value="Back" onclick="window.location.href = '/EMS2/EMS007/display'"/>
                        </center>
                        <br>
                    </div>

                    <center>
                        <div id="myModal" class="modal fade" role="dialog">
                            <div class="modal-dialog modal-sm"> <!-- ???????????? modal ?????????????? ???? xs, sm, md, lg -->
                                <div class="modal-md col-md-6" style="top: 150px; left:300px; background-color: #ddd; border: 1px solid #ccc; border-radius: 4px;">
                                    <div class="modal-header">
                                        <h4 class="modal-title" align="center">Duplicate Invoice No. !</h4>
                                        <br>
                                        <button name="ok" type="button" class="close" data-dismiss="modal">
                                            <font color = "white">OK</font>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- devbanban.com -->
                    </center>
                    <center>
                        <div id="myModal1" class="modal fade" role="dialog">
                            <div class="modal-dialog modal-sm"> <!-- ???????????? modal ?????????????? ???? xs, sm, md, lg -->
                                <div class="modal-md col-md-6" style="top: 150px; left:300px; background-color: #ddd; border: 1px solid #ccc; border-radius: 4px;">
                                    <div class="modal-header">
                                        <h4 class="modal-title" align="center">Invoice No. must be filled out !</h4>
                                        <br>
                                        <button name="ok" type="button" class="close" data-dismiss="modal">
                                            <font color = "white">OK</font>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- devbanban.com -->
                    </center>
                </form>
                <!--End Part 3-->
                <!--</div>  end #wrapper-top -->
            </div> <!-- end .container-fluid -->
        </div> <!-- end #wrapper -->


    </body>

</html>