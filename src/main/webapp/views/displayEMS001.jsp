<%@ include file="../fragments/taglibs.jsp" %>
<%@ include file="../fragments/imports.jsp" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.sql.*;" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>EMS</title>
        <!-- css :: vendors -->
        <jsp:include page = "../fragments/css.jsp" />
        <!-- additional custom :: my-style -->
        <link rel="stylesheet" href="../resources/assets/styles/myStyles.css">

        <!-- js :: vendors -->
        <jsp:include page = "../fragments/script.jsp" />
        <!-- additional custom :: my-script -->
        <script src="../resources/assets/scripts/myScripts.js" async></script>
        <script src="../resources/assets/scripts/toggleLoad.js" async></script>
        <style>
            input[type=text], select{
                width: 100%;
                padding: 5px 5px;
                margin: 8px 0;
                display: inline-block;
                border: 1px solid #ccc;
                border-radius: 4px;
                box-sizing: border-box;
            }

            input[type=date], input[type=number]{
                /*width: 100%;*/
                padding: 5px 5px;
                margin: 8px 0;
                display: inline-block;
                border: 1px solid #ccc;
                border-radius: 4px;
                box-sizing: border-box;
            }

            input[type=submit] {
                width: 100%;
                background-color: #00399b;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            input[type=submit]:hover {
                background-color: #3973d6;
            }

            input[type=button] {
                width: 100%;
                background-color: #ef604a;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            input[type=button]:hover {
                background-color: #e51e00;
            }
        </style>
        <script>
            $(document).ready(function () {
                $('#showTable').DataTable({
//                    "paging": false,
                    "lengthMenu": [[-1, 10, 25, 50, 100], ["All", 10, 25, 50, 100]],
                    "bSortClasses": false,
//                    "order": [[0, "desc"]],
                    "columnDefs": [
                        {orderable: false, targets: [14]},
//                        {"width": "12%", "targets": 0},
                        {"width": "15%", "targets": 14}
                    ]
                });

                // Setup - add a text input to each footer cell
                $('#showTable tfoot th').not(":eq(14)").each(function () {
                    var title = $(this).text();
                    $(this).html('<input type="text" />');
                });

                // DataTable
                var table = $('#showTable').DataTable();

                // Apply the search
                table.columns().every(function () {
                    var that = this;
                    $('input', this.footer()).on('keyup change', function () {
                        if (that.search() !== this.value) {
                            that
                                    .search(this.value)
                                    .draw();
                        }
                    });
                });
            });
        </script>
        <style>
            body {font-family: Arial, Helvetica, sans-serif;}

            /* The Modal (background) */
            .modal {
                display: none; /* Hidden by default */
                position: fixed; /* Stay in place */
                z-index: 1; /* Sit on top */
                padding-top: 100px; /* Location of the box */
                left: 0;
                top: 0;
                width: 100%; /* Full width */
                height: 100%; /* Full height */
                overflow: auto; /* Enable scroll if needed */
                background-color: rgb(0,0,0); /* Fallback color */
                background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
            }

            /* Modal Content */
            .modal-content {
                background-color: #fefefe;
                margin: auto;
                padding: 20px;
                border: 1px solid #888;
                width: fit-content;
                height: 320px;
            }

            /* The Close Button */
            .close {
                color: #aaaaaa;
                float: right;
                font-size: 28px;
                font-weight: bold;
            }

            .close:hover,
            .close:focus {
                color: #000;
                text-decoration: none;
                cursor: pointer;
            }
        </style>
        <script>
            function checkAll(ele) {
                var checkboxes = document.getElementsByTagName('input');
                if (ele.checked) {
                    for (var i = 0; i < checkboxes.length; i++) {
                        if (checkboxes[i].name === 'selectCk') {
                            checkboxes[i].checked = true;
                        }
                    }
                } else {
                    for (var i = 0; i < checkboxes.length; i++) {
                        if (checkboxes[i].name === 'selectCk') {
                            checkboxes[i].checked = false;
                        }
                    }
                }
            }

            function DelAll() {
                var checkboxes = document.getElementsByTagName('input');
                var cnt = 0;
                for (var i = 0; i < checkboxes.length; i++) {
                    if (checkboxes[i].name === 'selectCk' && checkboxes[i].checked === true) {
                        cnt++;
                    }
                }

                if (cnt !== 0) {
                    document.getElementById('myModal-deleteAll').style.display = 'block';
                } else {
                    document.getElementById('myModal-selectDel').style.display = 'block';
                }
            }

            function SetSearchData() {
                if (typeof (Storage) !== "undefined") {
                    // Store
                    sessionStorage.setItem("zone", document.getElementById('zone').value);
                    sessionStorage.setItem("style", document.getElementById('style').value);
                    sessionStorage.setItem("cup", document.getElementById('cup').value);
                    sessionStorage.setItem("size", document.getElementById('size').value);
                }
            }

            function GetSearchData() {
                if (sessionStorage.getItem('zone') !== null) {
                    document.getElementById('zone').value = sessionStorage.getItem('zone');
                }
                document.getElementById('style').value = sessionStorage.getItem('style');
                document.getElementById('cup').value = sessionStorage.getItem('cup');
                document.getElementById('size').value = sessionStorage.getItem('size');

//                document.getElementById('searchForm').submit();
            ${search}

            }

            function CopyAll() {
                $('#zoneOld').val($('#zone').val());
                $('#styleOld').val($('#style').val());
                $('#zoneNew').val($('#zone').val());
                $('#styleNew').val($('#style').val());
                document.getElementById('myModal-4').style.display = 'block';
            }

            function setNewSearch() {
                if (typeof (Storage) !== "undefined") {
                    sessionStorage.setItem("zone", $('#zoneNew').val());
                    sessionStorage.setItem("style", $('#styleNew').val());
                }
            }
        </script>
    </head>
    <body onload="GetSearchData();">
        <div id="wrapper">

            <!-- /#sidebar-wrapper -->
            <%@ include file="../fragments/sidebar.jsp" %>

            <!-- Page Content -->
            <!-- nav-head-custom -->
            <%@ include file="../fragments/nav_head.jsp" %>

            <div class="container-fluid">
                <!--<div id="wrapper-top">-->
                <%-- PART 2 --%>
                <!--                    <div class="row">
                                        <div class="col-lg-12">
                                            <div id="set-height" style="height:415px;margin-top:0px;">
                                                <div id="sidebar-wrapper-top" class="">
                                                    <b class="page-header" style="padding-left:5px;font-size:18px;">
                <%-- <hr style="margin:0px;margin-top:10px;margin-bottom:10px;"> --%>
            </b>
        </div>
    </div>
    </div>
    </div> -->
                <%-- PART 3 --%>
                <!--show Qdest table-->
                <form action="../resources/manual/EMS001.pdf" target="_blank">
                    <table width="100%">
                        <tr id="dont">
                            <td width="94%" align="left"></td>
                            <td width="6%" align="right"><input type="submit" value="Manual" /></td>
                        </tr>
                    </table>
                </form>
                <div style="border: 2px solid #ccc; border-radius: 5px; padding: 20px;">
                    <form id="searchForm" action="display" method="post">
                        <table style="margin: 20px;">
                            <tr>
                                <th style="width: 100px;">
                                    ZONE :
                                </th>
                                <td style="width: 200px;">
                                    <select id="zone" name="zone" style=" height: 33px;">
                                        <option value="${zone}" selected hidden>${zone}</option>
                                        <option value=""></option>
                                        <option value="EUROPE">EUROPE</option>
                                        <option value="AMERICA">AMERICA</option>
                                        <option value="AMERICA2">AMERICA2</option>
                                        <option value="BRAZIL">BRAZIL</option>
                                        <option value="USA-CWX">USA-CWX</option>
                                        <option value="ASEAN">ASEAN</option>
                                        <option value="CHINA">CHINA</option>
                                        <option value="CWX-JPN">CWX-JPN</option>
                                    </select>
                                </td>
                                <th style="width: 100px; text-align: right;">
                                    STYLE : &nbsp;
                                </th>
                                <td style="width: 200px;">
                                    <input type="text" id="style" name="style" value="${style}" onkeyup="this.value = this.value.toUpperCase()">
                                </td>
                                <th style="width: 100px; text-align: right;">
                                    CUP : &nbsp;
                                </th>
                                <td style="width: 100px;">
                                    <input type="text" id="cup" name="cup" value="${cup}" onkeyup="this.value = this.value.toUpperCase();">
                                </td>
                                <th style="width: 100px; text-align: right;">
                                    SIZE : &nbsp;
                                </th>
                                <td style="width: 100px;">
                                    <input type="text" id="size" name="size" value="${size}">
                                </td>
                                <th style="width: 300px; text-align: right;">
                                    <input type="submit" value="Search" onclick="SetSearchData();" style="width: 150px;"/>
                                </th>
                                <td style="width: 500px;"></td>
                            </tr>
                        </table>
                    </form>
                    <form id="delAllForm" action="deleteAll" method="post">
                        <table id="showTable" class="display" style="width:100%; border: 1px solid #ccc; border-radius: 4px;">
                            <thead>
                                <tr>
                                    <th colspan="6" style=" border-bottom-color:transparent;"></th>
                                    <th colspan="5" style="text-align: center;">BOX QTY</th>
                                </tr>
                                <tr>
                                    <th>ZONE</th>
                                    <th>STYLE</th>
                                    <th>CUP</th>
                                    <th>SIZE</th>
                                    <th>TYPE/DESCRIPTION</th>
                                    <th>COMPONENT 1</th>
                                    <th>6E</th>
                                    <th>6D</th>
                                    <th>4E</th>
                                    <th>4D</th>
                                    <th>2E</th>
                                    <th>PIECE PACK</th>
                                    <th>WEIGHT</th>
                                    <th>PRICE</th>
                                    <th style="text-align: center;">Option
                                        &nbsp;&nbsp;&nbsp;
                                        <a href="create"><i class="fa fa-plus-circle" aria-hidden="true" style=" font-size: 30px; color: #001384;"></i></a>
                                        &nbsp;&nbsp;&nbsp;
                                        <a title="Copy All by Zone and Style" onclick="CopyAll();" style="cursor: pointer;"><i class="fa fa-files-o" aria-hidden="true" style=" font-size: 30px; color: #2d9e29;"></i></a>
                                        <div id="myModal-4" class="modal">
                                            <!-- Modal content -->
                                            <div class="modal-content" style=" height: fit-content; width: 700px;">
                                                <span id="span-4" class="close" onclick="document.getElementById('myModal-4').style.display = 'none';">&times;</span>
                                                <p><b><font size="4"></font></b></p>
                                                <table width="100%">
                                                    <tr style="background-color: white;">
                                                        <td align="center">
                                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                            <b style="color: #00399b;">
                                                                <font size="5">Copy All</font><hr>
                                                            </b>
                                                            <div class="row">
                                                                <div class="col-sm-6" style=" text-align: left; color: #00b533;">
                                                                    <h3>From</h3>
                                                                    <font size="4">Zone :
                                                                    <select id="zoneOld" name="zoneOld" style=" height: 33px; width: 150px;">
                                                                        <option value="EUROPE">EUROPE</option>
                                                                        <option value="AMERICA">AMERICA</option>
                                                                        <option value="AMERICA2">AMERICA2</option>
                                                                        <option value="BRAZIL">BRAZIL</option>
                                                                        <option value="USA-CWX">USA-CWX</option>
                                                                        <option value="ASEAN">ASEAN</option>
                                                                        <option value="CHINA">CHINA</option>
                                                                        <option value="CWX-JPN">CWX-JPN</option>
                                                                    </select>
                                                                    </font><br>
                                                                    <font size="4">Style : <input type="text" id="styleOld" name="styleOld" style="width: 150px;" onkeyup="this.value = this.value.toUpperCase()" maxlength="10"></font><br>
                                                                </div>
                                                                <div class="col-sm-6" style=" text-align: left; color: #00399b;">
                                                                    <h3>To</h3>
                                                                    <font size="4">Zone :
                                                                    <select id="zoneNew" name="zoneNew" style=" height: 33px; width: 150px;">
                                                                        <option value="EUROPE">EUROPE</option>
                                                                        <option value="AMERICA">AMERICA</option>
                                                                        <option value="AMERICA2">AMERICA2</option>
                                                                        <option value="BRAZIL">BRAZIL</option>
                                                                        <option value="USA-CWX">USA-CWX</option>
                                                                        <option value="ASEAN">ASEAN</option>
                                                                        <option value="CHINA">CHINA</option>
                                                                        <option value="CWX-JPN">CWX-JPN</option>
                                                                    </select>
                                                                    </font><br>
                                                                    <font size="4">Style : <input type="text" id="styleNew" name="styleNew" style="width: 150px;" onkeyup="this.value = this.value.toUpperCase()" maxlength="10"></font><br>
                                                                </div>
                                                            </div>
                                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                            <br><br><br>
                                                            <a class="btn btn btn-outline btn-danger" onclick="document.getElementById('myModal-4').style.display = 'none';">
                                                                <i class="fa fa-times-circle-o" style="font-size:20px;"></i> Cancel</a>
                                                            &nbsp;
                                                            <a class="btn btn btn-outline btn-success" onclick="setNewSearch(); window.location.href = 'copyAll?styleOld=' + $('#styleOld').val() + '&zoneOld=' + $('#zoneOld').val() + '&styleNew=' + $('#styleNew').val() + '&zoneNew=' + $('#zoneNew').val() + '&uid=' + sessionStorage.getItem('uid');">
                                                                <i class="fa fa-check-circle-o" style="font-size:20px;"></i> Confirm</a>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                        &nbsp;&nbsp;&nbsp;
                                        <a title="Delete All Selected" onclick="DelAll();" style="cursor: pointer;"><i class="fa fa-trash-o" aria-hidden="true" style=" font-size: 30px; color: #e51e00;"></i></a>
                                    </th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th style="text-align: center;">Check All &nbsp;
                                        <input style="width: 25px; height: 25px;" type="checkbox" name="selectAll" onchange="checkAll(this)">
                                    </th>
                                </tr>
                            </tfoot>
                            <tbody>
                                <c:forEach items="${aheadList}" var="x">
                                    <tr>
                                        <td>${x.EMMZONE}</td>
                                        <td>${x.EMMSTYLE}</td>
                                        <td>${x.EMMCUP}</td>
                                        <td>${x.EMMSIZE}</td>
                                        <td>${x.EMMTYPE}</td>
                                        <td>${x.EMMCOMPO1}</td>
                                        <td align="center">${x.EMMBOXS6E}</td>
                                        <td align="center">${x.EMMBOXS6D}</td>
                                        <td align="center">${x.EMMBOXS4E}</td>
                                        <td align="center">${x.EMMBOXS4D}</td>
                                        <td align="center">${x.EMMBOXS2E}</td>
                                        <td align="right">${x.EMMPPACK}&nbsp;&nbsp;&nbsp;</td>
                                        <td align="right">${x.EMMWEIGHT}&nbsp;&nbsp;&nbsp;</td>
                                        <td align="right">${x.EMMPRICE}&nbsp;&nbsp;&nbsp;</td>
                                        <td style=" text-align: center;">
                                            <a href="edit?style=${x.EMMSTYLE}&cup=${x.EMMCUP}&size=${x.EMMSIZE}&zone=${x.EMMZONE}" ><i class="fa fa-edit" aria-hidden="true" style=" font-size: 30px; color: #001384;"></i></a>
                                            &nbsp;&nbsp;
                                            <a href="sdisplay?style=${x.EMMSTYLE}&cup=${x.EMMCUP}&size=${x.EMMSIZE}&zone=${x.EMMZONE}" ><i class="fa fa-television" aria-hidden="true" style=" font-size: 30px; color: #001384;"></i></a>
                                            &nbsp;&nbsp;
                                            <a onclick="document.getElementById('myModal-4-${x.EMMSTYLE}-${x.EMMCUP}-${x.EMMSIZE}-${x.EMMZONE}').style.display = 'block';" style="cursor: pointer;"><i class="fa fa-files-o" aria-hidden="true" style=" font-size: 30px; color: #001384;"></i></a>
                                            <div id="myModal-4-${x.EMMSTYLE}-${x.EMMCUP}-${x.EMMSIZE}-${x.EMMZONE}" class="modal">
                                                <!-- Modal content -->
                                                <div class="modal-content" style=" height: fit-content; width: 700px;">
                                                    <span id="span-4-${x.EMMSTYLE}-${x.EMMCUP}-${x.EMMSIZE}-${x.EMMZONE}" class="close" onclick="document.getElementById('myModal-4-${x.EMMSTYLE}-${x.EMMCUP}-${x.EMMSIZE}-${x.EMMZONE}').style.display = 'none';">&times;</span>
                                                    <p><b><font size="4"></font></b></p>
                                                    <table width="100%">
                                                        <tr style="background-color: white;">
                                                            <td align="center">
                                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                <b style="color: #00399b;">
                                                                    <font size="5">Do you want to Copy ?</font><hr>
                                                                </b>
                                                                <div class="row">
                                                                    <div class="col-sm-6" style=" text-align: left; color: #00b533;">
                                                                        <h3>From</h3>
                                                                        <font size="4">Zone : <input type="text" id="zoneOld-${x.EMMSTYLE}-${x.EMMCUP}-${x.EMMSIZE}-${x.EMMZONE}" name="zoneOld-${x.EMMSTYLE}-${x.EMMCUP}-${x.EMMSIZE}-${x.EMMZONE}" value="${x.EMMZONE}" style="width: 150px;" readonly></font><br>
                                                                        <font size="4">Style : <input type="text" id="styleOld-${x.EMMSTYLE}-${x.EMMCUP}-${x.EMMSIZE}-${x.EMMZONE}" name="styleOld-${x.EMMSTYLE}-${x.EMMCUP}-${x.EMMSIZE}-${x.EMMZONE}" value="${x.EMMSTYLE}" style="width: 150px;" readonly></font><br>
                                                                        <font size="4">Cup : <input type="text" id="cupOld-${x.EMMSTYLE}-${x.EMMCUP}-${x.EMMSIZE}-${x.EMMZONE}" name="cupOld-${x.EMMSTYLE}-${x.EMMCUP}-${x.EMMSIZE}-${x.EMMZONE}" value="${x.EMMCUP}" style="width: 150px;" readonly></font><br>
                                                                        <font size="4">Size : <input type="text" id="sizeOld-${x.EMMSTYLE}-${x.EMMCUP}-${x.EMMSIZE}-${x.EMMZONE}" name="sizeOld-${x.EMMSTYLE}-${x.EMMCUP}-${x.EMMSIZE}-${x.EMMZONE}" value="${x.EMMSIZE}" style="width: 150px;" readonly></font>
                                                                    </div>
                                                                    <div class="col-sm-6" style=" text-align: left; color: #00399b;">
                                                                        <h3>To</h3>
                                                                        <font size="4">Zone :
                                                                        <select id="zoneNew-${x.EMMSTYLE}-${x.EMMCUP}-${x.EMMSIZE}-${x.EMMZONE}" name="zoneNew-${x.EMMSTYLE}-${x.EMMCUP}-${x.EMMSIZE}-${x.EMMZONE}" style=" height: 33px; width: 150px;">
                                                                            <option value="${x.EMMZONE}" selected hidden>${x.EMMZONE}</option>
                                                                            <option value="EUROPE">EUROPE</option>
                                                                            <option value="AMERICA">AMERICA</option>
                                                                            <option value="AMERICA2">AMERICA2</option>
                                                                            <option value="BRAZIL">BRAZIL</option>
                                                                            <option value="USA-CWX">USA-CWX</option>
                                                                            <option value="ASEAN">ASEAN</option>
                                                                            <option value="CHINA">CHINA</option>
                                                                            <option value="CWX-JPN">CWX-JPN</option>
                                                                        </select>
                                                                        </font><br>
                                                                        <font size="4">Style : <input type="text" id="styleNew-${x.EMMSTYLE}-${x.EMMCUP}-${x.EMMSIZE}-${x.EMMZONE}" name="styleNew-${x.EMMSTYLE}-${x.EMMCUP}-${x.EMMSIZE}-${x.EMMZONE}" value="${x.EMMSTYLE}" style="width: 150px;" onkeyup="this.value = this.value.toUpperCase()" maxlength="10"></font><br>
                                                                        <font size="4">Cup : <input type="text" id="cupNew-${x.EMMSTYLE}-${x.EMMCUP}-${x.EMMSIZE}-${x.EMMZONE}" name="cupNew-${x.EMMSTYLE}-${x.EMMCUP}-${x.EMMSIZE}-${x.EMMZONE}" value="${x.EMMCUP}" style="width: 150px;" onkeyup="this.value = this.value.toUpperCase()" maxlength="4"></font><br>
                                                                        <font size="4">Size : <input type="text" id="sizeNew-${x.EMMSTYLE}-${x.EMMCUP}-${x.EMMSIZE}-${x.EMMZONE}" name="sizeNew-${x.EMMSTYLE}-${x.EMMCUP}-${x.EMMSIZE}-${x.EMMZONE}" value="${x.EMMSIZE}" style="width: 150px;" onkeyup="this.value = this.value.toUpperCase()" maxlength="5"></font>
                                                                    </div>
                                                                </div>
                                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                <br><br><br>
                                                                <a class="btn btn btn-outline btn-danger" onclick="document.getElementById('myModal-4-${x.EMMSTYLE}-${x.EMMCUP}-${x.EMMSIZE}-${x.EMMZONE}').style.display = 'none';">
                                                                    <i class="fa fa-times-circle-o" style="font-size:20px;"></i> Cancel</a>
                                                                &nbsp;
                                                                <a class="btn btn btn-outline btn-success" onclick="window.location.href = 'copy?styleOld=${x.EMMSTYLE}&cupOld=${x.EMMCUP}&sizeOld=${x.EMMSIZE}&zoneOld=${x.EMMZONE}&styleNew=' + document.getElementById('styleNew-${x.EMMSTYLE}-${x.EMMCUP}-${x.EMMSIZE}-${x.EMMZONE}').value + '&cupNew=' + document.getElementById('cupNew-${x.EMMSTYLE}-${x.EMMCUP}-${x.EMMSIZE}-${x.EMMZONE}').value + '&sizeNew=' + document.getElementById('sizeNew-${x.EMMSTYLE}-${x.EMMCUP}-${x.EMMSIZE}-${x.EMMZONE}').value + '&zoneNew=' + document.getElementById('zoneNew-${x.EMMSTYLE}-${x.EMMCUP}-${x.EMMSIZE}-${x.EMMZONE}').value + '&uid=' + sessionStorage.getItem('uid');">
                                                                    <i class="fa fa-check-circle-o" style="font-size:20px;"></i> Confirm</a>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                            &nbsp;&nbsp;
                                            <a onclick="document.getElementById('myModal-3-${x.EMMSTYLE}-${x.EMMCUP}-${x.EMMSIZE}-${x.EMMZONE}').style.display = 'block';" style="cursor: pointer;"><i class="fa fa-trash" aria-hidden="true" style=" font-size: 30px; color: #001384;"></i></a>
                                            <div id="myModal-3-${x.EMMSTYLE}-${x.EMMCUP}-${x.EMMSIZE}-${x.EMMZONE}" class="modal">
                                                <!-- Modal content -->
                                                <div class="modal-content" style=" height: fit-content; width: 500px;">
                                                    <span id="span-3-${x.EMMSTYLE}-${x.EMMCUP}-${x.EMMSIZE}-${x.EMMZONE}" class="close" onclick="document.getElementById('myModal-3-${x.EMMSTYLE}-${x.EMMCUP}-${x.EMMSIZE}-${x.EMMZONE}').style.display = 'none';">&times;</span>
                                                    <p><b><font size="4"></font></b></p>
                                                    <table width="100%">
                                                        <tr style="background-color: white;">
                                                            <td align="center">
                                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                <b style="color: #00399b;">
                                                                    <font size="5">Do you want to delete ?</font><hr>
                                                                    <font size="4">Zone : ${x.EMMZONE}</font><br>
                                                                    <font size="4">Style : ${x.EMMSTYLE}</font><br>
                                                                    <font size="4">Cup : ${x.EMMCUP}</font><br>
                                                                    <font size="4">Size : ${x.EMMSIZE}</font>
                                                                </b>
                                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                <br><br><br>
                                                                <a class="btn btn btn-outline btn-danger" onclick="document.getElementById('myModal-3-${x.EMMSTYLE}-${x.EMMCUP}-${x.EMMSIZE}-${x.EMMZONE}').style.display = 'none';">
                                                                    <i class="fa fa-times-circle-o" style="font-size:20px;"></i> Cancel</a>
                                                                &nbsp;
                                                                <a class="btn btn btn-outline btn-success" onclick="window.location.href = 'delete?style=${x.EMMSTYLE}&cup=${x.EMMCUP}&size=${x.EMMSIZE}&zone=${x.EMMZONE}'">
                                                                    <i class="fa fa-check-circle-o" style="font-size:20px;"></i> Confirm</a>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                            &nbsp;&nbsp;
                                            <input style="width: 25px; height: 25px;" type="checkbox" name="selectCk" value="${x.EMMZONE}<=>${x.EMMSTYLE}<=>${x.EMMCUP}<=>${x.EMMSIZE}">
                                        </td>
                                    </tr>
                                </c:forEach>
                            </tbody>
                        </table>
                        <div id="myModal-deleteAll" class="modal">
                            <!-- Modal content -->
                            <div class="modal-content" style=" height: fit-content; width: 500px;">
                                <span id="span-deleteAll" class="close" onclick="document.getElementById('myModal-deleteAll').style.display = 'none';">&times;</span>
                                <p><b><font size="4"></font></b></p>
                                <table width="100%">
                                    <tr style="background-color: white;">
                                        <td align="center">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <b style="color: #00399b;">
                                                <font size="5">Do you want to delete ?</font>
                                            </b>
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <br><br><br>
                                            <a class="btn btn btn-outline btn-danger" onclick="document.getElementById('myModal-deleteAll').style.display = 'none';">
                                                <i class="fa fa-times-circle-o" style="font-size:20px;"></i> Cancel</a>
                                            &nbsp;
                                            <a class="btn btn btn-outline btn-success" onclick="document.getElementById('delAllForm').submit();">
                                                <i class="fa fa-check-circle-o" style="font-size:20px;"></i> Confirm</a>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div id="myModal-selectDel" class="modal">
                            <!-- Modal content -->
                            <div class="modal-content" style=" height: fit-content; width: 500px;">
                                <span id="span-selectDel" class="close" onclick="document.getElementById('myModal-selectDel').style.display = 'none';">&times;</span>
                                <p><b><font size="4"></font></b></p>
                                <table width="100%">
                                    <tr style="background-color: white;">
                                        <td align="center">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <b style="color: #00399b;">
                                                <font size="5">Please select data to delete !</font>
                                            </b>
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <br><br><br>
                                            <a class="btn btn btn-outline btn-primary" onclick="document.getElementById('myModal-selectDel').style.display = 'none';">
                                                OK
                                            </a>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </form>
                    <!--**********************-->
                    <!--End Part 3-->
                    <!--</div>  end #wrapper-top-->
                </div>
                <br>
            </div> <!-- end .container-fluid -->
        </div> <!-- end #wrapper -->
    </body>
</html>