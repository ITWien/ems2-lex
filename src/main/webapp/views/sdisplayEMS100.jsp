<%@ include file="../fragments/taglibs.jsp" %>
<%@ include file="../fragments/imports.jsp" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.sql.*;" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>DISPLAY MODE</title>
        <!-- css :: vendors -->
        <jsp:include page = "../fragments/css.jsp" />
        <!-- additional custom :: my-style -->
        <link rel="stylesheet" href="../resources/assets/styles/myStyles.css">

        <!-- js :: vendors -->
        <jsp:include page = "../fragments/script.jsp" />
        <!-- additional custom :: my-script -->
        <script src="../resources/assets/scripts/myScripts.js" async></script>
        <script src="../resources/assets/scripts/toggleLoad.js" async></script>
        <script type="text/javascript"> /* add something code to your script */</script>
        ${sendMessage}
        <script>
            var show1 = function () {
                $('#myModal1').modal('show');
            };

            function validateForm() {
                var x = document.forms["frm"]["id"].value;


                if (x === "") {
                    window.setTimeout(show1, 0);
                    return false;
                }
            }
        </script>
        <style>
            input[type=text], select {
                width: 100%;
                padding: 5px 5px;
                margin: 8px 0;
                display: inline-block;
                border: 1px solid #ccc;
                border-radius: 4px;
                box-sizing: border-box;
            }

            input[type=submit] {
                width: 100%;
                background-color: #2bd14a;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            input[type=submit]:hover {
                background-color: #45a049;
            }

            input[type=button] {
                width: 100%;
                background-color: #ef604a;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            input[type=button]:hover {
                background-color: #e51e00;
            }

            button[name=ok] {
                width: 25%;
                background-color: #008cff;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            button[name=ok]:hover {
                background-color: #008cff;
            }
        </style>
        <script>
            $(document).ready(function () {
                $('#showTable').DataTable({
                    "paging": false,
                    "lengthMenu": [[10, 25, 50, 100], [10, 25, 50, 100]],
                    "bSortClasses": false,
//                    "order": [[0, "desc"]],
                    "columnDefs": [
                        {orderable: false, targets: [12]},
                        {"width": "15%", "targets": 12},
//                        {"width": "15%", "targets": 11}
                    ]
                });

                // Setup - add a text input to each footer cell
                $('#showTable tfoot th').not(":eq(12)").each(function () {
                    var title = $(this).text();
                    $(this).html('<input type="text" />');
                });

                // DataTable
                var table = $('#showTable').DataTable();

                // Apply the search
                table.columns().every(function () {
                    var that = this;
                    $('input', this.footer()).on('keyup change', function () {
                        if (that.search() !== this.value) {
                            that
                                    .search(this.value)
                                    .draw();
                        }
                    });
                });
            });
        </script>
    </head>
    <body onload="document.getElementById('userid').value = sessionStorage.getItem('uid');">
        <div id="wrapper">

            <!-- /#sidebar-wrapper -->
            <%@ include file="../fragments/sidebar.jsp" %>

            <!-- Page Content -->
            <!-- nav-head-custom -->
            <%@ include file="../fragments/nav_head.jsp" %>

            <div class="container-fluid">
                <!--                <div id="wrapper-top" align="left">
                <%-- PART 2 --%>
                <div class="row">
                    <div class="col-lg-12">
                        <div id="set-height" style="height:415px;margin-top:0px;">
                            <div id="sidebar-wrapper-top" class="">
                                <b class="page-header" style="padding-left:5px;font-size:18px;">
                <%-- <hr style="margin:0px;margin-top:10px;margin-bottom:10px;"> --%>
            </b>
        </div>
    </div>
</div>
</div> -->

                <%-- PART 3 --%>
                <form action="edit" method="POST" name="frm" onsubmit="return validateForm()">
                    <input type="hidden" id="userid" name="userid">
                    <br>
                    <div style="border: 2px solid #ccc; border-radius: 5px; padding: 20px;">
                        <div style="width: 100%; height: 50px; background-color: #e0e0e0;">
                            <br>
                            <h4><b>DISPLAY MODE</b></h4>
                        </div>
                        <div style="border-style: solid; border-width: thin; border-color: #e0e0e0;">
                            <table style="margin: 20px; width: 100%;">
                                <tr>
                                    <th style="width: 8%;">
                                        CUSTOMER ID :
                                    </th>
                                    <td style="width: 10%;">
                                        <input type="text" value="${EMHCUSID}" style=" width: 87%;" readonly>
                                    </td>
                                    <th style="width: 8%;">
                                        SHIP MARK ID :
                                    </th>
                                    <td style="width: 10%;">
                                        <input type="text" value="${EMHSHIPID}" style=" width: 87%;" readonly>
                                    </td>
                                    <th style="width: 10%;">
                                        TRANSPORTATION ID :
                                    </th>
                                    <td style="width: 10%;">
                                        <input type="text" value="${EMHTRANSPID}" style=" width: 87%;" readonly>
                                    </td>
                                    <th style="width: 8%;">
                                        SHIP DATE :
                                    </th>
                                    <td style="width: 10%;">
                                        <input type="text" value="${EMHSHIPDATE}" style=" width: 87%;" readonly>
                                    </td>
                                    <th style="width: 10%;">
                                        INVOICE NO. :
                                    </th>
                                    <th style="width: 10%;">
                                        <input type="text" value="${EMHINVID}" style=" width: 87%;" readonly>
                                    </th>
                                </tr>
                                <tr>
                                    <th style="width: 8%;">
                                        SHIP TO :
                                    </th>
                                    <td style="width: 10%;">
                                        <input type="text" value="${EMHCUSSHIPID}" style=" width: 87%;" readonly>
                                    </td>
                                    <th style="width: 8%;">
                                        CURRENCY :
                                    </th>
                                    <td style="width: 10%;">
                                        <input type="text" value="${EMHCURRENCY}" style=" width: 87%;" readonly>
                                    </td>
                                    <th style="width: 10%;">
                                        UNIT :
                                    </th>
                                    <td style="width: 10%;">
                                        <input type="text" value="${EMHUNIT}" style=" width: 87%;" readonly>
                                    </td>
                                    <th style="width: 8%;">
                                        CARTON AMOUNT :
                                    </th>
                                    <td style="width: 10%;">
                                        <input type="text" value="${EMHAMTCTN}" style=" width: 87%;" readonly>
                                    </td>
                                    <th style="width: 10%;">
                                    </th>
                                    <td style="width: 10%;">
                                        <input style="width: 100px;" type="button" value="Back" onclick="window.location.href = '/EMS2/EMS100/display?uid=' + sessionStorage.getItem('uid');"/>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <table id="showTable" class="display" style="width:100%; border: 1px solid #ccc; border-radius: 4px;">
                            <thead>
                                <tr>
                                    <th>PO NO.</th>
                                    <th>LOT</th>
                                    <th>AUTH</th>
                                    <th>BRAND</th>
                                    <th>TYPE</th>
                                    <th>STYLE</th>
                                    <th>COLOR</th>
                                    <th>CODE</th>
                                    <th>CUP</th>
                                    <th>SIZE</th>
                                    <th>COST</th>
                                    <th>AMOUNT</th>
                                    <th style="text-align: center;">Option</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </tfoot>
                            <tbody>
                                <c:forEach items="${aheadList}" var="x">
                                    <tr>
                                        <td>${x.EMDPONO}</td>
                                        <td>${x.EMDORDID}</td>
                                        <td>${x.EMDAUTH}</td>
                                        <td>${x.EMDBRAND}</td>
                                        <td>${x.EMDTYPE}</td>
                                        <td>${x.EMDSTYLE}</td>
                                        <td>${x.EMDCOLOR}</td>
                                        <td>${x.EMDCODE}</td>
                                        <td>${x.EMDCUP}</td>
                                        <td style="text-align: center;">${x.EMDSIZES}</td>
                                        <td style="text-align: right;">${x.EMDCOST}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                        <td style="text-align: right;">${x.EMDAMOUNT}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                        <td style="text-align: center;"></td>
                                    </tr>
                                </c:forEach>
                            </tbody>
                        </table>
                        <center>
                            <input style="width: 100px;" type="button" value="Back" onclick="window.location.href = '/EMS2/EMS100/display?uid=' + sessionStorage.getItem('uid');"/>
                        </center>
                        <br>
                    </div>
                    <center>
                        <div id="myModal" class="modal fade" role="dialog">
                            <div class="modal-dialog modal-sm"> <!-- ???????????? modal ?????????????? ???? xs, sm, md, lg -->
                                <div class="modal-md col-md-6" style="top: 150px; left:300px; background-color: #ddd; border: 1px solid #ccc; border-radius: 4px;">
                                    <div class="modal-header">
                                        <h4 class="modal-title" align="center">Duplicate Transport ID !</h4>
                                        <br>
                                        <button name="ok" type="button" class="close" data-dismiss="modal">
                                            <font color = "white">OK</font>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- devbanban.com -->
                    </center>
                    <center>
                        <div id="myModal1" class="modal fade" role="dialog">
                            <div class="modal-dialog modal-sm"> <!-- ???????????? modal ?????????????? ???? xs, sm, md, lg -->
                                <div class="modal-md col-md-6" style="top: 150px; left:300px; background-color: #ddd; border: 1px solid #ccc; border-radius: 4px;">
                                    <div class="modal-header">
                                        <h4 class="modal-title" align="center">Transport ID must be filled out !</h4>
                                        <br>
                                        <button name="ok" type="button" class="close" data-dismiss="modal">
                                            <font color = "white">OK</font>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- devbanban.com -->
                    </center>
                </form>
                <!--End Part 3-->
                <!--</div>  end #wrapper-top -->
            </div> <!-- end .container-fluid -->
        </div> <!-- end #wrapper -->


    </body>

</html>