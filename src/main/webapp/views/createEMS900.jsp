<%@ include file="../fragments/taglibs.jsp" %>
<%@ include file="../fragments/imports.jsp" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.sql.*;" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>CREATE MODE</title>
        <!-- css :: vendors -->
        <jsp:include page = "../fragments/css.jsp" />
        <!-- additional custom :: my-style -->
        <link rel="stylesheet" href="../resources/assets/styles/myStyles.css">

        <!-- js :: vendors -->
        <jsp:include page = "../fragments/script.jsp" />
        <!-- additional custom :: my-script -->
        <script src="../resources/assets/scripts/myScripts.js" async></script>
        <script src="../resources/assets/scripts/toggleLoad.js" async></script>
        <script type="text/javascript"> /* add something code to your script */</script>
        <script>
            var show1 = function () {
                $('#myModal1').modal('show');
            };

            var show = function () {
                $('#myModal').modal('show');
            };

            var show2 = function () {
                $('#myModal-error').modal('show');
            };

            function validateForm() {
                var x = document.forms["frm"]["id"].value;


                if (x === "") {
                    window.setTimeout(show1, 0);
                    return false;
                }
            }
        </script>
        <script>
            $(document).ready(function () {
            ${sendMessage}

                $('#tableError').DataTable({
                    "paging": false,
                    "ordering": false
//                    columnDefs: [
//                        {targets: 0, className: 'dt-body-right'},
//                        {targets: 1, className: 'dt-body-right'},
//                        {targets: 2, className: 'dt-body-right'}
//                    ]
                });

                $('#tableError2').DataTable({
                    "paging": false,
                    "ordering": false
//                    columnDefs: [
//                        {targets: 0, className: 'dt-body-right'},
//                        {targets: 1, className: 'dt-body-right'},
//                        {targets: 2, className: 'dt-body-right'}
//                    ]
                });
            });
        </script>
        <style>
            input[type=text], select {
                width: 100%;
                padding: 5px 5px;
                margin: 8px 0;
                display: inline-block;
                border: 1px solid #ccc;
                border-radius: 4px;
                box-sizing: border-box;
            }

            input[type=submit] {
                width: 100%;
                background-color: #2bd14a;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            input[type=submit]:hover {
                background-color: #45a049;
            }

            input[type=button] {
                width: 100%;
                background-color: #ef604a;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            input[type=button]:hover {
                background-color: #e51e00;
            }

            button[name=ok] {
                width: 25%;
                background-color: #008cff;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            button[name=ok]:hover {
                background-color: #008cff;
            }
        </style>
        <script>
            function TWC(id) {
                if (id === "twc") {
                    if (document.getElementById(id).checked === true) {
                        document.getElementById('other').checked = false;
                        document.getElementById('all').checked = false;
                    }
                } else if (id === "other") {
                    if (document.getElementById(id).checked === true) {
                        document.getElementById('twc').checked = false;
                        document.getElementById('all').checked = false;
                    }
                } else {
                    if (document.getElementById(id).checked === true) {
                        document.getElementById('twc').checked = false;
                        document.getElementById('other').checked = false;
                    }
                }
            }

            function changeEXBUS(zone) {
                if (zone === "EUROPE") {
                    document.getElementById('exp').value = "603";
                } else if (zone === "AMERICA") {
                    document.getElementById('exp').value = "601";
                } else if (zone === "BRAZIL") {
                    document.getElementById('exp').value = "601";
                } else if (zone === "USA-CWX") {
                    document.getElementById('exp').value = "602";
                } else if (zone === "ASEAN") {
                    document.getElementById('exp').value = "602";
                } else if (zone === "CHINA") {
                    document.getElementById('exp').value = "602";
                } else if (zone === "CWX-JPN") {
                    document.getElementById('exp').value = "602";
                } else if (zone === "AMERICA2") {
                    document.getElementById('exp').value = "601";
                }
            }
        </script>
    </head>
    <body onload="document.getElementById('userid').value = sessionStorage.getItem('uid');">
        <div id="wrapper">

            <!-- /#sidebar-wrapper -->
            <%@ include file="../fragments/sidebar.jsp" %>

            <!-- Page Content -->
            <!-- nav-head-custom -->
            <%@ include file="../fragments/nav_head.jsp" %>

            <div class="container-fluid">
                <!--                <div id="wrapper-top" align="left">
                <%-- PART 2 --%>
                <div class="row">
                    <div class="col-lg-12">
                        <div id="set-height" style="height:415px;margin-top:0px;">
                            <div id="sidebar-wrapper-top" class="">
                                <b class="page-header" style="padding-left:5px;font-size:18px;">
                <%-- <hr style="margin:0px;margin-top:10px;margin-bottom:10px;"> --%>
            </b>
        </div>
    </div>
</div>
</div> -->

                <%-- PART 3 --%>
                <form action="create" method="POST" name="frm" onsubmit="return validateForm()">
                    <input type="hidden" id="userid" name="userid">
                    <br>
                    <div style="width: 100%; height: 50px; background-color: #e0e0e0;">
                        <br>
                        <h4><b>CREATE MODE</b></h4>
                    </div>
                    <div style="border-style: solid; border-width: thin; border-color: #e0e0e0;">
                        <table style="margin: 20px;">
                            <tr>
                                <th style="width: 200px;"></th>
                                <th style="width: 200px;">
                                    ZONE :
                                </th>
                                <th>
                                    <select id="zone" name="zone" style=" height: 33px;" onchange="changeEXBUS(this.value);">
                                        <option value="${zone}" selected hidden>${zone}</option>
                                        <option value="EUROPE">EUROPE</option>
                                        <option value="AMERICA">AMERICA</option>
                                        <option value="AMERICA2">AMERICA2</option>
                                        <option value="BRAZIL">BRAZIL</option>
                                        <option value="USA-CWX">USA-CWX</option>
                                        <option value="ASEAN">ASEAN</option>
                                        <option value="CHINA">CHINA</option>
                                        <option value="CWX-JPN">CWX-JPN</option>
                                    </select>
                                </th>
                            </tr>
                            <tr>
                                <th style="width: 200px;"></th>
                                <th style="width: 200px;">
                                    DELIVERY NO. :
                                </th>
                                <th>
                                    <input type="text" id="delno" name="delno" value="${delno}">
                                </th>
                            </tr>
                            <tr>
                                <th style="width: 200px;"></th>
                                <th style="width: 200px;">
                                    SALES ORGANIZATION :
                                </th>
                                <th>
                                    <input type="text" id="sale" name="sale" value="${sale}">
                                </th>
                            </tr>
                            <tr>
                                <th style="width: 200px;"></th>
                                <th style="width: 200px;">
                                    SHIP MARK ID :
                                </th>
                                <th>
                                    <!--                                    <select id="ship" name="ship" style="height:35px;">
                                                                            <option value="${ship}" selected hidden>${ship}</option>
                                                                            <option value=""></option>
                                    <c:forEach items="${shipList}" var="x">
                                        <option value="${x.EMCMSHIPID}">${x.EMCMSHIPID} : ${x.EMCMDESC1}</option>
                                    </c:forEach>
                                </select>-->

                                    <input list="inv" type="text" name="ship" id="ship" size="50" style="height:35px; width: 313px;" maxlength="100" autocomplete="off">

                                    <datalist id="inv">
                                        <option value="${ship}" selected hidden>${ship}</option>
                                        <option value=""></option>
                                        <c:forEach items="${shipList}" var="x">
                                            <option value="${x.EMCMSHIPID}">${x.EMCMSHIPID} : ${x.EMCMDESC1}</option>
                                        </c:forEach>
                                    </datalist>
                                </th>
                            </tr>
                            <tr>
                                <th style="width: 200px;"></th>
                                <th style="width: 200px;">
                                    TRANSPORTATION ID :
                                </th>
                                <th>
                                    <select id="tsp" name="tsp" style="height:35px;">
                                        <option value="${tsp}" selected hidden>${tsp}</option>
                                        <option value=""></option>
                                        <c:forEach items="${tspList}" var="x">
                                            <option value="${x.EMTPID}">${x.EMTPID} : ${x.EMTPDESC}</option>
                                        </c:forEach>
                                    </select>
                                </th>
                            </tr>
                            <tr>
                                <th style="width: 200px;"></th>
                                <th style="width: 200px;">
                                    DISTRIBUTION CHANEL :
                                </th>
                                <th>
                                    <input type="text" id="dist" name="dist" style=" width: 40%;" value="${dist}">
                                </th>
                            </tr>
                            <tr>
                                <th style="width: 200px;"></th>
                                <th style="width: 200px;">
                                    EXPORT BUSINESS :
                                </th>
                                <th>
                                    <input type="text" id="exp" name="exp" value="${exp}">
                                </th>
                            </tr>
                            <tr>
                                <th style="width: 200px;"></th>
                                <th style="width: 200px;">
                                    COMPANY :
                                </th>
                                <th>
                                    <input type="checkbox" style="width: 20px; height: 20px;" id="twc" name="comp" value="Y" onchange="TWC(this.id)" ${compY}>
                                    &nbsp; TWC
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    <input type="checkbox" style="width: 20px; height: 20px;" id="other" name="comp" value="N" onchange="TWC(this.id)" ${compN}>
                                    &nbsp; Other
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    <input type="checkbox" style="width: 20px; height: 20px;" id="all" name="comp" value="X" onchange="TWC(this.id)" ${compX}>
                                    &nbsp; All
                                </th>
                            </tr>
                        </table>
                        <br>
                        <center>
                            <input style="width: 100px;" type="button" value="Cancel" onclick="window.location.href = '/EMS2/EMS900/display?uid=' + sessionStorage.getItem('uid');"/>
                            <input style="width: 100px;" type="submit" value="Confirm" />
                        </center>
                        <br>
                    </div>
                    <center>
                        <div id="myModal" class="modal fade" role="dialog">
                            <div class="modal-dialog modal-sm"> <!-- ???????????? modal ?????????????? ???? xs, sm, md, lg -->
                                <div class="modal-md col-md-6" style="top: 150px; left:300px; background-color: #ddd; border: 1px solid #ccc; border-radius: 4px;">
                                    <div class="modal-header">
                                        <h4 class="modal-title" align="center">Duplicate Download Transaction with No. ${no} !</h4>
                                        <br>
                                        <button style=" background-color: #f27474;" name="ok" type="button" onclick="window.location.href = 'delete?id=${no}&action=create';">
                                            <font color = "white">Delete No. ${no}</font>
                                        </button>
                                        <button name="ok" type="button" data-dismiss="modal">
                                            <font color = "white">OK</font>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- devbanban.com -->
                    </center>
                    <center>
                        <div id="myModal-error" class="modal fade" role="dialog">
                            <div class="modal-dialog modal-sm" style="height: 100%;"> <!-- ???????????? modal ?????????????? ???? xs, sm, md, lg -->
                                <div class="modal-md col-md-6" style="top: 50px; left:300px; background-color: #ffffff; border: 1px solid #ccc; border-radius: 4px;">
                                    <div class="modal-header">
                                        <span class="close" data-dismiss="modal">&times;</span>
                                        <h4 class="modal-title" align="center">${headModal}</h4>
                                        <h4 class="modal-title" align="center">${headModalDet}</h4>
                                        <table id="tableError" style="width: 100%;">
                                            <thead>
                                                <tr style=" background-color: #e8e8e8;">
                                                    <th colspan="5">Master Details</th>
                                                </tr>
                                                <tr>
                                                    <th>Zone</th>
                                                    <th>Style</th>
                                                    <th>Cup</th>
                                                    <th>Size</th>
                                                    <th style="text-align: center;">Option</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <c:forEach items="${errorList}" var="x">
                                                    <tr>
                                                        <td>${x.EMMZONE}</td>
                                                        <td>${x.EMMSTYLE}</td>
                                                        <td>${x.EMMCUP}</td>
                                                        <td>${x.EMMSIZE}</td>
                                                        <td style="text-align: center;">
                                                            <a href="/EMS2/EMS001/create?zone=${x.EMMZONE}&style=${x.EMMSTYLE}&cup=${x.EMMCUP}&size=${x.EMMSIZE}" target="_blank"><i class="fa fa-plus-circle" aria-hidden="true" style=" font-size: 30px; color: #001384;"></i></a>
                                                        </td>
                                                    </tr>
                                                </c:forEach>
                                            </tbody>
                                        </table>
                                        <hr>
                                        <table id="tableError2" style="width: 100%;">
                                            <thead>
                                                <tr style=" background-color: #e8e8e8;">
                                                    <th colspan="4">Color Code</th>
                                                </tr>
                                                <tr>
                                                    <th>Zone</th>
                                                    <th>Code</th>
                                                    <th style="text-align: center;">Option</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <c:forEach items="${errorList2}" var="x">
                                                    <tr>
                                                        <td>${x.EMCDZONE}</td>
                                                        <td>${x.EMCDCODE}</td>
                                                        <td style="text-align: center;">
                                                            <a href="/EMS2/EMS008/create?zone=${x.EMCDZONE}&code=${x.EMCDCODE}" target="_blank"><i class="fa fa-plus-circle" aria-hidden="true" style=" font-size: 30px; color: #001384;"></i></a>
                                                        </td>
                                                    </tr>
                                                </c:forEach>
                                            </tbody>
                                        </table>
                                        <br>
                                        <button style=" background-color: #f27474;" name="ok" type="button" onclick="this.form.submit();">
                                            <font color = "white">Re-Download</font>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- devbanban.com -->
                    </center>
                    <center>
                        <div id="myModal1" class="modal fade" role="dialog">
                            <div class="modal-dialog modal-sm"> <!-- ???????????? modal ?????????????? ???? xs, sm, md, lg -->
                                <div class="modal-md col-md-6" style="top: 150px; left:300px; background-color: #ddd; border: 1px solid #ccc; border-radius: 4px;">
                                    <div class="modal-header">
                                        <h4 class="modal-title" align="center">Delivery No. must be filled out !</h4>
                                        <br>
                                        <button name="ok" type="button" class="close" data-dismiss="modal">
                                            <font color = "white">OK</font>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- devbanban.com -->
                    </center>
                </form>
                <!--End Part 3-->
                <!--</div>  end #wrapper-top -->
            </div> <!-- end .container-fluid -->
        </div> <!-- end #wrapper -->


    </body>

</html>