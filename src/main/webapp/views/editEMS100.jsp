<%@ include file="../fragments/taglibs.jsp" %>
<%@ include file="../fragments/imports.jsp" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.sql.*;" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>EDIT MODE</title>
        <!-- css :: vendors -->
        <jsp:include page = "../fragments/css.jsp" />
        <!-- additional custom :: my-style -->
        <link rel="stylesheet" href="../resources/assets/styles/myStyles.css">

        <!-- js :: vendors -->
        <jsp:include page = "../fragments/script.jsp" />
        <!-- additional custom :: my-script -->
        <script src="../resources/assets/scripts/myScripts.js" async></script>
        <script src="../resources/assets/scripts/toggleLoad.js" async></script>
        <script type="text/javascript"> /* add something code to your script */</script>
        ${sendMessage}
        <script>
            var show1 = function () {
                $('#myModal1').modal('show');
            };
            var show2 = function () {
                $('#myModalNoMas').modal('show');
            };
            function validateForm() {
                var x = document.forms["frm"]["id"].value;
                if (x === "") {
                    window.setTimeout(show1, 0);
                    return false;
                }
            }
        </script>
        <style>
            input[type=text], select {
                width: 100%;
                padding: 5px 5px;
                margin: 8px 0;
                display: inline-block;
                border: 1px solid #ccc;
                border-radius: 4px;
                box-sizing: border-box;
            }

            input[type=submit] {
                width: 100%;
                background-color: #2bd14a;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            input[type=submit]:hover {
                background-color: #45a049;
            }

            input[type=button] {
                width: 100%;
                background-color: #ef604a;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            input[type=button]:hover {
                background-color: #e51e00;
            }

            button[name=ok] {
                width: 25%;
                background-color: #008cff;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            button[name=ok]:hover {
                background-color: #008cff;
            }
        </style>
        <script>
            $(document).ready(function () {
            ${noMas}
                $('#showTable').DataTable({
                    "paging": false,
                    "lengthMenu": [[10, 25, 50, 100], [10, 25, 50, 100]],
                    "bSortClasses": false,
//                    "order": [[0, "desc"]],
                    "columnDefs": [
                        {orderable: false, targets: [12]},
                        {"width": "15%", "targets": 12}
                    ]
                });
                // Setup - add a text input to each footer cell
                $('#showTable tfoot th').not(":eq(12)").each(function () {
                    var title = $(this).text();
                    $(this).html('<input type="text" />');
                });
                // DataTable
                var table = $('#showTable').DataTable();
                // Apply the search
                table.columns().every(function () {
                    var that = this;
                    $('input', this.footer()).on('keyup change', function () {
                        if (that.search() !== this.value) {
                            that
                                    .search(this.value)
                                    .draw();
                        }
                    });
                });

                $('.reCreate').click(function () {
                    $(this).addClass('disabled');
                });
            });
        </script>
        <style>
            body {font-family: Arial, Helvetica, sans-serif;}

            /* The Modal (background) */
            .modal {
                display: none; /* Hidden by default */
                position: fixed; /* Stay in place */
                z-index: 1; /* Sit on top */
                padding-top: 100px; /* Location of the box */
                left: 0;
                top: 0;
                width: 100%; /* Full width */
                height: 100%; /* Full height */
                overflow: auto; /* Enable scroll if needed */
                background-color: rgb(0,0,0); /* Fallback color */
                background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
            }

            /* Modal Content */
            .modal-content {
                background-color: #fefefe;
                margin: auto;
                padding: 20px;
                border: 1px solid #888;
                width: fit-content;
                height: 320px;
            }

            /* The Close Button */
            .close {
                color: #aaaaaa;
                float: right;
                font-size: 28px;
                font-weight: bold;
            }

            .close:hover,
            .close:focus {
                color: #000;
                text-decoration: none;
                cursor: pointer;
            }
        </style>
        <script>
            function NonTWC() {
                var total = document.getElementById("non-total").value;
                var net = document.getElementById("non-net").value;
                var gross = document.getElementById("non-gross").value;
                var m = document.getElementById("non-m").value;
                var di1 = document.getElementById("non-di1").value;
                var di2 = document.getElementById("non-di2").value;
                var di3 = document.getElementById("non-di3").value;

//                window.location.href = "nonTWC?total=" + total + "&net=" + net + "&gross=" + gross + "&m=" + m + "&di1=" + di1 + "&di2=" + di2 + "&di3=" + di3;
                window.open("nonTWC?inv=${EMHINVID}&total=" + total + "&net=" + net + "&gross=" + gross + "&m=" + m + "&di1=" + di1 + "&di2=" + di2 + "&di3=" + di3, "_blank")
            }
        </script>
    </head>
    <body onload="document.getElementById('userid').value = sessionStorage.getItem('uid');
            document.getElementById('userid2').value = sessionStorage.getItem('uid');">
        <div id="wrapper">

            <!-- /#sidebar-wrapper -->
            <%@ include file="../fragments/sidebar.jsp" %>

            <!-- Page Content -->
            <!-- nav-head-custom -->
            <%@ include file="../fragments/nav_head.jsp" %>

            <div class="container-fluid">
                <!--                <div id="wrapper-top" align="left">
                <%-- PART 2 --%>
                <div class="row">
                    <div class="col-lg-12">
                        <div id="set-height" style="height:415px;margin-top:0px;">
                            <div id="sidebar-wrapper-top" class="">
                                <b class="page-header" style="padding-left:5px;font-size:18px;">
                <%-- <hr style="margin:0px;margin-top:10px;margin-bottom:10px;"> --%>
            </b>
        </div>
    </div>
</div>
</div> -->

                <%-- PART 3 --%>
                <form action="edit" method="POST" name="frm" onsubmit="return validateForm()">
                    <input type="hidden" id="userid" name="userid">
                    <br>
                    <div style="border: 2px solid #ccc; border-radius: 5px; padding: 20px;">
                        <div style="width: 100%; height: 50px; background-color: #e0e0e0;">
                            <br>
                            <h4><b>EDIT MODE</b></h4>
                        </div>
                        <div style="border-style: solid; border-width: thin; border-color: #e0e0e0;">
                            <table style="margin: 20px; width: 100%;">
                                <tr>
                                    <th style="width: 8%;">
                                        CUSTOMER ID :
                                    </th>
                                    <td style="width: 10%;">
                                        <input type="text" value="${EMHCUSID}" style=" width: 87%;" readonly>
                                    </td>
                                    <th style="width: 8%;">
                                        SHIP MARK ID :
                                    </th>
                                    <td style="width: 10%;">
                                        <input type="text" value="${EMHSHIPID}" style=" width: 87%;" readonly>
                                    </td>
                                    <th style="width: 10%;">
                                        TRANSPORTATION ID :
                                    </th>
                                    <td style="width: 10%;">
                                        <input type="text" value="${EMHTRANSPID}" style=" width: 87%;" readonly>
                                    </td>
                                    <th style="width: 8%;">
                                        SHIP DATE :
                                    </th>
                                    <td style="width: 10%;">
                                        <input type="text" value="${EMHSHIPDATE}" style=" width: 87%;" readonly>
                                    </td>
                                    <th style="width: 10%;">
                                        INVOICE NO. :
                                    </th>
                                    <th style="width: 10%;">
                                        <input type="text" value="${EMHINVID}" style=" width: 87%;" readonly>
                                    </th>
                                </tr>
                                <tr>
                                    <th style="width: 8%;">
                                        SHIP TO :
                                    </th>
                                    <td style="width: 10%;">
                                        <input type="text" value="${EMHCUSSHIPID}" style=" width: 87%;" readonly>
                                    </td>
                                    <th style="width: 8%;">
                                        CURRENCY :
                                    </th>
                                    <td style="width: 10%;">
                                        <input type="text" value="${EMHCURRENCY}" style=" width: 87%;" readonly>
                                    </td>
                                    <th style="width: 10%;">
                                        UNIT :
                                    </th>
                                    <td style="width: 10%;">
                                        <input type="text" value="${EMHUNIT}" style=" width: 87%;" readonly>
                                    </td>
                                    <th style="width: 8%;">
                                        CARTON AMOUNT :
                                    </th>
                                    <td style="width: 10%;">
                                        <input type="text" value="${EMHAMTCTN}" style=" width: 87%;" readonly>
                                    </td>
                                    <th style="width: 10%;">
                                    </th>
                                    <td style="width: 10%;">
                                        <input style="width: 100px;" type="button" value="Back" onclick="window.location.href = '/EMS2/EMS100/display?uid=' + sessionStorage.getItem('uid');"/>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <table id="showTable" class="display" style="width:100%; border: 1px solid #ccc; border-radius: 4px;">
                            <thead>
                                <tr>
                                    <th>PO NO.</th>
                                    <th>LOT</th>
                                    <th>AUTH</th>
                                    <th>BRAND</th>
                                    <th>TYPE</th>
                                    <th>STYLE</th>
                                    <th>COLOR</th>
                                    <th>CODE</th>
                                    <th>CUP</th>
                                    <th>SIZE</th>
                                    <th>COST</th>
                                    <th>AMOUNT</th>
                                    <th style="text-align: center;">Option
                                        &nbsp;&nbsp;&nbsp;&nbsp;
                                        <a title="Calculated for Carton" onclick="document.getElementById('myModal-5').style.display = 'block';
                                                ToggleZone('${ZONE}');" style="cursor: pointer;${disno0}"><i class="fa fa-dropbox" aria-hidden="true" style=" font-size: 30px; color: #001384;"></i></a>
                                        <script>
                                            function ToggleZone(zone) {
                                                $('.' + zone).removeClass('hidden');
                                            }
                                        </script>
                                        <div id="myModal-5" class="modal">
                                            <!-- Modal content -->
                                            <div class="modal-content" style=" height: auto; width: 500px;">
                                                <span id="span-5" class="close" onclick="document.getElementById('myModal-5').style.display = 'none';">&times;</span>
                                                <p><b><font size="4"></font></b></p>
                                                <table width="100%">
                                                    <tr style="background-color: white;">
                                                        <td align="center" style="border-bottom: none;">
                                                            <b style="color: #00399b;">
                                                                <font size="5">DISTRIBUTION MODE</font><hr>
                                                            </b>
                                                            <!--EUROPE-->
                                                            <div class="EUROPE hidden">
                                                                <a class="btn btn btn-outline btn-primary text-left" style=" width: 200px;" onclick="window.location.href = 'dropbox3?inv=${EMHINVID}&dist=EUROPE'">
                                                                    <i class="fa fa-globe" style="font-size:20px;"></i>&nbsp;&nbsp;&nbsp;&nbsp; Display</a>
                                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                <a class="btn btn btn-outline btn-primary text-left" style=" width: 100px;" onclick="document.getElementById('myModal-europe').style.display = 'block';">
                                                                    Calculated</a>
                                                                <div id="myModal-europe" class="modal">
                                                                    <!-- Modal content -->
                                                                    <div class="modal-content" style=" height: 220px; width: 500px;">
                                                                        <span id="span-5" class="close" onclick="document.getElementById('myModal-europe').style.display = 'none';">&times;</span>
                                                                        <p><b><font size="4"></font></b></p>
                                                                        <table width="100%">
                                                                            <tr style="background-color: white;">
                                                                                <td align="center">
                                                                                    <b style="color: #00399b;">
                                                                                        <font size="5">Do you want to Calculated Data ?</font><hr>
                                                                                    </b>
                                                                                    <br>
                                                                                    <a class="btn btn btn-outline btn-danger text-left" style=" width: 100px;" onclick="document.getElementById('myModal-europe').style.display = 'none';">
                                                                                        Cancel</a>
                                                                                    <a class="btn btn btn-outline btn-primary text-left reCreate" style=" width: 100px;" onclick="window.location.href = 'dropbox?dist=europe&inv=${EMHINVID}&userid=' + document.getElementById('userid').value;">
                                                                                        Calculated</a>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </div>
                                                                </div>
                                                                <br><br>
                                                            </div>
                                                            <!--AMERICA-->
                                                            <div class="AMERICA hidden">
                                                                <a class="btn btn btn-outline btn-success text-left" style=" width: 200px;" onclick="window.location.href = 'dropbox3?inv=${EMHINVID}&dist=AMERICA'">
                                                                    <i class="fa fa-globe" style="font-size:20px;"></i>&nbsp;&nbsp;&nbsp;&nbsp; Display</a>
                                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                <a class="btn btn btn-outline btn-success text-left" style=" width: 100px;" onclick="document.getElementById('myModal-america').style.display = 'block';">
                                                                    Calculated</a>
                                                                <div id="myModal-america" class="modal">
                                                                    <!-- Modal content -->
                                                                    <div class="modal-content" style=" height: 220px; width: 500px;">
                                                                        <span id="span-5" class="close" onclick="document.getElementById('myModal-america').style.display = 'none';">&times;</span>
                                                                        <p><b><font size="4"></font></b></p>
                                                                        <table width="100%">
                                                                            <tr style="background-color: white;">
                                                                                <td align="center">
                                                                                    <b style="color: #00399b;">
                                                                                        <font size="5">Do you want to Calculated Data ?</font><hr>
                                                                                    </b>
                                                                                    <br>
                                                                                    <a class="btn btn btn-outline btn-danger text-left" style=" width: 100px;" onclick="document.getElementById('myModal-america').style.display = 'none';">
                                                                                        Cancel</a>
                                                                                    <a class="btn btn btn-outline btn-success text-left reCreate" style=" width: 100px;" onclick="window.location.href = 'dropbox?dist=america&inv=${EMHINVID}&userid=' + document.getElementById('userid').value;">
                                                                                        Calculated</a>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </div>
                                                                </div>
                                                                <br><br>
                                                            </div>
                                                            <!--AMERICA2-->
                                                            <div class="AMERICA2 hidden">
                                                                <a class="btn btn btn-outline btn-success text-left" style=" width: 200px;" onclick="window.location.href = 'dropbox3?inv=${EMHINVID}&dist=AMERICA2'">
                                                                    <i class="fa fa-globe" style="font-size:20px;"></i>&nbsp;&nbsp;&nbsp;&nbsp; Display</a>
                                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                <a class="btn btn btn-outline btn-success text-left" style=" width: 100px;" onclick="document.getElementById('myModal-america2').style.display = 'block';">
                                                                    Calculated</a>
                                                                <div id="myModal-america2" class="modal">
                                                                    <!-- Modal content -->
                                                                    <div class="modal-content" style=" height: 220px; width: 500px;">
                                                                        <span id="span-5" class="close" onclick="document.getElementById('myModal-america2').style.display = 'none';">&times;</span>
                                                                        <p><b><font size="4"></font></b></p>
                                                                        <table width="100%">
                                                                            <tr style="background-color: white;">
                                                                                <td align="center">
                                                                                    <b style="color: #00399b;">
                                                                                        <font size="5">Do you want to Calculated Data ?</font><hr>
                                                                                    </b>
                                                                                    <br>
                                                                                    <a class="btn btn btn-outline btn-danger text-left" style=" width: 100px;" onclick="document.getElementById('myModal-america2').style.display = 'none';">
                                                                                        Cancel</a>
                                                                                    <a class="btn btn btn-outline btn-success text-left reCreate" style=" width: 100px;" onclick="window.location.href = 'dropbox?dist=america2&inv=${EMHINVID}&userid=' + document.getElementById('userid').value;">
                                                                                        Calculated</a>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </div>
                                                                </div>
                                                                <br><br>
                                                            </div>
                                                            <!--BRAZIL-->
                                                            <div class="BRAZIL hidden">
                                                                <a class="btn btn btn-outline btn-danger text-left" style=" width: 200px;" onclick="window.location.href = 'dropbox3?inv=${EMHINVID}&dist=BRAZIL'">
                                                                    <i class="fa fa-globe" style="font-size:20px;"></i>&nbsp;&nbsp;&nbsp;&nbsp; Display</a>
                                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                <a class="btn btn btn-outline btn-danger text-left" style=" width: 100px;" onclick="document.getElementById('myModal-brazil').style.display = 'block';">
                                                                    Calculated</a>
                                                                <div id="myModal-brazil" class="modal">
                                                                    <!-- Modal content -->
                                                                    <div class="modal-content" style=" height: 220px; width: 500px;">
                                                                        <span id="span-5" class="close" onclick="document.getElementById('myModal-brazil').style.display = 'none';">&times;</span>
                                                                        <p><b><font size="4"></font></b></p>
                                                                        <table width="100%">
                                                                            <tr style="background-color: white;">
                                                                                <td align="center">
                                                                                    <b style="color: #00399b;">
                                                                                        <font size="5">Do you want to Calculated Data ?</font><hr>
                                                                                    </b>
                                                                                    <br>
                                                                                    <a class="btn btn btn-outline btn-danger text-left" style=" width: 100px;" onclick="document.getElementById('myModal-brazil').style.display = 'none';">
                                                                                        Cancel</a>
                                                                                    <a class="btn btn btn-outline btn-success text-left reCreate" style=" width: 100px;" onclick="window.location.href = 'dropbox?dist=brazil&inv=${EMHINVID}&userid=' + document.getElementById('userid').value;">
                                                                                        Calculated</a>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </div>
                                                                </div>
                                                                <br><br>
                                                            </div>
                                                            <!--USA-CWX-->
                                                            <div class="USA-CWX hidden">
                                                                <a class="btn btn btn-outline btn-info text-left" style=" width: 200px;" onclick="window.location.href = 'dropbox3?inv=${EMHINVID}&dist=USA-CWX'">
                                                                    <i class="fa fa-globe" style="font-size:20px;"></i>&nbsp;&nbsp;&nbsp;&nbsp; Display</a>
                                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                <a class="btn btn btn-outline btn-info text-left" style=" width: 100px;" onclick="document.getElementById('myModal-usa-cwx').style.display = 'block';">
                                                                    Calculated</a>
                                                                <div id="myModal-usa-cwx" class="modal">
                                                                    <!-- Modal content -->
                                                                    <div class="modal-content" style=" height: 220px; width: 500px;">
                                                                        <span id="span-5" class="close" onclick="document.getElementById('myModal-usa-cwx').style.display = 'none';">&times;</span>
                                                                        <p><b><font size="4"></font></b></p>
                                                                        <table width="100%">
                                                                            <tr style="background-color: white;">
                                                                                <td align="center">
                                                                                    <b style="color: #00399b;">
                                                                                        <font size="5">Do you want to Calculated Data ?</font><hr>
                                                                                    </b>
                                                                                    <br>
                                                                                    <a class="btn btn btn-outline btn-danger text-left" style=" width: 100px;" onclick="document.getElementById('myModal-usa-cwx').style.display = 'none';">
                                                                                        Cancel</a>
                                                                                    <a class="btn btn btn-outline btn-info text-left reCreate" style=" width: 100px;" onclick="window.location.href = 'dropbox?dist=usa-cwx&inv=${EMHINVID}&userid=' + document.getElementById('userid').value;">
                                                                                        Calculated</a>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </div>
                                                                </div>
                                                                <br><br>
                                                            </div>
                                                            <!--ASEAN-->
                                                            <div class="ASEAN hidden">
                                                                <a class="btn btn btn-outline btn-warning text-left" style=" width: 200px;" onclick="window.location.href = 'dropbox3?inv=${EMHINVID}&dist=ASEAN'">
                                                                    <i class="fa fa-globe" style="font-size:20px;"></i>&nbsp;&nbsp;&nbsp;&nbsp; Display</a>
                                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                <a class="btn btn btn-outline btn-warning text-left" style=" width: 100px;" onclick="document.getElementById('myModal-asean').style.display = 'block';">
                                                                    Calculated</a>
                                                                <div id="myModal-asean" class="modal">
                                                                    <!-- Modal content -->
                                                                    <div class="modal-content" style=" height: 220px; width: 500px;">
                                                                        <span id="span-5" class="close" onclick="document.getElementById('myModal-asean').style.display = 'none';">&times;</span>
                                                                        <p><b><font size="4"></font></b></p>
                                                                        <table width="100%">
                                                                            <tr style="background-color: white;">
                                                                                <td align="center">
                                                                                    <b style="color: #00399b;">
                                                                                        <font size="5">Do you want to Calculated Data ?</font><hr>
                                                                                    </b>
                                                                                    <br>
                                                                                    <a class="btn btn btn-outline btn-danger text-left" style=" width: 100px;" onclick="document.getElementById('myModal-asean').style.display = 'none';">
                                                                                        Cancel</a>
                                                                                    <a class="btn btn btn-outline btn-warning text-left reCreate" style=" width: 100px;" onclick="window.location.href = 'dropbox?dist=asean&inv=${EMHINVID}&userid=' + document.getElementById('userid').value;">
                                                                                        Calculated</a>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </div>
                                                                </div>
                                                                <br><br>
                                                            </div>
                                                            <!--CHINA-->
                                                            <div class="CHINA hidden">
                                                                <a class="btn btn btn-outline btn-default text-left" style=" width: 200px;" onclick="window.location.href = 'dropbox3?inv=${EMHINVID}&dist=CHINA'">
                                                                    <i class="fa fa-globe" style="font-size:20px;"></i>&nbsp;&nbsp;&nbsp;&nbsp; Display</a>
                                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                <a class="btn btn btn-outline btn-default text-left" style=" width: 100px;" onclick="document.getElementById('myModal-china').style.display = 'block';">
                                                                    Calculated</a>
                                                                <div id="myModal-china" class="modal">
                                                                    <!-- Modal content -->
                                                                    <div class="modal-content" style=" height: 220px; width: 500px;">
                                                                        <span id="span-5" class="close" onclick="document.getElementById('myModal-china').style.display = 'none';">&times;</span>
                                                                        <p><b><font size="4"></font></b></p>
                                                                        <table width="100%">
                                                                            <tr style="background-color: white;">
                                                                                <td align="center">
                                                                                    <b style="color: #00399b;">
                                                                                        <font size="5">Do you want to Calculated Data ?</font><hr>
                                                                                    </b>
                                                                                    <br>
                                                                                    <a class="btn btn btn-outline btn-danger text-left" style=" width: 100px;" onclick="document.getElementById('myModal-china').style.display = 'none';">
                                                                                        Cancel</a>
                                                                                    <a class="btn btn btn-outline btn-default text-left reCreate" style=" width: 100px;" onclick="window.location.href = 'dropbox?dist=china&inv=${EMHINVID}&userid=' + document.getElementById('userid').value;">
                                                                                        Calculated</a>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!--CWX-JPN-->
                                                            <div class="CWX-JPN hidden">
                                                                <a class="btn btn btn-outline btn-default text-left" style=" width: 200px;" onclick="window.location.href = 'dropbox3?inv=${EMHINVID}&dist=CWX-JPN'">
                                                                    <i class="fa fa-globe" style="font-size:20px;"></i>&nbsp;&nbsp;&nbsp;&nbsp; Display</a>
                                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                <a class="btn btn btn-outline btn-default text-left" style=" width: 100px;" onclick="document.getElementById('myModal-cwx-jpn').style.display = 'block';">
                                                                    Calculated</a>
                                                                <div id="myModal-cwx-jpn" class="modal">
                                                                    <!-- Modal content -->
                                                                    <div class="modal-content" style=" height: 220px; width: 500px;">
                                                                        <span id="span-5" class="close" onclick="document.getElementById('myModal-cwx-jpn').style.display = 'none';">&times;</span>
                                                                        <p><b><font size="4"></font></b></p>
                                                                        <table width="100%">
                                                                            <tr style="background-color: white;">
                                                                                <td align="center">
                                                                                    <b style="color: #00399b;">
                                                                                        <font size="5">Do you want to Calculated Data ?</font><hr>
                                                                                    </b>
                                                                                    <br>
                                                                                    <a class="btn btn btn-outline btn-danger text-left" style=" width: 100px;" onclick="document.getElementById('myModal-cwx-jpn').style.display = 'none';">
                                                                                        Cancel</a>
                                                                                    <a class="btn btn btn-outline btn-default text-left reCreate" style=" width: 100px;" onclick="window.location.href = 'dropbox?dist=cwx-jpn&inv=${EMHINVID}&userid=' + document.getElementById('userid').value;">
                                                                                        Calculated</a>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                        <a title="Form" onclick="document.getElementById('myModal-6').style.display = 'block';" style="cursor: pointer;${disno1}"><i class="fa fa-file-text-o" aria-hidden="true" style=" font-size: 30px; color: #001384;"></i></a>
                                        <div id="myModal-6" class="modal">
                                            <!-- Modal content -->
                                            <div class="modal-content" style=" height: 700px; width: 1000px;">
                                                <span id="span-6" class="close" onclick="document.getElementById('myModal-6').style.display = 'none';">&times;</span>
                                                <p><b><font size="4"></font></b></p>
                                                <b style="color: #00399b;">
                                                    <font size="5">TOTAL INVOICE</font>
                                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                    <font size="4">INVOICE NO. : ${EMHINVID}</font>
                                                    <hr>
                                                </b>
                                                <table width="100%">
                                                    <tr style="background-color: white;">
                                                        <td style="width: 60%; border-bottom-color:transparent; text-align: left;">
                                                            <b style="color: #00399b;">
                                                                <font size="4">
                                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                TOTAL :
                                                                </font>
                                                            </b>
                                                        </td>
                                                        <td style="width: 20%; border-bottom-color:transparent;">
                                                            <b style="color: #00399b;">
                                                                <input type="text" id="non-total" name="non-total" value="${EMHNONTOT}" style="text-align: right;">
                                                            </b>
                                                        </td>
                                                        <td style="width: 20%; border-bottom-color:transparent; text-align: left;">
                                                            <b style="color: #00399b;">
                                                                <font size="4">CTNS.</font>
                                                            </b>
                                                        </td>
                                                    </tr>
                                                    <tr style="background-color: white;">
                                                        <td style="width: 60%; border-bottom-color:transparent; text-align: left;">
                                                            <b style="color: #00399b;">
                                                                <font size="4">
                                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                NET WEIGHT :
                                                                </font>
                                                            </b>
                                                        </td>
                                                        <td style="width: 20%; border-bottom-color:transparent;">
                                                            <b style="color: #00399b;">
                                                                <input type="text" id="non-net" name="non-net" value="${EMHNONNET}" style="text-align: right;">
                                                            </b>
                                                        </td>
                                                        <td style="width: 20%; border-bottom-color:transparent; text-align: left;">
                                                            <b style="color: #00399b;">
                                                                <font size="4">KGS</font>
                                                            </b>
                                                        </td>
                                                    </tr>
                                                    <tr style="background-color: white;">
                                                        <td style="width: 60%; border-bottom-color:transparent; text-align: left;">
                                                            <b style="color: #00399b;">
                                                                <font size="4">
                                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                GROSS WEIGHT :
                                                                </font>
                                                            </b>
                                                        </td>
                                                        <td style="width: 20%; border-bottom-color:transparent;">
                                                            <b style="color: #00399b;">
                                                                <input type="text" id="non-gross" name="non-gross" value="${EMHNONGROSS}" style="text-align: right;">
                                                            </b>
                                                        </td>
                                                        <td style="width: 20%; border-bottom-color:transparent; text-align: left;">
                                                            <b style="color: #00399b;">
                                                                <font size="4">KGS</font>
                                                            </b>
                                                        </td>
                                                    </tr>
                                                    <tr style="background-color: white;">
                                                        <td style="width: 60%; border-bottom-color:transparent; text-align: left;">
                                                            <b style="color: #00399b;">
                                                                <font size="4">
                                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                M' MENT :
                                                                </font>
                                                            </b>
                                                        </td>
                                                        <td style="width: 20%; border-bottom-color:transparent;">
                                                            <b style="color: #00399b;">
                                                                <input type="text" id="non-m" name="non-m" value="${EMHNONM}" style="text-align: right;">
                                                            </b>
                                                        </td>
                                                        <td style="width: 20%; border-bottom-color:transparent; text-align: left;">
                                                            <b style="color: #00399b;">
                                                                <font size="4">CUM</font>
                                                            </b>
                                                        </td>
                                                    </tr>
                                                    <tr style="background-color: white;">
                                                        <td style="width: 60%; border-bottom-color:transparent; text-align: left;">
                                                            <b style="color: #00399b;">
                                                                <font size="4">
                                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                DIMENSION : 41 * 47 * 32 CMS.
                                                                </font>
                                                            </b>
                                                        </td>
                                                        <td style="width: 20%; border-bottom-color:transparent;">
                                                            <b style="color: #00399b;">
                                                                <input type="text" id="non-di1" name="non-di1" value="${EMHNONDI1}" style="text-align: right;">
                                                            </b>
                                                        </td>
                                                        <td style="width: 20%; border-bottom-color:transparent; text-align: left;">
                                                            <b style="color: #00399b;">
                                                                <font size="4">Cartons</font>
                                                            </b>
                                                        </td>
                                                    </tr>
                                                    <tr style="background-color: white;">
                                                        <td style="width: 60%; border-bottom-color:transparent; text-align: left;">
                                                            <b style="color: #00399b;">
                                                                <font size="4">
                                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                DIMENSION : 36 * 47 * 29 CMS.
                                                                </font>
                                                            </b>
                                                        </td>
                                                        <td style="width: 20%; border-bottom-color:transparent;">
                                                            <b style="color: #00399b;">
                                                                <input type="text" id="non-di2" name="non-di2" value="${EMHNONDI2}" style="text-align: right;">
                                                            </b>
                                                        </td>
                                                        <td style="width: 20%; border-bottom-color:transparent; text-align: left;">
                                                            <b style="color: #00399b;">
                                                                <font size="4">Cartons</font>
                                                            </b>
                                                        </td>
                                                    </tr>
                                                    <tr style="background-color: white;">
                                                        <td style="width: 60%; border-bottom-color:transparent; text-align: left;">
                                                            <b style="color: #00399b;">
                                                                <font size="4">
                                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                DIMENSION : 36 * 47 * 16 CMS.
                                                                </font>
                                                            </b>
                                                        </td>
                                                        <td style="width: 20%; border-bottom-color:transparent;">
                                                            <b style="color: #00399b;">
                                                                <input type="text" id="non-di3" name="non-di3" value="${EMHNONDI3}" style="text-align: right;">
                                                            </b>
                                                        </td>
                                                        <td style="width: 20%; border-bottom-color:transparent; text-align: left;">
                                                            <b style="color: #00399b;">
                                                                <font size="4">Cartons</font>
                                                            </b>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <br><br>
                                                <a class="btn btn btn-outline btn-danger" onclick="document.getElementById('myModal-6').style.display = 'none';">
                                                    <i class="fa fa-times-circle-o" style="font-size:20px;"></i> Cancel</a>
                                                &nbsp;
                                                <a class="btn btn btn-outline btn-success" onclick="NonTWC();">
                                                    <i class="fa fa-check-circle-o" style="font-size:20px;"></i> Confirm</a>
                                            </div>
                                        </div>
                                        &nbsp;&nbsp;&nbsp;
                                        <a title="Add" onclick="document.getElementById('myModal-7').style.display = 'block';" style="cursor: pointer;"><i class="fa fa-plus" aria-hidden="true" style=" font-size: 30px; color: #001384;"></i></a>
                                    </th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </tfoot>
                            <tbody>
                                <c:forEach items="${aheadList}" var="x">
                                    <tr>
                                        <td>${x.EMDPONO}</td>
                                        <td>${x.EMDORDID}</td>
                                        <td>${x.EMDAUTH}</td>
                                        <td>${x.EMDBRAND}</td>
                                        <td>${x.EMDTYPE}</td>
                                        <td>${x.EMDSTYLE}</td>
                                        <td>${x.EMDCOLOR}</td>
                                        <td>${x.EMDCODE}</td>
                                        <td>${x.EMDCUP}</td>
                                        <td style="text-align: center;">${x.EMDSIZES}</td>
                                        <td style="text-align: right;">${x.EMDCOST}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                        <td style="text-align: right;">${x.EMDAMOUNT}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                        <td style="text-align: center;">
                                            <a title="Edit" onclick="document.getElementById('myModal-4-${x.EMDPONOdata}-${x.EMDCOLORdata}-${x.EMDCUPdata}-${x.EMDSIZES}').style.display = 'block';" style="cursor: pointer;"><i class="fa fa-edit" aria-hidden="true" style=" font-size: 30px; color: #001384;"></i></a>
                                            <div id="myModal-4-${x.EMDPONOdata}-${x.EMDCOLORdata}-${x.EMDCUPdata}-${x.EMDSIZES}" class="modal">
                                                <!-- Modal content -->
                                                <div class="modal-content" style=" height: 550px; width: 500px;">
                                                    <span id="span-3-${x.EMDPONOdata}-${x.EMDCOLORdata}-${x.EMDCUPdata}-${x.EMDSIZES}" class="close" onclick="document.getElementById('myModal-4-${x.EMDPONOdata}-${x.EMDCOLORdata}-${x.EMDCUPdata}-${x.EMDSIZES}').style.display = 'none';">&times;</span>
                                                    <p><b><font size="4"></font></b></p>
                                                    <table width="100%">
                                                        <tr style="background-color: white;">
                                                            <td align="center">
                                                                <b style="color: #00399b;">
                                                                    <font size="5">EDIT MODE</font><hr>
                                                                </b>
                                                                <table>
                                                                    <tr style="background-color: white;">
                                                                        <td>
                                                                            <b style="color: #00399b;">
                                                                                <font size="4">PO No. : </font>
                                                                            </b>
                                                                        </td>
                                                                        <td>
                                                                            <b style="color: #00399b;">
                                                                                <input type="text" id="epo-${x.EMDPONOdata}-${x.EMDCOLORdata}-${x.EMDCUPdata}-${x.EMDSIZES}" name="epo" value="${x.EMDPONOdata}">
                                                                            </b>
                                                                        </td>
                                                                    </tr>
                                                                    <tr style="background-color: white;">
                                                                        <td>
                                                                            <b style="color: #00399b;">
                                                                                <font size="4">Color Code : </font>
                                                                            </b>
                                                                        </td>
                                                                        <td>
                                                                            <b style="color: #00399b;">
                                                                                <select id="ecode-${x.EMDPONOdata}-${x.EMDCOLORdata}-${x.EMDCUPdata}-${x.EMDSIZES}" name="ecode">
                                                                                    <option value="${x.EMDCODEdata}" hidden>${x.EMDCODEdata2}</option>
                                                                                    <option value=""></option>
                                                                                    <c:forEach items="${codeList}" var="p">
                                                                                        <option value="${p.EMCDCODE}">(${p.EMCDZONE}) ${p.EMCDCODE} : ${p.EMCDDESC}</option>
                                                                                    </c:forEach>
                                                                                </select>
                                                                            </b>
                                                                        </td>
                                                                    </tr>
                                                                    <tr style="background-color: white;">
                                                                        <td>
                                                                            <b style="color: #00399b;">
                                                                                <font size="4">Cost : </font>
                                                                            </b>
                                                                        </td>
                                                                        <td>
                                                                            <b style="color: #00399b;">
                                                                                <input type="text" id="ecost-${x.EMDPONOdata}-${x.EMDCOLORdata}-${x.EMDCUPdata}-${x.EMDSIZES}" name="ecost" value="${x.EMDCOST}">
                                                                            </b>
                                                                        </td>
                                                                    </tr>
                                                                    <tr style="background-color: white;">
                                                                        <td>
                                                                            <b style="color: #00399b;">
                                                                                <font size="4">Amount : </font>
                                                                            </b>
                                                                        </td>
                                                                        <td>
                                                                            <b style="color: #00399b;">
                                                                                <input type="text" id="eamt-${x.EMDPONOdata}-${x.EMDCOLORdata}-${x.EMDCUPdata}-${x.EMDSIZES}" name="eamt" value="${x.EMDAMOUNT}">
                                                                            </b>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                                <br><br><br>
                                                                <a class="btn btn btn-outline btn-danger" onclick="document.getElementById('myModal-4-${x.EMDPONOdata}-${x.EMDCOLORdata}-${x.EMDCUPdata}-${x.EMDSIZES}').style.display = 'none';">
                                                                    <i class="fa fa-times-circle-o" style="font-size:20px;"></i> Cancel</a>
                                                                &nbsp;
                                                                <a class="btn btn btn-outline btn-success" onclick="window.location.href = 'editDet?nontwc=${nontwc}&po=' + document.getElementById('epo-${x.EMDPONOdata}-${x.EMDCOLORdata}-${x.EMDCUPdata}-${x.EMDSIZES}').value + '&amt=' + document.getElementById('eamt-${x.EMDPONOdata}-${x.EMDCOLORdata}-${x.EMDCUPdata}-${x.EMDSIZES}').value + '&opo=${x.EMDPONOdata}&size=${x.EMDSIZES}&lotD=${x.EMDORDIDdata}&authD=${x.EMDAUTHdata}&brandD=${x.EMDBRANDdata}&typeD=${x.EMDTYPEdata}&styleD=${x.EMDSTYLEdata}&colorD=${x.EMDCOLORdata}&codeD=${x.EMDCODEdata}&cupD=${x.EMDCUPdata}&inv=${EMHINVID}&userid=' + document.getElementById('userid').value + '&code=' + document.getElementById('ecode-${x.EMDPONOdata}-${x.EMDCOLORdata}-${x.EMDCUPdata}-${x.EMDSIZES}').value + '&cost=' + document.getElementById('ecost-${x.EMDPONOdata}-${x.EMDCOLORdata}-${x.EMDCUPdata}-${x.EMDSIZES}').value;">
                                                                    <i class="fa fa-check-circle-o" style="font-size:20px;"></i> Confirm</a>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                            &nbsp;&nbsp;&nbsp;&nbsp;
                                            <a title="Delete" onclick="document.getElementById('myModal-3-${x.EMDPONOdata}-${x.EMDCOLORdata}-${x.EMDCUPdata}-${x.EMDSIZES}').style.display = 'block';" style="cursor: pointer;"><i class="fa fa-trash" aria-hidden="true" style=" font-size: 30px; color: #bf2121;"></i></a>
                                            <div id="myModal-3-${x.EMDPONOdata}-${x.EMDCOLORdata}-${x.EMDCUPdata}-${x.EMDSIZES}" class="modal">
                                                <!-- Modal content -->
                                                <div class="modal-content" style=" height: 350px; width: 500px;">
                                                    <span id="span-3-${x.EMDPONOdata}-${x.EMDCOLORdata}-${x.EMDCUPdata}-${x.EMDSIZES}" class="close" onclick="document.getElementById('myModal-3-${x.EMDPONOdata}-${x.EMDCOLORdata}-${x.EMDCUPdata}-${x.EMDSIZES}').style.display = 'none';">&times;</span>
                                                    <p><b><font size="4"></font></b></p>
                                                    <table width="100%">
                                                        <tr style="background-color: white;">
                                                            <td align="center">
                                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                <b style="color: #00399b;">
                                                                    <font size="5">Do you want to delete ?</font><hr>
                                                                    <font size="4">Invoice No. : ${EMHINVID}</font><br>
                                                                    <font size="4">PO No. : ${x.EMDPONOdata}</font><br>
                                                                    <font size="4">Size : ${x.EMDSIZES}</font>
                                                                </b>
                                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                <br><br><br>
                                                                <a class="btn btn btn-outline btn-danger" onclick="document.getElementById('myModal-3-${x.EMDPONOdata}-${x.EMDCOLORdata}-${x.EMDCUPdata}-${x.EMDSIZES}').style.display = 'none';">
                                                                    <i class="fa fa-times-circle-o" style="font-size:20px;"></i> Cancel</a>
                                                                &nbsp;
                                                                <a class="btn btn btn-outline btn-success" onclick="window.location.href = 'deleteDet?nontwc=${nontwc}&inv=${EMHINVID}&po=${x.EMDPONOdata}&lotD=${x.EMDORDIDdata}&authD=${x.EMDAUTHdata}&brandD=${x.EMDBRANDdata}&typeD=${x.EMDTYPEdata}&styleD=${x.EMDSTYLEdata}&colorD=${x.EMDCOLORdata}&codeD=${x.EMDCODEdata}&cupD=${x.EMDCUPdata}&size=${x.EMDSIZES}'">
                                                                    <i class="fa fa-check-circle-o" style="font-size:20px;"></i> Confirm</a>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                </c:forEach>
                            </tbody>
                        </table>
                        <center>
                            <input style="width: 100px;" type="button" value="Back" onclick="window.location.href = '/EMS2/EMS100/display?uid=' + sessionStorage.getItem('uid');"/>
                        </center>
                        <br>
                    </div>
                    <center>
                        <div id="myModal" class="modal fade" role="dialog">
                            <div class="modal-dialog modal-sm"> <!-- ???????????? modal ?????????????? ???? xs, sm, md, lg -->
                                <div class="modal-md col-md-6" style="top: 150px; left:300px; background-color: #ddd; border: 1px solid #ccc; border-radius: 4px;">
                                    <div class="modal-header">
                                        <h4 class="modal-title" align="center">Duplicate Transport ID !</h4>
                                        <br>
                                        <button name="ok" type="button" class="close" data-dismiss="modal">
                                            <font color = "white">OK</font>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- devbanban.com -->
                    </center>
                    <center>
                        <div id="myModal1" class="modal fade" role="dialog">
                            <div class="modal-dialog modal-sm"> <!-- ???????????? modal ?????????????? ???? xs, sm, md, lg -->
                                <div class="modal-md col-md-6" style="top: 150px; left:300px; background-color: #ddd; border: 1px solid #ccc; border-radius: 4px;">
                                    <div class="modal-header">
                                        <h4 class="modal-title" align="center">Transport ID must be filled out !</h4>
                                        <br>
                                        <button name="ok" type="button" class="close" data-dismiss="modal">
                                            <font color = "white">OK</font>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- devbanban.com -->
                    </center>
                    <center>
                        <div id="myModalNoMas" class="modal fade" role="dialog">
                            <div class="modal-dialog modal-sm"> <!-- ???????????? modal ?????????????? ???? xs, sm, md, lg -->
                                <div class="modal-md col-md-6" style="top: 150px; left:300px; background-color: #ddd; border: 1px solid #ccc; border-radius: 4px;">
                                    <div class="modal-header">
                                        <h4 class="modal-title" align="center">Not found in Master Detail !</h4>
                                        <br>
                                        <center>
                                            <a class="btn btn btn-outline btn-danger" data-dismiss="modal">
                                                <i class="fa fa-times-circle-o" style="font-size:20px;"></i> Close</a>
                                            &nbsp;
                                            <a class="btn btn btn-outline btn-primary" onclick="window.open('/EMS2/EMS001/create?zone=${zone}&style=${style}&cup=${cup}&size=${size}', '_blank');">
                                                <i class="fa fa-check-circle-o" style="font-size:20px;"></i> Create Master</a>
                                        </center>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- devbanban.com -->
                    </center>
                </form>
                <form action="addDet" method="post" id="addForm">
                    <input type="hidden" id="inv" name="inv" value="${EMHINVID}">
                    <input type="hidden" id="nontwc" name="nontwc" value="${nontwc}">
                    <input type="hidden" id="userid2" name="userid2">
                    <div id="myModal-7" class="modal">
                        <!-- Modal content -->
                        <div class="modal-content" style=" height: 500px; width: 800px;">
                            <span id="span-7" class="close" onclick="document.getElementById('myModal-7').style.display = 'none';">&times;</span>
                            <p><b><font size="4"></font></b></p>
                            <b style="color: #00399b;">
                                <font size="5">CREATE MODE</font>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <font size="4">INVOICE NO. : ${EMHINVID}</font>
                                <hr>
                            </b>
                            <table width="100%">
                                <tr style="background-color: white;">
                                    <td style="width: 15%; text-align: left;">
                                        <b style="color: #00399b;">
                                            PO NO. :
                                        </b>
                                    </td>
                                    <td style="width: 35%;">
                                        <b style="color: #00399b;">
                                            <input type="text" id="add-po" name="add-po" style="width: 200px;" maxlength="15" required>
                                        </b>
                                    </td>
                                    <td style="width: 15%; text-align: left;">
                                        <b style="color: #00399b;">
                                            COLOR :
                                        </b>
                                    </td>
                                    <td style="width: 35%;">
                                        <b style="color: #00399b;">
                                            <input type="text" id="add-color" name="add-color" style="width: 200px;" maxlength="3">
                                        </b>
                                    </td>
                                </tr>
                                <tr style="background-color: white;">
                                    <td style="width: 15%; text-align: left;">
                                        <b style="color: #00399b;">
                                            LOT :
                                        </b>
                                    </td>
                                    <td style="width: 35%;">
                                        <b style="color: #00399b;">
                                            <input type="text" id="add-lot" name="add-lot" style="width: 200px;" maxlength="16">
                                        </b>
                                    </td>
                                    <td style="width: 15%; text-align: left;">
                                        <b style="color: #00399b;">
                                            CODE :
                                        </b>
                                    </td>
                                    <td style="width: 35%;">
                                        <b style="color: #00399b;">
                                            <select id="add-code" name="add-code" style="width: 200px;">
                                                <option value=""></option>
                                                <c:forEach items="${codeList}" var="p">
                                                    <option value="${p.EMCDCODE}">(${p.EMCDZONE}) ${p.EMCDCODE} : ${p.EMCDDESC}</option>
                                                </c:forEach>
                                            </select>
                                        </b>
                                    </td>
                                </tr>
                                <tr style="background-color: white;">
                                    <td style="width: 15%; text-align: left;">
                                        <b style="color: #00399b;">
                                            AUTH :
                                        </b>
                                    </td>
                                    <td style="width: 35%;">
                                        <b style="color: #00399b;">
                                            <input type="text" id="add-auth" name="add-auth" style="width: 200px;" maxlength="15">
                                        </b>
                                    </td>
                                    <td style="width: 15%; text-align: left;">
                                        <b style="color: #00399b;">
                                            CUP :
                                        </b>
                                    </td>
                                    <td style="width: 35%;">
                                        <b style="color: #00399b;">
                                            <input type="text" id="add-cup" name="add-cup" style="width: 200px;" maxlength="4">
                                        </b>
                                    </td>
                                </tr>
                                <tr style="background-color: white;">
                                    <td style="width: 15%; text-align: left;">
                                        <b style="color: #00399b;">
                                            BRAND :
                                        </b>
                                    </td>
                                    <td style="width: 35%;">
                                        <b style="color: #00399b;">
                                            <input type="text" id="add-brand" name="add-brand" style="width: 200px;" maxlength="75">
                                        </b>
                                    </td>
                                    <td style="width: 15%; text-align: left;">
                                        <b style="color: #00399b;">
                                            SIZE :
                                        </b>
                                    </td>
                                    <td style="width: 35%;">
                                        <b style="color: #00399b;">
                                            <input type="text" id="add-size" name="add-size" style="width: 200px;" maxlength="5" required>
                                        </b>
                                    </td>
                                </tr>
                                <tr style="background-color: white;">
                                    <td style="width: 15%; text-align: left;">
                                        <b style="color: #00399b;">
                                            TYPE :
                                        </b>
                                    </td>
                                    <td style="width: 35%;">
                                        <b style="color: #00399b;">
                                            <input type="text" id="add-type" name="add-type" style="width: 200px;" maxlength="40">
                                        </b>
                                    </td>
                                    <td style="width: 15%; text-align: left;">
                                        <b style="color: #00399b;">
                                            COST :
                                        </b>
                                    </td>
                                    <td style="width: 35%;">
                                        <b style="color: #00399b;">
                                            <input type="text" id="add-cost" name="add-cost" style="width: 200px; text-align: right;" maxlength="18">
                                        </b>
                                    </td>
                                </tr>
                                <tr style="background-color: white;">
                                    <td style="width: 15%; text-align: left;">
                                        <b style="color: #00399b;">
                                            STYLE :
                                        </b>
                                    </td>
                                    <td style="width: 35%;">
                                        <b style="color: #00399b;">
                                            <input type="text" id="add-style" name="add-style" style="width: 200px;" maxlength="10">
                                        </b>
                                    </td>
                                    <td style="width: 15%; text-align: left;">
                                        <b style="color: #00399b;">
                                            AMOUNT :
                                        </b>
                                    </td>
                                    <td style="width: 35%;">
                                        <b style="color: #00399b;">
                                            <input type="text" id="add-amt" name="add-amt" style="width: 200px; text-align: right;" maxlength="18">
                                        </b>
                                    </td>
                                </tr>
                            </table>
                            <br><br>
                            <center>
                                <a class="btn btn btn-outline btn-danger" onclick="document.getElementById('myModal-7').style.display = 'none';">
                                    <i class="fa fa-times-circle-o" style="font-size:20px;"></i> Cancel</a>
                                &nbsp;
                                <a class="btn btn btn-outline btn-success" onclick="document.getElementById('addForm').submit();">
                                    <i class="fa fa-check-circle-o" style="font-size:20px;"></i> Confirm</a>
                            </center>
                        </div>
                    </div>
                </form>
                <!--End Part 3-->
                <!--</div>  end #wrapper-top -->
            </div> <!-- end .container-fluid -->
        </div> <!-- end #wrapper -->


    </body>

</html>