<%@ include file="../fragments/taglibs.jsp" %>
<%@ include file="../fragments/imports.jsp" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.sql.*;" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>DISPLAY MODE</title>
        <!-- css :: vendors -->
        <jsp:include page = "../fragments/css.jsp" />
        <!-- additional custom :: my-style -->
        <link rel="stylesheet" href="../resources/assets/styles/myStyles.css"> 

        <!-- js :: vendors -->
        <jsp:include page = "../fragments/script.jsp" />
        <!-- additional custom :: my-script -->
        <script src="../resources/assets/scripts/myScripts.js" async></script>
        <script src="../resources/assets/scripts/toggleLoad.js" async></script>
        <script type="text/javascript"> /* add something code to your script */</script>
        ${sendMessage}
        <script>
            var show1 = function () {
                $('#myModal1').modal('show');
            };

            function validateForm() {
                var x = document.forms["frm"]["style"].value;
                var y = document.forms["frm"]["cup"].value;
                var z = document.forms["frm"]["size"].value;


                if (x === "" || y === "" || z === "") {
                    window.setTimeout(show1, 0);
                    return false;
                }
            }
        </script>
        <style>
            input[type=text], select {
                width: 100%;
                padding: 5px 5px;
                margin: 8px 0;
                display: inline-block;
                border: 1px solid #ccc;
                border-radius: 4px;
                box-sizing: border-box;
            }

            input[type=submit] {
                width: 100%;
                background-color: #2bd14a;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            input[type=submit]:hover {
                background-color: #45a049;
            }

            input[type=button] {
                width: 100%;
                background-color: #ef604a;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            input[type=button]:hover {
                background-color: #e51e00;
            }

            button[name=ok] {
                width: 25%;
                background-color: #008cff;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            button[name=ok]:hover {
                background-color: #008cff;
            }
        </style>
    </head>    
    <body onload="document.getElementById('userid').value = sessionStorage.getItem('uid');"> 
        <div id="wrapper">

            <!-- /#sidebar-wrapper -->
            <%@ include file="../fragments/sidebar.jsp" %>

            <!-- Page Content -->
            <!-- nav-head-custom -->
            <%@ include file="../fragments/nav_head.jsp" %>

            <div class="container-fluid">
                <!--                <div id="wrapper-top" align="left">
                <%-- PART 2 --%>
                <div class="row">
                    <div class="col-lg-12">
                        <div id="set-height" style="height:415px;margin-top:0px;">
                            <div id="sidebar-wrapper-top" class="">
                                <b class="page-header" style="padding-left:5px;font-size:18px;">
                <%-- <hr style="margin:0px;margin-top:10px;margin-bottom:10px;"> --%>
            </b>
        </div>
    </div>
</div>
</div> -->

                <%-- PART 3 --%> 
                <form action="edit" method="POST" name="frm" onsubmit="return validateForm()">
                    <input type="hidden" id="userid" name="userid">   
                    <br>
                    <div style="width: 100%; height: 50px; background-color: #e0e0e0;">
                        <br>
                        <h4><b>DISPLAY MODE</b></h4>
                    </div>
                    <div style="border-style: solid; border-width: thin; border-color: #e0e0e0;">
                        <table style="margin: 20px;">
                            <tr>
                                <th style="width: 200px;">
                                    ZONE :
                                </th>
                                <td style="width: 200px;">
                                    <input type="text" id="zone" name="zone" value="${zone}" style=" width: 50%; background-color: #e0e0e0;" readonly>
                                </td>
                                <th style="width: 100px; text-align: right;">
                                </th>
                                <td style="width: 100px;">
                                </td>
                                <th style="width: 100px; text-align: right;">
                                </th>
                                <td style="width: 100px;">
                                </td>
                                <th style="width: 300px; text-align: right;">
                                </th>
                                <td style="width: 500px;">
                                </td>
                            </tr>
                            <tr>
                                <th style="width: 200px;">
                                    STYLE :
                                </th>
                                <td style="width: 200px;">
                                    <input type="text" id="style" name="style" value="${style}" style="background-color: #e0e0e0;" readonly>
                                </td>
                                <th style="width: 100px; text-align: right;">
                                    CUP : &nbsp;
                                </th>
                                <td style="width: 100px;">
                                    <input type="text" id="cup" name="cup" value="${cup}" style="background-color: #e0e0e0;" readonly>
                                </td>
                                <th style="width: 100px; text-align: right;">
                                    SIZE : &nbsp;
                                </th>
                                <td style="width: 100px;">
                                    <input type="text" id="size" name="size" value="${size}" style="background-color: #e0e0e0;" readonly>
                                </td>
                                <th style="width: 300px; text-align: right;">
                                    TYPE/DESCRIPTION : &nbsp;
                                </th>
                                <td style="width: 500px;">
                                    <input type="text" id="type" name="type" value="${type}" readonly>
                                </td>
                            </tr>
                            <tr>
                                <th style="width: 200px;">
                                    WEIGHT :
                                </th>
                                <td style="width: 200px;">
                                    <input type="text" id="weight" name="weight" value="${weight}" style="width: 50%;" readonly>
                                </td>
                                <th style="width: 100px; text-align: right;">
                                    PRICE : &nbsp;
                                </th>
                                <td style="width: 100px;">
                                    <input type="text" id="price" name="price" value="${price}" readonly>
                                </td>
                                <th style="width: 100px; text-align: right;">
                                    UNIT : &nbsp;
                                </th>
                                <td style="width: 100px;">
                                    <select id="unit" name="unit" style="height:35px;" readonly>
                                        <option value="${unit}" selected hidden>${unit}</option>
                                    </select>
                                </td>
                                <th style="width: 300px; text-align: right;">
                                    HS.CODE : &nbsp;
                                </th>
                                <td style="width: 500px;">
                                    <input type="text" id="hs" name="hs" value="${hs}" readonly>
                                </td>
                            </tr>
                            <tr>
                                <th style="width: 200px;">
                                </th>
                                <td style="width: 200px;">
                                </td>
                                <th style="width: 100px; text-align: right;">
                                </th>
                                <td style="width: 100px;">
                                </td>
                                <th style="width: 100px; text-align: right;">
                                </th>
                                <td style="width: 100px;">
                                </td>
                                <th style="width: 300px; text-align: right;">
                                    RANGE : &nbsp;
                                </th>
                                <td style="width: 500px;">
                                    <input type="text" id="ran" name="ran" value="${ran}" readonly>
                                </td>
                            </tr>
                        </table>
                        <br>
                        <table style="margin: 20px;">
                            <tr>
                                <th style="width: 200px;">
                                    COMPONENTS :
                                </th>
                                <th style="width: 87%;">
                                    <input type="text" id="comp1" name="comp1" value="${comp1}" readonly>
                                </th>
                            </tr>
                            <tr>
                                <th style="width: 200px;"></th>
                                <th style="width: 87%;">
                                    <input type="text" id="comp2" name="comp2" value="${comp2}" readonly>
                                </th>
                            </tr>
                            <tr>
                                <th style="width: 200px;"></th>
                                <th style="width: 87%;">
                                    <input type="text" id="comp3" name="comp3" value="${comp3}" readonly>
                                </th>
                            </tr>
                        </table>
                        <br>
                        <table style="margin: 20px;">
                            <tr>
                                <th style="width: 200px;">
                                    BOX PACKAGE :
                                </th>
                                <th style="width: 200px;">
                                    BOX SIZE 6E
                                </th>
                                <th style="width: 200px;">
                                    BOX SIZE 6D
                                </th>
                                <th style="width: 200px;">
                                    BOX SIZE 4E
                                </th>
                                <th style="width: 200px;">
                                    BOX SIZE 4D
                                </th>
                                <th style="width: 200px;">
                                    BOX SIZE 2E
                                </th>
                                <th style="width: 200px;">
                                    PIECE PACK
                                </th>
                            </tr>
                            <tr>
                                <th style="width: 200px;"></th>
                                <th style="width: 200px;">
                                    <input type="text" id="box6e" name="box6e" value="${box6e}" style="width: 50%;" readonly>
                                </th>
                                <th style="width: 200px;">
                                    <input type="text" id="box6d" name="box6d" value="${box6d}" style="width: 50%;" readonly>
                                </th>
                                <th style="width: 200px;">
                                    <input type="text" id="box4e" name="box4e" value="${box4e}" style="width: 50%;" readonly>
                                </th>
                                <th style="width: 200px;">
                                    <input type="text" id="box4d" name="box4d" value="${box4d}" style="width: 50%;" readonly>
                                </th>
                                <th style="width: 200px;">
                                    <input type="text" id="box2e" name="box2e" value="${box2e}" style="width: 50%;" readonly>
                                </th>
                                <th style="width: 200px;">
                                    <input type="text" id="ppack" name="ppack" value="${ppack}" style="width: 50%;" readonly>
                                </th>
                            </tr>
                            <tr>
                                <th style="width: 200px;">
                                    BOX WEIGHT :
                                </th>
                                <th style="width: 200px;">
                                    <input type="text" id="wei6e" name="wei6e" value="${wei6e}" style="width: 50%;">
                                </th>
                                <th style="width: 200px;">
                                    <input type="text" id="wei6d" name="wei6d" value="${wei6d}" style="width: 50%;">
                                </th>
                                <th style="width: 200px;">
                                    <input type="text" id="wei4e" name="wei4e" value="${wei4e}" style="width: 50%;">
                                </th>
                                <th style="width: 200px;">
                                    <input type="text" id="wei4d" name="wei4d" value="${wei4d}" style="width: 50%;">
                                </th>
                                <th style="width: 200px;">
                                    <input type="text" id="wei2e" name="wei2e" value="${wei2e}" style="width: 50%;">
                                </th>
                                <th style="width: 200px;">
                                </th>
                            </tr>
                        </table>
                        <br>
                        <center>
                            <input style="width: 100px;" type="button" value="Back" onclick="window.location.href = '/EMS2/EMS001/display?search=true'"/>
                        </center>
                        <br>
                    </div>

                    <center>
                        <div id="myModal" class="modal fade" role="dialog">
                            <div class="modal-dialog modal-sm"> <!-- ???????????? modal ?????????????? ???? xs, sm, md, lg -->
                                <div class="modal-md col-md-6" style="top: 150px; left:300px; background-color: #ddd; border: 1px solid #ccc; border-radius: 4px;">
                                    <div class="modal-header">
                                        <h4 class="modal-title" align="center">Duplicate Style, Cup and Size !</h4>
                                        <br>
                                        <button name="ok" type="button" class="close" data-dismiss="modal">
                                            <font color = "white">OK</font>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- devbanban.com -->
                    </center>
                    <center>
                        <div id="myModal1" class="modal fade" role="dialog">
                            <div class="modal-dialog modal-sm"> <!-- ???????????? modal ?????????????? ???? xs, sm, md, lg -->
                                <div class="modal-md col-md-6" style="top: 150px; left:300px; background-color: #ddd; border: 1px solid #ccc; border-radius: 4px;">
                                    <div class="modal-header">
                                        <h4 class="modal-title" align="center">Style, Cup and Size must be filled out !</h4>
                                        <br>
                                        <button name="ok" type="button" class="close" data-dismiss="modal">
                                            <font color = "white">OK</font>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- devbanban.com -->
                    </center>
                </form>
                <!--End Part 3-->
                <!--</div>  end #wrapper-top -->
            </div> <!-- end .container-fluid -->
        </div> <!-- end #wrapper -->


    </body>

</html>