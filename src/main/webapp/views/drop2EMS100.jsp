<%@ include file="../fragments/taglibs.jsp" %>
<%@ include file="../fragments/imports.jsp" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.sql.*;" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>DISTRIBUTION MODE</title>
        <!-- css :: vendors -->
        <jsp:include page = "../fragments/css.jsp" />
        <!-- additional custom :: my-style -->
        <link rel="stylesheet" href="../resources/assets/styles/myStyles.css">

        <!-- js :: vendors -->
        <jsp:include page = "../fragments/script.jsp" />
        <!-- additional custom :: my-script -->
        <script src="../resources/assets/scripts/myScripts.js" async></script>
        <script src="../resources/assets/scripts/toggleLoad.js" async></script>
        <script type="text/javascript"> /* add something code to your script */</script>
        ${sendMessage}
        <script>
            var show1 = function () {
                $('#myModal1').modal('show');
            };
            function validateForm() {
                var x = document.forms["frm"]["id"].value;
                if (x === "") {
                    window.setTimeout(show1, 0);
                    return false;
                }
            }
        </script>
        <style>
            input[type=text], select {
                width: 100%;
                padding: 5px 5px;
                margin: 8px 0;
                display: inline-block;
                border: 1px solid #ccc;
                border-radius: 4px;
                box-sizing: border-box;
            }

            input[type=submit] {
                width: 100%;
                background-color: #2bd14a;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            input[type=submit]:hover {
                background-color: #45a049;
            }

            input[type=button] {
                width: 100%;
                background-color: #ef604a;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            input[type=button]:hover {
                background-color: #e51e00;
            }

            button[name=ok] {
                width: 25%;
                background-color: #008cff;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            button[name=ok]:hover {
                background-color: #008cff;
            }
        </style>
        <script>
            $(document).ready(function () {
                $('#showTable').DataTable({
                    "paging": false,
                    "ordering": false,
                    "lengthMenu": [[-1, 10, 25, 50, 100], ["All", 10, 25, 50, 100]],
                    "bSortClasses": false,
//                    "order": [[0, "desc"]],
                    "columnDefs": [
//                        {orderable: false, targets: [12]},
//                        {"width": "15%", "targets": 12},
//                        {"width": "15%", "targets": 11}
                    ]
                });
                // Setup - add a text input to each footer cell
                $('#showTable tfoot th').each(function () {
                    var title = $(this).text();
                    $(this).html('<input type="text" />');
                });
                // DataTable
                var table = $('#showTable').DataTable();
                // Apply the search
                table.columns().every(function () {
                    var that = this;
                    $('input', this.footer()).on('keyup change', function () {
                        if (that.search() !== this.value) {
                            that
                                    .search(this.value)
                                    .draw();
                        }
                    });
                });
            });
        </script>
        <style>
            body {font-family: Arial, Helvetica, sans-serif;}

            /* The Modal (background) */
            .modal {
                display: none; /* Hidden by default */
                position: fixed; /* Stay in place */
                z-index: 1; /* Sit on top */
                padding-top: 100px; /* Location of the box */
                left: 0;
                top: 0;
                width: 100%; /* Full width */
                height: 100%; /* Full height */
                overflow: auto; /* Enable scroll if needed */
                background-color: rgb(0,0,0); /* Fallback color */
                background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
            }

            /* Modal Content */
            .modal-content {
                background-color: #fefefe;
                margin: auto;
                padding: 20px;
                border: 1px solid #888;
                width: fit-content;
                height: 320px;
            }

            /* The Close Button */
            .close {
                color: #aaaaaa;
                float: right;
                font-size: 28px;
                font-weight: bold;
            }

            .close:hover,
            .close:focus {
                color: #000;
                text-decoration: none;
                cursor: pointer;
            }
        </style>
        <script>
            function allowDrop(ev) {
                ev.preventDefault();
            }

            function drag(ev) {
                ev.dataTransfer.setData("text", ev.target.id);
            }

            function drop(ev) {
                ev.preventDefault();
                var id = ev.dataTransfer.getData("text");
                ev.target.appendChild(document.getElementById(id));
            }

            function closeLine(line) {
                var input = document.getElementsByTagName("input");
                for (var i = 0; i < input.length; i++) {
                    if (input[i].name === "sizepcs-" + line) {
                        input[i].style.display = 'none';
                    }
                }
            }

            function reValue(me, rid) {
                if (me.style.cssText.toString().includes("background-color: rgb(100, 255, 97);")) {

                    var val = document.getElementById(rid).value;
                    var pcs = document.getElementById("pcs-" + me.id.toString().split(";")[1]);
                    var net = document.getElementById("net-" + me.id.toString().split(";")[1]);
                    var gross = document.getElementById("gross-" + me.id.toString().split(";")[1]);
                    var weight = document.getElementById("weight-" + me.id.toString().split(";")[1]);
                    var boxweight = document.getElementById("boxweight-" + me.id.toString().split(";")[1]);
                    var boxpack = document.getElementById("boxpack-" + me.id.toString().split(";")[1]);
                    var Spcs = document.getElementById("S-pcs-" + me.id.toString().split(";")[1]);
                    var Snet = document.getElementById("S-net-" + me.id.toString().split(";")[1]);
                    var Sgross = document.getElementById("S-gross-" + me.id.toString().split(";")[1]);
                    var hid = val.toString().split(',')[0];
                    var Dpcs = document.getElementById("pcs-" + hid.toString().split(";")[1]);
                    var Dnet = document.getElementById("net-" + hid.toString().split(";")[1]);
                    var Dgross = document.getElementById("gross-" + hid.toString().split(";")[1]);
                    var Dweight = document.getElementById("weight-" + hid.toString().split(";")[1]);
                    var Dboxweight = document.getElementById("boxweight-" + hid.toString().split(";")[1]);
                    var Dboxpack = document.getElementById("boxpack-" + hid.toString().split(";")[1]);
                    var DSpcs = document.getElementById("S-pcs-" + hid.toString().split(";")[1]);
                    var DSnet = document.getElementById("S-net-" + hid.toString().split(";")[1]);
                    var DSgross = document.getElementById("S-gross-" + hid.toString().split(";")[1]);
                    if ((parseInt(me.value) - parseInt(val.toString().split(';')[3])) === 0) {
                        me.value = '';
                    } else {
                        me.value = parseInt(me.value) - parseInt(val.toString().split(';')[3]);
                    }

//                alert(me.id.toString().split(";")[1]);
//                alert(me.style.cssText.toString().replace("background-color: rgb(100, 255, 97);", ""));
                    me.style.cssText = me.style.cssText.toString().replace("background-color: rgb(100, 255, 97);", "");
                    if (me.value.toString().trim() === "") {
                        document.getElementById(val.toString().split(',')[0]).value = val.toString().split(',')[1];
                    } else {
                        document.getElementById(val.toString().split(',')[0]).value = parseInt(val.toString().split(',')[1]) - parseInt(me.value);
                    }

                    var reValBd = document.getElementById(rid).value.toString().split(";")[6].split(",")[0] + ':' + document.getElementById(rid).value.toString().split(";")[3] + '%' + document.getElementById(rid).value.toString().split(";")[2] + ';';
                    document.getElementById('cupList-' + me.id.toString().split(";")[1]).value = document.getElementById('cupList-' + me.id.toString().split(";")[1]).value.toString().replace(reValBd, "");
                    document.getElementById('cupList-' + hid.toString().split(";")[1]).value += reValBd;

                    var cupo1 = $('#' + 'cupList-' + me.id.toString().split(";")[1]).val();
                    var cupo2 = $('#' + 'cupList-' + hid.toString().split(";")[1]).val();

                    var color1 = $('#' + 'colorList-' + me.id.toString().split(";")[1]).val();
                    var color2 = $('#' + 'colorList-' + hid.toString().split(";")[1]).val();
                    var tmpColor1 = '';
                    var tmpColor2 = '';
                    var color1List = color1.toString().trim().split(";");
                    for (var i = 0; i < color1List.length; i++) {
                        if (i !== color1List.length - 1) {
                            if (i < color1List.length - 2) {
                                tmpColor1 += color1List[i] + ';';
                            } else {
                                tmpColor2 += color1List[i] + ';';
                            }
                        }
                    }
                    $('#' + 'colorList-' + me.id.toString().split(";")[1]).val(tmpColor1);
                    $('#' + 'colorList-' + hid.toString().split(";")[1]).val(tmpColor2);

                    var cupo1list = cupo1.toString().split(';');
                    var cupo1listVal = '';
                    for (var i = 0; i < cupo1list.length; i++) {
                        var comma1 = ', ';
                        if (i === 0) {
                            comma1 = '';
                        }
                        cupo1listVal += comma1 + cupo1list[i].split(":")[0];
                    }

                    var cupo2list = cupo2.toString().split(';');
                    var cupo2listVal = '';
                    for (var i = 0; i < cupo2list.length; i++) {
                        var comma2 = ', ';
                        if (i === 0) {
                            comma2 = '';
                        }
                        cupo2listVal += comma2 + cupo2list[i].split(":")[0];
                    }

                    $('#' + 'cupOList-' + me.id.toString().split(";")[1]).html(cupo1listVal.trim().slice(0, -1));
                    $('#' + 'cupOList-' + hid.toString().split(";")[1]).html(cupo2listVal.trim().slice(0, -1));

                    document.getElementById(rid).value = '';
                    var tmppcs = pcs.value = parseInt(pcs.value) - parseInt(val.toString().split(';')[3]);
                    pcs.value = tmppcs;
                    net.value = currencyFormat(parseFloat(pcs.value) * parseFloat(weight.value));
                    gross.value = currencyFormat(parseFloat(net.value) + parseFloat(boxweight.value));
                    Spcs.innerHTML = tmppcs;
                    Snet.innerHTML = currencyFormat(parseFloat(pcs.value) * parseFloat(weight.value));
                    Sgross.innerHTML = currencyFormat(parseFloat(net.value) + parseFloat(boxweight.value));
//                ********************

                    var Dtmppcs = Dpcs.value = parseInt(Dpcs.value) + parseInt(val.toString().split(';')[3]);
                    Dpcs.value = Dtmppcs;
                    Dnet.value = currencyFormat(parseFloat(Dpcs.value) * parseFloat(Dweight.value));
                    Dgross.value = currencyFormat(parseFloat(Dnet.value) + parseFloat(Dboxweight.value));
                    DSpcs.innerHTML = Dtmppcs;
                    DSnet.innerHTML = currencyFormat(parseFloat(Dpcs.value) * parseFloat(Dweight.value));
                    DSgross.innerHTML = currencyFormat(parseFloat(Dnet.value) + parseFloat(Dboxweight.value));
                    var input = document.getElementsByTagName("input");
                    for (var i = 0; i < input.length; i++) {
                        if (input[i].name === ("sizepcs-" + hid.toString().split(";")[1])) {
                            input[i].style.display = '';
                        }
                    }

                }
            }

            function currencyFormat(num) {
                return num.toFixed(3).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
            }

            function dropinput(ev) {
                ev.preventDefault();
                var id = ev.dataTransfer.getData("text");
                var dg = document.getElementById(id);
                var destDir = ev.target;
//                var dest = ev.target;
                var dest = document.getElementById(ev.target.id.toString().split(":")[2]);
                var dgLine = dg.id.toString().trim().split(";")[1];
                var dtLine = destDir.id.toString().trim().split(";")[1];
                var dgSize = dg.id.toString().trim().split(";")[2];
                var dtSize = destDir.id.toString().trim().split(":")[0];
//                dest not empty
                if (ev.target.value.toString().trim() !== "") {
                    var isDum = destDir.id.toString().trim().split(":")[1];
                    if (isDum === "dum") {
                        dtSize = destDir.id.toString().trim().split(":")[0];
                    } else {
                        dtSize = destDir.id.toString().trim().split(";")[2];
                    }
                    dest = document.getElementById(ev.target.id.toString());
                }

                var dgStyle = dg.id.toString().trim().split(";")[5];
                var dtStyle = destDir.id.toString().trim().split(";")[5];
                var dgcup = dg.id.toString().trim().split(";")[6];
                var dtcup = destDir.id.toString().trim().split(";")[6];
                if (dgLine !== dtLine) {
                    if (dgSize === dtSize) {
                        if (dgStyle === dtStyle || '${dist}' === 'CWX-JPN') {
                            var pcs = document.getElementById("pcs-" + dest.id.toString().trim().split(";")[1]);
                            var net = document.getElementById("net-" + dest.id.toString().trim().split(";")[1]);
                            var gross = document.getElementById("gross-" + dest.id.toString().trim().split(";")[1]);
                            var weight = document.getElementById("weight-" + dest.id.toString().trim().split(";")[1]);
                            var boxweight = document.getElementById("boxweight-" + dest.id.toString().trim().split(";")[1]);
                            var boxpack = document.getElementById("boxpack-" + dest.id.toString().trim().split(";")[1]);
                            var reVal = document.getElementById("reVal-" + dest.id.toString().trim().split(";")[1] + "-" + dtSize);
                            var Spcs = document.getElementById("S-pcs-" + dest.id.toString().trim().split(";")[1]);
                            var Snet = document.getElementById("S-net-" + dest.id.toString().trim().split(";")[1]);
                            var Sgross = document.getElementById("S-gross-" + dest.id.toString().trim().split(";")[1]);
                            var Dpcs = document.getElementById("pcs-" + dg.id.toString().trim().split(";")[1]);
                            var Dnet = document.getElementById("net-" + dg.id.toString().trim().split(";")[1]);
                            var Dgross = document.getElementById("gross-" + dg.id.toString().trim().split(";")[1]);
                            var Dweight = document.getElementById("weight-" + dg.id.toString().trim().split(";")[1]);
                            var Dboxweight = document.getElementById("boxweight-" + dg.id.toString().trim().split(";")[1]);
                            var Dboxpack = document.getElementById("boxpack-" + dg.id.toString().trim().split(";")[1]);
                            var DSpcs = document.getElementById("S-pcs-" + dg.id.toString().trim().split(";")[1]);
                            var DSnet = document.getElementById("S-net-" + dg.id.toString().trim().split(";")[1]);
                            var DSgross = document.getElementById("S-gross-" + dg.id.toString().trim().split(";")[1]);
//                            alert(
//                                    "pcs : " + pcs.value
//                                    + "\nnet : " + net.value
//                                    + "\ngross : " + gross.value
//                                    + "\nweight : " + weight.value
//                                    + "\nboxweight : " + boxweight.value
//                                    + "\nboxpack : " + boxpack.value
//                                    );

                            var dgInt = parseInt(dg.value);
                            var dtInt = parseInt(pcs.value);
                            var res = parseInt(boxpack.value);
                            var resText = "Yes";
                            if ((dgInt + dtInt) > res || dgStyle !== dtStyle || isNaN(dgInt + dtInt)) {
                                resText = "No";
//                                alert("Not enough space in the box!");
                                document.getElementById('myModal-1').style.display = 'block';
                                if (typeof (Storage) !== "undefined") {
                                    // Store
                                    sessionStorage.setItem("REme", ev.target.id);
                                    sessionStorage.setItem("RErid", 'reVal-' + ev.target.id.toString().split(";")[1] + '-' + ev.target.id.toString().split(":")[0]);
                                }

                                AddAnyWay(dg, ev, reVal, pcs, net, gross, Spcs, Snet, Sgross, weight, boxweight, Dpcs
                                        , Dnet, Dgross, DSpcs, DSnet, DSgross, Dweight, Dboxweight, dtLine, dgLine, dgcup, dgSize);
                            } else {
                                var valInBox = dg.value;
                                // dest not empty
                                if (ev.target.value.toString().trim() !== "") {
                                    valInBox = parseInt(ev.target.value) + parseInt(dg.value);
                                }

                                ev.target.value = valInBox.toString();
                                ev.target.style.cssText += "background-color: rgb(100, 255, 97);";
//                                closeLine(dgLine);
//                                dg.value = '';

                                reVal.value = dg.id + "," + ev.target.value;
                                var tmppcs = pcs.value = parseInt(pcs.value) + parseInt(dg.value);
                                pcs.value = tmppcs;
                                net.value = currencyFormat(parseFloat(pcs.value) * parseFloat(weight.value));
                                gross.value = currencyFormat(parseFloat(net.value) + parseFloat(boxweight.value));
                                Spcs.innerHTML = tmppcs;
                                Snet.innerHTML = currencyFormat(parseFloat(pcs.value) * parseFloat(weight.value));
                                Sgross.innerHTML = currencyFormat(parseFloat(net.value) + parseFloat(boxweight.value));
//                                **********************
                                var Dtmppcs = Dpcs.value = parseInt(Dpcs.value) - parseInt(dg.value);
                                Dpcs.value = Dtmppcs;
                                Dnet.value = currencyFormat(parseFloat(Dpcs.value) * parseFloat(Dweight.value));
                                Dgross.value = currencyFormat(parseFloat(Dnet.value) + parseFloat(Dboxweight.value));
                                DSpcs.innerHTML = Dtmppcs;
                                DSnet.innerHTML = currencyFormat(parseFloat(Dpcs.value) * parseFloat(Dweight.value));
                                DSgross.innerHTML = currencyFormat(parseFloat(Dnet.value) + parseFloat(Dboxweight.value));
                                document.getElementById('cupList-' + dtLine).value += dgcup + ':' + dg.value + '%' + dgSize + ';';
                                document.getElementById('cupList-' + dgLine).value = document.getElementById('cupList-' + dgLine).value.toString().replace((dgcup + ':' + dg.value + '%' + dgSize + ';'), '');

                                var cupo1 = $('#' + 'cupList-' + dtLine).val();
                                var cupo2 = $('#' + 'cupList-' + dgLine).val();

                                var color1 = $('#' + 'colorList-' + dtLine).val();
                                var color2 = $('#' + 'colorList-' + dgLine).val();
                                $('#' + 'colorList-' + dtLine).val(color1 + color2);
                                $('#' + 'colorList-' + dgLine).val('');

                                var cupo1list = cupo1.toString().split(';');
                                var cupo1listVal = '';
                                for (var i = 0; i < cupo1list.length; i++) {
                                    var comma1 = ', ';
                                    if (i === 0) {
                                        comma1 = '';
                                    }
                                    cupo1listVal += comma1 + cupo1list[i].split(":")[0];
                                }

                                var cupo2list = cupo2.toString().split(';');
                                var cupo2listVal = '';
                                for (var i = 0; i < cupo2list.length; i++) {
                                    var comma2 = ', ';
                                    if (i === 0) {
                                        comma2 = '';
                                    }
                                    cupo2listVal += comma2 + cupo2list[i].split(":")[0];
                                }

                                $('#' + 'cupOList-' + dtLine).html(cupo1listVal.trim().slice(0, -1));
                                $('#' + 'cupOList-' + dgLine).html(cupo2listVal.trim().slice(0, -1));

                                dg.value = '';
                                var input = document.getElementsByTagName("input");
                                for (var i = 0; i < input.length; i++) {
                                    if (input[i].name === ("sizepcs-" + dg.id.toString().trim().split(";")[1])) {
                                        input[i].style.display = 'none';
                                    }
                                }
                            }

                            $('#cc-' + dtLine).val('isChange');
                            $('#cc-' + dgLine).val('isChange');

//                            alert(
//                                    "Drag id : " + dg.id
//                                    + "\nDest id : " + dest.id
//                                    + "\nProcess : " + (dgInt + dtInt)
//                                    + "\nResult : " + resText
//                                    );
                        } else {
//                            alert("Can not move in different style!");
                            document.getElementById('myModal-2').style.display = 'block';
                        }
                    } else {
//                        alert("Can not move in different size!");
                        document.getElementById('myModal-3').style.display = 'block';
                    }
                } else {
//                    alert("Can not move in same line!");
                    document.getElementById('myModal-4').style.display = 'block';
                }
            }

            function AddAnyWay(dg, ev, reVal, pcs, net, gross, Spcs, Snet, Sgross, weight, boxweight, Dpcs
                    , Dnet, Dgross, DSpcs, DSnet, DSgross, Dweight, Dboxweight, dtLine, dgLine, dgcup, dgSize) {
                var valInBox = dg.value;
                // dest not empty
                if (ev.target.value.toString().trim() !== "") {
                    valInBox = parseInt(ev.target.value) + parseInt(dg.value);
                }

                ev.target.value = valInBox.toString();
                ev.target.style.cssText += "background-color: rgb(100, 255, 97);";
//                                closeLine(dgLine);
//                                dg.value = '';

                reVal.value = dg.id + "," + ev.target.value;
                var tmppcs = pcs.value = parseInt(pcs.value) + parseInt(dg.value);
                pcs.value = tmppcs;
                net.value = currencyFormat(parseFloat(pcs.value) * parseFloat(weight.value));
                gross.value = currencyFormat(parseFloat(net.value) + parseFloat(boxweight.value));
                Spcs.innerHTML = tmppcs;
                Snet.innerHTML = currencyFormat(parseFloat(pcs.value) * parseFloat(weight.value));
                Sgross.innerHTML = currencyFormat(parseFloat(net.value) + parseFloat(boxweight.value));
//                                **********************
                var Dtmppcs = Dpcs.value = parseInt(Dpcs.value) - parseInt(dg.value);
                Dpcs.value = Dtmppcs;
                Dnet.value = currencyFormat(parseFloat(Dpcs.value) * parseFloat(Dweight.value));
                Dgross.value = currencyFormat(parseFloat(Dnet.value) + parseFloat(Dboxweight.value));
                DSpcs.innerHTML = Dtmppcs;
                DSnet.innerHTML = currencyFormat(parseFloat(Dpcs.value) * parseFloat(Dweight.value));
                DSgross.innerHTML = currencyFormat(parseFloat(Dnet.value) + parseFloat(Dboxweight.value));
                document.getElementById('cupList-' + dtLine).value += dgcup + ':' + dg.value + '%' + dgSize + ';';
                document.getElementById('cupList-' + dgLine).value = document.getElementById('cupList-' + dgLine).value.toString().replace((dgcup + ':' + dg.value + '%' + dgSize + ';'), '');

                var cupo1 = $('#' + 'cupList-' + dtLine).val();
                var cupo2 = $('#' + 'cupList-' + dgLine).val();

                var color1 = $('#' + 'colorList-' + dtLine).val();
                var color2 = $('#' + 'colorList-' + dgLine).val();
                $('#' + 'colorList-' + dtLine).val(color1 + color2);
                $('#' + 'colorList-' + dgLine).val('');

                var cupo1list = cupo1.toString().split(';');
                var cupo1listVal = '';
                for (var i = 0; i < cupo1list.length; i++) {
                    var comma1 = ', ';
                    if (i === 0) {
                        comma1 = '';
                    }
                    cupo1listVal += comma1 + cupo1list[i].split(":")[0];
                }

                var cupo2list = cupo2.toString().split(';');
                var cupo2listVal = '';
                for (var i = 0; i < cupo2list.length; i++) {
                    var comma2 = ', ';
                    if (i === 0) {
                        comma2 = '';
                    }
                    cupo2listVal += comma2 + cupo2list[i].split(":")[0];
                }

                $('#' + 'cupOList-' + dtLine).html(cupo1listVal.trim().slice(0, -1));
                $('#' + 'cupOList-' + dgLine).html(cupo2listVal.trim().slice(0, -1));

                dg.value = '';
                var input = document.getElementsByTagName("input");
                for (var i = 0; i < input.length; i++) {
                    if (input[i].name === ("sizepcs-" + dg.id.toString().trim().split(";")[1])) {
                        input[i].style.display = 'none';
                    }
                }

                $('#cc-' + dtLine).val('isChange');
                $('#cc-' + dgLine).val('isChange');
            }

            function disInput(but) {
                $(but).prop('disabled', true);
                $('#loadingIcon').show();
                $('#loadingIcon2').show();
                document.body.scrollTop = 0;
                document.documentElement.scrollTop = 0;

                var eles = $('input[name=cc][value=isChange]');

                $('input[name=ctno]').prop('disabled', true);
                $('input[name=pcs]').prop('disabled', true);
                $('input[name=net]').prop('disabled', true);
                $('input[name=gross]').prop('disabled', true);
                $('input[name=cupList]').prop('disabled', true);
                $('input[name=colorList]').prop('disabled', true);

                for (var i = 0; i < eles.length; i++) {
                    var line = eles[i].id.toString().replace(/cc-/g, '');
                    $('#ctno-' + line).prop('disabled', false);
                    $('#pcs-' + line).prop('disabled', false);
                    $('#net-' + line).prop('disabled', false);
                    $('#gross-' + line).prop('disabled', false);
                    $('#cupList-' + line).prop('disabled', false);
                    $('#colorList-' + line).prop('disabled', false);
                }

                $('input[name=cc]').prop('disabled', true);

                var table = $('#showTable').DataTable();

//                $('input[name=ctno]:disabled').remove();
//                $('input[name=pcs]:disabled').remove();
//                $('input[name=net]:disabled').remove();
//                $('input[name=gross]:disabled').remove();
//                $('input[name=cupList]:disabled').remove();
//                $('input[name=colorList]:disabled').remove();
//                $('input[name=cc]:disabled').remove();

                table.rows($('input[name=ctno]:disabled').parents('tr')).remove().draw();
                table.rows($('input[name=pcs]:disabled').parents('tr')).remove().draw();
                table.rows($('input[name=net]:disabled').parents('tr')).remove().draw();
                table.rows($('input[name=gross]:disabled').parents('tr')).remove().draw();
                table.rows($('input[name=cupList]:disabled').parents('tr')).remove().draw();
                table.rows($('input[name=colorList]:disabled').parents('tr')).remove().draw();
//                table.rows($('input[name=cc]:disabled').parents('tr')).remove().draw();

//DATABASE
                var inv = $('#inv').val();
                var dist = $('#dist').val();
//                $('#divTest').append('<br>' + inv);
//                $('#divTest').append('<br>' + dist);

                var ctno = $('input[name=ctno]');
                var pcs = $('input[name=pcs]');
                var net = $('input[name=net]');
                var gross = $('input[name=gross]');
                var cupList = $('input[name=cupList]');
                var colorList = $('input[name=colorList]');

                for (var i = 0; i < pcs.length; i++) {

                    var sizepcsList = $('input[name=sizepcs-' + $(ctno[i]).val() + ']');
                    var sizepcs = '';

                    var siList = $.ajax({
                        url: "/EMS2/ShowDataTablesEMS100",
                        type: "GET",
                        data: {
                            mode: 'findSizeList',
                            inv: inv,
                            dist: dist
                        },
                        async: false
                    }).responseJSON;

                    for (var j = 0; j < sizepcsList.length; j++) {
                        if ($(sizepcsList[j]).val().toString().trim() !== '') {
                            sizepcs += siList[j] + ':' + $(sizepcsList[j]).val() + ';';
                        }
                    }

                    var flag = 'N';
                    if (parseInt($(pcs[i]).val()) === 0) {
                        flag = 'W';
                    }

//                    $('#divTest').append('<br>'
//                            + ' | ' + $(ctno[i]).val()
//                            + ' | ' + $(pcs[i]).val()
//                            + ' | ' + $(net[i]).val()
//                            + ' | ' + $(gross[i]).val()
//                            + ' | ' + $(cupList[i]).val()
//                            + ' | ' + $(colorList[i]).val()
//                            + ' | ' + sizepcs
//                            + ' | ' + flag
//                            );

                    var update = $.ajax({
                        url: "/EMS2/ShowDataTablesEMS100",
                        type: "GET",
                        data: {
                            mode: 'editDropBox2',
                            inv: inv,
                            ctno: $(ctno[i]).val(),
                            sizeList: sizepcs,
                            pcs: $(pcs[i]).val(),
                            net: $(net[i]).val(),
                            gross: $(gross[i]).val(),
                            flag: flag,
                            userid: $('#userid').val(),
                            dist: dist,
                            cupList: $(cupList[i]).val(),
                            colorList: $(colorList[i]).val()
                        },
                        async: false
                    }).responseJSON;

                }

//              start update net gross

                var updateNetGross = $.ajax({
                    url: "/EMS2/ShowDataTablesEMS100",
                    type: "GET",
                    data: {
                        mode: 'updateNetGrossCac',
                        inv: inv,
                        dist: dist
                    },
                    async: false
                }).responseJSON;

//              end update net gross

                var updateCom = $.ajax({
                    url: "/EMS2/ShowDataTablesEMS100",
                    type: "GET",
                    data: {
                        mode: 'updateComplete',
                        inv: '${EMHINVID}',
                        dist: '${dist}',
                        sts: 'N'
                    },
                    async: false
                }).responseJSON;

                window.location.href = 'dropbox2?inv=' + inv + '&dist=' + dist;

            }

            function CompleteBox() {

                var update = $.ajax({
                    url: "/EMS2/ShowDataTablesEMS100",
                    type: "GET",
                    data: {
                        mode: 'updateComplete',
                        inv: '${EMHINVID}',
                        dist: '${dist}',
                        sts: 'Y'
                    },
                    async: false
                }).responseJSON;

                if (update) {
                    window.location.href = 'dropbox3?inv=${EMHINVID}&dist=${dist}';
                } else {
                    alertify.error('Update Complete Status Failed!');
                }
            }
        </script>
        <style>
            img {
                border: 1px solid #ddd; /* Gray border */
                border-radius: 4px;  /* Rounded border */
                padding: 5px; /* Some padding */
                width: 100px; /* Set a small width */
            }
        </style>
        <style>
            body {font-family: Arial, Helvetica, sans-serif;}

            /* The Modal (background) */
            .modal {
                display: none; /* Hidden by default */
                position: fixed; /* Stay in place */
                z-index: 1; /* Sit on top */
                padding-top: 100px; /* Location of the box */
                left: 0;
                top: 0;
                width: 100%; /* Full width */
                height: 100%; /* Full height */
                overflow: auto; /* Enable scroll if needed */
                background-color: rgb(0,0,0); /* Fallback color */
                background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
            }

            /* Modal Content */
            .modal-content {
                background-color: #fefefe;
                margin: auto;
                padding: 20px;
                border: 1px solid #888;
                width: fit-content;
                height: 320px;
            }

            /* The Close Button */
            .close {
                color: #aaaaaa;
                float: right;
                font-size: 28px;
                font-weight: bold;
            }

            .close:hover,
            .close:focus {
                color: #000;
                text-decoration: none;
                cursor: pointer;
            }
        </style>
    </head>
    <body onload="document.getElementById('userid').value = sessionStorage.getItem('uid');">
        <div id="wrapper">
            <div id="divTest"></div>
            <!-- /#sidebar-wrapper -->
            <%@ include file="../fragments/sidebar.jsp" %>

            <!-- Page Content -->
            <!-- nav-head-custom -->
            <%@ include file="../fragments/nav_head.jsp" %>

            <div class="container-fluid">
                <!--                <div id="wrapper-top" align="left">
                <%-- PART 2 --%>
                <div class="row">
                    <div class="col-lg-12">
                        <div id="set-height" style="height:415px;margin-top:0px;">
                            <div id="sidebar-wrapper-top" class="">
                                <b class="page-header" style="padding-left:5px;font-size:18px;">
                <%-- <hr style="margin:0px;margin-top:10px;margin-bottom:10px;"> --%>
            </b>
        </div>
    </div>
</div>
</div> -->

                <%-- PART 3 --%>
                <form id="formDrop2" action="dropbox2" method="POST" name="frm" onsubmit="return validateForm()">
                    <input type="hidden" id="userid" name="userid">
                    <br>
                    <div style="border: 2px solid #ccc; border-radius: 5px; padding: 20px;">
                        <div style="width: 100%; height: 50px; background-color: #e0e0e0;">
                            <br>
                            <h4><b>DISTRIBUTIONS MODE</b></h4>
                        </div>
                        <div style="border-style: solid; border-width: thin; border-color: #e0e0e0;">
                            <table style="margin: 20px; width: 100%;">
                                <tr>
                                    <th style="width: 8%;">
                                        CUSTOMER ID :
                                    </th>
                                    <td style="width: 10%;">
                                        <input type="text" value="${EMHCUSID}" style=" width: 87%;" readonly>
                                    </td>
                                    <th style="width: 8%;">
                                        SHIP MARK ID :
                                    </th>
                                    <td style="width: 10%;">
                                        <input type="text" value="${EMHSHIPID}" style=" width: 87%;" readonly>
                                    </td>
                                    <th style="width: 10%;">
                                        TRANSPORTATION ID :
                                    </th>
                                    <td style="width: 10%;">
                                        <input type="text" value="${EMHTRANSPID}" style=" width: 87%;" readonly>
                                    </td>
                                    <th style="width: 8%;">
                                        SHIP DATE :
                                    </th>
                                    <td style="width: 10%;">
                                        <input type="text" value="${EMHSHIPDATE}" style=" width: 87%;" readonly>
                                    </td>
                                    <th style="width: 10%;">
                                        INVOICE NO. :
                                    </th>
                                    <th style="width: 10%;">
                                        <input type="text" id="inv" name="inv" value="${EMHINVID}" style=" width: 87%;" readonly>
                                    </th>
                                </tr>
                                <tr>
                                    <th style="width: 8%;">
                                        SHIP TO :
                                    </th>
                                    <td style="width: 10%;">
                                        <input type="text" value="${EMHCUSSHIPID}" style=" width: 87%;" readonly>
                                    </td>
                                    <th style="width: 8%;">
                                        CURRENCY :
                                    </th>
                                    <td style="width: 10%;">
                                        <input type="text" value="${EMHCURRENCY}" style=" width: 87%;" readonly>
                                    </td>
                                    <th style="width: 10%;">
                                        UNIT :
                                    </th>
                                    <td style="width: 10%;">
                                        <input type="text" value="${EMHUNIT}" style=" width: 87%;" readonly>
                                    </td>
                                    <th style="width: 8%;">
                                        CARTON AMOUNT :
                                    </th>
                                    <td style="width: 10%;">
                                        <input type="text" value="${EMHAMTCTN}" style=" width: 87%;" readonly>
                                    </td>
                                    <th style="width: 10%;">
                                    </th>
                                    <td style="width: 10%;">
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <br>
                        <script>
                            $(document).ready(function () {
                                $('[data-toggle="tooltip"]').tooltip();
                            });
                        </script>
                        <table style=" width: 100%;">
                            <tr>
                                <td style=" width: 50%; text-align: right;">
                                    <div id="loadingIcon" hidden>
                                        <h1 style="display: inline-block;">Loading...</h1>
                                        <i class="fa fa-spinner fa-pulse fa-5x fa-fw"></i>
                                        <span class="sr-only">Loading...</span>
                                    </div>

                                    <input type="hidden" id="dist" name="dist" value="${dist}">
                                </td>
                                <td style="text-align: right; width: 50%; ${disnone}">
                                    <button type="button" class="btn btn-danger" style="width: 100px;" onclick="window.location.href = '/EMS2/EMS100/edit?id=${EMHINVID}'">Back</button>
                                    &nbsp;&nbsp;&nbsp;&nbsp;
                                    <a href="dropbox3?inv=${EMHINVID}&dist=${dist}"><i data-toggle="tooltip" data-placement="top" title="แสดงกล่องรวมทั้งหมด" class="fa fa-cube" aria-hidden="true" style=" font-size: 50px; color: #001384;"></i></a>
                                    &nbsp;&nbsp;&nbsp;&nbsp;
                                    <img src="../resources/images/${distText}.png" >
                                    ${cwx}<br>
                                    <b>${dist}</b>
                                </td>
                            </tr>
                        </table>
                        <table id="showTable" class="display" style="width:100%; border: 1px solid #ccc; border-radius: 4px;">
                            <thead>
                                <tr>
                                    <th>LINE</th>
                                    <th>CTN NO.</th>
                                    <th>ORDER</th>
                                    <th>AUTH</th>
                                    <th>STYLE</th>
                                    <th>COLOR</th>
                                    <th>CUP</th>
                                        ${thead}
                                    <th>PO</th>
                                    <th>PCS</th>
                                    <th>NET WEIGHT</th>
                                    <th>GROSS WEIGHT</th>
                                    <th>BOX TYPE</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                        ${tfoot}
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </tfoot>
                            <tbody>
                                <c:forEach varStatus="i" items="${aheadList}" var="x">
                                    <tr>
                                        <td style="text-align: center;">${x.EMCCTNO}<input type="hidden" id="ctno-${x.EMCCTNO}" name="ctno" value="${x.EMCCTNO}"></td>
                                        <td style="text-align: center;">${x.EMCCARTONNO}<input type="hidden" id="cc-${x.EMCCTNO}" name="cc"></td>
                                        <td style="text-align: center;">${x.EMCORDID}</td>
                                        <td>${x.EMCAUTH}</td>
                                        <td>${x.EMCSTYLE}</td>
                                        <td>${x.EMCCOLOR}
                                            <input type="hidden" id="color-${x.EMCCTNO}" name="color" value="${x.EMCCOLOR}">
                                            <input type="hidden" id="colorList-${x.EMCCTNO}" name="colorList" value="${x.EMCCOLORLIST}">
                                        </td>
                                        <td title="${x.EMCCUPLIST2}">
                                            <!--${x.EMCCUP}-->
                                            <input type="hidden" id="cupList-${x.EMCCTNO}" name="cupList" value="${x.EMCCUPLIST}">
                                            <span id="cupOList-${x.EMCCTNO}" name="cupOList">${x.EMCCUPOLIST}</span>
                                        </td>
                                        ${x.tbody}
                                        <td>${x.EMCPONO}</td>
                                        <td style="text-align: right;"><span id="S-pcs-${x.EMCCTNO}">${x.EMCPCS}</span><input type="hidden" id="pcs-${x.EMCCTNO}" name="pcs" value="${x.EMCPCS}">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                        <td style="text-align: right;"><span id="S-net-${x.EMCCTNO}">${x.EMCNET}</span><input type="hidden" id="net-${x.EMCCTNO}" name="net" value="${x.EMCNET}"><input type="hidden" id="weight-${x.EMCCTNO}" name="weight" value="${x.weight}">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                        <td style="text-align: right;"><span id="S-gross-${x.EMCCTNO}">${x.EMCGROSS}</span><input type="hidden" id="gross-${x.EMCCTNO}" name="gross" value="${x.EMCGROSS}"><input type="hidden" id="boxweight-${x.EMCCTNO}" name="boxweight" value="${x.boxweight}">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                        <td style="text-align: center;">
                                            <select id="edit-boxtype-${x.EMCCTNO}" name="edit-boxtype-${x.EMCCTNO}" onchange="editBoxtypeCac('${x.EMCSTYLE}', '${x.EMCCUP}', '${x.EMCSIZES}', '${x.EMCPCS}', '${EMHINVID}', '${x.EMCCTNO}', '${dist}', this.value);" style="width: 80px;">
                                                <option value="${x.EMCBOXTYPE}" hidden>${x.EMCBOXTYPE}</option>
                                                <option value=""></option>
                                                <option value="6E">6E</option>
                                                <option value="6D">6D</option>
                                                <option value="4E">4E</option>
                                                <option value="4D">4D</option>
                                                <option value="2E">2E</option>
                                            </select>
                                            <script>
                                                function editBoxtypeCac(style, cup, size, pcs, inv, ctno, dist, boxtype) {

                                                    var res = $.ajax({
                                                        url: "/EMS2/ShowDataTablesEMS100",
                                                        type: "GET",
                                                        data: {mode: 'editBoxtypeCac', inv: inv, ctno: ctno, dist: dist, boxtype: boxtype, style: style, cup: cup, size: size, pcs: pcs},
                                                        async: false
                                                    }).responseJSON;
                                                    if (res) {
                                                        alertify.success('Update Box Type Success !');
                                                        //              start update net gross

                                                        var updateNetGross = $.ajax({
                                                            url: "/EMS2/ShowDataTablesEMS100",
                                                            type: "GET",
                                                            data: {
                                                                mode: 'updateNetGrossCac',
                                                                inv: inv,
                                                                dist: dist
                                                            },
                                                            async: false
                                                        }).responseJSON;

                                                        //              end update net gross
                                                    } else {
                                                        alertify.error('Update Box Type Failed !');
                                                    }

                                                }
                                            </script>
                                            <input type="hidden" id="boxpack-${x.EMCCTNO}" name="boxpack" value="${x.BOXPACK}">
                                        </td>
                                    </tr>
                                </c:forEach>
                            </tbody>
                        </table>
                        <center>
                            <div id="loadingIcon2" hidden>
                                <h1 style="display: inline-block;">Loading...</h1>
                                <i class="fa fa-spinner fa-pulse fa-5x fa-fw"></i>
                                <span class="sr-only">Loading...</span>
                            </div>
                            <!--Hide : <input type="text" id="tohide" name="tohide">-->
                            <!--Update : <input type="text" id="toupdate" name="toupdate">-->
                            <button type="button" class="btn btn-danger" style="width: 100px;" onclick="window.location.href = '/EMS2/EMS100/edit?id=${EMHINVID}'">Back</button>
                            <button type="button" class="btn btn-success" style="width: 100px;" onclick="disInput(this);">Save</button>
                            <br><br>
                            <button type="button" class="btn btn-primary" style="width: 100px;" onclick="CompleteBox();">Complete</button>
                        </center>
                        <br>
                        <div id="myModal-1" class="modal">
                            <!-- Modal content -->
                            <div class="modal-content" style=" height: fit-content; width: 500px;">
                                <span id="span-1" class="close" onclick="document.getElementById('myModal-1').style.display = 'none';">&times;</span>
                                <p><b><font size="4"></font></b></p>
                                <table width="100%">
                                    <tr style="background-color: white;">
                                        <td align="center">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <b style="color: #00399b;">
                                                <font size="5">Not enough space in the box!</font><hr>
                                                <font size="4">Do you want to add any way?</font>
                                            </b>
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <br><br><br>
                                            <a class="btn btn btn-outline btn-danger" style="width: 100px;" onclick="reValue(document.getElementById(sessionStorage.getItem('REme')), sessionStorage.getItem('RErid'));
                                                    document.getElementById('myModal-1').style.display = 'none';">
                                                Cancel
                                            </a>
                                            <a class="btn btn btn-outline btn-success" style="width: 100px;" onclick="document.getElementById('myModal-1').style.display = 'none';">
                                                Confirm
                                            </a>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div id="myModal-2" class="modal">
                            <!-- Modal content -->
                            <div class="modal-content" style=" height: fit-content; width: 500px;">
                                <span id="span-2" class="close" onclick="document.getElementById('myModal-2').style.display = 'none';">&times;</span>
                                <p><b><font size="4"></font></b></p>
                                <table width="100%">
                                    <tr style="background-color: white;">
                                        <td align="center">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <b style="color: #00399b;">
                                                <font size="5">Can not move in different style !</font>
                                            </b>
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <br><br><br>
                                            <a class="btn btn btn-outline btn-info" style="width: 100px;" onclick="document.getElementById('myModal-2').style.display = 'none';">
                                                OK
                                            </a>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div id="myModal-3" class="modal">
                            <!-- Modal content -->
                            <div class="modal-content" style=" height: fit-content; width: 500px;">
                                <span id="span-3" class="close" onclick="document.getElementById('myModal-3').style.display = 'none';">&times;</span>
                                <p><b><font size="4"></font></b></p>
                                <table width="100%">
                                    <tr style="background-color: white;">
                                        <td align="center">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <b style="color: #00399b;">
                                                <font size="5">Can not move in different size !</font>
                                            </b>
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <br><br><br>
                                            <a class="btn btn btn-outline btn-info" style="width: 100px;" onclick="document.getElementById('myModal-3').style.display = 'none';">
                                                OK
                                            </a>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div id="myModal-4" class="modal">
                            <!-- Modal content -->
                            <div class="modal-content" style=" height: fit-content; width: 500px;">
                                <span id="span-4" class="close" onclick="document.getElementById('myModal-4').style.display = 'none';">&times;</span>
                                <p><b><font size="4"></font></b></p>
                                <table width="100%">
                                    <tr style="background-color: white;">
                                        <td align="center">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <b style="color: #00399b;">
                                                <font size="5">Can not move in same line !</font>
                                            </b>
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <br><br><br>
                                            <a class="btn btn btn-outline btn-info" style="width: 100px;" onclick="document.getElementById('myModal-4').style.display = 'none';">
                                                OK
                                            </a>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </form>
                <!--End Part 3-->
                <!--</div>  end #wrapper-top -->
            </div> <!-- end .container-fluid -->
        </div> <!-- end #wrapper -->

        <br>
    </body>

</html>